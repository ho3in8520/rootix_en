<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use KuCoin\SDK\Auth;
use KuCoin\SDK\PrivateApi\Withdrawal;
use KuCoin\SDK\PublicApi\Time;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use Tzsk\Sms\Facades\Sms;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    (new Authenticator(request()))->logout();
    return redirect()->route('login.form');
})->name('logout');
Route::get('/image/{type}/{id}/{file}',
    function ($type, $id, $file) {
        $path = storage_path('app/uploaded/' . $type . '/' . $id . '/' . $file);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    })->name('image.view');

Route::get('/image/download/{type}/{id}/{file}',
    function ($type, $id, $file) {
        $path = storage_path('app/uploaded/' . $type . '/' . $id . '/' . $file);
        if (!File::exists($path)) {
            abort(404);
        }
        $type = File::mimeType($path);
        $file_name = str_replace('/', '.', $type);
        $headers = [
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename=' . $file_name . ';'
        ];

        return response()->download($path, $file_name, $headers);
    })->name('image.download');
Route::get('get-cities', function () {
    $cities = \App\Models\City::select(['id', 'name'])->where('parent', request('id'))->get();
    $html = '<option value="">انتخاب کنید...</option>';
    foreach ($cities as $row) {
        $html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
    }
    return json_encode($html);
})->name('get-cities');
Route::namespace("App\Http\Controllers\Auth")->middleware('guest')->group(function () {
    Route::get('auth/login', 'LoginRegisterController@loginForm')->name('login.form');
    Route::post('auth/login/verify', 'LoginRegisterController@login')->name('login.login');
    Route::get('auth/register', 'LoginRegisterController@register')->name('register.form');
    Route::post('auth/register/store', 'LoginRegisterController@registerStore')->name('register.store');
    Route::get('forget-password', 'LoginRegisterController@forgetPasswordForm')->name('login.forget_password.form');
    Route::post('forget-password', 'LoginRegisterController@forgetPasswordSendEmail')->name('login.forget_password.send');
    Route::post('forget-change-password', 'LoginRegisterController@forgetChangePassword')->name('login.forget_password.change-password');
    Route::get('/login', 'GoogleController@loginWithGoogle')->name('login');
    Route::get('callback', 'GoogleController@callbackFromGoogle')->name('callback');
});
// SIGN IN WITH GOOGLE ROUTES
//Route::prefix('google')->name('google.')->group( function(){
//    Route::get('/login', ['GoogleController@loginWithGoogle'])->name('login');
//    Route::get('/callback', ['GoogleController@callbackFromGoogle'])->name('callback');
//});
// END
Route::get('verify-email/token/{token}', "App\Http\Controllers\User\StepsController@verifyEmail")->name('step.verify-token.email');
/// Route Landing
Route::namespace("App\Http\Controllers\Landing")->group(function () {
    Route::get('/', 'HomeController@index')->name('landing.home');
    Route::get('/join', 'HomeController@test');
    Route::get('/about-rootix', 'HomeController@about')->name('landing.about');
    Route::get('/blog-page', 'HomeController@blog')->name('landing.blog');
    Route::get('/services', 'HomeController@services')->name('landing.services');
    Route::get('/faq', 'HomeController@faq')->name('landing.faq');
    Route::get('/education', 'HomeController@education')->name('landing.education');
    Route::get('/prices', 'HomeController@prices')->name('landing.prices');
    Route::get('/price-detail/{currency}', 'HomeController@price_detail')->name('landing.price_detail');
    Route::get('/chart', 'HomeController@chart')->name('landing.chart');
    Route::get('/post/{slug}', 'HomeController@post_show')->name('landing.post');
    Route::get('/search', 'HomeController@post_search')->name('landing.search');
    Route::get('/contact-rootix', 'HomeController@contact_us')->name('landing.contact_us');

});

Route::get('withdraw/token/{token}', "App\Http\Controllers\User\WithdrawController@verifyEmail")->name('verify-withdraw.email');
Route::get('reject/token/{token}', "App\Http\Controllers\User\WithdrawController@rejectEmail")->name('reject-withdraw.email');


///// Route User
Route::middleware(['auth', 'steps', '2fa'])->namespace("App\Http\Controllers\User")->group(function () {
    ///// Route Step
    Route::prefix('step')->group(function () {
        Route::get('verify-mobile', "StepsController@mobile")->name('step.verify-mobile');
        Route::get('verify-email', "StepsController@email")->name('step.verify-email');
        Route::post('verify-mobile/send-verify', "StepsController@sendVerify")->name('step.send-verify');
        Route::post('verify-email/send-link', "StepsController@sendEmail")->name('step.send-email');
        Route::get('information', "StepsController@information")->name('step.information');
        Route::post('information/store', "StepsController@informationStore")->name('step.information.store');
        Route::get('upload', "StepsController@docs")->name('step.upload');
        Route::post('upload/store', "StepsController@DocsStore")->name('step.upload.store');
        Route::get('finish', "StepsController@finish")->name('step.finish');
    });

    Route::post('google2fa/authenticate', function () {
        return redirect(\route('user.dashboard'));
    });
    Route::get('dashboard', "HomeController@dashboard")->name('user.dashboard');
    Route::get('profile', "ProfileController@formProfile")->name('user.profile.form');
    Route::post('profile/about/{id}', "ProfileController@update")->name('profile.update.about');
//    Route::get('swap/{unit}', 'SwapController@show')->name('swap.show');


    Route::get('swap', 'SwapController@index')->name('swap.index');
    Route::post('swap', 'SwapController@store')->name('swap.store');


    Route::post('get-list-currencies', 'AssetController@list_currencies')->name('asset.get_list_currencies');
//    Route::post('get-unit-price', 'SwapController@get_unit_price')->name('get-unit-price');
//    Route::post('get-unit-swap-fee-price', 'SwapController@get_unit_swap_fee_price')->name('get-unit-swap-fee-price');
    Route::post('get_theter_price', 'HomeController@get_theter_price')->name('get_theter_price');
//    Route::post('exchange', 'SwapController@exchange')->name('exchange');
    Route::post('createAccount/{unit}', 'ProfileController@wallet')->name('createAccount');
//    Route::post('check-withdraw-amount', 'WithdrawController@check_withdraw_amount')->name('check-withdraw-amount');
//    Route::post('withdraw-from-wallet', 'WithdrawController@withdraw_from_wallet')->name('withdraw-from-wallet');
//    Route::post('send-message-admin', 'WithdrawController@send_message_admin')->name('send-message-admin');
    Route::post('get-total-usdt', 'WithdrawController@get_total_usdt')->name('get-total-usdt');
//    Route::get('withdraw/{unit}','WithdrawController@index')->name('withdraw.index');
//    Route::post('withdraw','WithdrawController@store')->name('withdraw.store');
    Route::get('withdraw/token/{token}', "WithdrawController@verifyEmail")->name('verify-withdraw.email');

//    Route::resource('withdraw-rial','WithdrawRialController');
    Route::get('wallet/withdraw/rial', 'WDController@W_withdraw_index_rial')->name('wallet.withdraw.rial.index');
    Route::get('wallet/withdraw/rial/create', 'WDController@W_withdraw_create_rial')->name('wallet.withdraw.rial.create');
    Route::post('wallet/withdraw/rial', 'WDController@W_withdraw_store_rial')->name('wallet.withdraw.rial.store');

    Route::get('wallet/deposit/rial/create', 'WDController@W_deposit_create_rial')->name('wallet.deposit.rial.create');
    Route::post('wallet/deposit/rial', 'WDController@W_deposit_store_rial')->name('wallet.deposit.rial.store');

    Route::get('reject/token/{token}', "WithdrawController@rejectEmail")->name('reject-withdraw.email');
    Route::get('change-pass', 'ProfileController@change_pass_view')->name('change_pass.index');
    Route::post('change-pass', 'ProfileController@change_pass')->name('change_pass.store');
    Route::get('/charge-wallet', '\App\Http\Controllers\General\PaymentController@chargeWallet')->name('charge_wallet.view');
    Route::resource('tickets', 'TicketController');
    Route::post('comment/{id}', 'TicketController@comment')->name('comment.store');
    Route::post('/setting/change_confirmation_type', 'SettingController@change_confirmation_type')->name('setting.change_confirmation_type');
    Route::post('/setting/verify_confirmation_type', 'SettingController@verify_confirmation_type')->name('setting.verify_confirmation_type');
    Route::get('setting', 'SettingController@form')->name('setting.form');
    Route::post('/setting/status-google-authenticator', 'SettingController@status_google_authenticator')->name('setting.status_google_authenticator');
    Route::post('/setting/login_2fa', 'SettingController@login_2fa')->name('setting.login_2fa');

    Route::get('/sell-currency', 'SellBuyController@sellCurrencyView')->name('sell_currency.view');
    Route::post('get-user-assets', 'AssetController@get_user_assets');
    Route::resource('wallet', 'AssetController');
    Route::post('wallet/refresh', 'AssetController@refresh_wallet')->name('wallet.refresh');
    Route::resource('banks', 'BankController')->middleware("can:user-check,bank");
    Route::post('update-profile-picture/{user}', 'ProfileController@update_avatar')->name('profile.update.picture');
    Route::post('/complete-registration', 'ProfileController@completeRegistration')->name('complete-registration');
    Route::post('get-address-wallet', 'WDController@getAddressWallet')->name('wallet.get-address-wallet');
    Route::post('withdrawal-fee', 'WDController@WithdrawalFee')->name('wallet.withdrawal-fee');
    Route::post('withdraw', 'WDController@withdraw')->name('withdraw.store')->middleware(['auth', '2fa']);
    Route::get('withdraw/index', 'WDController@index')->name('withdraw.index');
    Route::get('wallet/deposit/{unit}', 'WDController@deposit_form')->name('deposit-form');
    Route::get('wallet/withdraw/{unit}', 'WDController@withdraw_form')->name('withdraw-form');
    Route::post('security-withdraw', 'WDController@security_withdraw')->name('withdraw.security');
    Route::post('security-send-sms', 'WDController@security_send_sms')->name('withdraw.send-sms');
    Route::get('transactions', 'TransactionController@index')->name('transactions.index');
    //Route::resource('notification', 'NotificationController')->names('notification');
    Route::delete('/notifications/clear_all', 'NotificationController@clear_all')->name('notifications.clear_all');
    Route::get('exchange/{des_unit}-{source_unit}', 'ExchangeController@index')->name('exchange.index');
    Route::post('exchange/new-trade', 'ExchangeController@new_trade')->name('exchange.new-trade');
    Route::get('exchange/cancel-trade/{id}', 'ExchangeController@cancel_trade')->name('exchange.cancel-trade');
    Route::post('exchange/list-request', 'ExchangeController@list_request')->name('exchange.list-request');
    Route::post('exchange/last-transaction', 'ExchangeController@last_transaction')->name('exchange.list-transaction');
    Route::post('exchange/markets', 'ExchangeController@markets')->name('exchange.list-markets');

    Route::get('/logged-in-devices', 'LoggedInDeviceManager@index')->name('logged-in-devices.list');

    Route::get('/logout/all', 'LoggedInDeviceManager@logoutAllDevices')->name('logged-in-devices.logoutAll');

    Route::get('/logout/{device_id}', 'LoggedInDeviceManager@logoutDevice')->name('logged-in-devices.logoutSpecific');

    Route::delete('/clear-notif', 'HomeController@clear_notif')->name('clear_notif');
});

///// Route Admin
Route::middleware('guest.admin')->prefix('admin')->namespace("App\Http\Controllers\Admin\Auth")->group(function () {
    Route::get('login', 'LoginController@loginForm')->name('admin.login.form');
    Route::post('login', 'LoginController@login')->name('admin.login');
});
Route::middleware('auth.admin')->prefix('admin')->namespace("App\Http\Controllers\Admin")->group(function () {
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('dashboard', "AdminController@dashboard")->name('admin.dashboard');
    Route::get('manage-withdraw-swap-page', 'AdminController@manage_withdraw_swap_page')->name('manage-withdraw-swap-page');
    Route::post('manage_withdraw_update', 'AdminController@manage_withdraw_update')->name('manage-withdraw-update');
    Route::post('manage-swap-currency', 'AdminController@manage_swap_update')->name('manage-swap-update');
    Route::post('change-unit', 'AdminController@change_unit')->name('change-unit');
    Route::post('change-unit-for-withdraw', 'AdminController@change_unit_for_withdraw')->name('change-unit-for-withdraw');

    Route::post('withdraw-rial/reject/{id}', 'WithdrawRialController@reject')->name('admin.withdraw-rial.reject');
    Route::post('withdraw-rial/confirm/{id}', 'WithdrawRialController@confirm')->name('admin.withdraw-rial.confirm');
    Route::resource('withdraw-rial', 'WithdrawRialController', ['as' => 'admin']);

    //    TRADE MANAGE
    Route::get('trade-management', 'AdminController@manage_trade_page')->name('trade-management');
    Route::post('trade-fee-update', 'AdminController@trad_fee_update')->name('trad-fee-update');
    Route::get('trade-fees-update', 'AdminController@trad_fees_update')->name('trad-fees-update');
    Route::get('get-currency-fees', 'AdminController@get_currency_fees')->name('get-currency-fees');
    Route::get('set-fees', 'AdminController@set_fees')->name('set-fees');

    Route::get('users', 'UserController@index')->name('user.index');
    Route::get('users/authenticate/{user}', 'UserController@authenticate')->name('user.authenticate');
    Route::get('users/{user}', 'UserController@show')->name('user.show');
    Route::post('users/{user}/wallet', 'UserController@wallet')->name('admin.users.wallet');
    Route::get('user/changeStatus', 'UserController@change_status');
    Route::post('user/{user}/edit', 'UserController@edit')->name('admin.user.edit');
    Route::get('user/{user}/login-panel', 'UserController@login_panel')->name('admin.user.login-panel');
    Route::get('user/disableStatus', 'UserController@disable_status');
    Route::get('tickets', 'TicketController@index')->name('ticket.admin.index');
    Route::get('ticket/{id}', 'TicketController@show')->name('ticket.admin.show');
    Route::get('ticket/ban/{id}', 'TicketController@ban')->name('ticket.admin.ban');
    Route::delete('ticket/destroy/{id}', 'TicketController@destroy')->name('ticket.destroy');
    Route::post('ticket/route/{id}', 'TicketController@route')->name('ticket.route');
    Route::post('comment/{id}', 'TicketController@comment')->name('comment.admin.store');
    Route::resource('currencies', 'CurrencyController');
    Route::get('roles', "RolesController@index")->name('roles.index');
    Route::get('roles/create', "RolesController@create")->name('roles.create');
    Route::post('roles/store', "RolesController@store")->name('roles.store');
    Route::get('roles/{id}/edit', "RolesController@edit")->name('roles.edit');
    Route::post('roles/{id}/update', "RolesController@update")->name('roles.update');
    Route::delete('roles/delete', "RolesController@destroy")->name('roles.destroy');
    Route::get('banks', 'BankController@index')->name('admin.bank.index');
    Route::post('banks/{bank}/confirm', 'BankController@confirm')->name('admin.bank.confirm');
    Route::post('banks/{bank}/reject', 'BankController@reject')->name('admin.bank.reject');
    Route::resource('notifications', 'NotificationController');
    Route::get('get-user', 'NotificationController@users')->name('notifications.get_user');
    Route::get('reports', 'ReportController@index')->name('admin.report.index');
    Route::resource('posts', 'PostController');
    Route::post('posts/reject', 'PostController@reject')->name('posts.reject');
    Route::post('posts/accept', 'PostController@accept')->name('posts.accept');
    Route::resource('categories', 'CategoryController');
    Route::post('categories/delete', 'CategoryController@delete')->name('categories.delete');
    Route::post('manage-fee-withdraw-admin-update', 'AdminController@fee_withdraw_admin_update')->name('manage-fee-withdraw-admin-update');
    Route::get('request/withdraw', 'WDController@index_withdraw')->name('admin.request.withdraw');
    Route::get('request/withdraw/{id}/show', 'WDController@show_withdraw')->name('admin.request.withdraw.show');
    Route::post('request/withdraw/balance/admin', 'WDController@balance_admin')->name('admin.request.withdraw.balance-admin');
    Route::post('request/withdraw/update-status', 'WDController@update_status')->name('admin.request.withdraw.update-status');
});
Route::match(['post', 'get'], 'payment/verify', "App\Http\Controllers\General\PaymentController@paymentSuccess")->name('payment.success');
Route::get('payment/{type}', "App\Http\Controllers\General\PaymentController@paymentShow")->name('payment.show');
Route::get('force-logout/{token}', 'App\Http\Controllers\Auth\LoginRegisterController@force_logout')->name('force-logout');

///// Route General
Route::middleware('auth')->namespace("App\Http\Controllers\General")->group(function () {
    Route::post('payment', "PaymentController@payment")->name('payment');
});
Route::match(['post', 'get'], 'payment/verify', "App\Http\Controllers\General\PaymentController@paymentSuccess")->name('payment.success');
Route::get('payment/{type}', "App\Http\Controllers\General\PaymentController@paymentShow")->name('payment.show');
Route::get('force-logout/{token}', 'App\Http\Controllers\Auth\LoginRegisterController@force_logout')->name('force-logout');

//Route::get('test1', function () {
//    $token = Tron()->generateAddress();
//    dd(json_encode($token->getRawData()));
//});
