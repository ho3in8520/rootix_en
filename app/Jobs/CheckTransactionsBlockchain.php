<?php

namespace App\Jobs;

use App\Library\tron\Tron;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckTransactionsBlockchain implements ShouldQueue,ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Tron;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $address;
    private $private_key;
    private $user;


    public function __construct(string $address, string $private_key, User $user)
    {
        $this->address = $address;
        $this->private_key = $private_key;
        $this->user = $user;
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return $this->user->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->checkAmountUnit($this->address, $this->private_key, $this->user);
    }
}
