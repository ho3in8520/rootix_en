<?php


namespace App\Models\Scopes;


use App\Models\Currency;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MergeAssetsCurrencies implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
//        $columns= collect(['*'])->merge([
//            'name'=>Currency::select('name')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'name_fa'=>Currency::select('name_fa')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'unit'=>Currency::select('unit')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'type_token'=>Currency::select('type_token')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'network'=>Currency::select('network')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'color'=>Currency::select('color')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'priority'=>Currency::select('priority')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//            'status'=>Currency::select('status')->whereColumn('assets.currency_id','currencies.id')->limit(1),
//        ])->toArray();
//
        $select=['assets.*','cu.name','cu.name_fa','cu.unit', 'cu.type_token','cu.network','cu.color','cu.priority','cu.status'];
        return $builder->select($select)->join('currencies as cu','cu.id','assets.currency_id')->whereNull('assets.deleted_at')->whereNull('cu.deleted_at')->where('cu.status',1)->orderBy('priority','asc');
    }
}
