<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactionGateway extends Model
{
    use HasFactory;

    public function transaction()
    {
        return $this->morphMany('App\Models\Finance_transaction','financeable');
    }

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
