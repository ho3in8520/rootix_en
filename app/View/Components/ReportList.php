<?php

namespace App\View\Components;

use App\Exports\TotalExport;
use App\Models\Asset;
use App\Models\Currency;
use App\Models\Finance_transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\Component;
use Maatwebsite\Excel\Facades\Excel;

class ReportList extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $request = request();
//        dd($request->all());
        $start_created_at = \request()->start_created_at ? jalali_to_gregorian(request()->start_created_at . " 00:00:00") : false;
        $end_created_at = \request()->end_created_at ? jalali_to_gregorian(request()->end_created_at . " 23:59:59") : false;
        $assets = Currency::all();
        $reports = Finance_transaction::where('user_id', $this->user->id);
        \request()->type && in_array(\request()->type, [1, 2]) ? $reports->where('type', \request()->type) : '';
        \request()->amount ? $reports->where('amount', \request()->amount) : '';
        \request()->code ? $reports->where('tracking_code', \request()->code) : '';
        $start_created_at ? $reports->where('created_at', '>=', $start_created_at) : '';
        $end_created_at ? $reports->where('created_at', '<=', $end_created_at) : '';
        if (\request()->currency) {
            $reports->whereHasMorph(
                'financeable',
                [Asset::class],
                function (Builder $query, $type) {
                    $query->where('unit', \request()->currency);
                }
            );
        }
        $reports->orderBy('created_at', 'desc');
        $reports = $reports->paginate(10);


        if ($request->has('export_report') && !empty($request->export_report)) {

//            dd($this->export($reports));
            if ($request->export_report == 'excel')
                return Excel::download(new TotalExport($this->export($reports)), 'reports.xlsx');
            else {
                $data = $this->export($reports);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('reports.pdf');
            }
        }
        return view('components.report-list', compact('reports', 'assets'));
    }

    public function export($data)
    {
        $result = [];
        foreach ($data as $item) {
            array_push($result, [
                'کد تراکنش' => $item->tracking_code?$item->tracking_code:'-',
                'نوع تراکنش' => $item->type == 1 ? "برداشت" : "واریز",
                'ارز' => $item->financeable->unit,
                'مبلغ' => $item->amount,
                'توضیحات' => $item->description,
                'تاریخ عضویت' => jdate_from_gregorian($item->created_at, 'Y.m.d H:i')
            ]);
        }
        return $result;
    }
}
