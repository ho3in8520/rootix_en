<?php

namespace App\View\Components;

use App\Exports\TotalExport;
use App\Models\Asset;
use App\Models\Currency;
use App\Models\Finance_transaction;
use App\Models\transactionGateway;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\Component;
use Maatwebsite\Excel\Facades\Excel;

class DirhamReportList extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $request = request();
        $assets = Currency::all();
        $reports=transactionGateway::query()->where('user_id',$this->user->id)->orderBy('created_at','desc')->orderBy('status', 'asc')->paginate(10);
        $user=$this->user;
        if ($request->has('export_report') && !empty($request->export_report)) {

//            dd($this->export($reports));
            if ($request->export_report == 'excel')
                return Excel::download(new TotalExport($this->export($reports)), 'reports.xlsx');
            else {
                $data = $this->export($reports);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('reports.pdf');
            }
        }
        return view('components.dirham-report-list', compact('reports', 'assets','user'));
    }

    public function export($data)
    {
        $result = [];
        foreach ($data as $item) {
            array_push($result, [
                'کد تراکنش' => $item->tracking_code?$item->tracking_code:'-',
                'نوع تراکنش' => $item->type == 1 ? "برداشت" : "واریز",
                'ارز' => $item->financeable->unit,
                'مبلغ' => $item->amount,
                'توضیحات' => $item->description,
                'تاریخ عضویت' => jdate_from_gregorian($item->created_at, 'Y.m.d H:i')
            ]);
        }
        return $result;
    }
}
