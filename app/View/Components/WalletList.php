<?php

namespace App\View\Components;

use Illuminate\View\Component;

class WalletList extends Component
{

    public $assets, $list_currencies, $fields;

    public function __construct($assets, $listCurrencies, $fields)
    {
        $this->list_currencies= $listCurrencies;
        $this->assets= $assets;
        $this->fields= $fields;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {

        return view('components.wallet-list');
    }
}
