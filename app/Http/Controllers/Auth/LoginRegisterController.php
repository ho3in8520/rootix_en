<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\User\LoggedInDeviceManager;
use App\Jobs\AfterLogin;
use App\Jobs\ForceLogout;
use App\Mail\ForgetPassword;
use App\Mail\StepMail;
use App\Models\Country;
use App\Models\User;
use App\Rules\StrengthPasswordFormat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;

class LoginRegisterController extends Controller
{
    public function form($type = 'login')
    {
        $countries = Country::all();
        return view("landing.auth.login_register", compact('countries', 'type'));
    }

    /**
     * ویو مربوط به فراموشی رمز عبور
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function forgetPasswordForm()
    {
        $message = '';
        if (\request()->has('token')) { // اگر توکن ست شده بود
            $user = User::query()->where('verification', \request()->token)
                ->where('time_verification', '>', now())
                ->first();
            if ($user) {
                $message = [
                    'type' => 'change-pass',
                    'text' => 'The activation link is valid',
                    'token' => \request()->token,
                ];
            } else {
                $message = [
                    'type' => 'error',
                    'text' => 'Password recovery link is invalid'
                ];
            }
        }
        return view('landing.auth.main_forget-password', compact('message'));
    }

    /**
     * ارسال ایمیل فراموشی رمز عبور
     */
    public function forgetPasswordSendEmail(Request $request)
    {
        return redirect()->back();
        $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);
        $token = Str::random(100);
        $link = route('login.forget_password.form', ['token' => $token]);
        Mail::to(\request()->email)->send(new ForgetPassword($link));
        $user = User::query()->where('email', $request->email)->first();
        $user->verification = $token;
        $user->time_verification = now()->addMinutes(15);
        $user->save();
        Session::flash('success','Password recovery link successfully sent to your email');
        return redirect()->back();
    }

    /**
     * تغییر رمز بعد از ارسال لینک بازیابی رمز عبور
     */
    public function forgetChangePassword(Request $request)
    {
        return redirect()->back();
        $request->validate([
            'password' => ['required', 'min:8', 'confirmed', new StrengthPasswordFormat()],
            'token' => 'required|exists:users,verification'
        ]);
        $user = User::query()->where('verification', \request()->token)
            ->where('time_verification', '>', now())
            ->first();
        $user->verification = '';
        $user->time_verification = '';
        $user->password = Hash::make($request->password);
        $user->save();
        \alert()->success('','Your password has been successfully changed.');
        return redirect()->route('login.form');
    }

    public function loginForm()
    {
        $countries = Country::all();
        return view("landing.auth.main_login", compact('countries'));
    }

    public function login(Request $request)
    {
        return redirect()->back();
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ], [
            'g-recaptcha-response.required' => 'Recaptcha is required',
            'g-recaptcha-response.recaptcha' => 'There was a problem confirming the Recaptcha'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $token = bin2hex(base64_encode(random_bytes(50)));
            \auth()->user()->login_token = $token;
            \auth()->user()->save();
            $date= date('Y/m/d H:i:s');
            AfterLogin::dispatch($request->email, \auth()->user()->mobile, $token, $request->getClientIp(), $date);
            return redirect()->route('user.dashboard');
        }
        return redirect()->back()->with('flash', ['type' => 'error', 'msg' => 'Your information does not match our records.'])->withInput();
//        return response()->json(['status' => 500, 'msg' => 'اطلاعات شما با سوابق ما ما مطاقبت ندارد']);
    }

    public function register()
    {
        $countries = Country::all();
        return view("landing.auth.main_register", compact('countries'));
    }

    public function registerStore(Request $request)
    {
        return redirect()->back();
//        dd($request->all());
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => ['required', 'min:8', 'confirmed', new StrengthPasswordFormat()],
//            'country' => 'required|exists:countries,id',
            'g-recaptcha-response' => 'required|recaptcha',
            'referral_code' => 'nullable|exists:users,code'
        ], [
            'g-recaptcha-response.required' => 'Recaptcha is required',
            'g-recaptcha-response.recaptcha' => 'There was a problem confirming the recaptcha'
        ]);
        try {
            $user = User::create([
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'code' => rand(1000, 9999),
                'referral_id' => $request->referral_code
            ]);
            Auth::login($user);
            return redirect()->route('user.dashboard');
        } catch (\Exception $e) {
            return redirect()->back()->with('flash', ['type' => 'error', 'msg' => 'Something was wrong! Please try later.'])->withInput();
        }
    }

    /*
     * در صورتی که کاربر بعد از لاگین از ایمیل خودش روی خروج از پنل کلیک کرد
    همه کاربر هارو خارج میکنه و به صورت sms یک پیام جدید حاوی رمز عبور برای کاربر ارسال میکنه
    */
    public function force_logout(Request $request)
    {
        $new_password = random_int(10000, 999999);
        $token = $request->token;
        $user = User::query()
            ->where('login_token', $token)
            ->firstOrFail(['id', 'password', 'login_token']);
        $user->password = Hash::make($new_password);
        $user->login_token = null;
        $user->save();
        $user->fresh();
        $user= Auth::loginUsingId($user->id);
        (new LoggedInDeviceManager())->logoutAllDevices(new Request());
        $text = 'The user was successfully logged out of your panel' . "\n\r";
        $text .= 'Your new password: ' . $new_password;
        Alert::success('Successfully!', $text);
        ForceLogout::dispatch($user->email, $user->mobile, $new_password);
        return redirect(route('user.dashboard'));
    }

}
