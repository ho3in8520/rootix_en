<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BaseData;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:system_setting_menu'])->only('form');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:notifications_setting'])->only('admin_notifs_status');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:monetary_site'])->only('monetary_unit');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:swap_trade_setting'])->only('trade_swap_status');
    }

//    نمایش فرم تنظیمات سامانه
    public function form()
    {
        $monetary_unit = BaseData::query()->where('type', 'monetary_unit')->first();
        $trade_rial_status = BaseData::query()->where('type', 'trade_rial_status')->first();
        $trade_usdt_status = BaseData::query()->where('type', 'trade_usdt_status')->first();
        $swap_status = BaseData::query()->where('type', 'swap_status')->first();

        $admin_sms_notifs = BaseData::query()->where('type', 'admin_sms_notifs')->first();
        $admin_email_notifs = BaseData::query()->where('type', 'admin_email_notifs')->first();

        $admin_notifs = [
            'admin_sms_notifs' => $admin_sms_notifs,
            'admin_email_notifs' => $admin_email_notifs,
        ];

        $result = [
            'monetary_unit' => $monetary_unit,
            'trade_rial_status' => $trade_rial_status,
            'trade_usdt_status' => $trade_usdt_status,
            'swap_status' => $swap_status,
        ];
        return view('admin.system_setting', compact('result','admin_notifs'));
    }

//    تغییر واحد پولی سامانه (ریال یا تومان)
    public function monetary_unit(Request $request)
    {
        $monetary_unit = BaseData::query()->where('type', 'monetary_unit')->first();
        try {
            $monetary_unit->update(['extra_field1' => $request['monetary_unit']]);
            return response()->json(['status' => 100, 'msg' => 'واحد پولی سایت با موفقیت تغییر کرد.', 'refresh' => true]);
        } catch (\Exception $exception) {
            return response(['status' => 500, 'msg' => 'مشکلی رخ داده است. بعدا امتحان کنید']);
        }

    }

//    فعال یا غیر فعال کردن سواپ، ترید ریالی و ترید ارزی در سامانه
    public function trade_swap_status(Request $request)
    {
        $request->validate([
            'deactivate_reason' => 'required_if:status,==,0',
            'type' => ['required', Rule::in(['swap_status', 'trade_rial_status', 'trade_usdt_status'])],
        ]);
        try {
            $item = BaseData::query()->where('type', $request['type'])->first();
            $item->update(['extra_field1' => $request['status'], 'extra_field2' => $request['deactivate_reason'] ?? '']);
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام شد.', 'refresh' => true]);
        } catch (\Exception $exception) {
            return response(['status' => 500, 'msg' => 'مشکلی رخ داده است. بعدا امتحان کنید']);
        }
    }

    public function admin_notifs_status(Request $request)
    {
        try {
            if ($request->has('sms') && $request['sms'] == 'on'){
                $item=BaseData::query()->where('type','admin_sms_notifs')->first();
                $sms_arr=json_encode($request['Message']);
                $item->update(['extra_field1'=>1,'extra_field2'=>$sms_arr]);
            }else{
                $item=BaseData::query()->where('type','admin_sms_notifs')->first();
                $item->update(['extra_field1'=>0,'extra_field2'=>null]);
            }
            if ($request->has('mail') && $request['mail'] == 'on'){
                $item=BaseData::query()->where('type','admin_email_notifs')->first();
                $email_arr=json_encode($request['email']);
                $item->update(['extra_field1'=>1,'extra_field2'=>$email_arr]);
            }else{
                $item=BaseData::query()->where('type','admin_email_notifs')->first();
                $item->update(['extra_field1'=>0,'extra_field2'=>null]);
            }
            return response()->json(['status' => 100,'msg'=>'اطلاعات با موفقیت آپدیت شد','refresh'=>true]);
        }catch (\Exception $exception){
            dd($exception);
            return response()->json(['status' => 500,'msg'=>'خطایی رخ داده است. لطفا بعدا امتحان کنید.','refresh'=>true]);
        }

    }
}
