<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:banks_list_menu'])->only('index');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:bank_accept_btn'])->only('confirm');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:bank_reject_btn'])->only('reject');
    }

    public function index(Request $request)
    {
        $banks= Bank::query();
        \request()->code? $banks->whereHas('user', function ($q){
            $q->where('code',\request()->code);
        }):'';
        \request()->name? $banks->where('name','like',"%{$request->name}%"):'';
        \request()->card_number? $banks->where('card_number','like',"%{$request->card_number}%"):'';
        \request()->account_number? $banks->where('account_number','like',"%{$request->account_number}%"):'';
        \request()->sheba_number? $banks->where('sheba_number','like',"%{$request->sheba_number}%"):'';
        in_array(\request()->status,['0',1,2,3])? $banks->where('status',$request->status):'';
        $banks= $banks->paginate(20);
        return showData(view('admin.banks.index',compact('banks')));
    }

    public function reject(Request $request,$bank_id)
    {
        $request->validate([
            'reject_reason'=>'required'
        ]);
        try {
            Bank::query()
                ->where('id',$bank_id)
                ->update(['status' => 3, 'reject_reason'=>$request->reject_reason]);
            $bank=Bank::findOrFail($bank_id);
            $notif_array=[
                'type' => 'bank',
                'refer_id' => $bank_id,
                'user_id' =>$bank->user_id,
                'details' => [
                    'status' => 0,
                    'card_number' => $bank->card_number
                ]
            ];
            insert_notif($notif_array);
            return response()->json(['status'=>100,'msg'=>'حساب مورد نظر رد صلاحیت شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است!']);
        }
    }

    public function confirm($bank_id)
    {
        try {
            Bank::query()
                ->where('id',$bank_id)
                ->update(['status' => 1, 'reject_reason'=> '']);
            $bank=Bank::findOrFail($bank_id);
            $notif_array=[
                'type' => 'bank',
                'refer_id' => $bank_id,
                'user_id' =>$bank->user_id,
                'details' => [
                    'status' => 1,
                    'card_number' => $bank->card_number
                ]
            ];
            insert_notif($notif_array);
            return response()->json(['status'=>100,'msg'=>'حساب مورد نظر تایید شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است!']);
        }
    }
}
