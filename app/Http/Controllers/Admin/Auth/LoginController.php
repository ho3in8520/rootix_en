<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public $gourd = 'admin';

    public function loginForm()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
//            recaptchaFieldName() => recaptchaRuleName()
        ]);
        if ($this->attemp($request))
            return response()->json(['status' => 100, 'msg' => 'ورود موفق']);
        return response()->json(['status' => 500, 'msg' => 'اطلاعات با سوابق ما مطاقبت ندارد']);
    }

    public
    function only($request)
    {
        $array = $request->only(['email', 'password']);
        return $array;
    }

    public
    function attemp($request)
    {
      return Auth::guard($this->gourd)->attempt($this->only($request));
    }

    public function logout()
    {
        Auth::guard($this->gourd)->logout();
        return redirect()->route('admin.login.form');
    }
}
