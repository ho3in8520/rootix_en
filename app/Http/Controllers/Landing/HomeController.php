<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Models\GlobalMarketApi;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
//        $currency =  get_specific_currencies('btc,eth,usdt,trx,doge');
//        $new_post =  file_get_contents("https://blog.rootix.io/wp-json/wp/v2/posts?_fields=thumbnail_url,title,id,link,date,modified");
//        $new_post =  $new_post ? json_decode($new_post, true) : [];
        return view('landing.main');
    }

    public function test()
    {
        $user = auth()->user();
        return view('user.test', compact('user'));
    }

    public function about()
    {
        return view('landing.about');
    }

    public function blog(Request $request)
    {
        $posts = Post::query()
            ->where('type', 1)
            ->where('status', 1);
        $request->search && !empty($request->search) ? $posts->where('title', 'like', '%' . $request->search . '%') : '';
        $ids = $posts->pluck('category_id')->toArray();
        $categories = PostCategory::withCount(['posts' => function ($q) {
            $q->where('status', 1);
        }])->where('status', 1)->whereIn('id', $ids)->orderBy('posts_count', 'desc')->limit(5)->get();
        $posts = $posts->paginate(5);
        $new_posts = Post::where('type', 1)->where('status', 1)->latest()->limit(4)->get();
        return view('landing.blog', compact('posts', 'categories', 'new_posts'));
    }

    public function post_show($slug)
    {
        $post = Post::where('slug', $slug)->with('post_category')->first();
        return view('landing.blog_show', compact('post'));
    }

    public function services()
    {
        return view('landing.services');
    }

    public function faq()
    {
        return view('landing.faq');
    }

    public function education()
    {
        $posts = Post::query()
            ->where('type', 0)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->get();
        $newest = Post::where('type', 0)->where('status', 1)->latest()->limit(2)->get();
        return view('landing.education', compact('posts', 'newest'));
    }

    public function prices()
    {
        $currencies = get_all_currencies();
        $currencies = collect($currencies);
        $currencies = $currencies->except(['date','RLS']);
        $currencies = $currencies->sortByDesc('market_cap');
        $tether = $currencies['USDT']['rls'];

        return view('landing.prices', compact('currencies', 'tether'));
    }

    public function price_detail($currency)
    {
        $currency_info = get_specific_currencies($currency);
//        $currencies = Redis::get('currencies');
//        $currencies = collect(json_decode($currencies));
//        $currency = $currencies->where('id', $currency)->first();
        if ($currency == null)
            return redirect()->back();
        return view('landing.price_details', compact('currency', 'currency_info'));
    }

    public function tag_search($tag)
    {
        $posts = Post::query()->where('status', 1)->whereJsonContains('tags', $tag)->get();
//        return view('landing.blog',compact())
    }

    public function chart()
    {
        return view('landing.chart');
    }

    public function contact_us()
    {
        return view('landing.contact_us');
    }
}
