<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Jobs\SendSms;
use App\Library\tron\Tron;
use App\Mail\sendMessageAdmin;
use App\Models\Admin;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Country;
use App\Models\Currency;
use App\Models\GlobalMarketApi;
use App\Models\transactionGateway;
use App\Models\User;
use App\Models\Withdraw;
use App\Rules\Unit;
use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class WDController extends Controller
{
    use Tron;

    public function __construct()
    {
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:rial_withdraw'])->only('W_withdraw_create_rial');
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:rial_deposit'])->only('W_deposit_create_rial');
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:crypto_withdraw'])->only('withdraw_form');
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:crypto_deposit'])->only('deposit_form');
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:rial_transaction_menu'])->only('W_withdraw_index_rial');
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:crypto_transaction_menu'])->only('index');
    }

    //=============== CRYPTO ================
    // لیست برداشت های ارزی
    public function index(Request $request)
    {
        $withdraws = auth()->user()->withdraw();
        $request->network ? $withdraws->where('network', $request->network) : '';

        $request->unit ? $withdraws->where('unit', $request->unit) : '';

        $request->start_amount ? $withdraws->where('amount', '>=', $request->start_amount) : '';
        $request->end_amount ? $withdraws->where('amount', '<', $request->end_amount) : '';

        $request->start_date ? $withdraws->where('created_at', '>=', $request->start_date) : '';
        $request->end_date ? $withdraws->where('created_at', '<', $request->end_date) : '';

        $request->address ? $withdraws->where('address', $request->address) : '';

        $withdraws = $withdraws->paginate(25);
        $currencies = Currency::all();
        $networks = array_keys($currencies->groupBy('network')->toArray());
        return showData(view('user.wallet.crypto.withdraw_index', compact('withdraws', 'currencies', 'networks')));
    }

    public function getAddressWallet(Request $request)
    {
        $request->validate([
            'type' => [new Unit()]
        ]);
        $account = '';
        $user = auth()->user();
        $asset = Asset::where('user_id', $user->id)->where('unit', $request->type)->first();
        if (empty($asset)) {
            return response()->json(['status' => 300, 'msg' => 'Error performing operations']);
        } elseif (empty($asset->token)) {
            $account = $this->createAccountTron($request->type, $user);
        } else {
            $account = $asset->token;
        }
        $address = json_decode($account)->address_base58;
        return view('user.wallet.get_address_wallet', compact('address'));
    }

    private function createAccountTron($user)
    {
        $account = Tron()->createAccount();
        $account = json_encode($account->getRawData());
        $asset = Asset::where('user_id', $user->id)->update(['token' => $account]);
        return $account;
    }

    // گرفتن کیف پول های کاربر + تبدیل ارز ها به ریال+ تبدیل ارز ها به تتر
    public function get_user_assets(Request $request)
    {
        if ($request->ajax()) {
            try {
                $assets = Asset::with(['user' => function ($q) {
                    $q->select(['id', 'confirm_type']);
                }])->where('user_id', auth()->user()->id)->get();
                $assets_unit = $assets->pluck('unit')->toArray();
                $price_currency = GlobalMarketApi::query()->Price_currency()->first();
                $to_rial = $price_currency->currency_to_rial($assets_unit);
                $to_usdt = $price_currency->currency_to_usdt($assets_unit);
                return response()->json(['status' => '100', 'data' => ['assets' => $assets, 'to_rial' => $to_rial, 'to_usdt' => $to_usdt]]);
            } catch (\Exception $exception) {
                return response()->json(['status' => '500']);
            }
        }
    }

    public function WithdrawalFee(Request $request)
    {
        $request->validate([
            'unit' => [new Unit(), 'required'],
            'trc' => 'in:10,20'
        ]);
        $result = BaseData::where('type', 'fee_withdraw_' . $request->unit)->first();
        return response()->json(['status' => 100, 'data' => $result->extra_field1]);
    }

    public function deposit_form($unit)
    {
        return back();
        $user = auth()->user();
        $asset = $user->assets()->where('unit', $unit)->firstOrFail();
        if (empty($asset)) {
            return response()->json(['status' => 300, 'msg' => 'Error performing operations']);
        } elseif (empty($asset->token)) {
            $account = $this->createAccountTron($user);
        } else {
            $account = $asset->token;
        }
        $address = json_decode($account)->address_base58;
        return view('user.wallet.crypto.deposit', compact('asset', 'address'));
    }

    public function withdraw_form($unit)
    {
        return back();
        $user = auth()->user();
        $asset = $user->assets()->where('unit', $unit)->firstOrFail();
        $fee_currency = BaseData::where('type', 'fee_withdraw_' . $unit)->limit(1)->value('extra_field1');
        return view('user.wallet.crypto.withdraw', compact('asset', 'fee_currency', 'unit'));
    }

    //برداشت ارز
    public function withdraw(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'unit' => ['required', new Unit()],
            'amount' => 'required|between:0,99.99',
            'address' => 'required',
            'code' => [function ($attr, $val, $fail) use ($user) {
                if ($user->confirm_type == 'sms' && $val == '') {
                    $fail('Code is required.');
                }
            }],
            'one_time_password' => [function ($attr, $val, $fail) use ($user) {
                if ($user->confirm_type == 'google-authenticator' && $val == '') {
                    $fail('Code is required.');
                }
            }],
        ]);
        //بررسی صحت درخواست
//        if ($request->token != session()->get('withdraw_' . $request->unit)) {
//            return response()->json(['status' => 500, 'msg' => 'درخواست نامعتبر']);
//        }
        // اگر امنیت برداشت روی پیامک بود
        if ($user->confirm_type == 'sms') {
            // بررسی صحت کد تایید
            if ($user->time_verification > Carbon::now()) {
                if ($user->verification == $request->code) {
                    $user->time_verification = Carbon::now();
                    $user->save();
                    return $this->withdraw_store($request);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'Code is wrong']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'Code has expired']);
            }
        } else {
            $authenticator = app(Authenticator::class)->bootStateless($request);
            return $this->withdraw_store($request);
        }
    }

    private function withdraw_store($request)
    {
        // کارمزد ارز مشخص شده
//        $user = User::select(['id', 'code', 'email',
//            'fee' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
//            'transact_type' => BaseData::select('id')->where('type', 'transactions')->where('extra_field1', 3)->limit(1),
//            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1),
//            'asset_amount' => Asset::select('amount')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
//            'asset_unit' => Asset::select('unit')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
//            'asset_address' => Asset::select('token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
//            'type_token' => Asset::select('type_token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
//        ])->where('id', auth()->user()->id)->first();
        $user = Asset::where('unit', $request->unit)->where('user_id', auth()->id())->first();
        $base_data = BaseData::select([
            DB::raw("id as transact_type"),
            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1),
            'fee_withdraw' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
        ])->where('type', 'transactions')->where('extra_field1', 3)->first();

        // استعلام موجودی کیف پول ادمین برای کارمزد
//        $asset_admin_fee = coinex()->get_balances_coin('trx');
//        if ($asset_admin_fee < 15) {
//            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "مقدار موجودی ارز TRX رو به اتمام است لطفا نسبت به افزایش موجودی اقدام فرمایید با تشکر مقدار موجودی فعلی {$asset_admin_fee}"));
//        } elseif ($asset_admin_fee < 15) {
//            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "مقدار موجودی ارز TRX به اتمام رسیده است لطفا نسبت به افزایش موجودی اقدام فرمایید با تشکر مقدار موجودی فعلی {$asset_admin_fee}"));
//            return response()->json(['status' => 500, 'msg' => 'در حال حاضر تبدیل ارز در دسترس نمیباشد لطفا 30 دقیقه دیگر اقدام فرمایید']);
//        }
        // استعلام موجودی کیف پول ادمین برای ارز درخواست شده
//        $amount_admin_unit = coinex()->get_balances_coin($request->unit);
//        if ($amount_admin_unit < $request->amount) {
//            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin($user->unit, "موجودی شما  به اتمام رسیده است لطفا موجودی خود را افزایش دهید موجودی فعلی {$amount_admin_unit} مبلغ درخواست شده  مبلغ درخواست شده {$request->amount} کد کاربری {$user->code} ایمیل کاربر {$user->email}"));
//            return response()->json(['status' => 500, 'msg' => 'در حال حاضر امکان برداشت وجود ندارد لطفا 30 دقیقه دیگر اقدام فرمایید']);
//        }
        // زمانی ک کاربر کیف پول نساخته بود
        if (!$user->token) {
            return response()->json(['status' => 300, 'msg' => 'Dear user, your wallet has not been made. Please make a wallet.']);
        }
        //  حداقل برداشت
        if ($base_data->min_withdraw > $request->amount) {
            return response()->json(['status' => 300, 'msg' => "Minimum withdraw is {$base_data->min_withdraw} "]);
        }
        // اگر موجودی کاربر کمتر از مبلغ درخواستی بود
        if ($user->amount < $request->amount) {
            return response()->json(['status' => 300, 'msg' => 'Your inventory is low']);
        }
        $amount = $request->amount - $user->fee;
        $hex = Tron()->toHex($request->address);
        $param = convertToParameter($hex, $this->amount($amount, contractUnit(strtolower($request->unit))['precision'], 'multiplication'));
        $final_amount = $this->amount($amount, contractUnit(strtolower($request->unit))['precision'], 'multiplication');
        /// اگر آدرس اشتباه بود
        if ($param == '') {
            return response()->json(['status' => 500, 'msg' => 'The wallet address entered is incorrect']);
        }

        (!empty(session()->get('withdraw_' . $request->unit))) ? session()->remove('withdraw_' . $request->unit) : '';

        $withdraw = Withdraw::create([
            'user_id' => auth()->id(),
            'fee' => $base_data->fee_withdraw,
            'amount' => $amount,
            'unit' => $request->unit,
            'address' => $request->address,
            'network' => $request->network,
            'status' => 0,
        ]);
        if ($withdraw)
            return response()->json(['status' => 100, 'msg' => 'Your request has been successfully submitted']);
        return response()->json(['status' => 500, 'msg' => 'Error registering.Request contact support']);
    }

    // امنیت برداشت ارز
    public function security_withdraw(Request $request)
    {
        $request->validate([
            'unit' => ['required', new Unit()],
            'amount' => 'required|numeric|between:0,9999999.9999999',
            'address' => 'required'
        ]);

        // کارمزد ارز مشخص شده
        $user = Asset::where('unit', $request->unit)->where('user_id', auth()->id())->first();
        $base_data = BaseData::select([
            DB::raw("id as transact_type"),
            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1),
        ])->where('type', 'transactions')->where('extra_field1', 3)->first();
//dd($user);
//        $user = User::select(['id',
//            'fee' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
//            'currency'=>Currency::select('id')->where('unit',$request->unit)->limit(1),
//            'transact_type' => BaseData::select('id')->where('type', 'transactions')->where('extra_field1', 3)->limit(1),
//            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1),
//            'asset_amount' => Asset::select('amount')->whereColumn('assets.currency_id', 'currency')->where('unit', $request->unit)->limit(1),
////            'asset_unit' => Asset::select('unit')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
////            'asset_address' => Asset::select('token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
////            'type_token' => Asset::select('type_token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
//        ])->where('id', auth()->user()->id)->first();
//        dd($user['type_token']);
        // استعلام موجودی کیف پول ادمین
//        $asset_admin_fee = coinex()->get_balances_coin('trx');
//        if ($asset_admin_fee < 15) {
//            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "مقدار موجودی ارز TRX رو به اتمام است لطفا نسبت به افزایش موجودی اقدام فرمایید با تشکر مقدار موجودی فعلی {$asset_admin_fee}"));
//        } elseif ($asset_admin_fee < 15) {
//            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "مقدار موجودی ارز TRX به اتمام رسیده است لطفا نسبت به افزایش موجودی اقدام فرمایید با تشکر مقدار موجودی فعلی {$asset_admin_fee}"));
//            return response()->json(['status' => 500, 'msg' => 'در حال حاضر تبدیل ارز در دسترس نمیباشد لطفا 30 دقیقه دیگر اقدام فرمایید']);
//        }
        // استعلام موجودی کیف پول ادمین برای ارز درخواست شده
//        $amount_admin_unit = coinex()->get_balances_coin($request->unit);
//        if ($amount_admin_unit < $request->amount) {
//            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin($user->unit, "موجودی شما  به اتمام رسیده است لطفا موجودی خود را افزایش دهید موجودی فعلی {$amount_admin_unit} مبلغ درخواست شده {$request->amount}"));
//            return response()->json(['status' => 500, 'msg' => 'در حال حاضر امکان برداشت وجود ندارد لطفا 30 دقیقه دیگر اقدام فرمایید']);
//        }
        // زمانی ک کاربر کیف پول نساخته بود
        if (!$user->token) {
            return response()->json(['status' => 300, 'msg' => 'Dear user, your wallet has not been made. Please make a wallet']);
        }
        //  حداقل برداشت
        if ($user->min_withdraw > $request->amount) {
            return response()->json(['status' => 300, 'msg' => "Minimum withdraw is {$user->min_withdraw} "]);
        }
        // اگر موجودی کاربر کمتر از مبلغ درخواستی بود
        if ($user->amount < $request->amount) {
            return response()->json(['errors' => ['amount' => [0 => 'Your inventory is low']]], 422);
        }
        $token = Str::random(100);
        // درصورت موجود بودن توکن حذف شود و توکن جدید ایجاد شود
        session()->get('withdraw_' . $request->unit) ? session()->remove('withdraw_' . $request->unit) : '';
        session()->put('withdraw_' . $request->unit, $token);
        $user = auth()->user();
        // بررسی نوع امنیت برداشت
        $mode = $user->confirm_type;
        if ($mode == 'sms') {
            return response()->json(['status' => 200, 'view' => view('user.wallet.crypto.security_withdraw.sms', compact('token', 'user'))->render()]);
        } else {
            return response()->json(['status' => 200, 'view' => view('user.wallet.crypto.security_withdraw.google_authenticator', compact('token', 'user'))->render()]);
        }
    }

    public function security_send_sms(Request $request)
    {
        $request['country'] = 'IR';
        $request->validate([
            'number' => 'required|numeric',
            'country' => 'required',
        ]);
        $user = auth()->user();
        if ($user->time_verification >= Carbon::now()) {
            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
            return response()->json(['status' => 300, 'msg' => 'The code is sent to you once', 'time' => $seconds]);
        }
        $country = Country::where('code', $request->country)->first();
        $number = '+' . $country->phonecode . $request->number;
        $code = rand(10000, 99999);
        try {
            $user = auth()->user();
            $user->mobile = $request->number;
            $user->verification = $code;
            $user->time_verification = Carbon::now()->addMinutes(1);
            $user->save();
//            smsVerify($code, $number, 'verify');

            return response()->json(['status' => 100, 'msg' => 'The code was sent for the desired mobile', 'time' => 60]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'Error sending']);
        }
    }

    //=============== RIAL ================

    /**
     * لیست درخواست های برداشت کاربر
     */
    public function W_withdraw_index_rial()
    {
        $withdraws = auth()->user()->withdraw_rial()->with('bank', function ($q) {
            $q->select(['id', 'name', 'card_number', 'account_number', 'sheba_number']);
        });

        $start_deposit_date = \request()->start_deposit_date ? jalali_to_gregorian(\request()->start_deposit_date . " 00:00:00") : false;
        $end_deposit_date = \request()->end_deposit_date ? jalali_to_gregorian(\request()->end_deposit_date . " 23:59:59") : false;

        \request()->amount ? $withdraws->where('amount', unit_to_rial(\request()->amount)) : '';
        in_array(\request()->status, ['0', '1', '2']) ? $withdraws->where('status', \request()->status) : '';
        \request()->tracking_code ? $withdraws->where('tracking_code', \request()->tracking_code) : '';
        in_array(\request()->deposit_type, ['0', '1', '2']) ? $withdraws->where('deposit_type', \request()->deposit_type) : '';

        $start_deposit_date ? $withdraws->where('deposit_date', '>=', $start_deposit_date) : '';
        $end_deposit_date ? $withdraws->where('deposit_date', '<=', $end_deposit_date) : '';

        $withdraws = $withdraws->latest()->paginate(20);
        return showData(view('user.wallet.rial.withdraw.index', compact('withdraws')));
    }

    /**
     * صفحه ی ثبت درخواست برداشت کاربر
     */
    public function W_withdraw_create_rial()
    {
        return back();
        $rial_asset = auth()->user()->assets()->where('unit', 'rls')->first(['amount']);
        $min_withdraw = BaseData::query()->where('type', 'min_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $fee_withdraw = BaseData::query()->where('type', 'fee_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $banks = auth()->user()->bank()->active()->get();
        $today_withdraw = auth()->user()->withdraw_rial()->whereIn('status', [0, 1])->where(DB::raw('DATE(created_at)'), Carbon::today()->toDateString())->sum('amount');
        return view('user.wallet.rial.withdraw.create', compact('rial_asset', 'min_withdraw', 'fee_withdraw', 'banks', 'today_withdraw'));
    }

    /**
     * ذخیره درخواست برداشت ریالی
     */
    public function W_withdraw_store_rial(Request $request)
    {
        $banks = auth()->user()->bank()->active()->get(['id'])->pluck('id')->toArray();
        $rial_asset = auth()->user()->assets()->where('unit', 'rls')->first(['id', 'amount']);
        $min_withdraw = BaseData::query()->where('type', 'min_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $fee_withdraw = BaseData::query()->where('type', 'fee_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $today_withdraw = auth()->user()->withdraw_rial()->whereIn('status', [0, 1])->where(DB::raw('DATE(created_at)'), Carbon::today()->toDateString())->sum('amount');
        $max_daily_withdraw = max_withdraw_daily();
        if ($max_daily_withdraw == -1)
            $max = $rial_asset->amount; // وقتی سقفی نداره حداکثر میزان برداشتش میشه میزان موجودی کاربر
        else {
            $today_max = $max_daily_withdraw - $today_withdraw;
            $max = $today_max > $rial_asset->amount ? $rial_asset->amount : $today_max;
        }
        $min_withdraw2 = rial_to_unit($min_withdraw, 'rls', false);
        $request->validate([
            'amount' => "required|numeric|min:$min_withdraw2|max:{$max}",
            'bank_id' => "required|in:" . implode(',', $banks),
        ]);
        try {
            $amount_withdrawn = unit_to_rial($request->amount) - $fee_withdraw;
            if ($amount_withdrawn < 0)
                return response()->json(['status' => 500, 'msg' => 'Your withdrawn amount must be greater than 0']);
            $withdraw_rial = auth()->user()->withdraw_rial()->create([
                'amount' => unit_to_rial($request->amount),
                'user_des' => $request->user_des,
                'fee' => $fee_withdraw,
                'bank_id' => $request->bank_id
            ]);

            $rial_asset->amount = $rial_asset->amount - unit_to_rial($request->amount);
            $rial_asset->save();

            $unit = get_unit();
            $fee_withdraw = rial_to_unit($fee_withdraw, 'rls', true);
            $transact_type = BaseData::query()->withdraw()->first(['id'])->id;
            $rial_asset->transaction()->create([
                'user_id' => auth()->user()->id,
                'transact_type' => $transact_type,
                'amount' => unit_to_rial($request->amount),
                'type' => 1,
                'description' => "withdraw request $unit with $fee_withdraw fee",
            ]);
            //$admin= Admin::query()->first(['id']);
            /*insert_notif([
                'type' => 'withdraw',
                'refer_id' => $withdraw_rial->id,
                'user_id' => $admin->id,
                'details' => ['type' => 'rial'],
            ]);*/
            return response()->json(['status' => 100, 'msg' => 'Successfully', 'refresh' => true]);
        } catch (Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'There is a problem. Please contact support']);
        }
    }

    /**
     * صفحه ی واریز ریالی
     */
    public function W_deposit_create_aed()
    {
        return back();
        return view('user.wallet.dirham.deposit.deposit');
    }

    public function W_deposit_store_aed(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            'file' => 'required|mimes:jpeg,png,jpg|max:5120',
        ]);
//        $trans_type = BaseData::where('type', 'transactions')->where('extra_field1', 1)->first();
        try {
            $trans = new transactionGateway();
            $trans->user_id = auth()->user()->id;
            $trans->amount = $request->amount;
            $trans->transaction_id = 0;
            $trans->mode = $request->mode;
            $trans->status = 0;
            $trans->save();
            if ($request->has('file')) {
                $trans->files()->create([
                    'user_id' => auth()->user()->id,
                    'type' => 'bank_payment',
                    'path' => upload($request->file('file'), 'bank_payment'),
                ]);
            }
            $code = auth()->user()->code;
            $email = auth()->user()->email;
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('dirham deposit', "User with code : {$code} and Email : {$email}, deposits {$request->amount} dirhams. please check and confirm it."));
            SendSms::dispatch(env('MOBILE_SUPPORT_ROOTIX'), [$code, $email, $request->amount], 'dirham-deposit');
//            $asset=auth()->user()->assets()->whereHas('currency',function ($query){
//                $query->where('currency','aed');
//            })->first();
//            $asset->transaction()->create([
//                'user_id' => auth()->id(),
//                'refer_id' => $trans->id,
//                'amount' => 0,
//                'type' => 2,
//                'transact_type' => $trans_type->id,
//                'tracking_code' => null,
//                'description' => 'Charge the wallet in the amount : {$request->amount}'
////                'description' => "شارژ کیف پول $unit به مبلغ $amount2"
//            ]);

            return response()->json(['status' => 100, 'msg' => 'Your request has been successfully submitted. Wait for admin confirmation.']);

        } catch (\Exception $exception) {
            dd($exception);
            return response()->json(['status' => 500, 'msg' => 'Error registering.Request contact support']);
        }
    }

    public function dirhamDepositIndex()
    {
        $transactions=auth()->user()->transactions_gateway()->orderBy('status','asc')->paginate(25);
        return view('user.wallet.dirham.deposit.index',compact('transactions'));
    }

}
