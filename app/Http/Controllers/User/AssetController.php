<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Jobs\CheckTransactionsBlockchain;
use App\Library\tron\Tron;
use App\Mail\sendMessageAdmin;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Country;
use App\Models\GlobalMarketApi;
use App\Models\User;
use App\Rules\Unit;
use Carbon\Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class AssetController extends Controller
{
    use Tron;

    protected $trc10 = ['btt' => 1002000];

    private $user;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = \auth()->user()->assets()->where('unit', '!=', 'rls')->get();
        dispatch(new CheckTransactionsBlockchain($assets[0]->address, $assets[0]->privateKey, auth()->user()));
        $list_currencies = get_specific_currencies(implode(',', $assets->pluck('unit')->toArray()));
        $dirham = ['AED' => [
            "currency" => "AED",
            "name" => "dirham",
            "name_fa" => "درهم",
            "logo_url" => null,
            "price" => 0.27,
            "price_btc" => 5.2136923091517E-5,
            "price_date" => "2022-10-23T08:50:00.000Z",
            "market_cap" => null,
            "color" => "#26A17B",
            "volume" => 24286242662.503,
            "rls" => 89460,
            "percent" => 0,
            "1h" => null,
            "1d" => null,
            "7d" => null,
            "30d" => null,
            "60d" => null,
            "90d" => null,
        ]];
        $list_currencies=array_merge($list_currencies,$dirham);
//        $rial_asset = $assets->where('unit', 'rls')->first();
        $usdt_asset = $assets->where('unit', 'usdt')->first()->amount;
        $currency_asset_usdt = 0;
        foreach ($assets as $asset) {
//            if ($asset->unit != 'rls')
            if ($asset->unit != 'usdt')
                $currency_asset_usdt += convert_currency($asset->unit, 'usdt', $asset->amount, false, $list_currencies);
        }
//        $currency_asset_rial = convert_currency('usdt', 'rls', $currency_asset_usdt, false, $list_currencies);
//        $rial_asset_usdt = convert_currency('rls', 'usdt', $rial_asset->amount);
        /*$data = [
            'total_asset_usdt' => $currency_asset_usdt + $rial_asset_usdt,
            'total_asset_rial' => rial_to_unit($rial_asset->amount,'rls',false) + $currency_asset_rial,
            'rial_asset' => $rial_asset->amount,
            'rial_asset_usdt' => $rial_asset_usdt,
            'currency_asset_usdt' => $currency_asset_usdt,
            'currency_asset_rial' => $currency_asset_rial,
        ];*/

        $data = [
            'usdt_asset' => $usdt_asset,
            'currency_asset_usdt' => $currency_asset_usdt, // without usdt
            'total_asset_usdt' => $currency_asset_usdt + $usdt_asset,
        ];


        $crypto_chart = $assets->where('amount', '>', 0)->where('unit', '<>', 'rls');
        $property_crypto_chart = false;
        foreach ($crypto_chart as $item) {
            $property_crypto_chart += $list_currencies[strtoupper($item->unit)]['percent'];
        }
        $transactions = \auth()->user()->transactions()->with('financeable')->orderBy('id', 'DESC')->take(6)->get();
        return showData(view('user.wallet.index', compact('assets', 'data', 'list_currencies', 'transactions', 'crypto_chart', 'property_crypto_chart')));
    }

    /*
     * رفرش کردن کیف پول کاربر
     */
    public function refresh_wallet()
    {
        $assets = \auth()->user()->assets()->where('unit', '!=', 'rls')->get();
        dispatch(new CheckTransactionsBlockchain($assets[0]->address, $assets[0]->privateKey, auth()->user()));
        $list_currencies = get_specific_currencies(implode(',', $assets->pluck('unit')->toArray()));
        $fields = \request()->fields;
        return view('components.wallet-list', compact('assets', 'list_currencies', 'fields'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Asset $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        //
    }

    public function getAddressWallet(Request $request)
    {
        $request->validate([
            'type' => [new Unit()]
        ]);
        $account = '';
        $user = auth()->user();
        $asset = Asset::where('user_id', $user->id)->where('unit', $request->type)->first();
        if (empty($asset)) {
            return response()->json(['status' => 300, 'msg' => 'Error performing operations']);
        } elseif (empty($asset->token)) {
            $account = $this->createAccountTron($request->type, $user);
        } else {
            $account = $asset->token;
        }
        $address = json_decode($account)->address_base58;
        return view('user.wallet.get_address_wallet', compact('address'));
    }

    private function createAccountTron($type, $user)
    {
        $account = Tron()->createAccount();
        $account = json_encode($account->getRawData());
        $asset = Asset::where('user_id', $user->id)->update(['token' => $account]);
        return $account;
    }

    // گرفتن کیف پول های کاربر + تبدیل ارز ها به ریال+ تبدیل ارز ها به تتر
    public function get_user_assets(Request $request)
    {
        if ($request->ajax()) {
            try {
                $assets = Asset::with(['user' => function ($q) {
                    $q->select(['id', 'confirm_type']);
                }])->where('user_id', auth()->user()->id)->get();
                $assets_unit = $assets->pluck('unit')->toArray();
                $price_currency = GlobalMarketApi::query()->Price_currency()->first();
                $to_rial = $price_currency->currency_to_rial($assets_unit);
                $to_usdt = $price_currency->currency_to_usdt($assets_unit);
                return response()->json(['status' => '100', 'data' => ['assets' => $assets, 'to_rial' => $to_rial, 'to_usdt' => $to_usdt]]);
            } catch (\Exception $exception) {
                return response()->json(['status' => '500']);
            }
        }
    }

    public function WithdrawalFee(Request $request)
    {
        $request->validate([
            'unit' => [new Unit(), 'required'],
            'trc' => 'in:10,20'
        ]);
        $result = BaseData::where('type', 'fee_withdraw_' . $request->unit)->first();
        return response()->json(['status' => 100, 'data' => $result->extra_field1]);
    }

    public function withdraw_form($unit)
    {
        return view('user.wallet.deposit');
    }

//برداشت ارز
    public function withdraw(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'unit' => ['required', new Unit()],
            'amount' => 'required|between:0,99.99',
            'address' => 'required',
            'token' => 'required',
            'code' => [function ($attr, $val, $fail) use ($user) {
                if ($user->confirm_type == 'sms' && $val == '') {
                    $fail('Code field required');
                }
            }],
            'one_time_password' => [function ($attr, $val, $fail) use ($user) {
                if ($user->confirm_type == 'google-authenticator' && $val == '') {
                    $fail('Code field required');
                }
            }],
        ]);
        //بررسی صحت درخواست
        if ($request->token != session()->get('withdraw_' . $request->unit)) {
            return response()->json(['status' => 500, 'msg' => 'Invalid request']);
        }
        // اگر امنیت برداشت روی پیامک بود
        if ($user->confirm_type == 'sms') {
            // بررسی صحت کد تایید
            if ($user->time_verification > Carbon::now()) {
                if ($user->verification == $request->code) {
                    $user->time_verification = Carbon::now();
                    $user->save();
                    return $this->withdraw_store($request);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'Incorrect code']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'The code has expired']);
            }
        } else {
            $authenticator = app(Authenticator::class)->bootStateless($request);
            return $this->withdraw_store($request);
        }
    }

    private function withdraw_store($request)
    {
        // کارمزد ارز مشخص شده
        $user = User::select(['id', 'code', 'email',
            'fee' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
            'transact_type' => BaseData::select('id')->where('type', 'transactions')->where('extra_field1', 3)->limit(1),
            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1),
            'asset_amount' => Asset::select('amount')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_unit' => Asset::select('unit')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_address' => Asset::select('token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'type_token' => Asset::select('type_token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
        ])->where('id', auth()->user()->id)->first();
        // استعلام موجودی کیف پول ادمین برای کارمزد
        $asset_admin_fee = \Tron()->getBalance(env('ADDRESS_TRON_BASE')) / 1000000;
        if ($asset_admin_fee < 15) {
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "TRX currency balance is running low. Please increase your balance Thanks for the current balance {$asset_admin_fee}"));
        } elseif ($asset_admin_fee < 15) {
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "TRX currency balance has expired. Please increase the balance Thanks for the current balance {$asset_admin_fee}"));
            return response()->json(['status' => 500, 'msg' => 'Currency conversion is not available at the moment. Please wait another 30 minutes']);
        }
        // استعلام موجودی کیف پول ادمین برای ارز درخواست شده
        $asset_admin_unit = $this->getAmountsAdmin(env('ADDRESS_TRON_BASE'), $user->type_token);
        if (array_key_exists($request->unit, $asset_admin_unit) && ($asset_admin_unit[$request->unit] / 1000000) < $request->amount) {
            $amount_admin_unit = $asset_admin_unit[$request->unit] / 1000000;
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin($user->asset_unit, "Your balance has run out. Please increase your current balance {$amount_admin_unit} Amount requested {$request->amount} User code {$user->code} User email {$user->email}"));
            return response()->json(['status' => 500, 'msg' => 'It is not possible to withdraw at the moment. Please wait another 30 minutes']);
        }
        // زمانی ک کاربر کیف پول نساخته بود
        if (!$user->asset_address) {
            return response()->json(['status' => 300, 'msg' => 'Dear user, your wallet has not been made. Please make a wallet. Thank you']);
        }
        //  حداقل برداشت
        if ($user->min_withdraw > $request->amount) {
            return response()->json(['status' => 300, 'msg' => "Minimum withdraw is {$user->min_withdraw}"]);
        }
        // اگر موجودی کاربر کمتر از مبلغ درخواستی بود
        if ($user->asset_amount < $request->amount) {
            return response()->json(['status' => 300, 'msg' => 'Your inventory is low']);
        }
        $amount = $request->amount - $user->fee;
        $hex = Tron()->toHex($request->address);
        $param = convertToParameter($hex, $this->amount($amount, contractUnit($request->unit)['precision'], 'multiplication'));
        $final_amount = $this->amount($amount, contractUnit($request->unit)['precision'], 'multiplication');
        /// اگر آدرس اشتباه بود
        if ($param == '') {
            return response()->json(['status' => 500, 'msg' => 'Destination wallet address is incorrect']);
        }
        (!empty(session()->get('withdraw_' . $request->unit))) ? session()->remove('withdraw_' . $request->unit) : '';
        switch ($user->type_token) {
            case 10;
                DB::beginTransaction();
                try {
                    $transfer = Tron()->sendToken($request->address, $final_amount, contractUnit($user->asset_unit)['contract'], env('ADDRESS_TRON_HEX'));
                    $asset = Asset::where('user_id', $user->id)->where('unit', $user->asset_unit)->first();
                    // کاهش موجودی ولت کاربر
                    $asset->amount -= $request->amount;
                    $asset->save();
                    // لاگ تراکنش
                    $asset->transaction()->create([
                        'tracking_code' => $transfer['txID'],
                        'user_id' => $user->id,
                        'transact_type' => $user->transact_type,
                        'amount' => $amount,
                        'type' => 1,
                        'extra_field1' => $request->address,
                        'description' => "Currency withdraw {$user->asset_unit} The amount {$amount}",
                    ]);
                    DB::commit();
                    return response()->json(['status' => 100, 'msg' => 'The transfer operation was successful']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json(['status' => 500, 'msg' => 'Transfer failed. Please contact system management']);
                }
                break;
            case 20;
                DB::beginTransaction();
                try {
                    $transfer = Tron()->triggerSmartContractWithdraw($param, contractUnit($user->asset_unit)['contract'], env('ADDRESS_TRON_HEX'));

                    $asset = Asset::where('user_id', $user->id)->where('unit', $user->asset_unit)->first();
                    // کاهش موجودی ولت کاربر
                    $asset->amount -= $request->amount;
                    $asset->save();
                    // لاگ تراکنش
                    $asset->transaction()->create([
                        'tracking_code' => $transfer['txID'],
                        'user_id' => $user->id,
                        'transact_type' => $user->transact_type,
                        'amount' => $amount,
                        'type' => 1,
                        'extra_field1' => $request->address,
                        'description' => "Currency withdraw {$user->asset_unit} The amount {$amount}",
                    ]);
                    DB::commit();
                    return response()->json(['status' => 100, 'msg' => 'The transfer operation was successful']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json(['status' => 500, 'msg' => 'Transfer failed. Please contact system management']);
                }
                break;
        }
    }

    // امنیت برداشت ارز
    public function security_withdraw(Request $request)
    {
        $request->validate([
            'unit' => ['required', new Unit()],
            'amount' => 'required|numeric|between:0,999999.999999',
            'address' => 'required'
        ]);
        // کارمزد ارز مشخص شده
        $user = User::select(['id',
            'fee' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_' . $request->unit)->limit(1),
            'transact_type' => BaseData::select('id')->where('type', 'transactions')->where('extra_field1', 3)->limit(1),
            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $request->unit)->limit(1),
            'asset_amount' => Asset::select('amount')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_unit' => Asset::select('unit')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'asset_address' => Asset::select('token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
            'type_token' => Asset::select('type_token')->whereColumn('assets.user_id', 'users.id')->where('unit', $request->unit)->limit(1),
        ])->where('id', auth()->user()->id)->first();
        // استعلام موجودی کیف پول ادمین
        $asset_admin_fee = \Tron()->getBalance(env('ADDRESS_TRON_BASE')) / 1000000;
        if ($asset_admin_fee < 15) {
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "TRX currency balance is running low. Please increase your balance. Thanks for the current balance {$asset_admin_fee}"));
        } elseif ($asset_admin_fee < 15) {
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin('TRX', "TRX currency balance has expired. Please increase the balance. Thanks for the current balance {$asset_admin_fee}"));
            return response()->json(['status' => 500, 'msg' => 'This is a list of countries where the 30-year-old has been released.']);
        }
        // استعلام موجودی کیف پول ادمین برای ارز درخواست شده
        $asset_admin_unit = $this->getAmountsAdmin(env('ADDRESS_TRON_BASE'), $user->type_token);
        if (array_key_exists($request->unit, $asset_admin_unit) && ($asset_admin_unit[$request->unit] / 1000000) < $request->amount) {
            $amount_admin_unit = $asset_admin_unit[$request->unit] / 1000000;
            Mail::to(env('EMAIL_SUPPORT_ROOTIX'))->send(new sendMessageAdmin($user->asset_unit, "Unable to retrieve the contents of the selected file or file {$amount_admin_unit} Amount requested {$request->amount}"));
            return response()->json(['status' => 500, 'msg' => 'It is not possible to withdraw at the moment. Please wait another 30 minutes']);
        }
        // زمانی ک کاربر کیف پول نساخته بود
        if (!$user->asset_address) {
            return response()->json(['status' => 300, 'msg' => 'Dear user, your wallet has not been made. Please make a wallet. Thank you']);
        }
        //  حداقل برداشت
        if ($user->min_withdraw > $request->amount) {
            return response()->json(['status' => 300, 'msg' => "Minimum withdraw is {$user->min_withdraw}"]);
        }
        // اگر موجودی کاربر کمتر از مبلغ درخواستی بود
        if ($user->asset_amount < $request->amount) {
            return response()->json(['status' => 300, 'msg' => 'Your inventory is low']);
        }
        $token = Str::random(100);
        // درصورت موجود بودن توکن حذف شود و توکن جدید ایجاد شود
        session()->get('withdraw_' . $request->unit) ? session()->remove('withdraw_' . $request->unit) : '';
        session()->put('withdraw_' . $request->unit, $token);
        $user = auth()->user();
        // بررسی نوع امنیت برداشت
        $mode = $user->confirm_type;
        if ($mode == 'sms') {
            return view('user.wallet.security_withdraw.sms', compact('token', 'user'));
        } else {
            return view('user.wallet.security_withdraw.google_authenticator', compact('token', 'user'));
        }
    }

    public function security_send_sms(Request $request)
    {
        $request['country'] = 'IR';
        $request->validate([
            'number' => 'required|numeric',
            'country' => 'required',
        ]);
        $user = auth()->user();
//        if ($user->time_verification >= Carbon::now()) {
//            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
//            return response()->json(['status' => 300, 'msg' => 'The code is sent to you once', 'time' => $seconds]);
//        }
        $country = Country::where('code', $request->country)->first();
        $number = '+' . $country->phonecode . $request->number;
        $code = rand(10000, 99999);
        try {
            $user = auth()->user();
            $user->mobile = $request->number;
            $user->verification = $code;
            $user->time_verification = Carbon::now()->addMinutes(1);
            $user->save();
            smsVerify($code, $number, 'verify');

            return response()->json(['status' => 100, 'msg' => 'The code was sent for the desired mobile number', 'time' => 60]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'Error sending']);
        }
    }

    // این تابع قیمت ارز هارو به ما میده
    static public function list_currencies(Request $request)
    {

        if (isset($request->currencies))
            $list_currencies = get_specific_currencies($request->currencies);
        else
            $list_currencies = get_all_currencies();
//        $list_currencies['RIAL'] = $list_currencies['RLS'];
//        unset($list_currencies['RLS']);
        return $list_currencies;
    }


}
