<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\StepMail;
use App\Models\Admin;
use App\Models\AuthForm;
use App\Models\Bank;
use App\Models\City;
use App\Models\Country;
use App\Models\Notification;
use App\Models\User;
use App\Rules\JalaliDataFormat;
use App\Rules\NationalCodeFormat;
use App\Rules\ShebaFormat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use function Symfony\Component\Translation\t;

class StepsController extends Controller
{
    public function mobile()
    {
        $result = [];
        $countries = Country::all();
        $user = auth()->user();
        if ($user->step == 0 && !empty($user->verification) && $user->time_verification > Carbon::now()) {
            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
            $result['sms']['time'] = $seconds;
            $result['sms']['user'] = $user;
        }
        return view('user.steps.mobile', compact('countries', 'result', 'user'));
    }

    public function sendVerify(Request $request)
    {
        if ($request->mode == 1) {
            $request['country'] = 'IR';
            $request->validate([
                'number' => 'required|numeric',
                'country' => 'required',
            ]);
            $user = auth()->user();
            if ($user->time_verification >= Carbon::now()) {
                $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
                return response()->json(['status' => 300, 'msg' => 'The code is sent to you once', 'time' => $seconds]);
            }
            $country = Country::where('code', $request->country)->first();
            $number = '+' . $country->phonecode . $request->number;
            $code = rand(10000, 99999);
            try {
                $user = auth()->user();
                $user->mobile = $request->number;
                $user->verification = $code;
                $user->time_verification = Carbon::now()->addMinutes(1);
                $user->save();
//                smsVerify($code, $number, 'verify');

                return response()->json(['status' => 100, 'msg' => 'The code was sent for the desired mobile', 'time' => 60]);
            } catch (\Exception $e) {
                return response()->json(['status' => 500, 'msg' => 'Error sending']);
            }
        } elseif ($request->mode == 2) {
            $request->validate([
                'code' => 'required|numeric'
            ]);
            $user = auth()->user();
            if ($user->time_verification > Carbon::now()) {
                if ($user->verification == $request->code) {
                    $user->step = 1;
                    $user->step_complate = 1;
                    $user->reject_reason = '';
                    $user->status = 0;
                    $user->save();
                    return response()->json(['status' => 100, 'msg' => 'Operation was successful', 'refresh' => true]);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'Incorrect code']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'The code has expired']);
            }
        }
    }

    public function email()
    {
        $user = auth()->user();
        $countries = Country::all();
        return view('user.steps.email', compact('user', 'countries', 'user'));
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,email'
        ]);
        $token = Str::random(100);
        $link = route('step.verify-token.email', $token);
        $user = auth()->user();
        $user->verification = $token;
        $user->time_verification = Carbon::now()->addMinutes(15);
        $user->save();
        Mail::to($user->email)->send(new StepMail($link));
        return response()->json(['status' => 100, 'msg' => 'Operation was successful', 'refresh' => true]);
    }

    public function verifyEmail($token)
    {
        $user = User::where('verification', $token)->firstOrFail();
        if ($user->time_verification > Carbon::now()) {
            if ($user->verification == $token) {
                $user->step = 2;
                $user->step_complate = 2;
//                $user->is_complete_steps=1; // برای سایت خارجی که فقط احراز هویت ایمیل میخواست
                $user->reject_reason = '';
                $user->verification = null;
                $user->save();
                AuthForm::where('type', 'step.verify-email')->first()->userAuthForms()->create([
                    'user_id' => $user->id,
                    'status' => 2
                ]);
                return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'Email verified successfully', 'refresh' => true]);
            } else {
                return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'The link is incorrect']);
            }
        } else {
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'The link has expired']);
        }
    }

    public function information()
    {
        $user = auth()->user();
        $countries = Country::all();
        return view('user.steps.information', compact('countries', 'user'));
    }

    public function informationStore(Request $request)
    {
        $request->validate([
            'user.*' => 'required',
            'user.birth_day' => 'required',
            'user.nationality' => 'exists:countries,id',
            'user.gender_id' => 'in:1,2',
            'user.verify_type' => 'in:1,2',
            'user.id_card_number' => 'required',
            'user.id_card_exp' => 'required',
        ]);
        try {
            $model = User::where('id', auth()->user()->id)->update(array_merge($request->user, ['step' => 3, 'reject_reason' => '', 'status' => 0]));
//            $model = User::where('id', auth()->user()->id)->update(['step' => 3, 'reject_reason' => '', 'status' => 0]);
            AuthForm::where('type', 'step.information')->first()->userAuthForms()->create([
                'user_id' => auth()->user()->id,
                'status' => 1, // ho3in
                'reject_reason' => null, // ho3in
            ]);

            if ($model)
                return response()->json(['status' => 100, 'msg' => 'Operation was successful', 'refresh' => true]);
            return response()->json(['status' => 500, 'msg' => 'Registration error']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'There is a problem. Please contact the site support']);
        }

    }

//    متد برای نمایش فرم آپلود مدارک
    public function docs()
    {
        $user = auth()->user();
        $images = [
            'passport' => $user->files()->where('type', 'passport')->orderBy('id', 'desc')->first(),
            'ID_Card_first' => $user->files()->where('type', 'ID_Card_first')->orderBy('id', 'desc')->first(),
            'ID_Card_second' => $user->files()->where('type', 'ID_Card_second')->orderBy('id', 'desc')->first(),
            'selfie_image' => $user->files()->where('type', 'selfie_image')->orderBy('id', 'desc')->first(),
        ];
        return view('user.steps.upload_documents', compact('user', 'images'));
    }

//    متد ذخیره مدارک (پاسپورت یا آیدی کارت و عکس سلفی)
    public function DocsStore(Request $request)
    {
        $user = Auth::user();
        $verify_type = $user->verify_type;
        if ($verify_type == 1) {
            $request->validate([
                'image.passport' => ['required', 'mimes:png,jpg', 'max:1024'],
                'image.selfie_image' => ['required', 'mimes:png,jpg', 'max:1024'],
            ]);
        } else {
            $request->validate([
                'image.ID_Card_first' => ['required', 'mimes:png,jpg', 'max:1024'],
                'image.ID_Card_second' => ['required', 'mimes:png,jpg', 'max:1024'],
                'image.selfie_image' => ['required', 'mimes:png,jpg', 'max:1024'],
            ]);
        }

        try {
            $image = [];
            if ($request->has('image.passport')) {
                $image[] = [
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image.passport'), 'information'),
                    'type' => 'passport'
                ];
            }
            if ($request->has('image.ID_Card_first')) {
                $image[] = [
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image.ID_Card_first'), 'information'),
                    'type' => 'ID_Card_first'
                ];
            }
            if ($request->has('image.ID_Card_second')) {
                $image[] = [
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image.ID_Card_second'), 'information'),
                    'type' => 'ID_Card_second'
                ];
            }
            if ($request->has('image.selfie_image')) {
                $image[] = [
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image.selfie_image'), 'information'),
                    'type' => 'selfie_image'
                ];
            }

            if (count($image) > 0)
                User::find(auth()->user()->id)->files()->createMany($image);

            AuthForm::where('type', 'step.upload')->first()->userAuthForms()->create([
                'user_id' => auth()->user()->id,
                'status' => 1 // ho3in
            ]);

            $user = auth()->user();
            $user->step = 4;
            $user->reject_reason = '';
            $user->status = 0;
            $user->save();
            return response()->json(['status' => 100, 'msg' => 'Operation was successful', 'refresh' => true]);
            return response()->json(['status' => 500, 'msg' => 'Registration error']);
        } catch (\Exception $exception) {
            dd($exception);
            return response()->json(['status' => 500, 'msg' => 'There is a problem. Please contact the site support']);
        }

    }

    public function finish()
    {
        $user = auth()->user();
        return view('user.steps.finish', compact('user'));
    }

}
