<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Finance_transaction;
use App\Models\transactionGateway;
use App\Models\User;
use App\Rules\StrengthPasswordFormat;
use Egulias\EmailValidator\Exception\AtextAfterCFWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Intervention\Image\Facades\Image;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:profile_btn')->only('formProfile');
        $this->middleware('permission:about_me')->only('update');
        $this->middleware('permission:change_password')->only('change_pass');
    }

    public function formProfile(Request $request)
    {

        // Initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // Save the registration data in an array
        $registration_data = $request->all();

        // Add the secret key to the registration data
        $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();


        // Save the registration data to the user session for just the next request
        $request->session()->flash('registration_data', $registration_data);

//         Generate the QR image. This is the image the user will scan with their app
//         to set up two factor authentication
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            'sajjad.afg2016@gmail.com',
            $registration_data['google2fa_secret']
        );
        return view('user.profile.profile', ['QR_Image' => $QR_Image, 'secret' => $registration_data['google2fa_secret']]);

//        $user = auth()->user();
//        $usdt_asset = Asset::where('user_id', auth()->user()->id)->where('unit', 'usdt')->first();
//        $finance_transactions = Finance_transaction::where('user_id',$user->id)->where('transact_type',7)->orWhere('transact_type',2)->orderBy('id','desc')->paginate(10);
        //Validate the incoming request using the already included validator method
//        $this->validator($request->all());
//        return view('user.profile.profile', compact('user', 'usdt_asset','finance_transactions'));
    }

    public function wallet(Request $request, $unit)
    {
        try {
            $asset = Asset::where('user_id', auth()->user()->id)->where('unit', $unit)->first();
            $asset->token = $request->input('account');
            $asset->save();
            return response()->json(['status' => '100', 'msg' => 'Your wallet was successfully created.']);
        } catch (\Exception $e) {
            return response()->json(['status' => '500', 'msg' => 'There is a problem making the Wallet. Try later']);
        }


    }

    public function change_pass_view()
    {
        return view('user.profile.change-pass');
    }

    public function change_pass(Request $request)
    {
        $request->validate([
            'old_pass' => ['required', function ($attr, $val, $fail) use ($request) {
                if (!Hash::check($request->old_pass, \auth()->user()->password)) {
                    $fail('Your password does not match the previous one.');
                }
            }],
            'password' => ['required', 'min:8', 'confirmed', new StrengthPasswordFormat()],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->password)]);
        return response()->json(['status' => 100, 'msg' => 'Your password changed successfully.']);
    }

    public function update_avatar(Request $request, User $user)
    {

        $request->validate([
            'file' => 'required|mimes:jpeg,png,jpg|max:1028',
        ]);

        $user->files()->create([
            'user_id' => auth()->user()->id,
            'type' => 'profile_pic',
            'path' => upload($request->file('file'), 'profile_pic', true, 230, 230),
        ]);

        return redirect()->back()->with('flash', ['type' => 'success', 'msg' => 'Your avatar changed successfully.']);
    }

    public function completeRegistration(Request $request)
    {

        $user = User::where('id', \auth()->user()->id)->first();
        if (empty($user->google2fa_secret)) {
            $user->google2fa_secret = $request->input('secret');
            $user->save();
            return response()->json(['status' => 100, 'msg' => 'The authenticator was built successfully', 'type' => 'success']);
        } else {
            return response()->json(['status' => 500, 'msg' => 'Your authenticator is already built', 'type' => 'error']);
        }


    }

    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update([
                'about' => $request['about']
            ]);

            $user->save();
            return response()->json(['status' => 100, 'msg' => 'Your information updated']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'Something was wrong! Please try later']);
        }

    }

}
