<?php

namespace App\Http\Middleware;

use App\Models\AuthForm;
use App\Models\UserAuthForm;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Step
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \auth()->user();
        $complete_forms_user = UserAuthForm::completeForms($user->id);
        $forms = AuthForm::forms($complete_forms_user->pluck('auth_form_id')->toArray())->get()->toArray();
        $complete_forms_user = $complete_forms_user->get()->toArray();
        $prefix = str_replace('/', '', $request->route()->getPrefix());

        $routes = [
            'step.verify-token.email'
        ];
        if (Auth::check() && $user->is_complete_steps == 0 && $request->isMethod('get')) {
            // اگر کاربر جدید بود منتقل بشه به اولین فرم احراز هویت
            if (count($complete_forms_user) < 1 && $request->route()->getName() != $forms[0]['type']) {
                return redirect()->route($forms[0]['type']);
            }
            // بررسی مراحل احراز هویت
            foreach ($forms as $form) {
                if (count($complete_forms_user) >= 1 && $form['required'] == 1 && $request->route()->getName() != $form['type']) {
                    return redirect()->route($form['type']);
                } elseif ($request->route()->getName() != $form['type'] && $prefix == 'step') {
                    return redirect()->route($form['type']);
                }
                return $next($request);
            }
        }
        if (($prefix != 'step' && $request->isMethod('get')) || ($prefix != 'step' && $request->isMethod('post'))) // ho3in and ( () => mr7 )
            return $next($request);
        elseif ($prefix == 'step' && $request->isMethod('post'))
            return $next($request);
        return redirect()->route('user.dashboard');
//        $array = [
//            0 => 'step.verify-email',
//            1 => 'step.verify-mobile',
//            3 => 'step.upload',
//            2 => 'step.information',
//            4 => 'step.finish',
//        ];
//        $routes = [
//            'step.verify-token.email'
//        ];
//        $prefix = str_replace('/', '', $request->route()->getPrefix());
//        if (Auth::check() && $user->is_complete_steps == 0 && $request->isMethod('get')) {
//            if ($user->step < 2 && $request->route()->getName() != $array[$user->step]) {
//                if (in_array($request->route()->getName(), $routes)) {
//                    return $next($request);
//                }
//                return redirect()->route($array[$user->step]);
//            } elseif ($user->step >= 2 && $prefix == 'step') {
//                if (in_array($request->route()->getName(), $array) && $request->route()->getName() == $array[$user->step]) {
//                    return $next($request);
//                }
//                return redirect()->route($array[$user->step]);
//            }
//        }else {
//            if (in_array($request->route()->getName(),$array) && $user->is_complete_steps == 1) {
//                return redirect()->route('user.dashboard');
//            }
//        }
//        return $next($request);
    }
}
