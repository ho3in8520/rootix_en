<?php

namespace App\Library;

use App\Models\Currency;
use Illuminate\Support\Facades\Http;

class Coinex
{
    private $sign;
    private $coinex;
    private $access_id;
    private $secret_key;

    public function __construct($access_id, $secret_key)
    {
        $this->access_id = $access_id;
        $this->secret_key = $secret_key;
    }

    private function get_sign($params)
    {
        ksort($params);
        $pre_sign_ls = array();
        foreach ($params as $key => $val) {
            array_push($pre_sign_ls, "$key=$val");
        }
        array_push($pre_sign_ls, "secret_key=" . $this->secret_key);
        $pre_sign_str = join("&", $pre_sign_ls);
        //echo "$pre_sign_str\n";
        return strtoupper(md5($pre_sign_str));
    }

    private function request_get($url, $params)
    {
//        dd($params,$this->secret_key,$this->access_id);
        $headers = [
            'authorization:' . $this->get_sign($params),
            'Content-type: application/json'
        ];
        $ch = curl_init($url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return $output = curl_exec($ch);
    }

    private function request_post($url, $params)
    {
        $headers = [
            'authorization:' . $this->get_sign($params),
            'Content-type: application/json'
        ];
        $params = json_encode($params);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return $output = curl_exec($ch);
    }

    private function request_delete($url, $params)
    {
        $headers = [
            'authorization:' . $this->get_sign($params),
            'Content-type: application/json'
        ];

        $ch = curl_init($url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return $output = curl_exec($ch);
    }

    private function send_request($url, $params, $method = 'get')
    {
        switch ($method) {
            case 'get':
                return $this->request_get($url, $params);
                break;
            case 'post':
                return $this->request_post($url, $params);
                break;
            case 'delete';
                return $this->request_delete($url, $params);
                break;
        }
    }

    public function get_deposit_list($coin = null)
    {
        $url = 'https://api.coinex.com/v1/balance/coin/deposit';
        $tonce = round(microtime(true) * 1000);
        $params = [
            "access_id" => $this->access_id,
            "tonce" => $tonce,
        ];
        ($coin) ? $params = array_merge($params, ["coin_type" => $coin]) : '';

        return $this->send_request($url, $params);
    }

    public function get_balances()
    {
        $url = 'https://api.coinex.com/v1/balance/info';
        $tonce = round(microtime(true) * 1000);
        $params = [
            "access_id" => $this->access_id,
            "tonce" => $tonce,
        ];
        return $this->send_request($url, $params);
    }

    public function get_balances_coin($coin)
    {
        $available = 0;
        $data = $this->get_balances();
        $data = json_decode($data, true);
        $coin = strtoupper($coin);
        // اگر درخواست با موفقیت ارسال شد
        if (isset($data) && $data['code'] == 0) {
            /// اگر ارز مورد نظر وجود داشت
            if (isset($data['data'][$coin])) {
                $available = $data['data'][$coin]['available'];
            }
        }
        return $available;
    }

    public function get_transaction_tron($txid)
    {
        return Tron()->getTransaction($txid);
    }

    public function get_transaction_eth($txid)
    {
        $url = 'https://api.etherscan.io/api';
        $params = [
            "module" => 'transaction',
            "action" => 'getstatus',
            "txhash" => $txid,
            "apikey" => env('ETHER_AIP_KEY'),
        ];
        $headers = [
            'Content-type: application/json'
        ];
        $ch = curl_init($url . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        dd($output);
    }

    public function get_deposit_address($unit, $smart_contract_name = 'TRC20')
    {
        $url = 'https://api.coinex.com/v1/balance/deposit/address/' . strtolower($unit);
        $tonce = round(microtime(true) * 1000);
        $params = [
            "access_id" => $this->access_id,
            "tonce" => $tonce,
            "smart_contract_name" => $smart_contract_name,
        ];
        return $this->send_request($url, $params, 'get');
    }

    public function inquiry_deposit_currency_to_coinex($unit, $txid, $blockchain_type)
    {
        switch ($blockchain_type) {
            case 'tron':
                return $this->inquiry_deposit_currency_to_coinex_tron($unit, $txid);
                break;
            case 'ethereum':
                $this->inquiry_deposit_currency_to_coinex_ethereum($unit, $txid);
                break;
        }
    }

    private function inquiry_deposit_currency_to_coinex_tron($unit, $txid)
    {
        $tron = $this->get_transaction_tron($txid);
        $list_deposit = $this->get_deposit_list($unit);

        $list_deposit = json_decode($list_deposit, true)['data'];
        //// اگر هیچ تراکنششی موجود نبود
        if ($list_deposit['count'] == 0) {
            return false;
        }
        ///// اگر تراکنش از سمت ترون تایید شده بود
        if ($tron['ret'][0]['contractRet'] == 'SUCCESS') {
            foreach ($list_deposit['data'] as $item) {
                // پیدا کردن اطلاعات txid
                if ($item['tx_id'] == $txid) {
                    return $item['status'];
                }
            }
        }
    }

    private function inquiry_deposit_currency_to_coinex_ethereum($unit, $txid)
    {

    }

    public function new_trade(string $market, string $type, $amount, $price, string $source_id = null)
    {
        $url = 'https://api.coinex.com/v1/order/limit';
        $tonce = round(microtime(true) * 1000);
        $params = [
            "access_id" => $this->access_id,
            "tonce" => $tonce,
            "market" => $market,
            "type" => $type,
            "amount" => (string) $amount,
            "price" => (string) $price,
            "source_id" => $source_id,
        ];
        return $this->send_request($url, $params, 'post');
    }

    public function get_trade(string $type, array $params)
    {
        switch ($type) {
            case 'unexecuted':
                $url = 'https://api.coinex.com/v1/order/pending';
                break;
            case 'executed':
                $url = 'https://api.coinex.com/v1/order/finished';
                break;
            case 'order_status':
                $url = 'https://api.coinex.com/v1/order/status';
                break;
        }
        $tonce = round(microtime(true) * 1000);
        $params = array_merge([
            "access_id" => $this->access_id,
            "tonce" => $tonce,
        ], $params);
        return $this->send_request($url, $params, 'get');
    }

    public function cancel_trade(int $id, string $market)
    {
        $url = 'https://api.coinex.com/v1/order/pending';
        $tonce = round(microtime(true) * 1000);
        $params = [
            "access_id" => $this->access_id,
            "id" => $id,
            "market" => $market,
            "tonce" => $tonce,
        ];
        return $this->send_request($url, $params, 'delete');
    }

    public function information_withdraw_currency($coin = 'btc')
    {
        $url = 'https://api.coinex.com/v1/common/asset/config';
        $params = [
            "coin_type" => strtoupper($coin),
        ];
        return $this->send_request($url, $params, 'get');
    }

    public function trade_status($val)
    {
        $status = 0;
        switch ($val) {
            case 'not_deal':
                $status = 0;
                break;
            case 'part_deal';
                $status = 0;
                break;
            case 'cancel';
                $status = 2;
                break;
            case 'done';
                $status = 1;
                break;
        }
        return $status;
    }

    public function withdrawal($coin, $address, $amount)
    {
        $url = 'https://api.coinex.com/v1/balance/coin/withdraw';
        $tonce = round(microtime(true) * 1000);
        $currency = Currency::where('unit', $coin)->firstOrFail();
        $smart_contract_name = $currency->network . $currency->type_token;
        $params = [
            "access_id" => $this->access_id,
            "tonce" => $tonce,
            "smart_contract_name" => $smart_contract_name,
            "coin_type" => strtoupper($coin),
            "coin_address" => $address,
            "actual_amount" => $amount,
            "transfer_method" => 'onchain',
        ];
        return $this->send_request($url, $params, 'post');
    }

    public function inquire_withdrawal_list($coin_withdraw_id = null)
    {
        $url = 'https://api.coinex.com/v1/balance/coin/withdraw';
        $tonce = round(microtime(true) * 1000);
        $params = [
            "access_id" => $this->access_id,
            "tonce" => $tonce,
        ];
        if (!empty($coin_withdraw_id)) {
            $params = array_merge(["coin_withdraw_id" => $coin_withdraw_id], $params);
        }
        return $this->send_request($url, $params, 'get');
    }
}
