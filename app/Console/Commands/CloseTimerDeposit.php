<?php

namespace App\Console\Commands;

use App\Models\Asset;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CloseTimerDeposit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:timer_deposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $assets = Asset::query()->where('unit', 'usdt')->where('timer_deposit', '!=', 0)->get();
        foreach ($assets as $asset) {
            if (Carbon::now() > Carbon::parse($asset->expire_deposit)->addDays(2)) {
                $asset->expire_deposit = null;
                $asset->timer_deposit = 0;
                $asset->save();
            }
        }
        return Command::SUCCESS;
    }
}
