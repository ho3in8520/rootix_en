<?php

namespace App\Console\Commands;

use App\Jobs\CloseTickets;
use App\Models\TicketMaster;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CloseTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close Tickets With More Than 72 Hours Any Update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tickets=TicketMaster::query()->select(['id'])->whereIn('status',[1,2,3])->whereDate('updated_at',Carbon::now()->subDays(7))->get();
        foreach ($tickets as $ticket){
            dispatch(new CloseTickets($ticket->id,$ticket->updated_at));
        }
        return Command::SUCCESS;
    }
}
