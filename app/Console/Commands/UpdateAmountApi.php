<?php

namespace App\Console\Commands;

use App\Models\GlobalMarketApi;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;

class UpdateAmountApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currencies = 'KLV,BTC,ETH,USDT,TRX,WIN,JST,BTT,MATIC,COMP,QTUM
        ,SXP,DOT,SUSHI,SOL,LUNA,ALPHA,BCH,XVG,VET,WINGS
        ,LINK,HOT,CHZ,VITE,ATOM,CLV,ETC,BNB,NEO,ZIL
        ,ALGO,OMG,DOGE,ADA,AVAX,AAVE,MINA,TLM,POND,1INCH
        ,LINA,ICP,DASH,IOST,THETA,KAVA,WAVES,SRM,NEAR,TOMO
        ,FIL,OCEAN,CVC,ZEN,YOYO,LRC,MANA,POE,WINGS,CHAT
        ,NANO,NAV,NAS,FTT,BULL,AVA,ANT,UMA,SUN,DODO
        ,LINA,CAKE,EPS,DEGO,AUTO,PUNDIX,BAR,SHIB,RAY,GNO
        ,CRO,KEEP,ATA,C98,SLP,REEF,GRT,BAKE,ASRF
        ,UNI,MIR,CFX,POLS,BURGER,TORN,PERP,FIS,EASY,BSV,LTC,OKB';
        $result3=Http::post('http://api.rootix.io/currencies/market/stats/all-currencies', ['api_token' => 'isZXuDxtOOnMwEl2YwORhHeBo2woMaf3U8JwwV47tlEnPvsbN9', 'currencies' => $currencies])->body();
        $result = Http::get('https://api.nobitex.ir/market/stats', ['srcCurrency' => 'btc,eth,ltc,usdt,xrp,bch,bnb,eos,xlm,etc,trx,doge,pmn', 'dstCurrency' => 'rls'])->body();
        $result=json_decode($result,true);
        $stats=$result['stats'];
        $result=json_encode($result);
        $result2 = Http::get('https://api.nomics.com/v1/currencies/ticker', ['key' => '8364d24c1e1bde44c08800d7f14fcd9cb6574c1e', 'ids' => $currencies, 'interval' => '1d,7d',
            'convert' => 'USDT'])->body();
        $result2=json_decode($result2);
        $result2=array_merge($result2,['usdt-rls'=>$stats['usdt-rls']['latest']]);
        $result2=json_encode($result2);
        if (json_decode($result)->status == 'ok') {
            GlobalMarketApi::updateOrCreate([
                'type' => 'price_currency'
            ],
                ['type' => 'price_currency', 'value' => $result],
            );
        }
        if ($result2 && !empty($result2)) {
            Redis::set('currencies', $result2);
        }
        if ($result3 && !empty($result3)) {
            Redis::set('rootix_currencies', $result3);
        }

        return $this->comment("success");
    }
}
