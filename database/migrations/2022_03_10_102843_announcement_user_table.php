<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AnnouncementUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcementables', function (Blueprint $table) {
            $table->unsignedInteger('announcement_id');
            $table->unsignedBigInteger('announcementable_id');
            $table->string('announcementable_type');

            $table->primary(['announcement_id', 'announcementable_id', 'announcementable_type'],'custom_primary_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
