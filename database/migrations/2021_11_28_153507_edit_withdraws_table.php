<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('withdraws');
        Schema::create('withdraws', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users");
            $table->unsignedBigInteger('coin_withdraw_id')->default(0);
            $table->float('amount', 20, 2)->default(0);
            $table->float('fee', 20, 2)->default(0);
            $table->string('unit')->comment('BTC','ETH');
            $table->string('address');
            $table->string('tx_id')->nullable();
            $table->string('network')->comment('TRC,ERC');
            $table->boolean('status')->default(1)->comment('0=>new 1=>confirm admin 2=>success Exchange 3=>reject admin 4=>reject Exchange');
            $table->string('reject_reason', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}
