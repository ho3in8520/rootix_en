<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimerDepositColumnToAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->float('timer_deposit', 20, 8)->default(0)->nullable()->after('token')->comment('برای تکنیک بازاریابی، عددی که کاربر در مدال وارد میکند اینجا ذخیره میکنیم');
            $table->timestamp('expire_deposit')->nullable()->after('timer_deposit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
