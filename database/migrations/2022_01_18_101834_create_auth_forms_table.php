<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_forms', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('title');
            $table->text('data')->nullable();
            $table->tinyInteger('priority');
            $table->boolean('status')->default(1);
            $table->boolean('required')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_forms');
    }
}
