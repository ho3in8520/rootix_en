<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAuthFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_auth_forms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('auth_form_id');
            $table->unsignedBigInteger('user_id');
            $table->text('value')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0=>new , 1=> completed ,2=>confirmed ,3 =>rejected');
            $table->string('reject_reason',500)->nullable();
            $table->foreign('auth_form_id')->references('id')->on('auth_forms');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_auth_forms');
    }
}
