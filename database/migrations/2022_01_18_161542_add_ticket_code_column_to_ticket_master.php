<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTicketCodeColumnToTicketMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_master', function (Blueprint $table) {
            $table->unsignedBigInteger('ticket_code')->after('user_id');
        });
        $sql='
        CREATE TRIGGER `after_ticket_update` BEFORE INSERT ON `ticket_master`
 FOR EACH ROW BEGIN
IF (NEW.ticket_code IS NULL OR NEW.ticket_code IS NOT NULL) THEN
        set @max_ticket_code = 1000;
        select ifnull(max(cast(ticket_code as unsigned )),1000) into @max_ticket_code from ticket_master;
        set new.ticket_code = @max_ticket_code + 1;
        END IF;
    END;';
        \Illuminate\Support\Facades\DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('ticket_code');
        \Illuminate\Support\Facades\DB::statement("DROP TRIGGER IF EXISTS after_ticket_update");
    }
}
