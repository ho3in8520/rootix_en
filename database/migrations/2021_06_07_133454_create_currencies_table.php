<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('currencies');
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_fa');
            $table->string('unit');
            $table->string('type_token');
            $table->string('network')->default('TRC');
            $table->string('color')->nullable();
            $table->tinyInteger('priority');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
        $seed=new \Database\Seeders\CurrencySeeder();
        $seed->run();



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
