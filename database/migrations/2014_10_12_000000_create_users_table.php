<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('code')->unique();
            $table->string('mobile', 11)->nullable();
            $table->string('email')->unique();
            $table->text('google2fa_secret')->nullable();
            $table->boolean('login_2fa')->default(false);
            $table->string('google_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedBigInteger('nationality')->nullable();
            $table->foreign('nationality')->references('id')->on('countries');
            $table->string('postal_code', 10)->nullable();
            $table->string('birth_day', 20)->nullable();
            $table->tinyInteger('language')->comment('1=>fa 2=>en')->nullable();
            $table->tinyInteger('gender_id')->comment('1=>mard 2=>khanom')->nullable();
            $table->tinyInteger('step')->default(0)->comment("0=>new register 1=>verification mobile 2=>verification email 3=>bank 4=>Complete information 5=>panel ready");
            $table->tinyInteger('step_complate')->default(0)->comment("0=>new register 1=>verification mobile and email 2=>bank 3=>Complete information 4=>panel ready");
            $table->boolean('is_complete_steps')->default(0);
            $table->boolean('status')->default(0);
            $table->tinyInteger('panel_type')->nullable();
            $table->enum('confirm_type',['sms','google-authenticator'])->default('sms');
            $table->enum('verify_type',[1,2])->default(1)->comment('1=>Passport , 2 =>ID Card');
            $table->string('id_card_number')->nullable()->comment('شماره پاسپورت یا آیدی کارت');
            $table->string('id_card_exp')->nullable()->comment('تاریخ انقضای پاسپورت، اگر دائمی باشد کلمه permanent');
            $table->string('verification', 300)->nullable();
            $table->string('login_token')->nullable();
            $table->string('time_verification',20)->nullable();
            $table->unsignedBigInteger('referral_id')->nullable();
            $table->string('reject_reason', 255)->nullable();
            $table->text('about')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
