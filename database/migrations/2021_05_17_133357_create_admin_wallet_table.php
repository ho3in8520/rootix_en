<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminWalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_wallet', function (Blueprint $table) {
            $table->id();
            $table->string('private_key');
            $table->string('base58');
            $table->string('hex');
        });

        $array = [
            ['private_key' => 'd40ea0e75b754ef6e59cc8d6589ee9192664e90d0a0173a5e7adfba2faa5e2ad', 'base58' => 'TKSYFGowi6hzUvHEiex6MGEppEHgsxMjua', 'hex' => '4167e46d91927744c999ceb6bf625550b7e7f52eb7'],
        ];

        \Illuminate\Support\Facades\DB::table('admin_wallet')->insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_wallet');
    }
}
