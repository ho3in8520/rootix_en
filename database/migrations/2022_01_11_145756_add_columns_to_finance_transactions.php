<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToFinanceTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('finance_transactions', function (Blueprint $table) {
            $table->string('unit')->after('type')->nullable()->comment('واحد ارز می باشد مثلا usdt یا btc');
            $table->float('theter_price',20,8)->after('unit')->nullable()->comment('قیمت لحظه ای ارز به usdt ذخیره شود.');
            $table->float('theter_rls',20,8)->after('theter_price')->nullable()->comment('قیمت لحظه ای تتر به ریال ذخیره شود.');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('finance_transactions', function (Blueprint $table) {
            //
        });
    }
}
