<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Role;
use App\Models\User;
use Dompdf\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            // ایجاد رول کاربر
            $role_user = Role::firstOrCreate(['name' => 'user'], ['guard_name' => 'web']);
            // ایجاد رول ادمین
            $admin_role = Role::firstOrCreate(['name' => 'admin'], ['guard_name' => 'web']);

            //        انتصاب همه ی دسترسی ها به رول admin
            $permissions = Permission::pluck('id')->toArray();
//            foreach ($permissions as $permission)
            $admin_role->syncPermissions($permissions);

//        انتصاب دسترسی های مربوط به رول user
            $user_permissions = ['dashboard_menu', 'wallet_menu', 'rial_deposit', 'rial_deposit_btn', 'rial_withdraw', 'rial_withdraw_btn',
                'crypto_deposit', 'crypto_deposit_btn', 'crypto_withdraw', 'crypto_withdraw_btn', 'banks_menu', 'user_bank_menu', 'bank_create',
                'bank_edit', 'swap_menu', 'swap_store', 'trade_menu', 'rial_trade_buy', 'rial_trade_sell', 'usdt_trade_buy', 'usdt_trade_sell',
                'rial_list', 'usdt_list', 'user_setting_menu', 'authenticate_active', 'authenticate_deactive', 'tickets_menu', 'user_ticket_menu',
                'ticket_create', 'ticket_show', 'ticket_close', 'list_menu', 'user_transaction_menu', 'rial_transaction_menu', 'crypto_transaction_menu',
                'profile_btn', 'about_me', 'change_password', 'authenticate_btn'];
            $user_permissions = Permission::whereIn('name', $user_permissions)->pluck('id')->toArray();
//            foreach ($user_permissions as $user_permission)
            $role_user->syncPermissions($user_permissions);


//        انتصاب رول user به کاربران قدیمی
            $users = User::where('is_admin', 0)->get();
            foreach ($users as $user) {
                $user->assignRole($role_user->id);
            }

//        انتصاب رول admin به ادمین سامانه
            $admin = User::where('is_admin', 1)->first();
            $admin->assignRole($admin_role->id);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
    }


}
