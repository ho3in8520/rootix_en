<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\User;
use Illuminate\Database\Seeder;

class AddTokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users= User::all();
        foreach ($users as $user) {
            $token= Asset::query()->where('user_id',$user->id)->whereNotNull('token')->value('token');
            if ($token==null) {
                $token= json_encode(Tron()->generateAddress()->getRawData());
            }
            Asset::query()->where('user_id',$user->id)->update([
                'token' => $token
            ]);
        }
    }
}
