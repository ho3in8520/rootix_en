<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'first_name' => 'مدیر',
            'last_name' => 'مدیری',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'activated' => 1,
            'activated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
