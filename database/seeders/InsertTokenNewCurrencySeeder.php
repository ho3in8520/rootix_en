<?php

namespace Database\Seeders;

use App\Models\Asset;
use Illuminate\Database\Seeder;

class InsertTokenNewCurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $assets=Asset::select('token','user_id','unit')->whereNotNull('token')->groupBy('token','user_id','unit')->get();
        foreach ($assets as $asset)
        {
            Asset::where('user_id',$asset->user_id)->update(['token'=>$asset->token]);
        }
    }
}
