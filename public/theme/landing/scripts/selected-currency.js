document.addEventListener("DOMContentLoaded", () => {
  const selected = document.querySelector(".currency-details__prices-selected");
  const selectedCurrency = document.querySelector(".selected-currency");

  document.addEventListener("click", (e) => {
    const clicked = e.target;

    if (selected.contains(clicked) && !selectedCurrency.contains(e.target)) {
      selected.classList.toggle("active");
    }
  });

  const selectedCurrencyItems = [...selectedCurrency.querySelectorAll("li")];
  const selectedTitle = selected.querySelector(".title-selected h5");
  selectedCurrencyItems.forEach((item) => {
    item.addEventListener("click", (e) => {
      const itemValue = e.target.textContent;
      selectedTitle.innerHTML = itemValue
    });
  });
});
