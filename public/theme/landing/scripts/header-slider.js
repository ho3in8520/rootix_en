const currency = document.querySelector(".currencys");
const headerCurrencyInformations = [
  ...document.querySelectorAll(".header-currency-informations"),
];

let currentCurrency = 0;

let scrollC = 0;
function updateScroll() {
  scrollC += headerCurrencyInformations[currentCurrency].clientHeight;
  if (currentCurrency === headerCurrencyInformations.length - 1) {
    scrollC = 0;
    currentCurrency = 0;
  }
}

setInterval(() => {
  currentCurrency += 1;
  updateScroll();
  currency.scroll({
    top: scrollC,
    behavior: "smooth",
  });
}, 5000);