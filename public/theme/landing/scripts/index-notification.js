const notificationTextContainerRight = document.querySelector(
  ".notification-text-container-right"
);

const notificationTextRight = document.querySelector(
  ".notification-text-right"
);

const notificationTextContainerLeft = document.querySelector(
  ".notification-text-container-left"
);

const notificationTextLeft = document.querySelector(
  ".notification-text-left"
);

let scrollValue = 0;

function newLine(container, child) {
  const containerHeight = container.offsetHeight;
  const childHeight = child.offsetHeight - containerHeight;
  setInterval(() => {
    scrollValue += containerHeight;

    console.log(childHeight, scrollValue);

    container.scroll({
      top: scrollValue > childHeight ? (scrollValue = 0) : scrollValue,
      behavior: "smooth",
    });
  }, 2000);
}

let scrollValue2 = 0;

function newLine2(container, child) {
    const containerHeight = container.offsetHeight;
    const childHeight = child.offsetHeight - containerHeight;
    setInterval(() => {
      scrollValue2 += containerHeight;
  
      console.log(childHeight, scrollValue2);
  
      container.scroll({
        top: scrollValue2 > childHeight ? (scrollValue2 = 0) : scrollValue2,
        behavior: "smooth",
      });
    }, 2000);
  }
newLine(notificationTextContainerRight, notificationTextRight);
newLine2(notificationTextContainerLeft, notificationTextLeft);