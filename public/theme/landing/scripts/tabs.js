const tabBtns = [...document.querySelectorAll(".currency-tabel-btns button")];
const tabContent = [...document.querySelectorAll(".currency-table")];

for (let i = 0; i < tabBtns.length; i++) {
  tabBtns[i].addEventListener("click", (e) => {
    tabContent.map((item) => {
      item.classList.remove("active");
    });

    tabBtns.map((item) => {
      item.classList.remove("active");
    });

    tabBtns[i].classList.add("active");
    tabContent[i].classList.add("active");
  });
}
