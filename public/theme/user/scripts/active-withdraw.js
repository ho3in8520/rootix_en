const currencyInputs = [...document.querySelectorAll(".currency-value-input")];
const requestBoxes = [...document.querySelectorAll(".currentInput")];
const requestBox2 = document.querySelector(".request-js-box-2");
const requestBox4 = document.querySelector(".request-js-box-4");
const requestBox3 = document.querySelector(".request-js-box-3");
const input1 = document.querySelector("#currency-value-input-1");
const volumeForm9 = document.querySelector(".volume-form-2");
const volumeFormSelected9 = document.querySelector(".volume-form__selected-2");
const volumeFormMenu = document.querySelector(".volume-form__menu");
const input2 = document.querySelector("#currency-value-input-2");


volumeFormSelected9.addEventListener("click", () => {
  console.log(volumeFormSelected9)
  volumeFormSelected9.classList.toggle("active");
});

input1.addEventListener("input", () => {
  checkActiveBtnForInput();
});

input2.addEventListener("input", () => {
  if (!checkNumberRegex(input2)) {
    input2.value = "";
  }
});

const checkActiveBtnForInput = () => {
  if (checkNumberRegex(input1) && !inputValueIsEmpty(input1)) {
    requestBox4.classList.add("active");
    requestBox4.classList.add("enabled");
    requestBox2.classList.remove("active")
  } else {
    input1.value = "";
    requestBox4.classList.remove("active");
    requestBox4.classList.remove("enabled");
    requestBox2.classList.add("active")
  }
};

const checkActiveBtnSelected = () => {
  requestBox3.classList.add("active");
  requestBox3.classList.add("enabled");
  requestBox4.classList.remove("active")
  const reqInput = requestBox3.querySelector("input");
  reqInput.disabled = false;
};

const inputValueIsEmpty = (input) => input.value.trim().length === 0;

const checkNumberRegex = (input) => {
  const numberRegex = /^\d*\.?\d*$/;

  return numberRegex.test(input.value.trim());
};

volumeFormMenu.addEventListener("click", checkActiveBtnSelected);