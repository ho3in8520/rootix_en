$(document).ready(function () {
  let inputsWrapper = $('#toggle-filters-inputs');
  inputsWrapper.slideUp(0);
  $('#filters').click(function () {
    if (inputsWrapper.hasClass('up-animation')) {
      inputsWrapper.removeClass('up-animation');
      inputsWrapper.slideDown(500);
    } else {
      inputsWrapper.addClass('up-animation');
      inputsWrapper.slideUp(500);
    }
  });

  $('#transaction__btn2').click(function () {
    if (!inputsWrapper.hasClass('up-animation')) {
      inputsWrapper.addClass('up-animation');
      inputsWrapper.slideUp(700);
    }
  });
});

$('#aioConceptName').change(function () {
  var $option = $(this).find('option:selected');

  var value = $option.val();
  var text = $option.text();
});
