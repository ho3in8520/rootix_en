const currencyValueInputs = [
  ...document.querySelectorAll(".currency-value-input"),
];
const volumeFormItem = [...document.querySelectorAll(".volume-form__item")];
const selectedBank = document.querySelector(".selected-bank");

document.addEventListener("DOMContentLoaded", () => {
  inputJustGetNumber(currencyValueInputs);
  setLiContentForSelected(volumeFormItem, selectedBank);
});