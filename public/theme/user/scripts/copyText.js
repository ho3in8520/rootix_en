const copyBtn = document.querySelector(".copy-btn");

copyBtn.addEventListener("click", copyToClipBoard);

function copyToClipBoard() {
  const walletAddressEl = document.querySelector(".wallet-address-character");
  const tooltipText = document.querySelector(".tooltip-address__text")

  tooltipText.classList.add("active")
  setTimeout(() => {
    tooltipText.classList.remove("active")
  }, 2000)
  
  navigator.clipboard.writeText(walletAddressEl.textContent);
}
  function copy_text(id) {
  var copyText = document.getElementById(id);
  var input = document.createElement("input");
  input.value = $.trim(copyText.getAttribute('data-content'));
  document.body.appendChild(input);
  input.select();
  document.execCommand("Copy");
  input.remove();
}
