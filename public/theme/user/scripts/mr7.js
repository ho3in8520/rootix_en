$(document).ready(function () {
    $("ul.mr7-select li").click(function () {
        $(this).parent().children('li').removeClass('selected');
        $(this).addClass('selected');
    });

    var clear_notif= function () {
        $("ul.notification").html('<li class="text-center"><h6>چیزی برای نمایش وجود ندارد</h6></li>');
    }

    $(".btn-delete").click(function () {
        ajax({
            action: '/notifications/clear_all',
            isObject: true,
            method: 'delete',
            button: $(this)
        },clear_notif)
    })

})
