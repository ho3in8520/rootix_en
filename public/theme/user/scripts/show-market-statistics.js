const mainStatistics = document.querySelector(".main-statistics");
const selectedVolumeShow = document.querySelector(".selected-volume");

mainStatistics.addEventListener("click", () => {
  selectedVolumeShow.classList.toggle("active");
});

const statistics = [...document.querySelectorAll(".statistics")];
const mainStatisticsTextEl = document.querySelector(".main-statistics h5");

statistics.forEach((statistic, index) => {
  statistic.addEventListener("click", () => {
    const statisticsText = statistics[index].querySelector("h5").textContent;
    mainStatisticsTextEl.textContent = statisticsText;
  });
});

const statisticsMenu = document.querySelector(".statistics-menu");
document.addEventListener("click", (e) => {
  if (
    !mainStatistics.contains(e.target) &&
    !statisticsMenu.contains(e.target)
  ) {
    selectedVolumeShow.classList.remove("active");
  }
});
