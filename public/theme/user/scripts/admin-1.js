const btns = [...document.querySelectorAll('.edit-btn')];

if (btns != null) {
  btns.forEach((btn, index) => {
    btn.addEventListener('click', () => {
      const currentInputParent = [...document.querySelectorAll('.inputs-box')];
      console.log(btn, index)
      activeAllInputs(currentInputParent, index);
    });
  });
}
