Livewire.on('landingRegisterSuccess', function (response) {
    if (response.status == 100) {
        $(".alert-message").attr('class', "alert-message alert alert-success");
        $(".alert-message").html(response.msg);
        setTimeout(function () {
            window.location.reload();
        }, 3000)
    } else {
        $(".alert-message").attr('class', "alert-message alert alert-danger");
        $(".alert-message").html(response.msg);
        setTimeout(function () {
            $(".alert-message").closest('.form-group').slideUp();
        }, 5000)
    }
    $(".alert-message").closest('.form-group').slideDown();

})

Livewire.on('swal', function (response) {
    if (response.status == 100) {
        swal(response.msg, '', 'success');
        if (response.time){
            setTimeout(function () {
                window.location.reload();
            }, response.time)
        }
    } else {
        swal(response.msg, '', 'error')
    }
})

