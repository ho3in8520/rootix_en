<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>@yield('title_browser')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Routix, a safe market for buying and selling Bitcoin, Ethereum and digital currencies, is proud to serve you dear ones by facilitating trading and having the standard features of the world's top digital currency exchanges in a professional environment.">
@include('templates.landing_page.layout.header')
    @yield('style')

    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="540a27be-c263-4365-8c1e-9a55fd83554d";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

    <!-- Start of txt.me widget code -->
{{--    <script src="https://v3.txt.me/livechat/js/wrapper/e49e9c1f-f673-479a-9d2f-98661ca37940" async></script>--}}
{{--    <noscript><a href="https://txt.me/reviews/e49e9c1f-f673-479a-9d2f-98661ca37940" rel="nofollow">Rate New Widget customer support</a>, powered by <a href="https://txt.me" rel="noopener nofollow" target="_blank">txt.me</a></noscript>--}}
    <!-- End of txt.me widget code -->
</head>
<body class="en-body">
<div class="loader">
    <img src="{{asset('theme/landing/images/loader.svg')}}">
</div>
@include('templates.landing_page.layout.mobile.top_menu')

<main>
    @yield('content')
</main>
<!-- Footer Starts -->
<footer>
    <div class="container-fluid">

        <div class="footer-logo">

            <h2>
                R
            </h2>
            <h2>
                OOTIX</h2>
        </div>

        <div class="row footer-row">
            <div class="col-6 col-lg-3">
                <div class="footer-contact footer-item">
                    <h3 class="footer-title">Rootix</h3>
                    <div>
                        <h5>info@rootix.com</h5>
{{--                        <h5>648-145-8360</h5>--}}
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-sign-in footer-item">
                    <h3 class="footer-title">Join us</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="{{route('register.form')}}"> Register</a>
                        </li>
                        <li>
                            <a href="{{route('login.form')}}"> Login </a>
                        </li>
                        <li>
                            <a href="{{route('landing.faq')}}"> FAQ </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-about-us footer-item">
                    <h3 class="footer-title">About us</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="{{route('landing.about')}}"> About Rootix </a>
                        </li>
                        <li>
                            <a href="#"> Our teams </a>
                        </li>
                        <li>
                            <a href="#"> Job opportunities </a>
                        </li>
                        <li>
                            <a href="#">User comments</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-quick-access footer-item">
                    <h3 class="footer-title">Quick access</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> Rootix method  </a>
                        </li>
                        <li class="footer-rootix-menu-item">
                            <a href="#">Rootix features </a>
                        </li>
                        <li>
                            <a href=""> Blog </a>
                        </li>
                        <li>
                            <a href="{{route('landing.prices')}}"> Prices </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr />

        <div class="footer-bottom">
            <div class="social-networks">
            </div>

            <h5>© 2018 Rootix All Right Reserved</h5>
            <h5 class="inf">info@rootix.io</h5>
        </div>
    </div>
</footer>
<!-- Footer Ends -->
@include("templates.landing_page.layout.footer")
<script>
    $(document).ready(function (){
        $('.loader').hide();
        $("body").css({"overflow-y":"visible"});
    });
</script>
@yield('script')
</body>
</html>
