<div>
    <a href="{{route('landing.home')}}" class="logo">
        <img src="{{asset('theme/landing/images/logo.png')}}" alt="لوگو"/>
    </a>
    <ul>
        <li><a href="{{route('landing.home')}}"> home </a></li>
        <li><a href="{{route('landing.about')}}"> About us </a></li>
        <li><a href="{{route('landing.services')}}"> Services </a></li>
        <li><a href="{{route('landing.faq')}}">FAQ</a></li>
    </ul>
</div>

<div class="login-container">
    <a href="#" class="language">
        <p>En</p>
        <div>
            <img src="{{asset('theme/landing/images/flag2.png')}}" alt="flag"/>
        </div>
    </a>
    <a href="{{route('login.form')}}" class="login-btn">
        login
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            width="18"
            height="9"
            viewBox="0 0 18 9"
        >
            <image
                id="icons8-long_arrow_up_filled"
                width="18"
                height="9"
                xlink:href="images/login-right-arrow.png"
            />
        </svg>
    </a>
</div>

