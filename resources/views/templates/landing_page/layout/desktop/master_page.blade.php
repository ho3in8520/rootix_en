<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>@yield('title_browser')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Routix, a safe market for buying and selling Bitcoin, Ethereum and digital currencies, is proud to serve you dear ones by facilitating trading and having the standard features of the world's top digital currency exchanges in a professional environment.">
{{--    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="28bb5c2a-385c-4a21-a311-4af23631a68e";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>--}}

@include('templates.landing_page.layout.header')
@yield('style')

{{--    <script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o+"?href="+window.location.href;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="ab96fde8-18ba-4828-a1ed-8189027d5fcf";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script>--}}
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="540a27be-c263-4365-8c1e-9a55fd83554d";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

<!-- Start of txt.me widget code -->
{{--    <script src="https://v3.txt.me/livechat/js/wrapper/e49e9c1f-f673-479a-9d2f-98661ca37940" async></script>--}}
{{--    <noscript><a href="https://txt.me/reviews/e49e9c1f-f673-479a-9d2f-98661ca37940" rel="nofollow">Rate New Widget customer support</a>, powered by <a href="https://txt.me" rel="noopener nofollow" target="_blank">txt.me</a></noscript>--}}
    <!-- End of txt.me widget code -->
</head>
<body class="en-body">
<div class="loader">
    <img src="{{asset('theme/landing/images/loader.svg')}}">
</div>
@yield('header')
{{--@include('templates.landing_page.layout.desktop.top_menu')--}}
<main>
    @yield('content')
</main>
<!-- Footer Starts -->
<footer>
    <div class="container-fluid">
        <div class="footer-logos">
        </div>

        <div class="row footer-row">
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-contact">
                    <h3 class="footer-title">Rootix</h3>
                    <h5>info@rootix.io</h5>
                    {{--                    <h5>648-145-8360</h5>--}}
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-sign-in">
                    <h3 class="footer-title">Join us</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="{{route('register.form')}}"> Register </a>
                        </li>
                        <li>
                            <a href="{{route('login.form')}}"> Login </a>
                        </li>
                        <li>
                            <a href="{{route('landing.faq')}}"> FAQ</a>
                        </li>

<!--                        <li>
                            <a href="{{route('landing.contact_us')}}"> Contact us </a>
                        </li>-->
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-about-us">
                    <h3 class="footer-title">About us</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> About Rootix</a>
                        </li>
                        <li>
                            <a href="#"> Our teams </a>
                        </li>
                        <li>
                            <a href="#"> Job opportunities</a>
                        </li>
                        <li>
                            <a href="#"> User comments </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-quick-access">
                    <h3 class="footer-title">Quick access</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> Rootix method  </a>
                        </li>
                        <li>
                            <a href="#"> Rootix features</a>
                        </li>
                        <li>
                            <a href=""> Blog </a>
                        </li>
                        <li>
                            <a href="{{route('landing.prices')}}"> Prices </a>
                        </li>
                        <li>
                            <a href="#"> Report a bug</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr/>

        <div class="footer-bottom">
            <div class="social-networks">
            </div>

            <h5>© 2018 Rootix All Right Reserved</h5>
        </div>
    </div>
    @include("templates.landing_page.layout.footer")
</footer>
<!-- Footer Ends -->
<script>
    $(document).ready(function () {
        $('.loader').hide();
        $("body").css({"overflow-y": "visible"});
    });
</script>
@yield('script')
</body>
</html>
