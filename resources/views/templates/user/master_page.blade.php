<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>@yield('title_browser')</title>
    @include('templates.user.layout.header')
    @yield('style')

<!-- Start of txt.me widget code -->
    <script src="https://v3.txt.me/livechat/js/wrapper/e49e9c1f-f673-479a-9d2f-98661ca37940" async></script>
    <noscript><a href="https://txt.me/reviews/e49e9c1f-f673-479a-9d2f-98661ca37940" rel="nofollow">Rate New Widget customer support</a>, powered by <a href="https://txt.me" rel="noopener nofollow" target="_blank">txt.me</a></noscript>
    <!-- End of txt.me widget code -->
</head>
<body>
@include('sweetalert::alert')
{{--<section class="section">--}}
{{--    <div class="d-flex">--}}
<!-- Side Bar -->
@include('templates.user.layout.sidebar')

<!-- Content -->
<div class="content @yield('content_class')">
    @yield('extra_div')
        <!-- header -->
    @include('templates.user.layout.top_menu')

    <!-- Main Content -->
        <main style="flex: 1">
            <div class="text-center usdt-alert-1 mt-3">
                <div class="circles-3">
                    <div class="circle-3 delay1"></div>
                    <div class="circle-3 delay2"></div>
                    <div class="circle-3 delay3"></div>
                    <h5 class="text-danger font-weight-bold">Top up your USDT wallet</h5>
                </div>
            </div>
            @include('general.flash_message')
            @yield('content')
        </main>

        <h5 class="text-center footer-title"> 2020. All Rights Reserved</h5>

</div>
{{--    </div>--}}
{{--</section>--}}
@include('templates.user.layout.footer')
@yield('script')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>
