{{--<div class="sidebar-container">--}}
{{--    <div class="sidebar">--}}
{{--        <div class="sidebar__logo">--}}
{{--            <a href="#">--}}
{{--                <img src="{{asset('theme/user/images/logo.png')}}" alt="لوگو"/>--}}
{{--            </a>--}}
{{--        </div>--}}

{{--        <ul class="sidebar-menu">--}}
{{--            <li class="sidebar-menu__item {{request()->url() == route('user.dashboard')?'active':''}}">--}}
{{--                <a href="{{route('user.dashboard')}}" class="sidebar-menu__link">--}}
{{--                    <div class="sidebar-menu__svg">--}}
{{--                        <div data-toggle="tooltip" data-placement="left" title="داشبورد">--}}
{{--                            <svg--}}
{{--                                xmlns="http://www.w3.org/2000/svg"--}}
{{--                                width="20"--}}
{{--                                height="23"--}}
{{--                                viewBox="0 0 24.597 27.107">--}}
{{--                                <g transform="translate(1 1)">--}}
{{--                                    <path--}}
{{--                                        d="M4.5,11.788,15.8,3l11.3,8.788V25.6a2.511,2.511,0,0,1-2.511,2.511H7.011A2.511,2.511,0,0,1,4.5,25.6Z"--}}
{{--                                        transform="translate(-4.5 -3)"--}}
{{--                                        fill="none"--}}
{{--                                        stroke="#000"--}}
{{--                                        stroke-linecap="round"--}}
{{--                                        stroke-linejoin="round"--}}
{{--                                        stroke-width="2"--}}
{{--                                    />--}}
{{--                                    <path--}}
{{--                                        d="M13.5,30.554V18h7.532V30.554"--}}
{{--                                        transform="translate(-5.968 -5.446)"--}}
{{--                                        fill="none"--}}
{{--                                        stroke="#000"--}}
{{--                                        stroke-linecap="round"--}}
{{--                                        stroke-linejoin="round"--}}
{{--                                        stroke-width="2"--}}
{{--                                    />--}}
{{--                                </g>--}}
{{--                            </svg>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    داشبورد--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item sidebar__submenu-item"--}}
{{--                data-is-open="false">--}}
{{--                <div href="#" class="sidebar-menu__link d-flex justify-between align-items-center pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="کاربران">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="25.5"--}}
{{--                                     viewBox="0 0 25.5 25.5">--}}
{{--                                    <path--}}
{{--                                        d="M18,18a6,6,0,1,0-6-6A6,6,0,0,0,18,18Zm0,3c-4.005,0-12,2.01-12,6v3H30V27C30,23.01,22.005,21,18,21Z"--}}
{{--                                        transform="translate(-5.25 -5.25)" fill="none" stroke="#000"--}}
{{--                                        stroke-width="1.5"/>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>کاربران</span>--}}
{{--                    </div>--}}

{{--                    <span class="sidebar-item__arrow"></span>--}}
{{--                </div>--}}

{{--                <ul class="submenu">--}}
{{--                    <li {{request()->url() == route('user.index')?'active':''}}>--}}
{{--                        <a href="{{route('user.index')}}">لیست کاربران </a>--}}
{{--                    </li>--}}
{{--                    <li {{request()->url() == route('user.authenticate.index')?'active':''}}>--}}
{{--                        <a href="{{ route('user.authenticate.index') }}">احراز هویت</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item sidebar__submenu-item"--}}
{{--                data-is-open="false">--}}
{{--                <div href="#"--}}
{{--                     class="sidebar-menu__link d-flex justify-between align-items-center pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="برداشت ها">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="32" viewBox="0 0 32 32">--}}
{{--                                    <g transform="translate(-2 -2)">--}}
{{--                                        <path d="M33,18A15,15,0,1,1,18,3,15,15,0,0,1,33,18Z" fill="none" stroke="#000"--}}
{{--                                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>--}}
{{--                                        <path d="M24,18l-6-6-6,6" fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                              stroke-linejoin="round" stroke-width="2"/>--}}
{{--                                        <path d="M18,24V12" fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                              stroke-linejoin="round" stroke-width="2"/>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>برداشت ها</span>--}}
{{--                    </div>--}}

{{--                    <span class="sidebar-item__arrow"></span>--}}
{{--                </div>--}}

{{--                <ul class="submenu">--}}
{{--                    <li>--}}
{{--                        <a href="{{route('admin.withdraw-rial.index')}}">لیست برداشت ریالی </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{route('admin.request.withdraw')}}">لیست برداشت ارزی </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{route('admin.wd-currency-setting')}}">تنظیمات برداشت </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item {{request()->url() == route('admin.manage.fees.store')?'active':''}}">--}}
{{--                <a href="{{route('admin.manage.fees.store')}}" class="sidebar-menu__link">--}}
{{--                    <div class="sidebar-menu__svg">--}}
{{--                        <div data-toggle="tooltip" data-placement="left" title="مدیریت کارمزدها">--}}
{{--                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="32" viewBox="0 0 23 32">--}}
{{--                                <path--}}
{{--                                    d="M24.987,7.435a.455.455,0,0,0-.012-.061.492.492,0,0,0-.12-.225h0l-7-7a.492.492,0,0,0-.229-.122.47.47,0,0,0-.058-.012A.484.484,0,0,0,17.5,0H4.5A2.5,2.5,0,0,0,2,2.5v27A2.5,2.5,0,0,0,4.5,32h18A2.5,2.5,0,0,0,25,29.5V7.5a.484.484,0,0,0-.013-.065ZM18,1.707,23.293,7H19.5A1.5,1.5,0,0,1,18,5.5ZM17.513,7H6.5A.5.5,0,0,1,6,6.5v-2A.5.5,0,0,1,6.5,4H17V5.5A2.476,2.476,0,0,0,17.513,7ZM24,29.5A1.5,1.5,0,0,1,22.5,31H4.5A1.5,1.5,0,0,1,3,29.5V2.5A1.5,1.5,0,0,1,4.5,1H17V3H6.5A1.5,1.5,0,0,0,5,4.5v2A1.5,1.5,0,0,0,6.5,8H24ZM12.5,13.8h2c.286,0,.5.159.5.3a.5.5,0,0,0,1,0,1.413,1.413,0,0,0-1.5-1.3H14v-.3a.5.5,0,0,0-1,0v.3h-.5A1.413,1.413,0,0,0,11,14.1v1.6A1.412,1.412,0,0,0,12.5,17h2c.286,0,.5.158.5.3v1.6c0,.142-.214.3-.5.3h-2c-.286,0-.5-.159-.5-.3a.5.5,0,0,0-1,0,1.413,1.413,0,0,0,1.5,1.3H13v.3a.5.5,0,0,0,1,0v-.3h.5A1.413,1.413,0,0,0,16,18.9V17.3A1.412,1.412,0,0,0,14.5,16h-2c-.286,0-.5-.158-.5-.3V14.1C12,13.958,12.214,13.8,12.5,13.8Zm9,14.2H5.5a.5.5,0,0,0,0,1h16a.5.5,0,0,0,0-1ZM5,25.5a.5.5,0,0,0,.5.5h16a.5.5,0,0,0,0-1H5.5A.5.5,0,0,0,5,25.5ZM13.5,9A7.5,7.5,0,1,0,21,16.5,7.5,7.5,0,0,0,13.5,9Zm0,14A6.5,6.5,0,1,1,20,16.5,6.5,6.5,0,0,1,13.5,23Z"--}}
{{--                                    transform="translate(-2)"/>--}}
{{--                            </svg>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    مدیریت کارمزدها--}}
{{--                </a>--}}
{{--            </li>--}}
        <!--
{{--                   <li class="sidebar-menu__item {{request()->url() == route('trade-management')?'active':''}}">--}}
        {{--                <a href="{{route('trade-management')}}" class="sidebar-menu__link">--}}
            <div class="sidebar-menu__svg">
                <div data-toggle="tooltip" data-placement="left" title="تنظیمات ترید">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32.69" height="28.821"
                         viewBox="0 0 32.69 28.821">
                        <g transform="translate(0 -3.786)">
                            <path
                                d="M32.277,25.831a6.438,6.438,0,1,0-6.436,6.441A6.45,6.45,0,0,0,32.277,25.831Zm-11.886,0a5.45,5.45,0,1,1,5.45,5.45A5.459,5.459,0,0,1,20.391,25.831Z"
                                transform="translate(-9.491 -7.634)"/>
                            <path
                                d="M31.522,30.506a1.53,1.53,0,0,0,.409-.531,1.587,1.587,0,0,0,.143-.69,1.549,1.549,0,0,0-.128-.669,1.463,1.463,0,0,0-.342-.46,1.807,1.807,0,0,0-.48-.312c-.179-.082-.363-.158-.552-.225s-.373-.133-.552-.194a2.452,2.452,0,0,1-.48-.225,1.148,1.148,0,0,1-.342-.322.846.846,0,0,1-.123-.47.946.946,0,0,1,.061-.342.644.644,0,0,1,.2-.266,1.043,1.043,0,0,1,.363-.174,2.08,2.08,0,0,1,.541-.061,1.96,1.96,0,0,1,.506.061,3.3,3.3,0,0,1,.4.133,2.616,2.616,0,0,1,.286.133.436.436,0,0,0,.163.061.077.077,0,0,0,.056-.02.09.09,0,0,0,.036-.056.448.448,0,0,0,.02-.1,1.488,1.488,0,0,0,.005-.158.985.985,0,0,0-.005-.128c-.005-.036-.01-.072-.015-.1a.337.337,0,0,0-.026-.077.308.308,0,0,0-.056-.072.63.63,0,0,0-.148-.092,1.363,1.363,0,0,0-.245-.1c-.092-.031-.184-.056-.286-.082a2.118,2.118,0,0,0-.3-.051l.1-.945a.259.259,0,0,0-.01-.066.087.087,0,0,0-.046-.051.242.242,0,0,0-.1-.031,1.512,1.512,0,0,0-.174-.01,1.392,1.392,0,0,0-.158.005.4.4,0,0,0-.092.026.1.1,0,0,0-.046.046.1.1,0,0,0-.015.056l-.092.955a2.609,2.609,0,0,0-.771.158,1.655,1.655,0,0,0-.547.337,1.339,1.339,0,0,0-.327.48,1.607,1.607,0,0,0-.107.593,1.549,1.549,0,0,0,.128.669,1.53,1.53,0,0,0,.337.46,1.928,1.928,0,0,0,.475.317,5.5,5.5,0,0,0,.541.225c.184.066.363.133.541.194a2.568,2.568,0,0,1,.475.23,1.27,1.27,0,0,1,.337.322.791.791,0,0,1,.128.465.867.867,0,0,1-.347.72,1.641,1.641,0,0,1-1.016.266,2.647,2.647,0,0,1-.649-.072,2.765,2.765,0,0,1-.455-.153,3.218,3.218,0,0,1-.3-.153.373.373,0,0,0-.174-.066.148.148,0,0,0-.072.015.126.126,0,0,0-.051.061,1.112,1.112,0,0,0-.031.112,1.512,1.512,0,0,0-.01.174.84.84,0,0,0,.02.22.255.255,0,0,0,.066.128.944.944,0,0,0,.153.107,2.577,2.577,0,0,0,.255.107,3.544,3.544,0,0,0,.342.1,3.246,3.246,0,0,0,.419.072l-.107,1.022a.145.145,0,0,0,0,.066.091.091,0,0,0,.051.046.853.853,0,0,0,.1.031.829.829,0,0,0,.174.01,1.207,1.207,0,0,0,.153-.01.288.288,0,0,0,.092-.026.121.121,0,0,0,.046-.041.1.1,0,0,0,.015-.056l.1-1.027a2.911,2.911,0,0,0,.853-.158A1.747,1.747,0,0,0,31.522,30.506Z"
                                transform="translate(-13.752 -9.801)"/>
                            <path
                                d="M32.547,26.089l-5.685-5.675a.5.5,0,0,0-.848.352v1.9H23.44a8.359,8.359,0,0,0-3.228-11.9l.005-4.086a.507.507,0,0,0-.143-.352.5.5,0,0,0-.352-.143H6.676v-1.9a.5.5,0,0,0-.306-.46.5.5,0,0,0-.541.107L.143,9.606A.5.5,0,0,0,0,9.958a.507.507,0,0,0,.143.352l5.685,5.675a.507.507,0,0,0,.352.143.478.478,0,0,0,.189-.036.5.5,0,0,0,.306-.46v-1.89H9.25a8.354,8.354,0,0,0,3.228,11.9l.005,4.081a.493.493,0,0,0,.5.5H26.014v1.9a.5.5,0,0,0,.306.46.5.5,0,0,0,.189.036.517.517,0,0,0,.352-.143l5.685-5.675a.5.5,0,0,0,.143-.352A.486.486,0,0,0,32.547,26.089ZM6.181,12.747a.493.493,0,0,0-.5.5v1.2L1.2,9.958l4.49-4.48v1.2a.493.493,0,0,0,.5.5h13.04l-.005,3.157a8.34,8.34,0,0,0-9.245,2.426H6.181ZM8.949,18.2a7.4,7.4,0,1,1,7.4,7.4A7.41,7.41,0,0,1,8.949,18.2ZM27.005,30.916v-1.2a.493.493,0,0,0-.5-.5H13.475l-.005-3.157a8.339,8.339,0,0,0,9.24-2.421h3.8a.493.493,0,0,0,.5-.5v-1.2l4.485,4.48Z"
                                transform="translate(0)"/>
                        </g>
                    </svg>
                </div>
            </div>
            تنظیمات ترید
        </a>
    </li>-->
        <!--
{{--                    <li class="sidebar-menu__item {{request()->url() == route('manage-withdraw-swap-page')?'active':''}}">--}}
        {{--                <a href="{{route('manage-withdraw-swap-page')}}" class="sidebar-menu__link">--}}
            <div class="sidebar-menu__svg">
                <div data-toggle="tooltip" data-placement="left" title="تنظیمات سواپ">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="28.821"
                         viewBox="0 0 28.819 28.821">
                        <g transform="translate(-7 -6.996)">
                            <path
                                d="M16.8,26.6a9.8,9.8,0,1,1,9.8-9.8.576.576,0,0,1-.576.576,9.06,9.06,0,0,0-2.179.271.574.574,0,0,1-.709-.622V16.8a6.34,6.34,0,1,0-6.334,6.34h.219a.576.576,0,0,1,.628.709,9.061,9.061,0,0,0-.271,2.173.576.576,0,0,1-.576.576Zm0-18.444a8.65,8.65,0,1,0-.576,17.291,9.729,9.729,0,0,1,.138-1.153,7.493,7.493,0,1,1,7.931-7.914,9.585,9.585,0,0,1,1.153-.156A8.646,8.646,0,0,0,16.8,8.153Z"
                                transform="translate(0 -0.002)"/>
                            <path
                                d="M18.493,25.986a7.493,7.493,0,1,1,7.493-7.493,2.358,2.358,0,0,1,0,.352.576.576,0,0,1-.427.5,8.646,8.646,0,0,0-6.219,6.2.576.576,0,0,1-.49.427A2.473,2.473,0,0,1,18.493,25.986Zm0-13.833a6.341,6.341,0,0,0-.15,12.68,9.827,9.827,0,0,1,6.49-6.49A6.34,6.34,0,0,0,18.493,12.153Zm6.916,6.628Z"
                                transform="translate(-1.695 -1.696)"/>
                            <path
                                d="M32.8,42.6A9.8,9.8,0,0,1,23,32.8a10.161,10.161,0,0,1,.305-2.455,9.844,9.844,0,0,1,7.032-7.037A10.242,10.242,0,0,1,32.8,23a9.8,9.8,0,1,1,0,19.6Zm0-18.444a9.06,9.06,0,0,0-2.179.271,8.646,8.646,0,0,0-6.2,6.2,8.974,8.974,0,0,0-.271,2.173A8.646,8.646,0,1,0,32.8,24.153Z"
                                transform="translate(-6.778 -6.78)"/>
                            <path
                                d="M34.493,34.784a7.493,7.493,0,1,1,7.493-7.493,7.493,7.493,0,0,1-7.493,7.493Zm0-13.833a6.34,6.34,0,1,0,6.34,6.34A6.34,6.34,0,0,0,34.493,20.951Zm7.493-2.305a.576.576,0,0,1-.576-.576V16.34a5.222,5.222,0,0,0-5.187-5.187H34.493a.576.576,0,1,1,0-1.153h1.729a6.34,6.34,0,0,1,6.34,6.34v1.729A.576.576,0,0,1,41.986,18.646Z"
                                transform="translate(-8.473 -1.273)"/>
                            <path
                                d="M29.02,11.609a.576.576,0,0,1-.409-.167L26.882,9.713a.576.576,0,0,1,0-.818l1.729-1.729a.579.579,0,0,1,.818.818L28.1,9.3l1.326,1.32a.576.576,0,0,1-.409.986ZM18.069,34.088H16.34A6.34,6.34,0,0,1,10,27.748V26.019a.576.576,0,1,1,1.153,0v1.729a5.222,5.222,0,0,0,5.187,5.187h1.729a.576.576,0,1,1,0,1.153Z"
                                transform="translate(-1.271)"/>
                            <path
                                d="M20.574,47.68a.576.576,0,0,1-.409-.986l1.326-1.32-1.326-1.32a.579.579,0,1,1,.818-.818l1.729,1.729a.576.576,0,0,1,0,.818l-1.729,1.729A.576.576,0,0,1,20.574,47.68Zm11.383-6.916H29.8a.576.576,0,1,1,0-1.153h2.161a.576.576,0,0,0,0-1.153h-.865a1.729,1.729,0,0,1,0-3.458h2.161a.576.576,0,1,1,0,1.153H31.093a.576.576,0,0,0,0,1.153h.865a1.729,1.729,0,0,1,0,3.458Z"
                                transform="translate(-5.505 -11.863)"/>
                            <path
                                d="M31.527,28.663a.576.576,0,0,1-.576-.576v-.865a.576.576,0,0,1,1.153,0v.865A.576.576,0,0,1,31.527,28.663Zm0,6.052a.576.576,0,0,1-.576-.576v-.865a.576.576,0,0,1,1.153,0v.865A.576.576,0,0,1,31.527,34.715ZM22.38,24.34c-1.337,0-2.38-1.395-2.38-3.17S21.043,18,22.38,18a2.035,2.035,0,0,1,1.5.7.576.576,0,0,1-.841.784.922.922,0,0,0-.657-.334c-.651,0-1.228.945-1.228,2.017s.576,2.017,1.228,2.017a.922.922,0,0,0,.657-.334.576.576,0,0,1,.842.784,2.035,2.035,0,0,1-1.5.7Z"
                                transform="translate(-5.507 -4.662)"/>
                            <path
                                d="M21.882,22.153H19.576a.576.576,0,0,1,0-1.153h2.305a.576.576,0,0,1,0,1.153Zm0,1.729H19.576a.576.576,0,0,1,0-1.153h2.305a.576.576,0,0,1,0,1.153Z"
                                transform="translate(-5.084 -5.933)"/>
                        </g>
                    </svg>
                </div>
            </div>
            تنظیمات سواپ
        </a>
    </li>-->

{{--            <li class="sidebar-menu__item sidebar__submenu-item"--}}
{{--                data-is-open="false">--}}
{{--                <div href="#"--}}
{{--                     class="sidebar-menu__link d-flex justify-between align-items-center pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="گزارشات">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="24.716" height="28.821"--}}
{{--                                     viewBox="0 0 24.716 28.821">--}}
{{--                                    <g transform="translate(-38 -0.1)">--}}
{{--                                        <path d="M345.614,70.623h2.928L345,67.2v2.888A.6.6,0,0,0,345.614,70.623Z"--}}
{{--                                              transform="translate(-289.715 -63.322)" fill="none"/>--}}
{{--                                        <path--}}
{{--                                            d="M93.646,47.3a2.83,2.83,0,0,1-2.866-2.793V40.134L78.586,40.1a.592.592,0,0,0-.586.563V63.848a.629.629,0,0,0,.614.574H97.7a.543.543,0,0,0,.512-.574V47.3Zm-2.021,4.448a1.126,1.126,0,1,1,0,2.252H81.772a1.126,1.126,0,0,1,0-2.252Zm-9.909-4.5h6.531a1.126,1.126,0,1,1,0,2.252H81.716a1.126,1.126,0,1,1,0-2.252ZM94.44,58.505H81.772a1.126,1.126,0,0,1,0-2.252H94.44a1.126,1.126,0,1,1,0,2.252Z"--}}
{{--                                            transform="translate(-37.748 -37.748)" fill="none"/>--}}
{{--                                        <path--}}
{{--                                            d="M62.165,7.4,55.313.77A2.263,2.263,0,0,0,53.742.134L40.838.1A2.828,2.828,0,0,0,38,2.915V26.1a2.883,2.883,0,0,0,2.866,2.821H59.952a2.8,2.8,0,0,0,2.764-2.826V8.748A1.852,1.852,0,0,0,62.165,7.4Zm-6.88-3.524L58.826,7.3H55.9a.6.6,0,0,1-.614-.54Zm4.667,22.791H40.866a.624.624,0,0,1-.614-.574V2.915a.592.592,0,0,1,.586-.563l12.195.034V6.761A2.834,2.834,0,0,0,55.9,9.553h4.566V26.095A.543.543,0,0,1,59.952,26.669Z"/>--}}
{{--                                        <path--}}
{{--                                            d="M125,248.126a1.129,1.129,0,0,0,1.126,1.126h9.853a1.126,1.126,0,0,0,0-2.252h-9.853A1.129,1.129,0,0,0,125,248.126Z"--}}
{{--                                            transform="translate(-82.102 -232.999)"/>--}}
{{--                                        <path--}}
{{--                                            d="M125.126,169.252h6.531a1.126,1.126,0,1,0,0-2.252h-6.531a1.126,1.126,0,1,0,0,2.252Z"--}}
{{--                                            transform="translate(-81.158 -157.503)"/>--}}
{{--                                        <path--}}
{{--                                            d="M138.794,327H126.126a1.126,1.126,0,1,0,0,2.252h12.668a1.126,1.126,0,1,0,0-2.252Z"--}}
{{--                                            transform="translate(-82.102 -308.495)"/>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>گزارشات</span>--}}
{{--                    </div>--}}

{{--                    <span class="sidebar-item__arrow"></span>--}}
{{--                </div>--}}

{{--                <ul class="submenu">--}}
{{--                    <li>--}}
{{--                        <a href="{{route('admin.report.index')}}">لیست تراکنش ها</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item sidebar__submenu-item"--}}
{{--                data-is-open="false">--}}
{{--                <div href="#"--}}
{{--                     class="sidebar-menu__link d-flex justify-between align-items-center pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="بانک ها">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="29.821"--}}
{{--                                     viewBox="0 0 29.82 29.821">--}}
{{--                                    <g transform="translate(-1.5 -1.499)">--}}
{{--                                        <g transform="translate(15.004 5.828)">--}}
{{--                                            <path d="M26.47,10.628a1.406,1.406,0,1,0-1.406,1.406"--}}
{{--                                                  transform="translate(-23.658 -8.716)" fill="none" stroke="#000"--}}
{{--                                                  stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"--}}
{{--                                                  stroke-width="1"/>--}}
{{--                                            <path d="M23.658,15.308A1.406,1.406,0,1,0,25.064,13.9"--}}
{{--                                                  transform="translate(-23.658 -10.584)" fill="none" stroke="#000"--}}
{{--                                                  stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"--}}
{{--                                                  stroke-width="1"/>--}}
{{--                                            <path d="M26,18.577v.511" transform="translate(-24.594 -12.452)" fill="none"--}}
{{--                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"--}}
{{--                                                  stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M26,8.376v.512" transform="translate(-24.594 -8.376)" fill="none"--}}
{{--                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"--}}
{{--                                                  stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                        </g>--}}
{{--                                        <g transform="translate(2 1.999)">--}}
{{--                                            <path d="M30.82,14.967,16.41,2,2,14.967Z" transform="translate(-2 -1.999)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M2,50H30.82" transform="translate(-2 -21.179)" fill="none"--}}
{{--                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"--}}
{{--                                                  stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M43.867,39.452V23.6" transform="translate(-18.73 -10.631)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M8.127,39.452V23.6" transform="translate(-4.448 -10.631)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M32.047,23.6V39.45" transform="translate(-14.006 -10.63)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M19.953,39.451V23.6" transform="translate(-9.174 -10.63)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                        </g>--}}
{{--                                        <g transform="translate(15.004 5.828)">--}}
{{--                                            <path d="M26.47,10.628a1.406,1.406,0,1,0-1.406,1.406"--}}
{{--                                                  transform="translate(-23.658 -8.716)" fill="none" stroke="#000"--}}
{{--                                                  stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"--}}
{{--                                                  stroke-width="1"/>--}}
{{--                                            <path d="M23.658,15.308A1.406,1.406,0,1,0,25.064,13.9"--}}
{{--                                                  transform="translate(-23.658 -10.584)" fill="none" stroke="#000"--}}
{{--                                                  stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"--}}
{{--                                                  stroke-width="1"/>--}}
{{--                                            <path d="M26,18.577v.511" transform="translate(-24.594 -12.452)" fill="none"--}}
{{--                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"--}}
{{--                                                  stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M26,8.376v.512" transform="translate(-24.594 -8.376)" fill="none"--}}
{{--                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"--}}
{{--                                                  stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                        </g>--}}
{{--                                        <g transform="translate(2 1.999)">--}}
{{--                                            <path d="M30.82,14.967,16.41,2,2,14.967Z" transform="translate(-2 -1.999)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M2,50H30.82" transform="translate(-2 -21.179)" fill="none"--}}
{{--                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"--}}
{{--                                                  stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M43.867,39.452V23.6" transform="translate(-18.73 -10.631)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M8.127,39.452V23.6" transform="translate(-4.448 -10.631)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M32.047,23.6V39.45" transform="translate(-14.006 -10.63)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                            <path d="M19.953,39.451V23.6" transform="translate(-9.174 -10.63)"--}}
{{--                                                  fill="none" stroke="#000" stroke-linecap="round"--}}
{{--                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>--}}
{{--                                        </g>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>بانک ها</span>--}}
{{--                    </div>--}}

{{--                    <span class="sidebar-item__arrow"></span>--}}
{{--                </div>--}}

{{--                <ul class="submenu">--}}
{{--                    <li>--}}
{{--                        <a href="{{route('admin.bank.index')}}">لیست بانک ها </a>--}}
{{--                    </li>--}}
{{--                    @can('bank_btn')--}}
{{--                        <li>--}}
{{--                            <a href="{{route('banks.index')}}">حساب بانکی </a>--}}
{{--                        </li>--}}
{{--                    @endcan--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item sidebar__submenu-item"--}}
{{--                data-is-open="false">--}}
{{--                <div href="#"--}}
{{--                     class="sidebar-menu__link d-flex justify-between align-items-center pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="پیغام ها">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="22.476" height="28.132"--}}
{{--                                     viewBox="0 0 22.476 28.132">--}}
{{--                                    <g transform="translate(-6.775 -3.93)">--}}
{{--                                        <path--}}
{{--                                            d="M20.37,28.336a.911.911,0,0,0-.893.717,1.762,1.762,0,0,1-.352.766,1.329,1.329,0,0,1-1.132.415,1.351,1.351,0,0,1-1.132-.415,1.762,1.762,0,0,1-.352-.766.911.911,0,0,0-.893-.717h0a.917.917,0,0,0-.893,1.118,3.142,3.142,0,0,0,3.27,2.609,3.136,3.136,0,0,0,3.27-2.609.92.92,0,0,0-.893-1.118Z"/>--}}
{{--                                        <path--}}
{{--                                            d="M28.969,24.764c-1.083-1.427-3.213-2.264-3.213-8.655,0-6.56-2.9-9.2-5.6-9.83-.253-.063-.436-.148-.436-.415v-.2a1.725,1.725,0,0,0-1.687-1.73h-.042a1.725,1.725,0,0,0-1.687,1.73v.2c0,.26-.183.352-.436.415-2.707.64-5.6,3.27-5.6,9.83,0,6.391-2.13,7.221-3.213,8.655A1.4,1.4,0,0,0,8.177,27H27.872A1.4,1.4,0,0,0,28.969,24.764Zm-2.742.408H9.83a.308.308,0,0,1-.232-.513,8.518,8.518,0,0,0,1.477-2.348,15.934,15.934,0,0,0,1.005-6.2,10.783,10.783,0,0,1,1.47-6.1A4.512,4.512,0,0,1,16.27,8.065a2.464,2.464,0,0,0,1.308-.738.556.556,0,0,1,.837-.014,2.547,2.547,0,0,0,1.322.752,4.512,4.512,0,0,1,2.721,1.941,10.783,10.783,0,0,1,1.47,6.1,15.934,15.934,0,0,0,1.005,6.2,8.615,8.615,0,0,0,1.512,2.384A.291.291,0,0,1,26.227,25.172Z"/>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>پیغام ها</span>--}}
{{--                    </div>--}}

{{--                    <span class="sidebar-item__arrow"></span>--}}
{{--                </div>--}}

{{--                <ul class="submenu">--}}
{{--                    <li>--}}
{{--                        <a href="">لیست پیغام ها </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="">ایجاد پیغام جدید</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item sidebar__submenu-item"--}}
{{--                data-is-open="false">--}}
{{--                <div href="#"--}}
{{--                     class="sidebar-menu__link d-flex justify-between align-items-center pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="تیکت ها">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="28.985"--}}
{{--                                     viewBox="0 0 32.206 28.985">--}}
{{--                                    <path--}}
{{--                                        d="M22.862,0a9.407,9.407,0,0,1,9.344,9.413h0v2.353l-.01.156a1.142,1.142,0,0,1-1.129.992h0l-.014-.029-.179-.014a1.137,1.137,0,0,1-.626-.322,1.152,1.152,0,0,1-.334-.812h0V9.413a7.148,7.148,0,0,0-7.051-7.1H9.344a7.148,7.148,0,0,0-7.051,7.1h0V19.572a7.148,7.148,0,0,0,7.051,7.1H22.862a7.148,7.148,0,0,0,7.051-7.1,1.153,1.153,0,0,1,2.293,0,9.407,9.407,0,0,1-9.344,9.413H9.344A9.393,9.393,0,0,1,0,19.572H0V9.413A9.379,9.379,0,0,1,9.344,0H22.862ZM6.915,8.674a1.12,1.12,0,0,1,.833.251h0l6.695,5.338a2.294,2.294,0,0,0,2.849,0h0l6.623-5.338h.014l.14-.1a1.145,1.145,0,0,1,1.285,1.89h0l-6.623,5.352a4.575,4.575,0,0,1-5.769,0h0L6.324,10.719,6.2,10.6a1.169,1.169,0,0,1-.048-1.5A1.123,1.123,0,0,1,6.915,8.674Z"/>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <span>تیکت ها</span>--}}
{{--                    </div>--}}

{{--                    <span class="sidebar-item__arrow"></span>--}}
{{--                </div>--}}

{{--                <ul class="submenu">--}}
{{--                    <li>--}}
{{--                        <a href="{{route('ticket.admin.index')}}">تیکت های جدید</a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{route('ticket.admin.index',['type'=>'my_tickets'])}}">تیکت های من</a>--}}
{{--                    </li>--}}
{{--                    @can('ticket_btn')--}}
{{--                        <li>--}}
{{--                            <a href="{{route('tickets.index')}}">تیکت پشتیبانی</a>--}}
{{--                        </li>--}}
{{--                    @endcan--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class="sidebar-menu__item">--}}
{{--                <a href="{{route('admin.setting')}}"--}}
{{--                   class="sidebar-menu__link d-flex justify-between justify-between pl-2">--}}
{{--                    <div class="d-flex align-items-center">--}}
{{--                        <div class="sidebar-menu__svg">--}}
{{--                            <div data-toggle="tooltip" data-placement="left" title="تنظیمات سامانه">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">--}}
{{--                                    <path--}}
{{--                                        d="M18,15a3,3,0,1,0,3,3A3.009,3.009,0,0,0,18,15ZM28.5,4.5H7.5a3,3,0,0,0-3,3v21a3,3,0,0,0,3,3h21a3,3,0,0,0,3-3V7.5A3,3,0,0,0,28.5,4.5ZM25.875,18a7.591,7.591,0,0,1-.075,1.02l2.22,1.74a.53.53,0,0,1,.12.675l-2.1,3.63a.531.531,0,0,1-.645.225l-2.61-1.05a8.044,8.044,0,0,1-1.77,1.035l-.39,2.775a.544.544,0,0,1-.525.45H15.9a.546.546,0,0,1-.525-.435l-.39-2.775a7.713,7.713,0,0,1-1.77-1.035l-2.61,1.05a.531.531,0,0,1-.645-.225l-2.1-3.63a.53.53,0,0,1,.12-.675l2.22-1.74A8.006,8.006,0,0,1,10.125,18a7.591,7.591,0,0,1,.075-1.02L7.98,15.24a.53.53,0,0,1-.12-.675l2.1-3.63a.531.531,0,0,1,.645-.225l2.61,1.05a8.044,8.044,0,0,1,1.77-1.035l.39-2.775A.544.544,0,0,1,15.9,7.5h4.2a.546.546,0,0,1,.525.435l.39,2.775a7.713,7.713,0,0,1,1.77,1.035l2.61-1.05a.531.531,0,0,1,.645.225l2.1,3.63a.53.53,0,0,1-.12.675l-2.22,1.74A8.006,8.006,0,0,1,25.875,18Z"--}}
{{--                                        transform="translate(-4 -4)" fill="none" stroke="#000" stroke-width="1"/>--}}
{{--                                </svg>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        تنظیمات سامانه--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}

{{--    <div class="open__sidebar" isClosed="true">--}}
{{--        <svg--}}
{{--            xmlns="http://www.w3.org/2000/svg"--}}
{{--            xmlns:xlink="http://www.w3.org/1999/xlink"--}}
{{--            width="38"--}}
{{--            height="38"--}}
{{--            viewBox="0 0 48 48"--}}
{{--        >--}}
{{--            <defs>--}}
{{--                <filter--}}
{{--                    id="a"--}}
{{--                    x="0"--}}
{{--                    y="0"--}}
{{--                    width="48"--}}
{{--                    height="48"--}}
{{--                    filterUnits="userSpaceOnUse"--}}
{{--                >--}}
{{--                    <feOffset input="SourceAlpha"/>--}}
{{--                    <feGaussianBlur stdDeviation="3" result="b"/>--}}
{{--                    <feFlood flood-opacity="0.161"/>--}}
{{--                    <feComposite operator="in" in2="b"/>--}}
{{--                    <feComposite in="SourceGraphic"/>--}}
{{--                </filter>--}}
{{--            </defs>--}}
{{--            <g transform="translate(-1796 -80)">--}}
{{--                <g transform="matrix(1, 0, 0, 1, 1796, 80)" filter="url(#a)">--}}
{{--                    <circle--}}
{{--                        cx="15"--}}
{{--                        cy="15"--}}
{{--                        r="15"--}}
{{--                        transform="translate(9 9)"--}}
{{--                        fill="#fff"--}}
{{--                    />--}}
{{--                </g>--}}
{{--                <path--}}
{{--                    d="M19.824,12.885l-4.691,4.681-4.691-4.681L9,14.326l6.133,6.133,6.133-6.133Z"--}}
{{--                    transform="translate(1836.672 88.868) rotate(90)"--}}
{{--                    opacity="0.6"--}}
{{--                />--}}
{{--            </g>--}}
{{--        </svg>--}}
{{--    </div>--}}
{{--</div>--}}

{{--<div>--}}
{{--    <div class="container-menu">--}}
{{--        <ul class="ul-res-menu">--}}
{{--            <h2>روتیکس</h2>--}}
{{--            <li>--}}
{{--                <a href="{{route('user.dashboard')}}">--}}
{{--                    <svg--}}
{{--                        class="sidebar-menu__svg"--}}
{{--                        id="Icon"--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        width="25"--}}
{{--                        height="20"--}}
{{--                        viewBox="0 0 29.445 23.955"--}}
{{--                    >--}}
{{--                        <g id="Group_69" data-name="Group 69">--}}
{{--                            <g id="_32" data-name="32">--}}
{{--                                <g id="Group_68" data-name="Group 68">--}}
{{--                                    <path--}}
{{--                                        id="Path_50"--}}
{{--                                        data-name="Path 50"--}}
{{--                                        d="M19.241,11.585c-.7.237-6.854,2.373-8.207,3.947a4.14,4.14,0,0,0,.475,5.869,4.22,4.22,0,0,0,5.915-.469c1.351-1.576,2.5-7.946,2.621-8.668a.605.605,0,0,0-.207-.567A.612.612,0,0,0,19.241,11.585Zm-2.752,8.557a3.069,3.069,0,0,1-4.186.332,2.93,2.93,0,0,1-.336-4.152c.759-.883,4.1-2.3,6.677-3.235C18.13,15.7,17.239,19.268,16.488,20.143ZM14.722,2.089A14.785,14.785,0,0,0,0,16.9a15.422,15.422,0,0,0,2.895,8.882.613.613,0,0,0,.5.258H26.049a.611.611,0,0,0,.5-.258,15.525,15.525,0,0,0,2.9-8.952A14.752,14.752,0,0,0,14.722,2.089ZM25.728,24.825H3.716A14.192,14.192,0,0,1,1.227,16.9a13.5,13.5,0,1,1,26.99-.069A14.305,14.305,0,0,1,25.728,24.825ZM14.722,5.591A10.359,10.359,0,0,0,4.384,15.948a.614.614,0,0,0,1.227,0,9.112,9.112,0,1,1,18.223-.033.614.614,0,0,0,1.227,0A10.343,10.343,0,0,0,14.722,5.591Z"--}}
{{--                                        transform="translate(0 -2.089)"--}}
{{--                                        fill="#1b274c"--}}
{{--                                    />--}}
{{--                                </g>--}}
{{--                            </g>--}}
{{--                        </g>--}}
{{--                    </svg>--}}
{{--                    داشبورد--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{ route('wallet.index') }}">--}}
{{--                    <svg--}}
{{--                        class="sidebar-menu__svg"--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        width="28"--}}
{{--                        height="23"--}}
{{--                        viewBox="0 0 32.938 28.821"--}}
{{--                    >--}}
{{--                        <path--}}
{{--                            id="Icon_awesome-wallet"--}}
{{--                            data-name="Icon awesome-wallet"--}}
{{--                            d="M29.67,8.426H5.147a1.029,1.029,0,0,1,0-2.059h24.7A1.029,1.029,0,0,0,30.88,5.338,3.088,3.088,0,0,0,27.792,2.25H4.117A4.117,4.117,0,0,0,0,6.367V26.954a4.117,4.117,0,0,0,4.117,4.117H29.67a3.185,3.185,0,0,0,3.268-3.088V11.514A3.185,3.185,0,0,0,29.67,8.426ZM26.762,21.807a2.059,2.059,0,1,1,2.059-2.059A2.059,2.059,0,0,1,26.762,21.807Z"--}}
{{--                            transform="translate(0 -2.25)"--}}
{{--                            fill="#1b274c"--}}
{{--                        />--}}
{{--                    </svg>--}}

{{--                    کیف پول--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{ route('swap.index') }}">--}}
{{--                    <svg--}}
{{--                        class="sidebar-menu__svg"--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        width="33"--}}
{{--                        height="25"--}}
{{--                        viewBox="0 0 43.189 30.232"--}}
{{--                    >--}}
{{--                        <path--}}
{{--                            id="Icon_material-compare-arrows"--}}
{{--                            data-name="Icon material-compare-arrows"--}}
{{--                            d="M18.138,26.935H3v4.319H18.138v6.478l8.616-8.638-8.616-8.638v6.478Zm12.913-2.159V18.3H46.189V13.978H31.051V7.5l-8.616,8.638Z"--}}
{{--                            transform="translate(-3 -7.5)"--}}
{{--                            fill="#1b274c"--}}
{{--                        />--}}
{{--                    </svg>--}}

{{--                    تبادل--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{route('exchange.index',['des_unit'=>'btc','source_unit'=>'usdt'])}}">--}}
{{--                    <svg--}}
{{--                        class="sidebar-menu__svg"--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        width="30"--}}
{{--                        height="30"--}}
{{--                        viewBox="0 0 36 36"--}}
{{--                    >--}}
{{--                        <path--}}
{{--                            id="Icon_awesome-coins"--}}
{{--                            data-name="Icon awesome-coins"--}}
{{--                            d="M0,28.5v3C0,33.982,6.047,36,13.5,36S27,33.982,27,31.5v-3c-2.9,2.046-8.212,3-13.5,3S2.9,30.544,0,28.5ZM22.5,9C29.953,9,36,6.982,36,4.5S29.953,0,22.5,0,9,2.018,9,4.5,15.047,9,22.5,9ZM0,21.122V24.75c0,2.482,6.047,4.5,13.5,4.5S27,27.232,27,24.75V21.122c-2.9,2.391-8.22,3.628-13.5,3.628S2.9,23.513,0,21.122Zm29.25.773C33.279,21.115,36,19.666,36,18V15a17.267,17.267,0,0,1-6.75,2.426ZM13.5,11.25C6.047,11.25,0,13.767,0,16.875S6.047,22.5,13.5,22.5,27,19.983,27,16.875,20.953,11.25,13.5,11.25Zm15.42,3.959c4.219-.759,7.08-2.25,7.08-3.959v-3c-2.5,1.765-6.785,2.714-11.3,2.939A7.874,7.874,0,0,1,28.92,15.209Z"--}}
{{--                            fill="#1b274c"--}}
{{--                        />--}}
{{--                    </svg>--}}

{{--                    خرید و فروش--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{route('setting.form')}}">--}}
{{--                    <svg--}}
{{--                        class="sidebar-menu__svg"--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        width="27"--}}
{{--                        height="27"--}}
{{--                        viewBox="0 0 33.691 33.691"--}}
{{--                    >--}}
{{--                        <g--}}
{{--                            id="Icon_feather-settings"--}}
{{--                            data-name="Icon feather-settings"--}}
{{--                            transform="translate(1 1)"--}}
{{--                        >--}}
{{--                            <path--}}
{{--                                id="Path_52"--}}
{{--                                data-name="Path 52"--}}
{{--                                d="M22.143,17.821A4.321,4.321,0,1,1,17.821,13.5,4.321,4.321,0,0,1,22.143,17.821Z"--}}
{{--                                transform="translate(-1.976 -1.976)"--}}
{{--                                stroke="#1b274c"--}}
{{--                                fill="none"--}}
{{--                                stroke-linecap="round"--}}
{{--                                stroke-linejoin="round"--}}
{{--                                stroke-width="2"--}}
{{--                            />--}}
{{--                            <path--}}
{{--                                id="Path_53"--}}
{{--                                data-name="Path 53"--}}
{{--                                d="M28,21.667a2.377,2.377,0,0,0,.475,2.622l.086.086a2.883,2.883,0,1,1-4.077,4.077l-.086-.086a2.4,2.4,0,0,0-4.062,1.7v.245a2.881,2.881,0,1,1-5.762,0v-.13A2.377,2.377,0,0,0,13.024,28a2.377,2.377,0,0,0-2.622.475l-.086.086A2.883,2.883,0,1,1,6.239,24.49l.086-.086a2.4,2.4,0,0,0-1.7-4.062H4.381a2.881,2.881,0,1,1,0-5.762h.13a2.377,2.377,0,0,0,2.175-1.556A2.377,2.377,0,0,0,6.21,10.4l-.086-.086A2.883,2.883,0,1,1,10.2,6.239l.086.086a2.377,2.377,0,0,0,2.622.475h.115a2.377,2.377,0,0,0,1.44-2.175V4.381a2.881,2.881,0,1,1,5.762,0v.13a2.4,2.4,0,0,0,4.062,1.7l.086-.086A2.883,2.883,0,1,1,28.451,10.2l-.086.086a2.377,2.377,0,0,0-.475,2.622v.115a2.377,2.377,0,0,0,2.175,1.44h.245a2.881,2.881,0,1,1,0,5.762h-.13A2.377,2.377,0,0,0,28,21.667Z"--}}
{{--                                transform="translate(-1.5 -1.5)"--}}
{{--                                fill="none"--}}
{{--                                stroke="#1b274c"--}}
{{--                                stroke-linecap="round"--}}
{{--                                stroke-linejoin="round"--}}
{{--                                stroke-width="2"--}}
{{--                            />--}}
{{--                        </g>--}}
{{--                    </svg>--}}

{{--                    تنظیمات--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li>--}}
{{--                <a href="{{route('tickets.index')}}">--}}
{{--                    <svg--}}
{{--                        class="sidebar-menu__svg"--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        width="32"--}}
{{--                        height="25"--}}
{{--                        viewBox="0 0 40.215 31.943"--}}
{{--                    >--}}
{{--                        <g--}}
{{--                            id="Icon_feather-mail"--}}
{{--                            data-name="Icon feather-mail"--}}
{{--                            transform="translate(1.393 1)"--}}
{{--                        >--}}
{{--                            <path--}}
{{--                                id="Path_54"--}}
{{--                                data-name="Path 54"--}}
{{--                                d="M6.743,6H36.686a3.754,3.754,0,0,1,3.743,3.743V32.2a3.754,3.754,0,0,1-3.743,3.743H6.743A3.754,3.754,0,0,1,3,32.2V9.743A3.754,3.754,0,0,1,6.743,6Z"--}}
{{--                                transform="translate(-3 -6)"--}}
{{--                                fill="none"--}}
{{--                                stroke="#1b274c"--}}
{{--                                stroke-linecap="round"--}}
{{--                                stroke-linejoin="round"--}}
{{--                                stroke-width="2"--}}
{{--                            />--}}
{{--                            <path--}}
{{--                                id="Path_55"--}}
{{--                                data-name="Path 55"--}}
{{--                                d="M40.429,9,21.715,22.1,3,9"--}}
{{--                                transform="translate(-3 -5.257)"--}}
{{--                                fill="none"--}}
{{--                                stroke="#1b274c"--}}

{{--                                stroke-linecap="round"--}}
{{--                                stroke-linejoin="round"--}}
{{--                                stroke-width="2"--}}
{{--                            />--}}
{{--                        </g>--}}
{{--                    </svg>--}}

{{--                    تیکت پشتیبانی--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{route('user.profile.form')}}">--}}
{{--                    <svg xmlns="http://www.w3.org/2000/svg" width="36" height="31.5" viewBox="0 0 36 31.5">--}}
{{--                        <path d="M13.5,0V4.5h18V27h-18v4.5H36V0ZM9,9,0,15.75,9,22.5V18H27V13.5H9Z" fill="#1b274c"/>--}}
{{--                    </svg>--}}
{{--                    پروفایل--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="{{route('logout')}}">--}}
{{--                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">--}}
{{--                        <path--}}
{{--                            d="M18,3A15,15,0,1,0,33,18,15.005,15.005,0,0,0,18,3Zm0,4.5A4.5,4.5,0,1,1,13.5,12,4.494,4.494,0,0,1,18,7.5Zm0,21.3a10.8,10.8,0,0,1-9-4.83c.045-2.985,6-4.62,9-4.62s8.955,1.635,9,4.62a10.8,10.8,0,0,1-9,4.83Z"--}}
{{--                            transform="translate(-2 -2)" fill="none" stroke="#1b274c" stroke-width="2"/>--}}
{{--                    </svg>--}}
{{--                    خروج--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</div>--}}
