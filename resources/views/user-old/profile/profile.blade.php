@extends('templates.user.master_page')
@section('title_browser')
    تنظیمات
@endsection
@section('style')
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/css-rtl/pages/app-user.min.css')}}">

    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/menu/menu-types/horizontal-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/plugins/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/vendors/css/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/vendors/css/vendors-rtl.min.css')}}">
    <style>
        .avatar-img {
            cursor: pointer;
        }

        .hover-avatar {
            height: 25px;
            line-height: 25px;
            position: absolute;
            font-size: 11px;
            bottom: 0px;
            left: 0;
            right: 0;
            background: #00000070;
            color: #fff;
            text-align: center;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        .avatar-img label {
            cursor: pointer;
            display: inline;
            position: relative;
            overflow: hidden
        }
    </style>
@endsection
@section('content')

    <!-- Nav Filled Starts -->
    <section id="nav-filled">
        <div class="row">
            <div class="col-sm-12">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h4 class="card-title">پروفایل</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab-fill" data-toggle="tab" href="#home-fill"
                                       role="tab"
                                       aria-controls="home-fill" aria-selected="true">پروفایل</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab-fill" data-toggle="tab" href="#profile-fill"
                                       role="tab"
                                       aria-controls="profile-fill" aria-selected="false">اطلاعات</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="messages-tab-fill" data-toggle="tab" href="#messages-fill"
                                       role="tab"
                                       aria-controls="messages-fill" aria-selected="false">اطلاعات بانکی</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content pt-1">
                                <div class="tab-pane active" id="home-fill" role="tabpanel"
                                     aria-labelledby="home-tab-fill">
                                    <div class="tab-pane active" id="account" aria-labelledby="account-tab"
                                         role="tabpanel">

                                        <!-- users edit media object start -->
                                        <div class="col-12 mb-2 row align-items-center border p-1">
                                            <div class="avatar-img mx-auto mx-md-0">
                                                <form action="{{route('profile.update.picture',auth()->user()->id)}}"
                                                      method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <label class="mr-2 my-25 p-0 d-flex" href="#" for="upload_file">
                                                        <img class="users-avatar-shadow rounded" height="90" width="90"
                                                             src="{{ auth()->user()->avatar }}"
                                                             alt="users avatar">
                                                        <span class="hover-avatar">ویرایش آواتار</span>
                                                    </label>
                                                    <input type="file" id="upload_file" name="file" class="d-none"
                                                           onchange="form.submit()">
                                                </form>
                                            </div>
                                            <div class="col-md col-sm-12 text-center text-md-left mt-2 mt-md-0">
                                                <h4 class="mb-0">
                                                    {{ auth()->user()->full_name ?? 'بدون نام'}}
                                                </h4>
                                                <p style="margin: 5px 0px; font-size: 14px">
                                                    کد کاربری: {{auth()->user()->code}}
                                                </p>
                                                <p class="mb-0">
                                                    سطح شما : {{convertStepToPersian(auth()->user()->step_complate)}}
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{ route('step.verify-mobile') }}">ادامه احرازهویت</a>
                                                </p>
                                            </div>
                                        </div>

                                        <!--  فرم مشخصات کاربر -->
                                        <div class="col-12 row border p-1">
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <label>نام و نام خانوادگی</label>
                                                <input type="text" class="form-control"
                                                       placeholder="نام و نام خانوادگی"
                                                       value="{{ auth()->user()->name ?? 'بدون نام'}}"
                                                       required
                                                       readonly>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>کدملی</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="کدملی"
                                                               value="{{auth()->user()->national_code}}"
                                                               readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>پست الکترونیک</label>
                                                        <input type="email" class="form-control"
                                                               placeholder="ایمیل"
                                                               value="{{auth()->user()->email}}"
                                                               readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>کد معرف</label>
                                                        <input type="email" class="form-control"
                                                               placeholder="کد معرف"
                                                               value="{{auth()->user()->referral_id}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>لینک معرفی به دوستان</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="referral-link"
                                                                   placeholder="لینک معرف"
                                                                   value=" {{ route('register.form',['referral_code' => auth()->user()->code]) }}"
                                                                   readonly>
                                                            <div class="input-group-append">
                                                                <span class="btn btn-info copy-btn"
                                                                      data-input="referral-link"
                                                                      style="padding-right: 10px; padding-left: 10px;"><i
                                                                        class="fa fa-copy mx-0"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!--  فرم مشخصات کاربر -->

                                        <form action="{{route('profile.update.about',auth()->user()->id)}}" method="post">
                                            @csrf
                                            <div class="col-12 row border p-1 mt-2">
                                                <div class="col-md-8 col-sm-10 col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>درباره من</label>
                                                            <textarea name="about" type="text" class="form-control"
                                                                      rows="3"
                                                                      cols="100">
                                                                {{auth()->user()->about}}
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-4 col-12 my-auto">
                                                    <button type="button" class="btn btn-info ajaxStore">ویرایش</button>
                                                </div>
                                            </div>
                                        </form>

                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-fill" role="tabpanel"
                                     aria-labelledby="profile-tab-fill">
                                    <div class="tab-pane" id="information" aria-labelledby="information-tab"
                                         role="tabpanel">
                                        <!-- users edit Info form start -->
                                        <form novalidate>
                                            <div class="row mt-1">
                                                <div class="col-12 col-sm-6">
                                                    <h5 class="mb-1"><i class="feather icon-user mr-25"></i>اطلاعات شخصی
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>تاریخ تولد</label>
                                                                    <input type="text"
                                                                           value="{{auth()->user()->birth_day}}"
                                                                           class="form-control birthdate-picker"
                                                                           placeholder="تاریخ تولد"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>تلفن همراه</label>
                                                            <input type="text" class="form-control"
                                                                   value="{{auth()->user()->mobile}}"
                                                                   placeholder="تلفن همراه اینجا..."
                                                                   readonly>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>جنسیت</label>
                                                        <ul class="list-unstyled mb-0">
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                        <input type="radio" name="vueradio"
                                                                               @if(auth()->user()->gender_id == 1 ) checked
                                                                               @else disabled @endif
                                                                               value="false">
                                                                        <span class="vs-radio">
                                                                          <span class="vs-radio--border"></span>
                                                                          <span class="vs-radio--circle"></span>
                                                                        </span> مذکر
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                        <input type="radio" name="vueradio"
                                                                               @if(auth()->user()->gender_id == 2 ) checked
                                                                               @else disabled @endif
                                                                               value="false">
                                                                        <span class="vs-radio">
                                                                          <span class="vs-radio--border"></span>
                                                                          <span class="vs-radio--circle"></span>
                                                                        </span> مونث
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                        <input type="radio" name="vueradio"
                                                                               @if(empty(auth()->user()->gender_id)  ) checked
                                                                               @else disabled @endif
                                                                               value="false">
                                                                        <span class="vs-radio">
                                                                          <span class="vs-radio--border"></span>
                                                                          <span class="vs-radio--circle"></span>
                                                                        </span> نامشخص
                                                                    </div>
                                                                </fieldset>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <h5 class="mb-1 mt-2 mt-sm-0"><i
                                                            class="feather icon-map-pin mr-25"></i>نشانی</h5>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>تلفن ثابت</label>
                                                            <input type="text" class="form-control" required
                                                                   value="{{auth()->user()->phone}}"
                                                                   placeholder="آدرس خط 2"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>کد پستی</label>
                                                            <input type="text" class="form-control" required
                                                                   placeholder="کدپستی"
                                                                   value="{{auth()->user()->postal_code}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>شهر</label>
                                                            <input type="text" class="form-control" required
                                                                   value="{{isset(auth()->user()->city_name->name) ? auth()->user()->city_name->name : ''}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>کشور</label>
                                                            <input type="text" class="form-control"
                                                                   value="{{isset(auth()->user()->country->name) ? auth()->user()->country->name : ''}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit Info form ends -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages-fill" role="tabpanel"
                                     aria-labelledby="messages-tab-fill">
                                    <div class="tab-pane" id="social" aria-labelledby="social-tab" role="tabpanel">
                                        <!-- users edit socail form start -->
                                        <form novalidate>
                                            <div class="row">
                                                <div class="col-12 col-sm-12">
                                                    <div class="table table-hover table-striped">
                                                        <table style="width: 100%">
                                                            <thead>
                                                            <tr>
                                                                <td>بانک</td>
                                                                <td>شماره کارت</td>
                                                                <td>شماره شبا</td>
                                                                <td>شماره حساب</td>
                                                                <td>عملیات</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach(auth()->user()->bank as $bank)
                                                                <tr>
                                                                    <td>{{$bank->name}}</td>
                                                                    <td>{{$bank->card_number}}</td>
                                                                    <td>{{$bank->sheba_number}}</td>
                                                                    <td>{{$bank->account_number}}</td>
                                                                    <td><a href="#"><i class="feather icon-trash-2"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                        <div
                                                            class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                            <a href="{{ route('banks.create') }}"
                                                               class="btn btn-success mr-1 mb-1 waves-effect waves-light">
                                                                افزودن حساب
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit socail form ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('script')
    <script src="{{asset('theme/user/app-assets/js/scripts/pages/app-user.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/js/scripts/navs/navs.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/js/scripts/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/vendors.min.js')}}"></script>

    <script>
        function set_authenticator(secret) {
            $.ajax({
                url: "{{route('complete-registration')}}",
                type: 'POST',
                data: {secret: secret, "_token": csrf},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    if (data.status == 100) {
                        swal('موفق', data['msg'], 'success');
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    } else {
                        swal('ناموفق', data['msg'], 'error');
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    }
                }
            });
        }
    </script>
@endsection
