@extends('templates.user.master_page')
@section('title_browser')
    تغییر رمز عبور
@endsection
@section('content')
    <div class="row settings">
        <div class="col-md-12 col-lg-12">
            <div class="col-md-12">
                @include('general.flash_message')
            </div>
            <div class="tab-content col-lg-6 mx-auto" id="v-pills-tabContent">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">تغییر رمز عبور</h3>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('change_pass.store') }}">
                            @csrf
                            <div class="form-group">
                                <label>پسورد قبلی</label>
                                <input class="form-control" type="password" name="old_pass">
                            </div>
                            <div class="form-group">
                                <label>پسورد جدید</label>
                                <input class="form-control" type="password" name="password">
                            </div>
                            <div class="form-group">
                                <label>تکرار پسورد جدید</label>
                                <input class="form-control" type="password" name="password_confirmation">
                            </div>
                            <button type="button" class="btn btn-success mx-auto ajaxStore">ویرایش رمزعبور</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

