@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-ticket')}}
@endsection

@section('content')

    <div class="card" style="background-color: #f9f7f7;">
        <div class="card-body">
            <h5 class="card-title">{{__('attributes.ticket.title')}} : {{$ticket->title}}</h5>
            <div class=" settings-profile col-8 mx-auto" >
                <button type="button" id="change_lable" class="btn btn-success" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">
                    {{__('attributes.ticket.response')}}
                </button>
            </div>
            <div class="collapse multi-collapse settings-profile"  id="multiCollapseExample1">
                <form action="{{route('comment.store',$ticket->id)}}" method="post">
                    @csrf
                    <div class="form-row mt-4 col-8 mx-auto">
                        <div class="col-md-6 col-md-offset-6">
                            <label for="formFirst">{{__('attributes.ticket.title')}}</label>
                            <input id="formFirst" type="text" class="form-control" placeholder="{{__('attributes.ticket.title')}}" value="{{$ticket->title}}" readonly>
                        </div>

                        <div class="col-md-6 ">
                            <label for="selectLanguage">{{__('attributes.ticket.department')}}</label>
                            <select id="selectLanguage" class="custom-select" disabled>
                                <option @if($ticket->role_id == 1) selected @endif >{{__('attributes.ticket.accounting')}}</option>
                                <option @if($ticket->role_id == 2) selected @endif>{{__('attributes.ticket.technical')}}</option>
                                <option @if($ticket->role_id == 3) selected @endif>{{__('attributes.ticket.management')}}</option>
                                <option @if($ticket->role_id == 4) selected @endif>{{__('attributes.ticket.support')}}</option>
                            </select>
                        </div>

                        <div class="col-md-6 mt-2">
                            <label for="selectLanguage">{{__('attributes.ticket.urgency')}}</label>
                            <select id="selectLanguage" class="custom-select" disabled>
                                <option @if($ticket->priority == 1) selected @endif >{{__('attributes.ticket.low')}}</option>
                                <option @if($ticket->priority == 2) selected @endif>{{__('attributes.ticket.medium')}}</option>
                                <option @if($ticket->priority == 3) selected @endif>{{__('attributes.ticket.high')}}</option>
                            </select>
                        </div>


                    </div>
                        <div class="row ">
                            <div class="col-8 mx-auto">
                                <div >
                                        <div class="col-md-12 mb-3 mt-2">
                                            <label for="phoneNumber">{{__('attributes.ticket.text')}}</label>
                                            <textarea id="phoneNumber" type="text" name="description" class="form-control" placeholder="توضیحات خود را در اینجا بنویسید" rows="8"></textarea>
                                        </div>

                                        <div class="col-md-6">
                                            <input type="file" name="file" class="form-control" >
                                            <span style="color: red">{{__('attributes.ticket.limit')}}</span>
                                        </div>

                                        <div class="col-md-12">
                                            <input type="button" class="btn btn-success mt-2 ajaxStore" value="{{__('attributes.ticket.send')}}">
                                        </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
            @foreach($ticket->ticketDetail as $tickets)

            <div class="col-8 mx-auto">
                <div class="card mt-4 @if($tickets->type == 2) ml-4 @endif">
                    @if($tickets->type == 2)
                        <div class="card-header " style="background-color: rgb(10 11 210 / 31%) !important ;">
{{--                            {{$ticket->admin->name .' '.$ticket->admin->name}}--}}
                            <p class="mb-1"> بخش {{ $role->name}}</p>
                            <p class="mb-1 text-right"> {{jdate_from_gregorian($tickets->created_at,'H:i Y-m-d')}}</p>
                        </div>
                    @else
                    @foreach($tickets->user as $user)
                        <div class="card-header " style="background-color: rgba(0,0,0,.09) !important;">
                            <p class="mb-1"> {{ $user->email }}</p>
                            <p class="mb-1 text-right"> {{jdate_from_gregorian($tickets->created_at,'H:i Y-m-d')}}</p>
                        </div>
                    @endforeach
                    @endif
                    <div class="card-body">
                        <p class="card-text ">{{$tickets->description}}</p>
                        @if(isset($tickets->files[0]))
                            <hr/>
                            <span>
                                    <div class="glyph">
                                        <h6> {{__('attributes.ticket.file')}}</h6>
                                        <a class="btn btn-primary  default" title="{{__('attributes.ticket.show')}}" href="{{getImage($tickets->files[0]->path)}}">
                                            <i class="fa fa-file"></i>
                                        </a>
                                    </div>
                                </span>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach

        </div>

    </div>
@endsection
