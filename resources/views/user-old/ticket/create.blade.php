@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-ticket')}}
@endsection

@section('content')

    <div class="card" style="background-color: #f9f7f7;">
        <div class="card-body">
            <h5 class="card-title">{{__('attributes.ticket.new')}}</h5>

            <div class="settings-profile">
                <form action="{{route('ticket.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row mt-4 col-8 mx-auto">
                        <div class="col-md-6 col-md-offset-6">
                            <label for="formFirst">{{__('attributes.ticket.title')}}</label>
                            <input  name="title" type="text" class="form-control" placeholder="{{__('attributes.ticket.title')}}">

                        </div>

                        <div class="col-md-6 ">
                            <label for="selectLanguage">{{__('attributes.ticket.department')}}</label>
                            <select  name="role_id" class="custom-select">
                                @foreach($roles as $role)
                                    <option selected value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-6 mt-2">
                            <label >{{__('attributes.ticket.urgency')}}</label>
                            <select  name="priority" class="custom-select">
                                <option selected value="1">{{__('attributes.ticket.low')}}</option>
                                <option value="2">{{__('attributes.ticket.medium')}}</option>
                                <option value="3">{{__('attributes.ticket.high')}}</option>
                            </select>
                        </div>

                        <div class="col-md-12 mb-3 mt-2">
                            <label >{{__('attributes.ticket.text')}}</label>
                            <textarea  name="description" type="text" class="form-control" placeholder="{{__('attributes.ticket.description')}}" rows="8"></textarea>
                        </div>

                        <div class="col-md-6">
                            <input type="file" name="file" class="form-control" >
                            <span style="color: red">{{__('attributes.ticket.limit')}}</span>
                        </div>


                        <div class="col-md-12 mt-2">
                            <input type="button" value="{{__('attributes.ticket.send')}}" class="btn btn-success ajaxStore">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
