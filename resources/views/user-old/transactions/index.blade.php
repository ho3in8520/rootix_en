@extends('templates.user.master_page')
@section('title_browser')
    لیست تراکنش ها
@endsection
@section('content')

    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        لیست تراکنش ها
                    </div>
                    <hr class="mb-0">
                    <div class="card-body pt-0">
                        <form method="get" action="">
                            @csrf
                            <div class="row mt-4">
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="type">واریز/برداشت</label>
                                    <select id="type" name='type' class="form-select form-control">
                                        <option value="">همه</option>
                                        <option value="1" {{ request()->type=='1'?'selected':'' }}>برداشت</option>
                                        <option value="2" {{ request()->type=='2'?'selected':'' }}>واریز</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="unit">ارز</label>
                                    <select id="unit" name="unit" class="form-select form-control">
                                        <option value="">همه</option>
                                        <option value="usdt" {{ request()->unit=='usdt'?'selected':'' }}>usdt</option>
                                        <option value="rls" {{ request()->unit=='rls'?'selected':'' }} >rial</option>
                                        <option value="jst" {{ request()->unit=='jst'?'selected':'' }} >jst</option>
                                        <option value="win" {{ request()->unit=='win'?'selected':'' }} >win</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>مقدار <span class="text-danger">({{get_unit()}})</span></label>
                                    <div class="input-group">
                                        <input class="form-control" name="amount_from" placeholder="از"
                                               value="{{ request()->amount_from?request()->amount_from:'' }}">
                                        <input class="form-control" name="amount_to" placeholder="تا"
                                               value="{{ request()->amount_to?request()->amount_to:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>تاریخ</label>
                                    <div class="input-group">
                                        <input class="form-control datePicker" name="date_from" placeholder="از"
                                               value="{{ request()->date_from?request()->date_from:'' }}"
                                               readonly>
                                        <input class="form-control datePicker" name="date_to" placeholder="تا"
                                               value="{{ request()->date_to?request()->date_to:'' }}"
                                               readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>آدرس مقصد</label>
                                    <div class="input-group">
                                        <input class="form-control" name="destination_address"
                                               value="{{ request()->destination_address?request()->destination_address:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-info search-ajax">جستجو</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped text-center table-bordered">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>ارز</td>
                                    <td>مقدار</td>
                                    <td>کد پیگیری</td>
                                    <td>نوع</td>
                                    <td>توضیحات</td>
                                    <td>آدرس مقصد</td>
                                    <td>تاریخ</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($transactions) > 0)
                                    @foreach($transactions as $transaction)
                                        <tr>
                                            <td>{{ index($transactions,$loop) }}</td>
                                            @if ($transaction->financeable instanceof \App\Models\Asset)
                                                <td>{{ $transaction->financeable->unit }}</td>
                                            @else
                                                <td>-</td>
                                            @endif
{{--                                            <td>{{ $transaction->amount }}</td>--}}
                                            <td>{{ rial_to_unit($transaction->amount,'rls',true) }}</td>
                                            <td>
                                                @if($transaction->tracking_code)
                                                    <input id="tracking_{{$transaction->id}}" type="hidden"
                                                           class="form-control"
                                                           value="{{$transaction->tracking_code}}" disabled>
                                                    <button type="button" data-copy="tracking_{{$transaction->id}}"
                                                            class="btn btn-sm btn-primary copy-btn"><i
                                                            class="fa fa-copy"></i></button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <span class="badge badge-{{ $transaction->type==1?'danger':'success' }}">
                                                    {{ $transaction->type==1?'برداشت':'واریز' }}
                                                </span>
                                            </td>
                                            <td>
                                                {{$transaction->description}}
                                            </td>
                                            <td>
                                                @if($transaction->extra_field1)
                                                    <input id="destination_{{$transaction->id}}" type="hidden"
                                                           value="{{$transaction->extra_field1}}">
                                                    <span class="font-12 mr-2">{{$transaction->extra_field1}}</span>
                                                    <button type="button" data-copy="destination_{{$transaction->id}}"
                                                            class="btn btn-sm btn-info copy-btn"
                                                            data-destination="{{$transaction->extra_field1}}"><i
                                                            class="fa fa-copy"></i></button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                {{ jdate_from_gregorian($transaction->created_at) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <th colspan="8">موردی یافت نشد</th>
                                @endif
                                </tbody>

                            </table>
                            {!! $transactions->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $(document).on('click', ".copy-btn", function () {
            let elm = document.getElementById($(this).data('copy'));
            if (copyToClipboard(elm) == true) {
                swal('متن مورد نظر با موفقیت کپی شد', '', 'success');
            }
        });
    </script>


@endsection
