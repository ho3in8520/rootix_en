@extends('templates.user.master_page')
@section('title_browser')لیست برداشت ریالی@endsection

@section('content')
    <section id="basic-form-layouts">
        <!-- More Modal -->
        <div class="modal fade" id="moreModal" tabindex="-1" role="dialog" aria-labelledby="moreModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                            <tr class="amount">
                                <th>مبلغ</th>
                                <td></td>
                            </tr>
                            <tr class="fee">
                                <th>کامزد</th>
                                <td></td>
                            </tr>
                            <tr class="total_amount">
                                <th>مبلغ قابل پرداخت</th>
                                <td></td>
                            </tr>
                            <tr class="status">
                                <th>وضعیت</th>
                                <td><span></span></td>
                            </tr>
                            <tr class="user_des">
                                <th>توضیحات کاربر</th>
                                <td></td>
                            </tr>
                            <tr class="admin_des">
                                <th>توضیحات ادمین</th>
                                <td></td>
                            </tr>
                            <tr class="card_number">
                                <th>شماره کارت</th>
                                <td></td>
                            </tr>
                            <tr class="sheba_number">
                                <th>شماره شبا</th>
                                <td></td>
                            </tr>
                            <tr class="account_number">
                                <th>شماره حساب</th>
                                <td></td>
                            </tr>
                            <tr class="tracking_code">
                                <th>کد پیگیری</th>
                                <td></td>
                            </tr>
                            <tr class="deposit_type">
                                <th>نوع واریز</th>
                                <td></td>
                            </tr>
                            <tr class="deposit_date">
                                <th>تاریخ واریز</th>
                                <td></td>
                            </tr>
                            <tr class="created_at">
                                <th>تاریخ ایجاد</th>
                                <td></td>
                            </tr>
                            <tr class="updated_at">
                                <th>تاریخ بروزرسانی</th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div> <!--## More Modal -->
        <div class="row">
            <div class="col-12 card">
                <div class="card-body">
                    <form method="get" action="" class="row">
                        <div class="form-group col-md-3 col-sm-6">
                            <label>مبلغ <sub class="text-danger">({{get_unit()}})</sub></label>
                            <input type="text" name="amount" class="form-control" value="{{ request()->amount }}">
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label>وضعیت</label>
                            <select class="form-control" name="status">
                                <option value="">همه</option>
                                <option value="0" {{ request()->status==='0'?'selected':'' }}>در حال بررسی</option>
                                <option value="1" {{ request()->status==='1'?'selected':'' }}>واریز شده</option>
                                <option value="2" {{ request()->status==='2'?'selected':'' }}>رد شده</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label>کد پیگیری</label>
                            <input type="text" class="form-control" name="tracking_code"
                                   value="{{ request()->tracking_code }}">
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label>نوع واریز</label>
                            <select class="form-control" name="deposit_type">
                                <option value="">همه</option>
                                <option value="0" {{ request()->deposit_type=='0'?'selected':'' }}>شتاب</option>
                                <option value="1" {{ request()->deposit_type==='1'?'selected':'' }}>پایا</option>
                                <option value="2" {{ request()->deposit_type==='2'?'selected':'' }}>سانتا</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label>از تاریخ (واریز)</label>
                            <input type="text" class="form-control datePicker" name="start_deposit_date"
                                   value="{{ request()->start_deposit_date }}">
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label>تا تاریخ (واریز)</label>
                            <input type="text" class="form-control datePicker" name="end_deposit_date"
                                   value="{{ request()->end_deposit_date }}">
                        </div>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-success float-right search-ajax">فیلتر کردن</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card col-12">
                <div class="card-header">
                    <h3>لیست برداشت ریالی</h3>
                </div>
                <div class="card-body">
                    <a class="btn btn-info mb-1 float-right" href="{{ route('withdraw-rial.create') }}">برداشت جدید</a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>مبلغ</th>
                                <th>کارمزد</th>
                                <th>مبلغ واریزی</th>
                                <th>وضعیت</th>
                                <th>کد پیگیری</th>
                                <th>تاریخ ایجاد</th>
                                <th>بیشتر</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($withdraw_rial->count() >0)
                                @php($i=1)
                                @foreach($withdraw_rial as $item)
                                    <tr data-more="{{ json_encode($item) }}"
                                        data-status="{{ json_encode($item->status_info) }}">
                                        <td>{{ $i }}</td>
                                        {{--                                        <td>{{ number_format($item->amount) }} ریال</td>--}}
                                        <td>{{ rial_to_unit($item->amount,'rls',true) }} </td>
                                        {{--                                        <td>{{ number_format($item->fee) }} ریال</td>--}}
                                        <td>{{ rial_to_unit($item->fee,'rls',true) }} </td>
                                        {{--                                        <td>{{ number_format($item->amount-$item->fee) }} ریال</td>--}}
                                        <td>{{ rial_to_unit($item->amount-$item->fee,'rls',true) }}</td>
                                        <td><span
                                                class="badge badge-{{ $item->status_info['color'] }}">{{ $item->status_info['text'] }}</span>
                                        </td>
                                        <td>{{ $item->tracking_code?$item->tracking_code:'-' }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td><a href="" class="btn btn-info more-btn" data-toggle="modal"
                                               data-target="#moreModal">اطلاعات بیشتر</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="13" class="text-center">موردی جهت نمایش وجود ندارد</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                        {!! $withdraw_rial->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $('.more-btn').click(function () {
            let data = $(this).parents('tr').data('more');
            let bank = data.bank;
            let status = $(this).parents('tr').data('status');
            let table = $("#moreModal table tbody");
            // table.find(".amount td:last").text(numberFormat(data.amount) + ' ریال ');
            table.find(".amount td:last").text(rial_to_unit(data.amount, 'rls', true));
            // table.find(".fee td:last").text(numberFormat(data.fee) + ' ریال ');
            table.find(".fee td:last").text(rial_to_unit(data.fee, 'rls', true));
            // table.find(".total_amount td:last").text(numberFormat(data.amount - data.fee) + ' ریال ');
            table.find(".total_amount td:last").text(rial_to_unit(data.amount - data.fee, 'rls', true));
            table.find(".status td:last span").text(status['text']);
            table.find(".status td:last span").addClass('badge badge-' + status['color']);
            table.find(".user_des td:last").text(data.user_des);
            table.find(".admin_des td:last").text(data.admin_des);
            table.find(".card_number td:last").text(bank.name + ' - ' + bank.card_number);
            table.find(".sheba_number td:last").text(bank.sheba_number);
            table.find(".account_number td:last").text(bank.account_number);
            table.find(".tracking_code td:last").text(data.tracking_code);
            table.find(".deposit_type td:last").text(data.deposit_type);
            table.find(".deposit_date td:last").text(new Date(data.deposit_date).toLocaleString('fa-IR'));
            table.find(".created_at td:last").text(new Date(data.created_at).toLocaleString('fa-IR'));
            table.find(".updated_at td:last").text(new Date(data.updated_at).toLocaleString('fa-IR'));
        })
    </script>
@endsection
