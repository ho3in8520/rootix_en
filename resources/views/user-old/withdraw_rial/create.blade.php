@extends('templates.user.master_page')
@section('title_browser') ثبت درخواست برداشت ریالی @endsection
@section('content')
    @if ($banks->count() > 0)
        <!-- Modal -->
        <div class="modal fade" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="withdrawModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="withdrawModalLabel">تایید نهایی درخواست برداشت</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="d-block mx-auto" width="150" src="{{ asset('theme/user/assets/img/rls.png') }}">
                        <table class="table final-table">
                            <tbody>
                            <tr>
                                <th>مبلغ درخواستی شما</th>
                                <td class="amount"></td>
                            </tr>
                            <tr>
                                <th>کارمزد</th>
                                <td class="fee"></td>
                            </tr>
                            <tr>
                                <th>مبلغ نهایی (مبلغ درخواستی - کارمزد)</th>
                                <td class="final-amount"></td>
                            </tr>
                            <tr>
                                <th>واریز به</th>
                                <td class="destination-card"></td>
                            </tr>
                            </tbody>
                        </table>
                        <p class="alert alert-info font-weight-normal" style="font-size: 14px;">
                            جهت تایید موارد بالا روی ارسال درخواست کلیک کنید
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">لغو</button>
                        <button type="button" class="btn btn-info confirm-withdraw">ارسال درخواست</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="card col-12">
                <div class="card-header">
                    ثبت درخواست برداشت ریالی جدید
                </div>
                <div class="card-body">
                    <div class=" m-2">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>سطح شما:</th>
                                <td>{{ convertStepToPersian(auth()->user()->step_complate) }}</td>
                            </tr>
                            <tr>
                                <th>سقف برداشت روزانه شما({{get_unit()}}):</th>
{{--                                <td>{{ max_withdraw_daily()==-1 ? 'نامحدود' :number_format(max_withdraw_daily()).' ریال ' }} </td>--}}
                                <td>{{ max_withdraw_daily()==-1 ? 'نامحدود' :rial_to_unit(max_withdraw_daily(),'rls',true) }} </td>
                            </tr>
                            <tr>
                                <th>برداشت امروز شما({{get_unit()}}):</th>
{{--                                <td>{{ number_format($today_withdraw) }} ریال</td>--}}
                                <td>{{ rial_to_unit($today_withdraw,'rls',true) }} </td>
                            </tr>
                            <tr>
                                <th>سقف برداشت امروز شما({{get_unit()}}):</th>
{{--                                <td>{{ max_withdraw_daily() ==-1?'نامحدود':number_format(max_withdraw_daily() - $today_withdraw).' ریال ' }} </td>--}}
                                <td>{{ max_withdraw_daily() ==-1?'نامحدود':rial_to_unit(max_withdraw_daily() - $today_withdraw,'rls',true) }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <a class="btn btn-info mb-1 float-right" href="{{ route('withdraw-rial.index') }}">لیست درخواست های
                        برداشت</a>

                    <form class="row col-12" action="{{ route('withdraw-rial.store') }}" method="post">
                        @csrf
                        <div class="form-group col-md-6">
                            <label>
                                مبلغ ({{get_unit()}})
{{--                                <sub>موجودی: {{ number_format($rial_asset->amount) }} ریال</sub>--}}
                                <sub>موجودی: {{ rial_to_unit($rial_asset->amount,'rls',true) }} </sub>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control amount-input"
                                       onkeyup="($(this).val(numberFormat($(this).val())))" placeholder="مثال 100,000">
                                <input type="hidden" name="amount" value="">
                                <div class="input-group-append">
{{--                                <span class="btn btn-info input-group-btn px-1 all-amount"--}}
{{--                                      data-all="{{ $rial_asset->amount }}">کل موجودی--}}
{{--                                </span>--}}
                                    <span class="btn btn-info input-group-btn px-1 all-amount"
                                          data-all="{{ rial_to_unit($rial_asset->amount,'rls') }}">کل موجودی
                                </span>
                                </div>
                            </div>
{{--                            <p style="font-size: 13px; margin-bottom: 7px;" class="mt-1">کامزد--}}
{{--                                برداشت: {{ $fee_withdraw }}--}}
{{--                                ریال</p>--}}
                            <p style="font-size: 13px; margin-bottom: 7px;" class="mt-1">کامزد
                                برداشت: {{ rial_to_unit($fee_withdraw,'rls',true) }}
                                </p>
{{--                            <p style="font-size: 13px; margin-bottom: 7px;">حداقل برداشت: {{ $min_withdraw }} ریال</p>--}}
                            <p style="font-size: 13px; margin-bottom: 7px;">حداقل برداشت: {{ rial_to_unit($min_withdraw,'rls',true) }} </p>
                            <p style="font-size: 13px;" class="font-weight-bold text-primary">
                                <span>ملبغ پرداختی (مبلغ درخواستی - کارمزد): </span>
                                <span class="amount-withdrawn">-</span>
                            </p>
                        </div>
                        <div class="form-group col-md-6">
                            <label>توضیحات </label>
                            <textarea class="form-control" name="user_des" rows="5"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label>انتخاب حساب جهت واریز </label>
                            <select class="form-control" name="bank">
                                @foreach($banks as $bank)
                                    <option value="{{ $bank->id }}">{{ $bank->name }}
                                        - {{ $bank->card_number }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <button type="button" class="btn btn-success ajaxStore float-right disabled" disabled
                                    data-confirm="false">ثبت
                                درخواست
                            </button>
                            <button type="button" class="btn btn-primary d-none" data-toggle="modal"
                                    data-target="#withdrawModal"></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="card col-12">
                <div class="card-body">
                    <div class="alert alert-warning">
                        لطفا ابتدا از بخش حساب های بانکی، حساب بانکی مورد تایید برای خود ثبت کرده و بعد از تایید شدن
                        توسط مدیریت
                        اقدام به ثبت درخواست برداشت کنید
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script>
        get_unit()
        {{--let fee_withdraw = '{{ $fee_withdraw }}' * 1;--}}
        {{--let min_withdraw = '{{ $min_withdraw }}' * 1;--}}
        {{--let inventory = '{{ $rial_asset->amount }}' * 1;--}}
        {{--let max_daily_withdraw = '{{ max_withdraw_daily() }}';--}}
        {{--let today_withdraw = '{{ $today_withdraw }}';--}}
        {{--let today_max = max_daily_withdraw - today_withdraw;--}}
        let fee_withdraw = '{{ rial_to_unit($fee_withdraw,'rls') }}' * 1;
        let min_withdraw = '{{ rial_to_unit($min_withdraw,'rls') }}' * 1;
        let inventory = '{{ rial_to_unit($rial_asset->amount,'rls') }}' * 1;
        let max_daily_withdraw = '{{rial_to_unit( max_withdraw_daily(),'rls') }}';
        let today_withdraw = '{{ rial_to_unit($today_withdraw,'rls') }}';
        let today_max = max_daily_withdraw - today_withdraw;
        console.log('max_daily_withdraw', max_daily_withdraw);
        console.log('today_withdraw', today_withdraw);
        console.log('today_max', today_max);
        $(".all-amount").click(function () {
            let all_amount = $(this).data('all');
            let input = $(this).parents('.input-group').children('.amount-input');
            input.val(all_amount);
            input.keyup();
        });
        $(".amount-input").keyup(function () {
            $(".ajaxStore").addClass('disabled').attr('disabled', 'true');
            let amount = $(".amount-input").val();
            amount = amount.replaceAll(',', '') * 1;
            $("input[name=amount]").val(amount);
            let amount_withdrawn = 0;
            let check_amount = true;
            if (amount > inventory) {
                // amount_withdrawn = 'حداکثر مبلغ ' + numberFormat(inventory) + '  ریال می باشد ';
                amount_withdrawn = 'حداکثر مبلغ ' + numberFormat(inventory) +get_unit()+ ' می باشد ';
                check_amount = false;
            } else if (max_daily_withdraw !={{rial_to_unit(-1,'rls')}} && amount > today_max) {
                // amount_withdrawn = 'حداکثر برداشت امروز شما ' + numberFormat(today_max) + '  ریال می باشد ';
                amount_withdrawn = 'حداکثر برداشت امروز شما ' + today_max + get_unit()+'  می باشد ';
                check_amount = false;
            } else if (amount >= min_withdraw)
                amount_withdrawn = numberFormat(amount - fee_withdraw);

            if (check_amount == true)
                $(".ajaxStore").removeClass('disabled').removeAttr('disabled');

            $(".amount-withdrawn").text(amount_withdrawn);
        });

        $(".ajaxStore").click(function (e) {
            let amount = $("input[name=amount]").val() * 1;
            if (amount < min_withdraw) {
                // swal('خطا!', 'حداقل مقدار باید ' + numberFormat(min_withdraw) + ' ریال باشد', 'warning');
                swal('خطا!', 'حداقل مقدار باید ' + numberFormat(min_withdraw) + get_unit()+' باشد', 'warning');
                e.stopImmediatePropagation();
                return;
            }
            let amount_withdrawn = amount - fee_withdraw;
            if (amount_withdrawn <= 0) {
                swal('خطا!!', 'حداقل مقدار قابل واریز باید بیشتر از 0 باشد', 'warning');
                e.stopImmediatePropagation();
                return;
            }

            if ($(this).attr('data-confirm') !== 'true') {
                // $(".final-table .amount").text(numberFormat(amount) + ' ریال');
                $(".final-table .amount").text(numberFormat(amount) + ' '+get_unit());
                // $(".final-table .fee").text(numberFormat(fee_withdraw) + ' ریال');
                $(".final-table .fee").text(numberFormat(fee_withdraw) + ' '+get_unit());
                // $(".final-table .final-amount").text(numberFormat(amount_withdrawn) + ' ریال');
                $(".final-table .final-amount").text(numberFormat(amount_withdrawn) + ' '+get_unit());
                $(".final-table .destination-card").text($("select[name='bank'] option:selected").text())
                $('button[data-target="#withdrawModal"]').click();
                e.stopImmediatePropagation();
                return;
            }
            $(".ajaxStore").attr('data-confirm', 'false');
        })
        $(".confirm-withdraw").click(function () {
            $(".modal-header .close").click();
            $(".ajaxStore").attr('data-confirm', 'true').click();
        })
    </script>
@endsection
