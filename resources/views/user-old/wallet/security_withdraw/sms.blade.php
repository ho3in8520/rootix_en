<form method="post" action="">
    <input type="hidden" value="{{ $token }}" name="token">
<div class="row justify-content-center">
    <div class="col-md-7">
        <div class="input-group">
            <label class="m-auto">{{__('attributes.Authentication.phoneNum')}}
                :</label>
            <fieldset class="form-group position-relative input-divider-right mb-0">
                <input type="text" class="form-control text-right float-right"
                       id="iconLeft4" name="number" value="{{ $user->mobile }}"
                       placeholder="مثال: 9131234569" readonly>
                <div class="form-control-position">
                    98+
                </div>
            </fieldset>
        </div>
    </div>
    <div class="col-md-7 row justify-content-center time-expire mt-2" style="display: none">
        <div class="input-group">
            <label class="m-auto">کد تایید: </label>
            <fieldset class="form-group position-relative input-divider-right mb-0">
                <div class="code-verify"><!-- Js Complate --></div>
                <div class="form-control-position time " style="border-right: solid 1px gainsboro"></div>
            </fieldset>
        </div>
    </div>
    <div class="col-md-7 row justify-content-center mt-1">
        <button class="btn btn-success sendCode"
                type="button">{{__('attributes.Authentication.sendCode')}}
        </button>
    </div>
</div>
</form>
