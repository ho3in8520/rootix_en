@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-withdraw')}}
@endsection

@section('content')
    <div class="settings mtb15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 m-auto col-12 row">
                    <div class="row col-12 bg-dark text-white"
                         style="min-height: 200px; margin-bottom: -50px; padding-bottom: 40px;">
                        <div class="col-12 row align-items-end m-auto">
                            <div class="col row  m-auto">
                                <img src="{{asset('theme/user/assets/img/tether.svg')}}" alt="logo" width="80">
                                <h4 class="col-12">{{__('attributes.withdrawal')}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8 text-dark ">
                        <div class="card">
                            <div class="card-body">
                                <form>
                                    <input type="hidden" value="{{$unit}}" />
                                    <div class="form-group row">
                                        <label class="col-12">{{__('attributes.amountPrice')}}</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-primary total-amount">کل موجودی</button>
                                            </div>
                                            <input type="text" class="col-12 form-control" onkeyup="$(this).val(numberFormat($(this).val()))" id="amount">
                                        </div>
                                        <span  class="float-right usdt_balance col-12 " id="usdt_amount" data-label="usdt">0</span>
                                    </div>
                                    <div class="form-group mb-4 mt-5">
                                        <label class="col-12">{{__('attributes.Purification.address')}}</label>
                                        <input type="text" class="col-12 mx-auto form-control mb-3" id="address_wallet">
                                    </div>

                                    <div class="form-group mb-4 mt-3">
                                        <label class="col-12">{{__('attributes.Purification.fee')}}</label>
                                        <input type="text" value="{{ $fee_withdraw_usdt->extra_field2 }}" class="col-12 mx-auto form-control mb-3" readonly>
                                        <div class=" col-12 mb-2 alert alert-success" style="font-size: 12px">
                                            <p class="mb-0">{{__('attributes.Purification.feeBlackChin')}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group mb-4 mt-3">
                                        <button type="button" id="withdraw_button" data-unit="{{$unit}}"
                                                data-route-withdraw="{{ route('withdraw.store') }}"
                                                data-min-send-trx="{{ $min_send_trx->extra_field2 }}"
                                                data-fee-withdraw-usdt="{{ $fee_withdraw_usdt->extra_field2 }}"
                                                data-min-withdraw-usdt="{{ $min_withdraw_usdt->extra_field2 }}"
                                                data-csrf="{{ csrf_token()}}"
                                                data-address-wallet="{{ $token['address']['hex']}}"
                                                data-route-dashboard="{{route('user.dashboard')}}"
                                                data-route-send-message="{{ route('send-message-admin') }}" data-route-withdraw-from="{{ route('withdraw-from-wallet') }}" class="btn btn-success float-left">
                                            <i class="fa fa-save"></i>
                                            {{__('attributes.Purification.request')}}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 card ">
                        <div class="card-body">
                            <p class="alert alert-info">{{__('attributes.Purification.PaRequest')}}</p>
                            <div class=" col-12 mb-2 alert alert-success" style="font-size: 13px">
                                <p>{{__('attributes.Purification.asset')}} <b class="asset"></b> USDT</p>
{{--                                <p>{{__('attributes.Purification.totalDay')}}<b>0</b> {{__('attributes.Purification.of')}} <b>300,000,000</b> {{__('attributes.Purification.toman')}}</p>--}}
{{--                                <p class="mb-0">{{__('attributes.Purification.totalMonth')}} <b>58,819,515</b> {{__('attributes.Purification.of')}} <b>3,000,000</b> {{__('attributes.Purification.toman')}}</p>--}}
                            </div>
                            <div class="video mx-auto">
                                <video style="width: 100%;" height="240" controls>
                                    <source src="movie.mp4" type="video/mp4">
                                    <source src="movie.ogg" type="video/ogg">
                                    {{__('attributes.Purification.err')}}
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script type="module" src="{{asset('theme/user/assets/js/withdrawFromWallet.js')}}" ></script>
    <script>

        $(document).ready(function () {
            $(document).on('click','.total-amount',function () {
                $(this).closest('.input-group').find("input").val($.data(document,'usdt_amount'))
            });

        })
        $(window).on('load', function() {
            $(".asset").text($.data(document,'usdt_amount'));
        })
    </script>
@endsection
