@extends('templates.user.master_page')
@section('title_browser')
    نمایش پیغام
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {{ $notification->title }}
                </div>
                <div class="card-body">
                    {!! $notification->description !!}
                </div>
            </div>
        </div>
    </div>
@endsection
