<div class="tread-tabs__content position-relative" wire:init="loadRequestSell" wire:poll.3000ms>
    <div class="d-flex justify-center align-items-center h-100 spinner">
        <div class="spinner-border text-primary mx-auto" role="status">
        </div>
    </div>
    <div>
        <div class="table-header red-table tread-tabs__content-2">
            <ul>
                <li>قیمت ({{ strtoupper($markets['des']) }})</li>
                <li>مقدار ({{ strtoupper($markets['des']) }})</li>
                <li>جمع ({{ strtoupper($markets['des']) }})</li>
            </ul>
        </div>

        <div class="tbody-currency tread-tabs__content-2">
            <table class="tread-table bg-table order-table">

                <tbody>
                @foreach($result['sells'] as $item)
                    <tr class="bg-red currency-table-left" style="cursor: pointer">
                        <td class="red">{{ $item[0] }}</td>
                        <td>{{ number_format($item[1], 3, '.', '') }}</td>
                        <td>{{ number_format($item['total'],4,'.','') }}</td>
                        <td class="bg red-bg" style="width: {{ $item['percent_background'] }}%"></td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
    <div>
        <div class="table-header green-table">
            <ul class="tread-tabs__content-2">
                <li>
                    <div>
                        <p>آخرین قیمت</p>
                        <p>{{ $result['last'] ?? 0 }}</p>
                    </div>
                </li>
                <li>
                    <div>
                        <p>تومان</p>
                        <p>{{ num_format(convert_currency('btc','rls',$result['last'] ?? 0),0) }}</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="tbody-currency tread-tabs__content-2">
            <table class="tread-table bg-table order-table">
                <tbody>
                @foreach($result['buys'] as $item)
                    <tr class="bg-red currency-table-left" style="cursor: pointer">
                        <td class="red">{{ $item[0] }}</td>
                        <td>{{ number_format($item[1], 3, '.', '') }}</td>
                        <td>{{ number_format($item['total'],4,'.','') }}</td>
                        <td class="bg green-bg" style="width: {{ $item['percent_background'] }}%"></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
