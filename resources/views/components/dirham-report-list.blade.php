<div class="system-reports" id="basic-form-layouts">
    <!-- Modal Pay With Bank-->
    <div class="col-12 col-md-6 col-lg-3">
        <div class="modal fade mt-5" id="modalReject" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form method="post"
                          action="{{ route("admin.users.reject_dirham_diposit",$user) }}">
                        <input type="hidden" value="" name="transaction_id" id="transaction-id">
                        @csrf
                        <div class="modal-body border-0">
                            <h3 class="text-center my-3">Write reject reason</h3>
                        </div>
                        <div class="row col-12 justify-center mb-4">
                            <div class="col-5">
                                <div dir="ltr"
                                     class=" request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active">

                                    <input type="text" dir="ltr" class="currency-value-input"
                                           name="reject_reason"
                                           id="currency-value-input-1" placeholder="Reject reason...." autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <button
                                class="btn signin-btn deposit-btn">
                                Ok
                            </button>

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Pay With Bank-->

    <div class="component-report">
        <div class="row">
            <div class="report-table col-12">
                <div class="overflow-auto">
                    <div class="table-responsive specification">
                        <table class="container Specification-table mt-5">
                            <thead>
                            <tr>
                                <th class="headline">
                                    <div class="code">کد تراکنش</div>
                                </th>
                                <th class="header headline">
                                    <div>نوع تراکنش</div>
                                </th>
                                <th class="header headline">
                                    <div>ارز</div>
                                </th>
                                <th class="header headline">
                                    <div>مبلغ</div>
                                </th>
                                <th class="header headline">
                                    <div>عکس واریزی</div>
                                </th>
                                <th class="header headline">
                                    <div>وضعیت</div>
                                </th>
                                <th class="headline">
                                    <div class="date">تاریخ واریز</div>
                                </th>
                                <th class="headline">
                                    <div class="">عملیات</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reports as $report)
                                @php
                                    $status='';
                                     switch ($report->status){
                                         case 0:
                                             $status='جدید';
                                             break;
                                         case 1:
                                             $status='واریز شده توسط ادمین';
                                             break;
                                         case 2:
                                             $status='رد شده';
                                              break;
                                     }
                                @endphp
                                <tr>
                                    <td class="numbers pt-4">
                                        @if($report->transaction_id)
                                            {{ \Illuminate\Support\Str::limit($report->transaction_id,10) }}
                                            <a href="https://tronscan.org/#/transaction/{{ $report->transaction_id }}"
                                               title="نمایش وضعیت" target="_blank">
                                                <img class="arrow" src="{{ asset('theme/user/images/Group 1779.svg') }}"
                                                     alt="" width="30">
                                            </a>
                                        @else
                                            -
                                        @endif

                                    </td>
                                    <td class="header user-level pt-4">
                                        <span class="deposit">واریز</span>
                                    </td>
                                    <td class="currency header pt-4">dirham</td>
                                    <td class="amount header pt-4">
                                        {{ $report->amount }}
                                    </td>
                                    <td class="description header pt-4">
                                        <a href="{{getImage($report->files[0]->path,'download')}}"
                                           title="Download">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24"
                                                 viewBox="0 0 26.992 30.849">
                                                <path id="Icon_metro-attachment"
                                                      data-name="Icon metro-attachment"
                                                      d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                                      transform="translate(-3.535 -1.928)"/>
                                            </svg>
                                        </a>
                                    </td>
                                    <td class="amount header pt-4">
                                        {{ $status }}
                                    </td>
                                    <td class="date pt-5">
                                        {{ jdate_from_gregorian($report->created_at,'%A, %d %B %Y') }}
                                        <span class="time">{{ jdate_from_gregorian($report->created_at,'H:i') }}</span>
                                    </td>
                                    <td class="date pt-5">
                                        @if($report->status === 0)
                                            <form action="{{route('admin.users.confirm_dirham_diposit',$user)}}" method="post"
                                                  class="d-inline">
                                                <input type="hidden" name="amount" value="{{$report->amount}}">
                                                <input type="hidden" name="trans_id" value="{{$report->id}}">
                                                <button type="button"
                                                        class="create-new-bank withdraw-btn m-1 d-inline ajaxStore">
                                                    تایید
                                                </button>
                                            </form>

                                            <a href="" class="create-new-bank withdraw-btn m-1 d-inline reject-dirham"
                                               data-toggle="modal" data-target="#modalReject"
                                                data-data="{{$report->id}}" >رد </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $reports->withQueryString()->links() }}

            </div>
        </div>
    </div>
</div>
