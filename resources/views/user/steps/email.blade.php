@extends('templates.user.master_page')
@section('title_browser','Email-Authentication')
@section('style')
    <link href="{{ asset('theme/user/styles/cantactInfo.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        Authenticate
                    </h1>
                </div>
<!--                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 13%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div class="progress-circle active"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">Step 1: Contact information</h3>
                                <h3 class="desktop-title">Step 2: Receive the documents</h3>
                                <h3 class="desktop-title">Step 3: Address and Phone</h3>
                                <h3 class="desktop-title">Step 4: Banking information</h3>
                                <h3 class="d-lg-none">Step 1</h3>
                                <h3 class="d-lg-none">Step 2</h3>
                                <h3 class="d-lg-none">Step 3</h3>
                                <h3 class="d-lg-none">Step 4</h3>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </section>
    <!-- contactInfo -->
    <section class="contacts mt-3">
        <div class="container">
            <div class="cantacts-box p-3">
                <span class="f-text text-center d-block mt-3 mb-5">Confirm your email</span>
                <div class="">
                    <img alt="" class="i-img ml-1 mb-1 mb-md-0" src="{{ asset('theme/user/images/contactInfo/Group%201728.png') }}">
                    <span class="t-text">Click on receive link to send activation link to your email</span>
                </div>
                <div style="margin: 50px 0;">
                    <div id="email" class="row mb-5 mb-md-2">
                        <div class="col-12 col-lg-5">
                            <div class="item-right d-flex mb-2 mb-md-0 justify-between p-4 row"
                                 style="background-color:rgba(0, 0, 0, 0.28);color: #000;">
                                <span class="ttext col">{{ auth()->user()->email }}</span>
                                <span class="ttext">Email</span>
                                <input type="hidden" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="col-12 col-lg-7">
                            <div class="d-flex items-center mx-3">
                                <img class="i-img ml-3" src="{{ asset('theme/user/images/contactInfo/Group%201738.png') }}">
                                <div class="l-text text-left px-3">
                                    <p class="text-danger font-large-3">Notice that activation link valid for 30 minutes.</p>
                                    <p class="text-warning">If the email is not in the inbox, please check your spam folder as well.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="btns d-flex justify-center my-5">
            <button id="text" class="my-btn get-link" style="background-color: #2f31d0;">Send activation link</button>
            <button class="i-chat mr-4">
                <img src="{{ asset('theme/user/images/contactInfo/Group%201700.png') }}" alt="">
            </button>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var getLinkCallback= function () {
            $(".get-link").text('Send activation link').removeAttr('disabled');
        }
        $(document).on('click','.get-link',function () {
            $(".get-link").text('Sending link ...').attr('disabled',true);
            let email= $("input[name=email]").val();
            const form_obj = {
                action: '{{ route('step.send-email') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    email: email,
                },
                isObject: true,
            }
            ajax(form_obj,getLinkCallback)
        })
    </script>
@endsection
