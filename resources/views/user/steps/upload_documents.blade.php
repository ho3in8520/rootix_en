@extends('templates.user.master_page')
@section('title_browser','Upload Passport')
@section('style')
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        Authentication
                    </h1>
                </div>
                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 88%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="progress-circle active"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">Step 1: Basic Information</h3>
                                <h3 class="desktop-title">Step 2: Personal Information</h3>
                                <h3 class="desktop-title">Step 3: Upload Passport</h3>
                                <h3 class="d-lg-none">Step 1</h3>
                                <h3 class="d-lg-none">Step 2</h3>
                                <h3 class="d-lg-none">Step 3</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form method="post" action="{{ route('step.upload.store') }}">
        @csrf
        <section class="pt-1">
            <input type="hidden" placeholder="dfasfasdfasf">
            <div class="container">
                <div class="panel-box h-auto">
                    <h5 class="singing-currency-title mb-2 mt-3">
                        Upload Passport
                    </h5>

                    <div class="row justify-center">

                        @if($user->verify_type == 1)
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>Upload Your Passport</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['passport']))?getImage($images['passport']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[passport]" class="imgInp d-none"
                                               type="file"
                                               style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>Upload Your Selfie</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['selfie_image']))?getImage($images['selfie_image']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[selfie_image]" type="file"
                                               class="imgInp d-none" style="display: none">
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>Upload First Side of ID Card</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['ID_Card_first']))?getImage($images['ID_Card_first']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[ID_Card_first]" class="imgInp d-none"
                                               type="file"
                                               style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>Upload Second Side of ID Card</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['ID_Card_second']))?getImage($images['ID_Card_second']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[ID_Card_second]" type="file"
                                               class="imgInp d-none" style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>Upload Your Selfie</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['selfie_image']))?getImage($images['selfie_image']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[selfie_image]" type="file"
                                               class="imgInp d-none" style="display: none">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>

        <div class="btns-exit">

            <a href="/" class="btn signin-btn authentication-btn">
                Cancel
            </a>
            <button class="btn signin-btn record-btn ajaxStore" type="submit">
                Next
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script src="{{ asset('theme/user/scripts/selected-option.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/just-get-number.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/sell-request-selected-1.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/auth-4.js') }}"></script>

    <script>
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    // $('#blah').attr('src', e.target.result);
                    $(input).closest('.card-body').find("img").attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $(this).closest('.card-body').find("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
        });
        $(document).on('click', '#blah', function () {
            $(this).closest('.card-body').find("input").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $(this).closest('.card-body').find(".imgInp").val('')
            $(this).closest('.card-body').find("#blah").attr('src', "{{ asset('theme/user/assets/img/no-image.png') }}");
            $(this).closest('.card-body').find("#deleteImage").html('')
        });
    </script>
@endsection
