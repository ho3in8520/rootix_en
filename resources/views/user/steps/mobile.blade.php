@extends('templates.user.master_page')
@section('title_browser','Mobile and Email-Authentication')
@section('style')
    <link href="{{ asset('theme/user/styles/cantactInfo.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        Authentication
                    </h1>
                </div>
                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 13%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div class="progress-circle active"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">Step 1: Contact information</h3>
                                <h3 class="desktop-title">Step 2: Receive the documents</h3>
                                <h3 class="desktop-title">Step 3: Address and Phone</h3>
                                <h3 class="desktop-title">Step 4: Banking information</h3>
                                <h3 class="d-lg-none">Step 1</h3>
                                <h3 class="d-lg-none">Step 2</h3>
                                <h3 class="d-lg-none">Step 3</h3>
                                <h3 class="d-lg-none">Step 4</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contactInfo-->
    <section class="contacts mt-3">
        <div class="container">
            <div class="cantacts-box p-3">
                <span class="f-text text-center d-block mt-3 mb-5">Enter your contact information</span>
                <div class="">
                    <img alt="" class="i-img ml-1 mb-1 mb-md-0"
                         src="{{ asset('theme/user/images/contactInfo/Group%201728.png') }}">
                    <span class="t-text">
                        Enter your mobile number, after registering the number, the confirmation code will be sent to you via SMS, enter the confirmation code and press the register button.
                    </span>
                </div>
                <div style="margin: 100px 0;">
                    <div class="row mb-5 mb-md-2">
                        <div class="col-12 col-lg-5">
                            <div class="item-right d-flex justify-between align-items-center p-4 mb-1 mb-md-0 row">

                                <input placeholder="09177777878" type="number" name="number"
                                       {{ !empty($result['sms'])?'disabled':'' }}
                                       class="ttext mobile-number col"
                                       value="{{!empty($result['sms'])? $result['sms']['user']->mobile:'' }}"/>
                                <span class="ttext text-right">
                                    Phone number
                                    <p class="text-danger error-number"></p>
                                </span>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7">
                            <div class="d-flex items-center mx-3 text-con">
                                <img alt="" class="i-img ml-3"
                                     src="{{ asset('theme/user/images/contactInfo/Group%201738.png') }}">
                                <div class="text-left px-4">
                                    If your contact number changes, notify Rootix Support to change your number on the system. It should be noted that all transaction SMS will be sent to your registered number.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="codeMessage" class="row  mb-5 mb-md-2" style="
                    @if(empty($result['sms'])) display: none @endif">
                        <div class="col-12 col-lg-5">
                            <div class="item-right d-flex justify-between p-4 row align-items-center">
                                <input placeholder="123456" type="number" name="code" class="ttext col"/>
                                <span class="ttext">Verification code</span>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7">
                            <div class=" d-flex items-center mx-3 pt-3">
                                <img class="i-img ml-3"
                                     src="{{ asset('theme/user/images/contactInfo/Group%201738.png') }}">
                                <div class="l-text ">
                                    <span>Submit the code again :</span>
                                    <span class="stopwatch timer"></span>
                                </div>
                                <button class="resend-btn px-4 ml-2" disabled>resend</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="btns d-flex justify-center my-5">
            <button id="text" class="my-btn @if(!empty($result['sms'])) button-verify @else sendCode @endif" @if(empty($result['sms'])) disabled @endif style="background-color: #2f31d0;">
                @if(!empty($result['sms'])) Confirm code @else Receive
                code @endif
            </button>
            <button class="i-chat mr-4">
                <img src="{{ asset('theme/user/images/contactInfo/Group%201700.png') }}" alt="">
            </button>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".mobile-number").keyup(function () {
            $('.error-number').text('');
            $(".sendCode").removeAttr('disabled');
            $("input[name='number']").css({'border-color': '#A6A9AE'});
            var inputVal = $(this).val();
            if (!inputVal) {
                return false;
                $(".sendCode").attr('disabled', true)
            }
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(inputVal)) {
                $('.error-number').text('Mobile number format is incorrect');
                $(".sendCode").attr('disabled', true)
            }
        });

        let sendCodeCallback = function (response, params) {
            if (response.status == 100) {
                swal(response.msg, '', 'success');
                $("input[name='number']").attr('disabled', true);
                if (params.type == 'send') {
                    $('.sendCode').removeClass("sendCode").addClass("button-verify").text("Confirm code");
                    $('#codeMessage').show();
                    $('.resend-btn').attr('disabled', true);
                } else {
                    $('.resend-btn').text("resend").attr('disabled', true);
                }
                time(response.time);
            }
        }

        let verifyCodeCallback = function (response, params) {
            $(".button-verify").text('Confirm code')
        }

        $(document).on("click", '.sendCode', function () {
            var elem = $(this);
            var text_btn = elem;
            elem.text('Sending SMS ...');
            var number = $("input[name='number']").val();
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(number)) {
                $('.error-number').text('Mobile number format is incorrect');
                $(".sendCode").attr('disabled', true);
                return;
            }
            const form_obj = {
                action: '{{ route('step.send-verify') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    number: number,
                    mode: 1
                },
                isObject: true,
            }
            ajax(form_obj, sendCodeCallback, {type: 'send'});
        });

        $(document).on('click', '.resend-btn', function () {
            var elem = $(this);
            elem.text('Sending SMS ...');
            var number = $("input[name='number']").val();
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(number)) {
                $('.error-number').text('Mobile number format is incorrect');
                $(".sendCode").attr('disabled', true);
                return;
            }

            const form_obj = {
                action: '{{ route('step.send-verify') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    number: number,
                    mode: 1
                },
                isObject: true,
            }
            ajax(form_obj, sendCodeCallback, {btnClass: 'resend'});
        })

        $(document).on("click", '.button-verify', function () {
            var elem = $(this);
            elem.text('Pending ...');
            var code = $("input[name='code']").val();
            const form_obj = {
                action: '{{ route('step.send-verify') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    code: code,
                    mode: 2
                },
                isObject: true,
            }
            ajax(form_obj,verifyCodeCallback);
        })

        function time(time) {
            var minutes = 1;
            var seconds = minutes * time;

            function convertIntToTime(num) {
                var mins = Math.floor(num / 60);
                var secs = num % 60;
                var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                return (timerOutput);
            }

            var countdown = setInterval(function () {
                var current = convertIntToTime(seconds);
                $('.timer').text(current);

                if (seconds == 0) {
                    clearInterval(countdown);
                }
                seconds--;
                if (seconds < 0) {
                    $("input[name='number'], .resend-btn").removeAttr('disabled');
                }
            }, 1000);
        }

        @if($result && !empty($result['sms']))
        time(`{{ $result['sms']['time'] }}`);
        @endif

    </script>
@endsection
