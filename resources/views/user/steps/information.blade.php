@extends('templates.user.master_page')
@section('title_browser','personal verification')
@section('style')
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        Authentication
                    </h1>
                </div>
                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 50%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="progress-circle active"></div>
                                    <div class="progress-circle"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">Step 1: Basic Information</h3>
                                <h3 class="desktop-title">Step 2: Personal Information</h3>
                                <h3 class="desktop-title">Step 3: Upload Passport</h3>
                                <h3 class="d-lg-none">Step 1</h3>
                                <h3 class="d-lg-none">Step 2</h3>
                                <h3 class="d-lg-none">Step 3</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form method="post" action="{{ route('step.information.store') }}">
        @csrf
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto" style="min-height: 50vh !important;">
                    <h5 class="singing-currency-title mb-5 mt-3">Personal Information</h5>
                    @if($errors->any())
                        <ul class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div
                                dir="ltr"
                                class=" request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center">
                                <span class="request-box__title">
                                    First Name
                                    <br><span class="text-danger error-user first_name"></span>
                                </span>
                                <input type="text" dir="ltr" class="currency-value-input ml-3" id="currency-value-input-1"
                                    name="user[first_name]" value="{{ (!empty($user))?$user->first_name:'' }}"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div
                                dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center">
                                <span class="request-box__title">Last Name
                                    <br><span class="text-danger error-user last_name"></span>
                                </span>

                                <input type="text" dir="ltr"
                                    class="currency-value-input ml-3"
                                    id="currency-value-input-1" name="user[last_name]" value="{{ (!empty($user))?$user->last_name:'' }}"/>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div
                                dir="ltr"
                                class="
                       request-box request-box-2
                        auth__btn
                        request-js-box-2
                        enabled
                        d-flex
                        justify-between
                        items-center
                      "
                            >
                                <span class="request-box__title">
                                    Nationality
                                    <br><span class="text-danger error-user city"></span>
                                </span>

                                <select class="border-0 outline-none ml-3" name="user[nationality]">
                                    @foreach($countries as $item)
                                        <option
                                            value="{{ $item->id }}" {{ (!empty($user) && $user->nationality==$item->id)?'selected':'' }}>{{ $item->nicename }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-lg-4">
                            <div dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center">
                                <span class="request-box__title">Date of Birth
                                    <br><span class="text-danger error-user birth_day"></span>
                                </span>

                                <input type="date" dir="ltr"
                                    class="currency-value-input ml-3" id="birthday-input" name="user[birth_day]" value="{{ (!empty($user))?$user->birth_day:'' }}"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 col-lg-4">
                            <div
                                dir="ltr"
                                class="
                        request-box request-box-2
                        auth__btn
                        request-js-box-2
                        enabled
                        d-flex
                        justify-between
                        items-center
                        gener-btn
                      ">
                                <span class="request-box__title">
                                    Gender
                                    <br><span class="text-danger error-user gender_id"></span>
                                </span>
                                {{--                                <form class="d-flex items-center">--}}
                                <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                    Female
                                    <input type="radio" name="user[gender_id]"
                                           value="2" {{ ((!empty($user) && $user->gender_id==2) || $user->gender_id == null)?'checked':'' }}/>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio-container ml-4 mb-0 pl-4">
                                    Male
                                    <input type="radio" name="user[gender_id]"
                                           value="1" {{ (!empty($user) && $user->gender_id==1)?'checked':'' }}/>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 col-lg-4">
                            <div
                                dir="ltr"
                                class="
                        request-box request-box-2
                        auth__btn
                        request-js-box-2
                        enabled
                        d-flex
                        justify-between
                        items-center
                        gener-btn
                      ">
                                <span class="request-box__title">
                                    ID Type
                                    <br><span class="text-danger error-user verify_type"></span>
                                </span>
                                {{--                                <form class="d-flex items-center">--}}
                                <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                    ID Card
                                    <input type="radio" name="user[verify_type]"
                                           value="2" {{ ((!empty($user) && $user->verify_type==2) || $user->verify_type == null)?'checked':'' }}/>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio-container ml-4 mb-0 pl-4">
                                    Passport
                                    <input type="radio" name="user[verify_type]"
                                           value="1" {{ (!empty($user) && $user->verify_type==1)?'checked':'' }}/>
                                    <span class="checkmark"></span>
                                </label>

                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-4">
                            <div
                                dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center">
                                <span class="request-box__title">
                                   ID Number
                                    <br><span class="text-danger error-user id_card_number"></span>
                                </span>

                                <input type="text" dir="ltr" class="currency-value-input ml-3"
                                       id="currency-value-input-1" name="user[id_card_number]" value="{{ (!empty($user))?$user->id_card_number:'' }}"/>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center">
                                <span class="request-box__title">Date of Expiration
                                    <br><span class="text-danger error-user id_card_exp"></span>
                                </span>

                                <input type="date" dir="ltr"
                                       class="currency-value-input ml-3" id="birthday-input" name="user[id_card_exp]" value="{{ (!empty($user))?$user->id_card_exp:'' }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="btns-exit">

            <a href="/" class="btn signin-btn authentication-btn"> Cancel</a>
            <button class="btn signin-btn record-btn ajaxStore" type="submit">Next</button>

            <button type="reset">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="45"
                    height="45"
                    viewBox="0 0 55 55"
                >
                    <g
                        id="Group_1700"
                        data-name="Group 1700"
                        transform="translate(-114 -365)"
                    >
                        <circle
                            id="Ellipse_86"
                            data-name="Ellipse 86"
                            cx="27.5"
                            cy="27.5"
                            r="27.5"
                            transform="translate(114 365)"
                            fill="#666"
                        />
                        <g
                            id="Icon_ionic-ios-chatbubbles"
                            data-name="Icon ionic-ios-chatbubbles"
                            transform="translate(123.5 374.5)"
                        >
                            <path
                                id="Path_8611"
                                data-name="Path 8611"
                                d="M30.3,22.542a1.7,1.7,0,0,1,.232-.858,2.368,2.368,0,0,1,.148-.218,11.393,11.393,0,0,0,1.941-6.349A11.961,11.961,0,0,0,20.412,3.375,12.129,12.129,0,0,0,8.438,12.72a11.3,11.3,0,0,0-.26,2.4A11.927,11.927,0,0,0,20.2,27.014a14.48,14.48,0,0,0,3.319-.541c.795-.218,1.582-.506,1.786-.584a1.859,1.859,0,0,1,.654-.12,1.828,1.828,0,0,1,.71.141l3.987,1.413a.951.951,0,0,0,.274.07.56.56,0,0,0,.563-.562.9.9,0,0,0-.035-.19Z"
                                fill="#fff"
                            />
                            <path
                                id="Path_8612"
                                data-name="Path 8612"
                                d="M22.395,27.6c-.253.07-.577.148-.928.225a12.977,12.977,0,0,1-2.391.316A11.927,11.927,0,0,1,7.052,16.249a13.293,13.293,0,0,1,.105-1.5c.042-.3.091-.6.162-.9.07-.316.155-.633.246-.942L7,13.4A10.464,10.464,0,0,0,3.375,21.27a10.347,10.347,0,0,0,1.744,5.766c.162.246.253.436.225.563s-.837,4.359-.837,4.359a.564.564,0,0,0,.19.541.573.573,0,0,0,.359.127.5.5,0,0,0,.2-.042L9.2,31.029a1.1,1.1,0,0,1,.844.014,11.834,11.834,0,0,0,4.268.844,11.043,11.043,0,0,0,8.445-3.874s.225-.309.485-.675C22.985,27.429,22.69,27.52,22.395,27.6Z"
                                fill="#fff"
                            />
                        </g>
                    </g>
                </svg>
            </button>
        </div>

    </form>
@endsection
@section('script')

@endsection
