@extends('templates.user.master_page')
@section('title_browser')
    Dashboard-Rootix Exchange
@endsection
@section('style')
    <style>
        .price-chart-table {
            width: 95px;
            height: 57px;
            margin: 0 auto;
        }

        #text2{
            font-weight: bold;
        }

        #timer {
            display: inline-block;
            line-height: 1;
            padding: 20px;
        }

        #timer span {
            display: inline;
            font-size: 18px;
            color: #131212;
        }

        #days {
            font-size: 24px;
            font-weight: bold;
            color: #A149DD;
        }

        #hours {
            font-size: 24px;
            font-weight: bold;
            color: #697AFD;
        }

        #minutes {
            font-size: 24px;
            font-weight: bold;
            color: #F9A826;
        }

        #seconds {
            font-size: 24px;
            color: #FFEE00;
        }
    </style>
@endsection
@section('content')
    {{--    <div id="timeClock"></div>--}}
    <div class="row justify-center">
        <div id="timer" class="d-flex justify-center col-lg-7 col-12">
            <div id="days" class="ml-4 text-center"></div>
            <div id="hours" class="ml-4 text-center"></div>
            <div id="minutes" class="ml-4 text-center"></div>
            <div id="seconds" class="ml-4 text-center"></div>
            <div id="text2" class="ml-4 text-center mt-2"></div>
        </div>
    </div>
    <!-- Modal charge Wallet-->
    <div class="col-12 col-md-6 col-lg-3">
        <div class="modal fade mt-5" id="modalChargeWallet" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form method="post"
                          action="{{ route("user.timer_deposit") }}">
                        @csrf
                        <div class="modal-body border-0">
                            <p class="my-4">
                                How many do you want top up your USDT wallet ??
                            </p>
                            <p>
                                Enter amount at below
                            </p>
                        </div>
                        <div class="row col-12 justify-center mb-4">
                            <div class="col-6">
                                <div dir="ltr"
                                     class=" request-box request-box-2 request-js-box-2 modal__input enabled justify-between align-items-center active">

                                    <div class="d-flex pt-4">
                                        <span class="request-box__title mr-3">amount</span>

                                        <input type="text" dir="ltr" class="currency-value-input"
                                               name="amount"
                                               id="amount" placeholder="Please enter amount here">
                                    </div>
                                    <p class="text-danger error-amount" style="font-size: 14px"></p>

                                </div>
                                <p id="calculate" class="text-danger"></p>

                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <button
                                class="btn signin-btn deposit-btn ajaxStore">
                                Ok
                            </button>

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal charge Wallet-->

    <section class="dashboard-cart">
        <div class="container">
            <div class="row">
                <h1 class="dashboard-title desktop-title">Dashboard</h1>
            </div>
            <!--        start-navigation-->
            <div class="row">
                <div class="col-12 owl-button" dir="ltr">
                    <button class="btn-next nav-button" type="button">
                        <span class="next">❮</span>
                    </button>
                    <button class="btn-prev nav-button" type="button">
                        <span class="prev">❯</span>
                    </button>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-12">
                    <div class="owl-carousel owl-theme chart-slider" dir="ltr">
                        @foreach($header_currencies as $key=>$item)
                            @php
                                $color=$item['color']?$item['color']:'#627EEA';
                                $color=str_split(str_replace('#','',$color),2);
                                $r=hexdec($color[0]);
                                $g=hexdec($color[1]);
                                $b=hexdec($color[2]);
                            @endphp
                            <div class="chart-slider-item">
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container"
                                     data-href="{{route('landing.price_detail',$key)}}" style="cursor: pointer">
                                    <div class="tradingview-widget-container__widget"></div>
                                    <div style="pointer-events: none">
                                        <script type="text/javascript"
                                                src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                                async>
                                            {
                                                "symbol"
                                            :
                                                "{{$key}}USDT",
                                                    "width"
                                            :
                                                "100%",
                                                    "height"
                                            :
                                                "100%",
                                                    "locale"
                                            :
                                                "en",
                                                    "dateRange"
                                            :
                                                "1D",
                                                    "colorTheme"
                                            :
                                                "light",
                                                    "trendLineColor"
                                            :
                                                "rgba({{$r}}, {{$g}}, {{$b}}, 1)",
                                                    "underLineColor"
                                            :
                                                "rgba({{$r}}, {{$g}}, {{$b}}, 0.3)",
                                                    "underLineBottomColor"
                                            :
                                                "rgba({{$r}}, {{$g}}, {{$b}}, 0)",
                                                    "isTransparent"
                                            :
                                                false,
                                                    "autosize"
                                            :
                                                true,
                                                    "largeChartUrl"
                                            :
                                                ""
                                            }
                                        </script>
                                    </div>

                                </div>
                                <!-- TradingView Widget END -->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!--        end-navigation-->
        </div>
    </section>

    <section class="dashboard-chart">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">Total Market Volume
                            </h2>
                        </div>

                        <div class="chart" style="height: 464px;">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container" style="pointer-events: none">
                                <div id="tradingview_81a31"></div>
                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                <script type="text/javascript">
                                    new TradingView.MediumWidget(
                                        {
                                            "symbols": [
                                                [
                                                    "CRYPTOCAP:TOTAL",
                                                    "CRYPTOCAP:TOTAL"
                                                ],
                                            ],
                                            "chartOnly": false,
                                            "width": '100%',
                                            "height": 464,
                                            "locale": "en",
                                            "colorTheme": "light",
                                            "gridLineColor": "rgba(240, 243, 250, 0)",
                                            "trendLineColor": "#2962FF",
                                            "fontColor": "#787B86",
                                            "underLineColor": "rgba(41, 98, 255, 0.3)",
                                            "underLineBottomColor": "rgba(41, 98, 255, 0)",
                                            "isTransparent": false,
                                            "autosize": true,
                                            "showFloatingTooltip": false,
                                            "scalePosition": "right",
                                            "scaleMode": "Normal",
                                            "container_id": "tradingview_81a31"
                                        }
                                    );
                                </script>
                            </div>
                            <!-- TradingView Widget END -->
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">User Wallet</h2>
                        </div>

                        <div class="doughnut-chart">
                            @if ($result['usdt_asset'] > 0 || $result['usdt'] > 0)
                                <canvas id="wallet-assets-chart"></canvas>
                            @else
                                <h4 class="text-center">Your Wallet Balance is 0</h4>
                            @endif
                        </div>

                        <div class="all-assets">
                            <div
                                class="
                            d-flex
                            all-assets-container
                            items-center
                            justify-between
                          "
                            >
                                <h5 class="all-assets-title">Total Assets:</h5>
                                <div class="d-flex">
                                    <h5 class="all-assets-dollar">{{$result['usdt_asset']+$result['usdt']}}$</h5>
                                </div>
                            </div>
                            <div
                                class="
                            d-flex
                            all-assets-container
                            items-center
                            justify-between
                          "
                            >
                                <h5 class="all-assets-title">
                                    Usdt Asset:
                                </h5>
                                <div class="d-flex">
                                    <h5 class="all-assets-dollar">{{$result['usdt']}}$</h5>
                                </div>
                            </div>

                            <div class="d-flex all-assets-container items-center justify-between">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="price-table-currency">
        <div class="container">
            <div class="dashboard-table">
                <div class="d-flex justify-between position-relative">
                    <h2 class="market-volume-title">
                        Table of Rootix Exchange Cryptocurrencies
                    </h2>
                    <h3 class="table-bottom-title">
                        Visit the <a href="{{route('landing.prices')}}">prices
                            page</a> to see the
                        table of all
                        cryptocurrencies.
                    </h3>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="dashboard-table wallet-table-container-self">
                            <div class="wallet-table-container">
                                <table class="currency-table active dashboard-table-1">
                                    <tr>
                                        <td>
                                            <div class="td-row">Name</div>
                                        </td>

                                        <td>
                                            <div class="td-row">Price</div>
                                        </td>

                                        <td>
                                            <div class="td-row">% 24h</div>
                                        </td>

                                        <td>
                                            <div class="td-row">% 7d</div>
                                        </td>

                                        <td>
                                            <div class="td-row">Market Volume</div>
                                        </td>

                                        <td>
                                            <div class="td-row">Daily Value</div>
                                        </td>

                                        <td>
                                            <div class="td-row">Price Chart</div>
                                        </td>
                                    </tr>

                                    @foreach($currencies as $currency)
                                        @php
                                            $key=$currency['currency'];
                                        @endphp
                                        <tr>
                                            <td class="currency-title">
                                                <div class="currency-right-title">
                                                    <img
                                                        src="{{$currency['logo_url']?$currency['logo_url']:asset("theme/landing/images/currency/$key.png")}}"
                                                        alt="{{$currency['currency']}}_logo"
                                                    />
                                                    <h5>{{$currency['name']}}</h5>
                                                </div>
                                            </td>
                                            <td class="currency-last-price">
                                                <div class="currency-doloar-price">
                                                    <h5>@if($currency['price']>=1){{number_format($currency['price'],2)}}
                                                        $ @else{{number_format($currency['price'],7)}}$ @endif</h5>
                                                </div>
                                            </td>
                                            <td>
                                                @if($currency['1d'] != null)
                                                    @php
                                                        $img=(float)$currency['1d']<0?'red_arrow':'green_arrow';
                                                    @endphp
                                                    <span
                                                        class="currency-present-change {{(float)$currency['1d']<0?'red':'green'}}-currency">
                                                    <img style="padding-right: 5px;"
                                                         src="{{asset("theme/user/images/$img.svg")}}" alt="">
                                                {{(float)$currency['1d']<0?number_format((float)substr($currency['1d'],1),2):number_format((float)$currency['1d'],2)}}
                            </span>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td class="currency-most-price">
                                                @if($currency['7d'] != null)
                                                    @php
                                                        $img=(float)$currency['7d']<0?'red_arrow':'green_arrow';
                                                    @endphp
                                                    <span
                                                        class="currency-present-change {{(float)$currency['7d']<0?'red':'green'}}-currency">

                                                    <img style="padding-right: 5px;"
                                                         src="{{asset("theme/user/images/$img.svg")}}" alt="">
                                                    {{(float)$currency['7d']<0?number_format((float)substr($currency['7d'],1),2):number_format((float)$currency['7d'],2)}}


                            </span>
                                                @else - @endif
                                            </td>
                                            <td class="cur rency-chart">
                                                <div class="currency-doloar-price">
                                                    @php
                                                        $volume_info=market_cap_volume($currency['market_cap']);
                                                    @endphp
                                                    @if($volume_info[0] != 0)
                                                        <h5>{{$volume_info[0]}} {{$volume_info[1]}}</h5>
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="currency-buying-btn">
                                                <div class="currency-doloar-price">
                                                    @php
                                                        $volume_info=market_cap_volume($currency['volume']);
                                                    @endphp
                                                    @if($volume_info[0] != 0)
                                                        <h5>{{$volume_info[0]}} {{$volume_info[1]}}</h5>
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="price-chart-table">
                                                    <canvas id="{{$currency['currency']}}_chart" width="95"
                                                            height="30"></canvas>
                                                    <script type="text/javascript">
                                                        setTimeout(function () {
                                                            chart("{{$currency['currency']}}_chart", "{{$currency['color']}}");
                                                        }, 1000);
                                                    </script>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="d-flex items-center justify-between">
                            <h3 class="table-bottom-title">
                                Visit the <a href="{{route('landing.prices')}}">prices
                                    page</a> to see the
                                table of all
                                cryptocurrencies.
                            </h3>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            @if ($timer_deposit->timer_deposit == 0)
            $('#modalChargeWallet').modal('show');
            @endif

            function makeTimer() {

                //		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");
                let endTime = new Date(`{{$expire_date}}`);
                endTime = (Date.parse(endTime) / 1000);
                let usdt_amount={{$timer_deposit->timer_deposit}};
                let dirham_amount={{$timer_deposit->timer_deposit/$dirham_price['AED']['price']}};
                let now = new Date();
                now = (Date.parse(now) / 1000);

                let timeLeft = endTime - now;

                let days = Math.floor(timeLeft / 86400);
                let hours = Math.floor((timeLeft - (days * 86400)) / 3600);
                let minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60);
                let seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

                if (hours < "10") {
                    hours = "0" + hours;
                }
                if (minutes < "10") {
                    minutes = "0" + minutes;
                }
                if (seconds < "10") {
                    seconds = "0" + seconds;
                }

                $("#days").html(`${days} <span>D </span>`);
                $("#hours").html(`${hours} <span>H </span>`);
                $("#minutes").html(`${minutes} <span>M </span>`);
                $("#seconds").html(`${seconds} <span>S</span>`);
                $("#text2").html(`<sapn>time to pay ${parseFloat(dirham_amount).toPrecision(3)} DIRHAMS to get ${usdt_amount} USDT </span>`);

            }

            @if ($timer_deposit->timer_deposit != 0 && $timer_deposit->timer_deposit != -1)
            setInterval(function () {
                makeTimer();
            }, 1000);
            @endif

            $('#amount').on('keyup', function () {
                let dirham = $(this).val() / {{$dirham_price['AED']['price']}};
                $('#calculate').text(`you should pay ${numberFormat(parseFloat(dirham).toPrecision(3),3)}  dirhams`)
            })

            $(".tradingview-widget-container").each(function () {
                $(this).on('click', function () {
                    window.location.href = $(this).data('href');
                })
            });
        })

        @if ($result['usdt'] > 0 || $result['usdt_asset'] > 0)
        const chart_data = {
            assets_chart: {
                'usdt': '{{ $result['usdt'] * 100/($result['usdt']+$result['usdt_asset'])}}',
                'currency': '{{ $result['usdt_asset'] * 100/($result['usdt']+$result['usdt_asset']) }}',
            }
        }
        @endif
    </script>
    <script src="{{asset('theme/user/scripts/dashboard-charts.js')}}"></script>
    <script src="{{asset('theme/user/scripts/currency_chart.js')}}"></script>
@endsection
