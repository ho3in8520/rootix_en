@extends('templates.user.master_page')
@section('title_browser')
    Bank Accounts-Rootix Exchange
@endsection
@section('content_class','bank-content')
@section('')
    <div style="min-height: 100vh" class="d-flex flex-column justify-center">
        @endsection
        @section('content')
            <section class="wallet-cart pt-2">
                <div class="container">
                    <div class="bank-header">
                        <h1 class="dashboard-title bank-title desktop-title">Bank Accounts</h1>

                        @can('bank_create')
                            <a href="{{route('banks.create')}}" class="create-new-bank">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18.111"
                                    height="18.111"
                                    viewBox="0 0 18.111 18.111"
                                >
                                    <g
                                        id="Icon_feather-plus"
                                        data-name="Icon feather-plus"
                                        transform="translate(-6.5 -6.5)"
                                    >
                                        <path
                                            id="Path_8591"
                                            data-name="Path 8591"
                                            d="M18,7.5V23.611"
                                            transform="translate(-2.444)"
                                            fill="none"
                                            stroke="#fff"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="2"
                                        />
                                        <path
                                            id="Path_8592"
                                            data-name="Path 8592"
                                            d="M7.5,18H23.611"
                                            transform="translate(0 -2.444)"
                                            fill="none"
                                            stroke="#fff"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="2"
                                        />
                                    </g>
                                </svg>

                                Create Bank
                            </a>
                        @endcan
                    </div>

                    <div>
                        <div class="dashboard-table">
                            <div class="row">
                                <div class="col-12">
                                    <div class="overflow-auto">
                                        <div class="wallet-table-container">
                                            <table class="wallet-table bank-table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div>
                                                            Name

                                                            <button class="sort-table-btn">
                                                                <span
                                                                    class="icon-Icon-material-unfold-more table-arrow"></span>
                                                            </button>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            Card Number

                                                            <button class="sort-table-btn">
                                                                <span
                                                                    class="icon-Icon-material-unfold-more table-arrow"></span>
                                                            </button>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            Account Number

                                                            <button class="sort-table-btn">
                                                                <span
                                                                    class="icon-Icon-material-unfold-more table-arrow"></span>
                                                            </button>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            Sheba Number

                                                            <button class="sort-table-btn">
                                                                <span
                                                                    class="icon-Icon-material-unfold-more table-arrow"></span>
                                                            </button>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>
                                                            Status

                                                            <button class="sort-table-btn">
                                                                <span
                                                                    class="icon-Icon-material-unfold-more table-arrow"></span>
                                                            </button>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>
                                                            Create Time

                                                            <button class="sort-table-btn">
                                                                <span
                                                                    class="icon-Icon-material-unfold-more table-arrow"></span>
                                                            </button>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>Features</div>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @if(count($banks)>0)
                                                    @foreach($banks as $bank)
                                                        @php
                                                            $bank_logo=get_bank_logo($bank->name);
                                                        @endphp
                                                        <tr>
                                                            <td>
                                                                <div class="table-bank-title">
                                                                    <img width="59px" height="62px"
                                                                         src="{{asset("theme/user/images/bank-details/$bank_logo.svg")}}"
                                                                         alt="{{$bank_logo}}_logo"
                                                                    />

                                                                    <span class="table-bank-title__name"
                                                                    >Bank  {{$bank->name}}</span
                                                                    >
                                                                </div>
                                                            </td>

                                                            <td>{{$bank->card_number}}</td>

                                                            <td>{{$bank->account_number}}</td>

                                                            <td>{{$bank->sheba_number}}</td>

                                                            <td>
                                                                <span
                                                                    class="{{$bank->status == 1?'plus':'minus'}}">{{$bank->status == 1?'Accepted':'Rejected'}}</span>
                                                            </td>

                                                            <td>{{jdate_from_gregorian($bank->created_at,'%d %B %Y')}}</td>

                                                            <td>
                                                                @can('bank_edit')
                                                                    <a type="submit"
                                                                       href="{{route('banks.edit',$bank)}}"
                                                                       class="badge badge-success text-white p-2">
                                                                        Edit
                                                                    </a>
                                                                @endcan
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="7">No bank accounts have been registered for you.
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
@endsection
