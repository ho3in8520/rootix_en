@extends('templates.user.master_page')
@section('style')
    <link rel="stylesheet" href="{{asset('theme/user/assets/css/style.css')}}"/>
@endsection
@section('class-head-container','container-fluid')
@section('content')
    <div class="container-fluid mtb15 no-fluid">
        <div class="row sm-gutters">
            <div class="col-md-3">
                <livewire:user.exchange.trad.ticker :markets="$markets"></livewire:user.exchange.trad.ticker>
            </div>
            <div class="col-md-6">
                <div class="main-chart mb15">
                    <!-- TradingView Widget BEGIN -->
                    <div class="tradingview-widget-container">
                        <div id="tradingview_e8053"></div>
                        <script src="https://s3.tradingview.com/tv.js"></script>
                        <script>
                            new TradingView.widget({
                                width: "100%",
                                height: 550,
                                symbol: "{{ strtoupper(implode('',$markets)) }}",
                                interval: "D",
                                timezone: "Etc/UTC",
                                theme: "Light",
                                style: "1",
                                locale: "en",
                                toolbar_bg: "#f1f3f6",
                                enable_publishing: false,
                                withdateranges: true,
                                hide_side_toolbar: false,
                                allow_symbol_change: true,
                                show_popup_button: true,
                                popup_width: "1000",
                                popup_height: "650",
                                container_id: "tradingview_e8053",
                            });
                        </script>
                    </div>
                    <!-- TradingView Widget END -->
                </div>
                <div class="market-trade">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                data-toggle="pill"
                                href="#pills-trade-limit"
                                role="tab"
                                aria-selected="true"
                            >قیمت دلخواه</a
                            >
                        </li>
                        {{--                        <li class="nav-item">--}}
                        {{--                            <a--}}
                        {{--                                class="nav-link"--}}
                        {{--                                data-toggle="pill"--}}
                        {{--                                href="#pills-market"--}}
                        {{--                                role="tab"--}}
                        {{--                                aria-selected="false"--}}
                        {{--                            >قیمت بازار</a--}}
                        {{--                            >--}}
                        {{--                        </li>--}}
                        {{--                        <li class="nav-item">--}}
                        {{--                            <a--}}
                        {{--                                class="nav-link"--}}
                        {{--                                data-toggle="pill"--}}
                        {{--                                href="#pills-stop-limit"--}}
                        {{--                                role="tab"--}}
                        {{--                                aria-selected="false"--}}
                        {{--                            >استاپ قیمت دلخواه</a--}}
                        {{--                            >--}}
                        {{--                        </li>--}}

                        {{--                        <li class="nav-item">--}}
                        {{--                            <a--}}
                        {{--                                class="nav-link"--}}
                        {{--                                data-toggle="pill"--}}
                        {{--                                href="#pills-stop-market"--}}
                        {{--                                role="tab"--}}
                        {{--                                aria-selected="false"--}}
                        {{--                            >استاپ قیمت بازار</a--}}
                        {{--                            >--}}
                        {{--                        </li>--}}
                    </ul>
                    <div class="tab-content">
                        <div
                            class="tab-pane fade show active"
                            id="pills-trade-limit"
                            role="tabpanel"
                        >
                            <div class="d-flex justify-content-between">
                                <div class="market-trade-buy">
                                    <form action="{{route('exchange.new-trade')}}" method="post">
                                        <input type="hidden" name="type" value="buy">
                                        <input type="hidden" name="market" value="{{$markets['des']}}{{$markets['source']}}">
                                        @csrf
                                        <div class="input-group">
                                            <input name="amount"
                                                type="number"
                                                class="form-control"
                                                placeholder="Amount"
                                            />
                                            <div class="input-group-append">
                                                <span class="input-group-text">{{strtoupper($markets['des'])}}</span>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <input id="source_amount" name="price"
                                                   type="text"
                                                   class="form-control"
                                                   placeholder="Price"
                                            />
                                            <div class="input-group-append">
                                                <span class="input-group-text">{{strtoupper($markets['source'])}}</span>
                                            </div>
                                        </div>
                                        <ul id="buy" class="market-trade-list">
                                            <li>
                                                <button type="button">25%</button>
                                            </li>
                                            <li>
                                                <button type="button">50%</button>
                                            </li>
                                            <li>
                                                <button type="button">75%</button>
                                            </li>
                                            <li>
                                                <button type="button">100%</button>
                                            </li>
                                        </ul>
                                        <div class="d-flex align-items-center justify-content-between market-caption">
                                            <p>دارایی موجود</p>
                                            <span>{{strtoupper($markets['des'])}} = {{$markets_value['des_amount']}}</span>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between market-caption">
                                            <p>دارایی موجود:</p>
                                            <span>{{strtoupper($markets['source'])}} = {{$markets_value['source_amount']}}</span>
                                        </div>
                                        {{--                                    <div class="d-flex align-items-center justify-content-between market-caption">--}}
                                        {{--                                        <p>Margin:</p>--}}
                                        {{--                                        <span>0 BTC = 0 USD</span>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="d-flex align-items-center justify-content-between market-caption">--}}
                                        {{--                                        <p>قیمت:</p>--}}
                                        {{--                                        <span>0 BTC = 0 USD</span>--}}
                                        {{--                                    </div>--}}
                                        <button  class="btn buy ajaxStore">خرید</button>
                                    </form>
                                </div>
                                <div class="market-trade-sell">
                                    <form action="{{route('exchange.new-trade')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="type" value="sell">
                                        <input type="hidden" name="des_asset" value="{{$markets['des']}}">
                                        <input type="hidden" name="market" value="{{$markets['des']}}{{$markets['source']}}">
                                        <div class="input-group">
                                            <input name="price"
                                                type="number"
                                                class="form-control"
                                                placeholder="Price"
                                            />
                                            <div class="input-group-append">
                                                <span class="input-group-text">{{strtoupper($markets['source'])}}</span>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <input id="des_amount" name="amount"
                                                   type="number"
                                                   class="form-control"
                                                   placeholder="Amount"
                                            />
                                            <div class="input-group-append">
                                                <span class="input-group-text">{{strtoupper($markets['des'])}}</span>
                                            </div>
                                        </div>
                                        <ul id="sell" class="market-trade-list">
                                            <li>
                                                <button type="button">25%</button>
                                            </li>
                                            <li>
                                                <button type="button">50%</button>
                                            </li>
                                            <li>
                                                <button type="button">75%</button>
                                            </li>
                                            <li>
                                                <button type="button">100%</button>
                                            </li>
                                        </ul>
                                        <div class="d-flex align-items-center justify-content-between market-caption">
                                            <p>دارایی موجود</p>
                                            <span>{{strtoupper($markets['des'])}} = {{$markets_value['des_amount']}}</span>
                                        </div>

                                        <div class="d-flex align-items-center justify-content-between market-caption">
                                            <p>دارایی موجود:</p>
                                            <span>{{strtoupper($markets['source'])}} = {{$markets_value['source_amount']}}</span>
                                        </div>
                                        {{--                                    <div class="d-flex align-items-center justify-content-between market-caption">--}}
                                        {{--                                        <p>Margin:</p>--}}
                                        {{--                                        <span>0 BTC = 0 USD</span>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="d-flex align-items-center justify-content-between market-caption">--}}
                                        {{--                                        <p>قیمت:</p>--}}
                                        {{--                                        <span>0 BTC = 0 USD</span>--}}
                                        {{--                                    </div>--}}
                                        <button class="btn sell ajaxStore">فروش</button>
                                    </form>

                                    <form action="{{route('exchange.cancel-trade',60393271077)}}" method="get">
                                        @csrf
                                        <button type="submit">cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{--                        <div--}}
                        {{--                            class="tab-pane fade"--}}
                        {{--                            id="pills-market"--}}
                        {{--                            role="tabpanel"--}}
                        {{--                        >--}}
                        {{--                            <div class="d-flex justify-content-between">--}}
                        {{--                                <div class="market-trade-buy">--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Price"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">BTC</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Amount"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">ETH</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <ul class="market-trade-list">--}}
                        {{--                                        <li><a href="#!">25%</a></li>--}}
                        {{--                                        <li><a href="#!">50%</a></li>--}}
                        {{--                                        <li><a href="#!">75%</a></li>--}}
                        {{--                                        <li><a href="#!">100%</a></li>--}}
                        {{--                                    </ul>--}}
                        {{--                                    <p>Available: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>حجم: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>Margin: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>قیمت: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <button class="btn buy">خرید</button>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="market-trade-sell">--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Price"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">BTC</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Amount"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">ETH</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <ul class="market-trade-list">--}}
                        {{--                                        <li><a href="#!">25%</a></li>--}}
                        {{--                                        <li><a href="#!">50%</a></li>--}}
                        {{--                                        <li><a href="#!">75%</a></li>--}}
                        {{--                                        <li><a href="#!">100%</a></li>--}}
                        {{--                                    </ul>--}}
                        {{--                                    <p>Available: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>حجم: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>Margin: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>قیمت: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <button class="btn sell">فروش</button>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div--}}
                        {{--                            class="tab-pane fade"--}}
                        {{--                            id="pills-stop-limit"--}}
                        {{--                            role="tabpanel"--}}
                        {{--                        >--}}
                        {{--                            <div class="d-flex justify-content-between">--}}
                        {{--                                <div class="market-trade-buy">--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Price"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">BTC</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Amount"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">ETH</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <ul class="market-trade-list">--}}
                        {{--                                        <li><a href="#!">25%</a></li>--}}
                        {{--                                        <li><a href="#!">50%</a></li>--}}
                        {{--                                        <li><a href="#!">75%</a></li>--}}
                        {{--                                        <li><a href="#!">100%</a></li>--}}
                        {{--                                    </ul>--}}
                        {{--                                    <p>Available: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>حجم: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>Margin: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>قیمت: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <button class="btn buy">خرید</button>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="market-trade-sell">--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Price"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">BTC</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Amount"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">ETH</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <ul class="market-trade-list">--}}
                        {{--                                        <li><a href="#!">25%</a></li>--}}
                        {{--                                        <li><a href="#!">50%</a></li>--}}
                        {{--                                        <li><a href="#!">75%</a></li>--}}
                        {{--                                        <li><a href="#!">100%</a></li>--}}
                        {{--                                    </ul>--}}
                        {{--                                    <p>Available: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>حجم: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>Margin: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>قیمت: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <button class="btn sell">فروش</button>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div--}}
                        {{--                            class="tab-pane fade"--}}
                        {{--                            id="pills-stop-market"--}}
                        {{--                            role="tabpanel"--}}
                        {{--                        >--}}
                        {{--                            <div class="d-flex justify-content-between">--}}
                        {{--                                <div class="market-trade-buy">--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Price"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">BTC</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Amount"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">ETH</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <ul class="market-trade-list">--}}
                        {{--                                        <li><a href="#!">25%</a></li>--}}
                        {{--                                        <li><a href="#!">50%</a></li>--}}
                        {{--                                        <li><a href="#!">75%</a></li>--}}
                        {{--                                        <li><a href="#!">100%</a></li>--}}
                        {{--                                    </ul>--}}
                        {{--                                    <p>Available: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>حجم: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>Margin: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>قیمت: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <button class="btn buy">خرید</button>--}}
                        {{--                                </div>--}}
                        {{--                                <div class="market-trade-sell">--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Price"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">BTC</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="input-group">--}}
                        {{--                                        <input--}}
                        {{--                                            type="number"--}}
                        {{--                                            class="form-control"--}}
                        {{--                                            placeholder="Amount"--}}
                        {{--                                        />--}}
                        {{--                                        <div class="input-group-append">--}}
                        {{--                                            <span class="input-group-text">ETH</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <ul class="market-trade-list">--}}
                        {{--                                        <li><a href="#!">25%</a></li>--}}
                        {{--                                        <li><a href="#!">50%</a></li>--}}
                        {{--                                        <li><a href="#!">75%</a></li>--}}
                        {{--                                        <li><a href="#!">100%</a></li>--}}
                        {{--                                    </ul>--}}
                        {{--                                    <p>Available: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>حجم: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>Margin: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <p>قیمت: <span>0 BTC = 0 USD</span></p>--}}
                        {{--                                    <button class="btn sell">فروش</button>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="order-book mb15">
                    <h2 class="heading">لیست سفارشات</h2>
                    <livewire:user.exchange.trad.list-request
                        :markets="$markets"></livewire:user.exchange.trad.list-request>
                </div>
                <div class="market-history">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                data-toggle="pill"
                                href="#recent-trades"
                                role="tab"
                                aria-selected="true"
                            >آخرین خرید و فروش ها</a
                            >
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="pill"
                                href="#market-depth"
                                role="tab"
                                aria-selected="false"
                            >حجم بازار</a
                            >
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div
                            class="tab-pane fade show active"
                            id="recent-trades"
                            role="tabpanel"
                        >
                            <livewire:user.exchange.trad.last-transaction
                                :markets="$markets"></livewire:user.exchange.trad.last-transaction>
                        </div>
                        <div
                            class="tab-pane fade"
                            id="market-depth"
                            role="tabpanel"
                        >
                            <div class="depth-chart-container">
                                <div class="depth-chart-inner">
                                    <div id="lightDepthChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="market-news mt15">
                    <h2 class="heading">Market News</h2>
                    <ul>
                        <li>
                            <a href="#!">
                                <strong>KCS Pay Fees Feature is Coming Soon</strong>
                                To accelerate the ecosystem construction of KuCoin Share
                                (KCS) and promote the implementation of the KCS
                                application.
                                <span>2019-12-04 20:22</span>
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                <strong>KCS Pay Fees Feature is Coming Soon</strong>
                                To accelerate the ecosystem construction of KuCoin Share
                                (KCS) and promote the implementation of the KCS
                                application.
                                <span>2019-12-04 20:22</span>
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                <strong>KCS Pay Fees Feature is Coming Soon</strong>
                                To accelerate the ecosystem construction of KuCoin Share
                                (KCS) and promote the implementation of the KCS
                                application.
                                <span>2019-12-04 20:22</span>
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                <strong>KCS Pay Fees Feature is Coming Soon</strong>
                                To accelerate the ecosystem construction of KuCoin Share
                                (KCS) and promote the implementation of the KCS
                                application.
                                <span>2019-12-04 20:22</span>
                            </a>
                        </li>
                        <li>
                            <a href="#!">
                                <strong>KCS Pay Fees Feature is Coming Soon</strong>
                                To accelerate the ecosystem construction of KuCoin Share
                                (KCS) and promote the implementation of the KCS
                                application.
                                <span>2019-12-04 20:22</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <livewire:user.exchange.trad.list-orders
                :markets="$markets"></livewire:user.exchange.trad.list-orders>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('theme/user/assets/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('theme/user/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('theme/user/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('theme/user/assets/js/amcharts-core.min.js')}}"></script>
    <script src="{{asset('theme/user/assets/js/amcharts.min.js')}}"></script>
{{--    <script src="{{asset('theme/user/assets/js/jquery.mCustomScrollbar.js')}}"></script>--}}
    <script src="{{asset('theme/user/assets/js/custom.js')}}"></script>
    <script>
        $("tbody, .market-news ul").mCustomScrollbar({
            theme: "minimal",
        });

        $('#source_amount').on('keyup', function () {
            $("#buy li button").css('background-color', '#eff2f6');
            $("#buy li button").css('color', '4a4a4a');
        })
        $('#des_amount').on('keyup', function () {
            $("#sell li button").css('background-color', '#eff2f6');
            $("#sell li button").css('color', '4a4a4a');
        })

        $('#buy li').on('click', function () {
            let pct = $(this).text();
            let total_usdt = {!! $markets_value['source_amount'] !!};
            console.log(typeof total_usdt)
            let cal_usdt = calculate_usdt(total_usdt, pct, 'buy');
            $('#source_amount').val(cal_usdt);
        });

        $('#sell li').on('click', function () {
            let pct = $(this).text();
            let total_des = {!! $markets_value['des_amount'] !!};
            let cal_des = calculate_usdt(total_des, pct, 'sell');
            $('#des_amount').val(cal_des);
        });

        function calculate_usdt(value, pct, elm_id) {
            switch (pct.trim()) {
                case "25%":
                    change_pct_style(1, elm_id);
                    return value / 4;
                case '50%':
                    change_pct_style(2, elm_id);
                    return value / 2;
                case '75%':
                    change_pct_style(3, elm_id);
                    return value * 3 / 4;
                case '100%':
                    change_pct_style(4, elm_id);
                    return value / 1;
                default:
                    alert('pct id is ' + pct);
                    break;
            }
        }

        function change_pct_style(num, elm_id) {
            let background_color = elm_id === 'buy' ? '#26a69a' : '#ff231f';
            $("#" + elm_id + " li button").css('background-color', '#eff2f6');
            $("#" + elm_id + " li button").css('color', '4a4a4a');
            for (let j = 1; j <= num; j++) {
                $("#" + elm_id + " li:nth-child(" + j + ") button").css('background-color', background_color);
                $("#" + elm_id + " li:nth-child(" + j + ") button").css('color', '#fff');
            }
        }
    </script>
@endsection
