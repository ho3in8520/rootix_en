@extends('templates.user.master_page')
@section('title_browser')
    Transactions-Rootix Exchange
@endsection
@section('content_class','bank-content')
@section('')
    <div style="min-height: 100vh" class="d-flex flex-column justify-center">
        @endsection
        @section('content')
            <section id="basic-form-layouts" class="wallet-cart pt-2">
                <div class="container">
                    <h1 class="dashboard-title bank-title transaction-subtitle desktop-title mb-4">
                        Transactions
                    </h1>
                    <div>
                        <div class="dashboard-table transaction-table-container">
                            <form action="" method="get">
                                @csrf
                                <div class="d-flex align-items-end justify-between header-transaction mb-5">
                                    <div class="selected-transaction d-flex">
                                        <div class="w-100 bg-white">
                                            <span class="mb-2 d-block transaction__filter-title">Deposit /Withdraw</span>
                                            <select id="type" name='type'>
                                                <option value="">All</option>
                                                <option value="1" {{ request()->type=='1'?'selected':'' }}>Withdraw
                                                </option>
                                                <option value="2" {{ request()->type=='2'?'selected':'' }}>Deposit
                                                </option>
                                            </select>
                                        </div>
                                        <div class="w-100 ml-2 bg-white">
                                            <span class="mb-2 d-block transaction__filter-title">Currecny</span>
                                            <select id="unit" name="unit">
                                                <option value="">All</option>
                                                @foreach($currencies as $currency)
                                                    <option value="{{$currency}}" {{ request()->unit==$currency?'selected':'' }}>
                                                        {{$currency}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="volume-transaction bg-white">
                                        <span class="mb-2 d-block transaction__filter-title">Amount</span>

                                        <div class="d-flex">
                                            <input name="amount_from" type="text" placeholder="from"
                                                   class="transaction__value"
                                                   value="{{ request()->amount_from?request()->amount_from:'' }}"/>
                                            <input name="amount_to" type="text" placeholder="to"
                                                   class="transaction__value"
                                                   value="{{ request()->amount_to?request()->amount_to:'' }}"/>
                                        </div>
                                    </div>

                                    <div class="date-transaction bg-white">
                                        <span class="mb-2 d-block transaction__filter-title">Date</span>
                                        <div class="d-flex">
                                            <input id="date" name="date_from" type="date" placeholder="Date from"
                                                   value="{{ request()->date_from?request()->date_from:'' }}" />
                                            <input name="date_to" type="date" placeholder="Date to"
                                                   value="{{ request()->date_to?request()->date_to:'' }}" />
                                        </div>
                                    </div>

                                    <div class="destination-address-input bg-white">
                                        <span class="mb-2 d-block transaction__filter-title">Destination Address</span>
                                        <input type="text" name="destination_address" placeholder="Destination Address..."
                                               value="{{ request()->destination_address?request()->destination_address:'' }}"/>
                                    </div>

                                    <button class="transaction__btn search-ajax">Search</button>
                                </div>
                            </form>
                            <div class="table-loading">
                                <img
                                    src="{{asset('theme/user/images/loader.svg')}}"
                                    class="table-loding-spinner"
                                />
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="overflow-auto">
                                        <div class="wallet-table-container">
                                            <table class="wallet-table bank-table transaction-table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div>Id</div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                            Type
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                            Currency
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                            Amount
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>
                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                            Description
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>
                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                            Destination Address
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>Date</div>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @if(count($transactions) > 0)
                                                    @foreach($transactions as $transaction)
                                                        <tr>
                                                            <td>
                                                                @if($transaction->tracking_code)
                                                                    <span id="tracking_{{$transaction->id}}"
                                                                          class="font-12 mr-2 copy-btn"
                                                                          data-copy="tracking_{{$transaction->id}}"
                                                                          data-content="{{$transaction->tracking_code}}"
                                                                          onclick="copy_text('tracking_{{$transaction->id}}')">
                                                        {!! \Illuminate\Support\Str::limit($transaction->tracking_code,12) !!}
                                                    </span>
                                                                @else
                                                                    <span>-</span>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <div
                                                                    class="{{ $transaction->type==1?'harvest':'deposit' }}-status situation">
                                                                    {{ $transaction->type==1?'Withdraw':'Deposti' }}
                                                                </div>
                                                            </td>

                                                            @if ($transaction->financeable instanceof \App\Models\Asset)
                                                                <td>{{ $transaction->financeable->unit }}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif

                                                            <td>{{ rial_to_unit($transaction->amount,$transaction->financeable->unit,true) }}</td>

                                                            <td>{!! \Illuminate\Support\Str::limit($transaction->description,30) !!}</td>

                                                            <td>
                                                                @if($transaction->extra_field1)
                                                                    <span id="destination_{{$transaction->id}}"
                                                                          class="font-12 mr-2 copy-btn"
                                                                          data-content="{{$transaction->extra_field1}}"
                                                                          data-copy="destination_{{$transaction->id}}"
                                                                          onclick="copy_text('destination_{{$transaction->id}}')">
                                                        {!! \Illuminate\Support\Str::limit($transaction->extra_field1,12) !!}
                                                    </span>
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <p>{{ date_format($transaction->created_at,'m.d.Y') }}</p>
                                                                <p>{{ date_format($transaction->created_at,'H:i:s') }}</p>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        @endsection
        @section('script')

            {{--            <script src="{{ asset('general/js/popup.js') }}"></script>--}}
            <script src="{{asset('theme/user/scripts/selected-box.js?v=1.0.0')}}"></script>

            <script>
                function copy_text(id) {
                    // // alert(id);
                    var copyText = document.getElementById(id);
                    var input = document.createElement("input");
                    input.value = $.trim(copyText.getAttribute('data-content'));
                    document.body.appendChild(input);
                    input.select();
                    document.execCommand("Copy");
                    input.remove();
                    swal('Text Copied!', '', 'success');
                }
                $('#date').on('click',function (){
                    // alert('click here')
                })
            </script>


@endsection
