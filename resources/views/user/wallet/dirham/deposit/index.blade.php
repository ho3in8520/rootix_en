@extends('templates.user.master_page')
@section('title_browser','Dirham Deposit List')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <style>
        table td {
            text-align: right !important;
        }

        table th {
            padding: 20px;
        }
    </style>
@endsection
@section('content')
    <section class="wallet-cart pt-2" id="basic-form-layouts">
        <div class="container">
            <h1 class="dashboard-title bank-title transaction-subtitle desktop-title mb-4">
                Dirham Deposit List
            </h1>
            <div>
                <div class="dashboard-table transaction-table-container">
{{--                    <form method="get" action="">--}}
{{--                        <div class="d-flex align-items-end header-transaction mb-5">--}}
{{--                            <div class="selected-transaction d-flex">--}}
{{--                                <div class="w-100 bg-white">--}}
{{--                                    <span class="mb-2 d-block transaction__filter-title">--}}
{{--                                        نوع--}}
{{--                                    </span>--}}
{{--                                    <select class="custom-selected" name="deposit_type">--}}
{{--                                        <option value="" {{ request()->deposit_type=='' ?' selected':'' }}>همه</option>--}}
{{--                                        <option value="0" {{ request()->deposit_type=='0' ?' selected':'' }}>شتاب</option>--}}
{{--                                        <option value="1" {{ request()->deposit_type=='1' ?' selected':'' }}>پایا</option>--}}
{{--                                        <option value="2" {{ request()->deposit_type=='2' ?' selected':'' }}>ساتنا</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="w-100 mr-2 bg-white">--}}
{{--                                    <span class="mb-2 d-block transaction__filter-title">--}}
{{--                                        وضعیت--}}
{{--                                    </span>--}}
{{--                                    <select class="custom-selected" name="status">--}}
{{--                                        <option value="" {{ request()->status=='' ?' selected':'' }}>همه</option>--}}
{{--                                        <option value="0" {{ request()->status=='0' ?' selected':'' }}>در حال بررسی</option>--}}
{{--                                        <option value="1" {{ request()->status=='1' ?' selected':'' }}>واریز شده</option>--}}
{{--                                        <option value="2" {{ request()->status=='2' ?' selected':'' }}>رد شده</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="destination-address-input bg-white">--}}
{{--                                <span class="mb-2 d-block transaction__filter-title">--}}
{{--                                    کد پیگیری--}}
{{--                                </span>--}}
{{--                                <input class="address" name="tracking_code" type="text" placeholder="کد پیگیری" value="{{ request()->tracking_code }}">--}}
{{--                            </div>--}}
{{--                            <div class="volume-transaction bg-white">--}}
{{--                                <span class="mb-2 d-block transaction__filter-title">--}}
{{--                                    مقدار--}}
{{--                                </span>--}}
{{--                                <div class="d-flex">--}}
{{--                                    <input type="text" placeholder="مقدار از" name="start_amount"--}}
{{--                                           value="{{ request()->start_amount }}"--}}
{{--                                           class="transaction__value-right">--}}
{{--                                    <input type="text" placeholder="مقدار تا" name="end_amount"--}}
{{--                                           value="{{ request()->end_amount }}"--}}
{{--                                           class="transaction__value-left">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="date-transaction bg-white">--}}
{{--                                <span class="mb-2 d-block transaction__filter-title">--}}
{{--                                    تاریخ واریز--}}
{{--                                </span>--}}
{{--                                <div class="d-flex">--}}
{{--                                    <input type="text" placeholder="از تاریخ" name="start_deposit_date"--}}
{{--                                           value="{{ request()->start_deposit_date }}"--}}
{{--                                           onfocus="(this.type='date')"--}}
{{--                                           class="date-right">--}}
{{--                                    <input type="text" placeholder="تا تاریخ" name="end_deposit_date"--}}
{{--                                           value="{{ request()->end_deposit_date }}"--}}
{{--                                           onfocus="(this.type='date')"--}}
{{--                                           class="date-left">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <button class="transaction__btn search-ajax">جستجو</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
                    <div class="table-loading">
                        <img src="{{ asset('theme/user/images/wallet/spinner.svg') }}" class="table-loding-spinner">
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="overflow-auto">
                                <div class="wallet-table-container">
                                    <table class="wallet-table bank-table transaction-table text-right">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Amount</th>
                                            <th>Payment mode</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($transactions) > 0)
                                            @foreach($transactions as $key=>$transaction)
                                                @php
                                                $status_info=[];
                                                switch ($transaction->status){
                                                    case 0:
                                                        $status_info['text']='pending';
                                                        $status_info['color']='info';
                                                        break;
                                                    case 1:
                                                        $status_info['text']='confirmed';
                                                        $status_info['color']='success';
                                                        break;
                                                    case 2:
                                                        $status_info['text']='rejected';
                                                        $status_info['color']='danger';
                                                        break;
                                                }
                                                @endphp
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $transaction->amount }}</td>
                                                    <td>{{ $transaction->mode }}</td>
                                                    <td>
                                                        <span style="padding: 0.5rem 0.5rem 0.3rem 0.5rem; font-size: 0.7rem; border-radius: 0.25rem"
                                                            class=" badge-{{ $status_info['color'] }}">{{ $status_info['text'] }}</span>
                                                        @if ($transaction->status == 2)
                                                            <span
                                                                class='attention animate__animated animate__flash animate__infinite infinite animate__slower 3s'
                                                                data-toggle='tooltip'
                                                                title='{{ $transaction->reject_reason }}'>!</span>
                                                        @endif
                                                    </td>
                                                    <td dir="ltr">{{ $transaction->created_at }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="100" class="text-center">Nothing to show!</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {{ $transactions->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/selected-box.js') }}"></script>
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
