@extends('templates.user.master_page')
@section('title_browser','Dirhams Deposit
')
@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <!-- Modal Pay With Cash-->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="modal fade mt-5" id="modalCash" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-body border-0">
                                <h3 class="text-center my-3">Pay with cash information : </h3>
                                <p class="my-4">
                                    Dear user
                                    You can recharge your dirham wallet in person by referring to the following
                                    addresses:
                                </p>
                                <p>1- Dubai.port saeed road .alkhlafi building .shop no 11.bestdreams travel -
                                    042272221 - Reza</p>
                                <br>
                                <p>2- dubai business bay DBC tower - office 703 - +971505431096 - Abdullah</p>
                            </div>
                            <div class="modal-footer border-0 justify-content-center">
                                <button data-dismiss="modal"
                                        class="btn signin-btn deposit-btn ajaxStore">
                                    Ok
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Modal Pay With Cash-->

            <!-- Modal Pay With Bank-->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="modal fade mt-5" id="modalBank" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            coming soon....
{{--                            <form method="post"--}}
{{--                                  action="{{ route("wallet.deposit.aed.store") }}">--}}
{{--                                <input type="hidden" value="bank" name="mode">--}}
{{--                                @csrf--}}
{{--                                <div class="modal-body border-0">--}}
{{--                                    <h3 class="text-center my-3">Bank Information</h3>--}}
{{--                                    <p class="my-4">--}}
{{--                                        Admin explanation here .........--}}

{{--                                    </p>--}}
{{--                                </div>--}}
{{--                                <div class="row col-12 justify-center mb-4">--}}
{{--                                    <div class="col-5">--}}
{{--                                        <div dir="ltr"--}}
{{--                                             class=" request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active">--}}

{{--                                            <span class="request-box__title mr-3">amount</span>--}}

{{--                                            <input type="text" dir="ltr" class="currency-value-input"--}}
{{--                                                   name="amount"--}}
{{--                                                   id="currency-value-input-1" placeholder="Please enter amount here">--}}
{{--                                        </div>--}}
{{--                                        <div class="card image-infomation">--}}
{{--                                            <div class="card-body">--}}
{{--                                                <label>Upload Payment File Here</label>--}}
{{--                                                <div id="deleteImage">--}}
{{--                                                </div>--}}
{{--                                                <img title="payment image"--}}
{{--                                                     style="cursor:pointer;width:100%;height: 172px" id="blah"--}}
{{--                                                     src="{{ asset('theme/user/assets/img/no-image.png') }}"--}}
{{--                                                     alt="your image" height="172">--}}
{{--                                                <input accept="image/jpeg" name="file" class="imgInp d-none"--}}
{{--                                                       type="file"--}}
{{--                                                       style="display: none">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="modal-footer border-0 justify-content-center">--}}
{{--                                    <button--}}
{{--                                        class="btn signin-btn deposit-btn ajaxStore">--}}
{{--                                        Ok--}}
{{--                                    </button>--}}

{{--                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">--}}
{{--                                        Cancel--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </form>--}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Modal Pay With Bank-->

            <h1 class="dashboard-title bank-title desktop-title">Dirhams Deposit</h1>
            <div class="panel-box real-box-deposit">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="justify-content-center m-3">
                            <h4>Choose Deposit Methode : </h4>
                            <button type="button" class="create-new-bank m-3 d-inline" id="cash-btn" data-toggle="modal"
                                    data-target="#modalCash">Pay with cash
                            </button>
                            <button type="button" class="create-new-bank m-3 d-inline" id="bank-btn" data-toggle="modal"
                                    disabled
                                    data-target="#modalBank">Pay with bank
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    // $('#blah').attr('src', e.target.result);
                    $(input).closest('.card-body').find("img").attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $(this).closest('.card-body').find("#deleteImage").html("<i id='deleteImageUpload' title=\"delete\" class=\"fa fa-trash text-danger\"></i>")
        });
        $(document).on('click', '#blah', function () {
            $(this).closest('.card-body').find("input").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $(this).closest('.card-body').find(".imgInp").val('')
            $(this).closest('.card-body').find("#blah").attr('src', "{{ asset('theme/user/assets/img/no-image.png') }}");
            $(this).closest('.card-body').find("#deleteImage").html('')
        });
    </script>
@endsection
