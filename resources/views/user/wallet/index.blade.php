@extends('templates.user.master_page')
@section('title_browser','Wallet-Rootix Exchange')
@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <h1 class="dashboard-title desktop-title">Wallet</h1>
            <div class="row">
                @foreach([$list_currencies['BTC'],$list_currencies['ETH'],$list_currencies['ADA'],$list_currencies['BNB']] as $item)
                    @php $currency_1d= $item['1d']; @endphp
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="cart-currency-2 cart-currency-3">
                            <div class="cart-currency" style="border-color: {{ $item['color'] }}">
                                <div class="d-flex cart-currency__info justify-between">
                                    <div class="cart-currency__about-price">
                                        <p>{{ num_format($item['price'],contractUnit('usdt')['precision']) }}$</p>
                                        @if (!is_null($currency_1d))
                                            <span
                                                class=" {{ $currency_1d*100 >= 0?'green':'red' }}-currency"
                                                dir="ltr">
                                                {{ $currency_1d > 0 ? '+':''}}{{ num_format($currency_1d,2) }}
                                            </span>
                                        @endif
                                    </div>

                                    <div class="cart-currency__about-name d-flex items-center ">
                                        <div class="cart-currency__names d-flex flex-column items-end">
                                            <h5 class="cart-currency__abbreviation">{{ $item['currency'] }}</h5>
                                            <span class="cart-currency__name">{{ $item['name'] }}</span>
                                        </div>
                                        <img src="{{ $item['logo_url'] }}" alt="{{ $item['name_fa'] }}"
                                             class="cart-currency__img"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="dashboard-chart distance__box">
        <div class="container">
            <div class="row">
                <!-- Transactions List -->
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="panel-box recent-transactions">
                        <div
                            class="d-flex justify-between items-center recent-transactions__title">
                            <h2 class="market-volume-title">Recent transactions</h2>
                            <a href="{{ route('transactions.index') }}"> All transactions </a>
                        </div>

                        <div>
                            @foreach($transactions as $item)
                                @php $unit= $item->financeable->unit=='rial'?'RLS':strtoupper($item->financeable->unit); @endphp
                                <div class="d-flex justify-between recent-transactions__content items-center">
                                    <div class="d-flex items-center recent-transactions__desc">
                                        <img src="{{ $list_currencies[$unit]['logo_url']??asset("theme/landing/images/currency/dirham.png") }}"
                                             class="recent-transactions__img mx-3"
                                             alt="{{ $list_currencies[$unit]['name'] }}"/>
                                        <div>
                                            <h5>{{ $item->financeable->name }}</h5>
                                            <span>
                                                wallet {{ $unit=='RLS'?'Rial':'cryptocurrency' }}-
                                                {{ $item->type=='1'?'Withdraw':'Deposit' }}
                                            </span>
                                        </div>
                                    </div>

                                    <div
                                        class="recent-transactions__name d-flex flex-column items-end">
                                        <h5>{{ rial_to_unit($item->amount,$unit,true) }}</h5>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>  <!--## Transactions List -->

                <div class="col-12 col-md-6 col-lg-6">
                    <div class="panel-box distance__box-responsive-768">
                        <h2 class="market-volume-title">Cryptocurrency asset</h2>
                        @if (count($crypto_chart) > 0)
                            <div class="wallet-chart-2-container">
                                <div
                                    class="doughnut-chart doughnut-chart-2 wallet-chart-2"
                                    dir="ltr"
                                    style="--crypto-chart-color: {{ $property_crypto_chart>0?'#3cceef':'#ef3c3c' }}"
                                    user-property=" {{ $property_crypto_chart>0?"+":'' }}{{ num_format( ($property_crypto_chart/count($crypto_chart)) ,2) }}%">
                                    <canvas id="wallet-assets-chart"></canvas>
                                    <p class="wallet-assets-chart-title text-center">overall change</p>
                                </div>
                            </div>
                        @else
                            <div class="wallet-chart-2-container text-dark">
                                <span>Your currency code assets are zero</span>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="distance__box">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6" style="height: 351px;">
                    <!-- TradingView Widget BEGIN -->
                    <div class="tradingview-widget-container">
                        <div class="tradingview-widget-container__widget"></div>
                        <div style="pointer-events: none">
                            <script type="text/javascript"
                                    src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                    async>
                                {
                                    "symbol"
                                :
                                    "BTCSTUSDT",
                                        "width"
                                :
                                    "100%",
                                        "height"
                                :
                                    "100%",
                                        "locale"
                                :
                                    "en",
                                        "dateRange"
                                :
                                    "12M",
                                        "colorTheme"
                                :
                                    "light",
                                        "trendLineColor"
                                :
                                    "rgba(41, 98, 255, 1)",
                                        "underLineColor"
                                :
                                    "rgba(41, 98, 255, 0.3)",
                                        "underLineBottomColor"
                                :
                                    "rgba(41, 98, 255, 0)",
                                        "isTransparent"
                                :
                                    false,
                                        "autosize"
                                :
                                    true,
                                        "largeChartUrl"
                                :
                                    ""
                                }
                            </script>
                        </div>
                    </div>
                </div>



                <div class="col-12 col-lg-6">
                    <div class="left-wallet-chart distance__box-responsive">
                        <div>
                            <div class="left-wallet-chart-self-top-container" style="height: 157px">
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container" style="pointer-events: none">
                                    <script type="text/javascript"
                                            src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                            async>
                                        {
                                            "symbol"
                                        :
                                            "USDT.D",
                                                "width"
                                        :
                                            "100%",
                                                "height"
                                        :
                                            "100%",
                                                "locale"
                                        :
                                            "en",
                                                "dateRange"
                                        :
                                            "12M",
                                                "colorTheme"
                                        :
                                            "light",
                                                "trendLineColor"
                                        :
                                            "rgb(44,143,3)",
                                                "underLineColor"
                                        :
                                            "rgba(44, 143, 3, 0.3)",
                                                "underLineBottomColor"
                                        :
                                            "rgba(44, 143, 3, 0)",
                                                "isTransparent"
                                        :
                                            false,
                                                "autosize"
                                        :
                                            true,
                                                "largeChartUrl"
                                        :
                                            ""
                                        }
                                    </script>
                                </div>
                                <!-- TradingView Widget END -->
                            </div>
                        </div>
                        <div>
                            <div class="left-wallet-chart-self-top-container" style="height: 157px">
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container" style="pointer-events: none">
                                    <script type="text/javascript"
                                            src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                            async>
                                        {
                                            "symbol"
                                        :
                                            "BTC.D",
                                                "width"
                                        :
                                            "100%",
                                                "height"
                                        :
                                            "100%",
                                                "locale"
                                        :
                                            "en",
                                                "dateRange"
                                        :
                                            "12M",
                                                "colorTheme"
                                        :
                                            "light",
                                                "trendLineColor"
                                        :
                                            "rgb(211,93,0)",
                                                "underLineColor"
                                        :
                                            "rgba(211,93,0, 0.3)",
                                                "underLineBottomColor"
                                        :
                                            "rgba(211,93,0, 0)",
                                                "isTransparent"
                                        :
                                            false,
                                                "autosize"
                                        :
                                            true,
                                                "largeChartUrl"
                                        :
                                            ""
                                        }
                                    </script>
                                </div>
                                <!-- TradingView Widget END -->
                            </div>
                        </div>
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="distance__box">
        <div class="container">
            <div id="wallet-list">
                <x-wallet-list :assets="$assets" :list-currencies="$list_currencies"
                               :fields="['global-price','inventory','24h','deposit','withdraw']"/>
            </div>
        </div>
    </section>
@endsection
@section('script')

    @if ($data['total_asset_usdt'] > 0)
        <script>
            const chart_data = {
                crypto_chart: {
                    label: [],
                    data: [],
                    backgroundColor: [],
                }
            }
        </script>
    @endif
    @foreach ($crypto_chart as $item):
    <script>
        chart_data["crypto_chart"]['label'].push("{{ $item->unit }}");
        chart_data["crypto_chart"]['data'].push({{ convert_currency($item->unit,'usdt',$item->amount,false,$list_currencies) }});
        chart_data["crypto_chart"]['backgroundColor'].push("{{ $list_currencies[strtoupper($item->unit)]["color"] }}");
    </script>
    @endforeach
    <script src="{{ asset('theme/user/scripts/getTableDataRefresh.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/wallet-charts.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/filter-table.js') }}"></script>

    <!-- Refresh Wallet -->
    <script>
        const refresh_wallet = function (response, params) {
            if (response)
                $("#wallet-list").html(response);
        }
        $("#wallet-list").delegate(".table__refresh-btn", 'click', function (e) {
            $(this).parents('.dashboard-table').addClass('active');
            e.preventDefault();
            const form = {
                action: '{{ route('wallet.refresh') }}',
                method: 'post',
                data: {
                    _token: '{{ csrf_token() }}',
                    fields: ['global-price', 'inventory', '24h', 'deposit', 'withdraw'],
                },
                isObject: true,
            };
            ajax(form, refresh_wallet);
        });
    </script>
    <!--## Refresh Wallet -->
@endsection
