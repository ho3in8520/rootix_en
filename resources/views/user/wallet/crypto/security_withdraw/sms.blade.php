<div class="row justify-content-center">
    <div class="col-md-7">
        <p>Please enable your google authenticator by going to settings</p>
    </div>
    <div class="col-md-7 row justify-content-center mt-1">
        <a class="btn btn-success"
           href="{{ route('setting.form') }}">Settings
        </a>
    </div>
</div>

