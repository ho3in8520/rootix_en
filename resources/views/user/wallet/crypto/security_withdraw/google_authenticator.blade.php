<form method="post" action="{{ route('withdraw.store') }}">
    <input type="hidden" value="{{ $token }}" name="token">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="form-group">
            <div class="input-group mt-4" style="margin: unset">
                <label class="m-auto">Authenticator code
                    :</label>
                    <input type="text" class="form-control text-left float-left"
                           id="iconLeft4" name="one_time_password" maxlength="6">
            </div>
                <span class="text-danger pl-5 error-one_time_password"></span>
        </div>
        </div>
        <div class="col-md-7 row justify-content-center mt-1">
            <button class="btn btn-success button-verify"
                    type="button">Submit
            </button>
        </div>
    </div>
</form>
