@extends('templates.user.master_page')
@section('title_browser')
    Ticket Detail-Rootix Exchange
@endsection
@section('style')
    <style>
        .hide {
            display: none;
        }
    </style>
@endsection
@section('content_class','bank-content')

@section('content')
    <section class="mt-5">
        <div class="container">
            <div class="row align-items-start">
                <div class="col-12 col-lg-9 chat__col">
                    <div class="panel-box p-0 d-flex flex-column justify-between" style="min-height: 475px;">
                        <div class="chat-box p-4">
                            @foreach($answers as $answer)
                                @php
                                    $date=$answer->created_at;
                                    if ($date->isToday())
                                        $msg='Today';
                                    elseif ($date->isYesterday())
                                        $msg='Yesterday';
                                    else
                                        $msg='';
                                @endphp
                                <div class="chat-box__chat d-flex dir-{{$answer->type==1?'rtl':'ltr'}}">
                                    <div class="chat-box__user-img">
                                        <img
                                            src="{{$answer->type==1?$answer->user->avatar:asset('theme/landing/images/logo.png')}}"
                                            alt="Customer"/>
                                    </div>
                                    <div class="chat-box__user-info">
                                        @if($answer->files !=null && count($answer->files)>0)
                                            <div>
                                                <div class="chat-box__text">
                                                    <p>
                                                        {{$answer->description}}
                                                    </p>
                                                </div>
                                                <a href="{{getImage($answer->files[0]->path,'download')}}"
                                                   title="Download">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24"
                                                         viewBox="0 0 26.992 30.849">
                                                        <path id="Icon_metro-attachment"
                                                              data-name="Icon metro-attachment"
                                                              d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                                              transform="translate(-3.535 -1.928)"/>
                                                    </svg>
                                                </a>
                                            </div>

                                        @else
                                            <div class="chat-box__text">
                                                <p>
                                                    {{$answer->description}}
                                                </p>
                                            </div>
                                        @endif

                                        <div class="d-flex">
                                            <h5 class="chat-box__name">
                                                {{$answer->type==1?$answer->user->fullname:$answer->ticket_master->admin->fullname}}
                                                ({{$answer->type==1?'Customer':'Support'}})
                                                <span
                                                    class="chat-box__time"> - {{$msg}} {{$msg?date_format($date,'H:i'):date_format($date,'d.m.Y')}} </span>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="panel-box p-4 ticket-info__box">
                        <div class="ticket-info d-flex align-items-center justify-between mb-4">
                            <div class="ticket-info d-flex align-items-center">
                                <div class="mr-2">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="19"
                                        height="13"
                                        viewBox="0 0 23.264 15.842"
                                    >
                                        <path
                                            id="Icon_awesome-ticket-alt"
                                            data-name="Icon awesome-ticket-alt"
                                            d="M4.947,8.211H17.316v7.421H4.947Zm15.461,3.711a1.855,1.855,0,0,0,1.855,1.855v3.711a1.855,1.855,0,0,1-1.855,1.855H1.855A1.855,1.855,0,0,1,0,17.487V13.777a1.855,1.855,0,1,0,0-3.711V6.355A1.855,1.855,0,0,1,1.855,4.5H20.408a1.855,1.855,0,0,1,1.855,1.855v3.711A1.855,1.855,0,0,0,20.408,11.921ZM18.553,7.9a.928.928,0,0,0-.928-.928H4.638a.928.928,0,0,0-.928.928v8.04a.928.928,0,0,0,.928.928H17.625a.928.928,0,0,0,.928-.928Z"
                                            transform="translate(0.5 -4)"
                                            fill="none"
                                            stroke="#000"
                                            stroke-width="1"
                                        />
                                    </svg>
                                </div>
                                <h5 class="ticket-info__title mb-0 mr-2">Ticket information</h5>
                            </div>

                            <span class="icon-Icon-material-expand-more ticket-info__arrow"></span>
                        </div>

                        <h5 class="ticket-info__title mb-4">
                            Topic: {{$ticket->title}}
                        </h5>

                        @php
                            $status=[];
                            switch ($ticket->status){
                                case 1:
                                case 3:
                                    $status=['status'=>'Pending','class'=>'minus'];
                                    break;
                                case 2:
                                    $status=['status'=>'Answered','class'=>'plus'];
                                    break;
                                case 4:
                                    $status=['status'=>'Closed','class'=>'plus'];
                                    break;
                            }
                        @endphp
                        <h5 class="ticket-info__title mb-4">
                            Status : <span class="{{$status['class']}}"> {{$status['status']}} </span>
                        </h5>

                        @php
                            $role='';
                            switch ($ticket->role_id){
                                case 1:
                                    $role='Support';
                                    break;
                                case 2:
                                    $role='Technical';
                                    break;
                            }
                        @endphp
                        <h5 class="ticket-info__title mb-4">Department: {{$role}}</h5>

                        <h5 class="ticket-info__title mb-4">
                            Sent date: {{date_format($ticket->created_at,'Y.m.d')}}
                        </h5>

                        <h5 class="ticket-info__title mb-4">
                            Last update : {{$last_ticket}} ago
                        </h5>

                        @php
                            $priority='';
                            switch ($ticket->priority){
                                case 1:
                                    $priority='Low';
                                    break;
                                case 2:
                                    $priority='Medium';
                                    break;
                                case 3:
                                    $priority='High';
                                    break;
                            }
                        @endphp
                        <h5 class="ticket-info__title mb-4">Urgency: {{$priority}}</h5>

                        <form action="{{route('tickets.update',$ticket->id)}}" method="post"
                              class="{{$ticket->status==4?'hide':''}}">
                            @csrf
                            @method('put')
                            @can('ticket_close')
                                <button type="button" class="btn authentication-btn w-100 mt-5 closed-ticket ajaxStore">
                                    Close ticket
                                </button>
                            @endcan
                        </form>

                    </div>
                </div>
                <div class="col-12 col-lg-9 chat-message-box__col chat-message__box {{$ticket->status==4?'hide':''}}">
                    <form action="{{route('comment.store',$ticket->id)}}" method="post"
                          class="{{$ticket->status==4?'hide':''}}">
                        @csrf
                        <div class="mt-3 panel-box">
                      <textarea name="description"
                                class="w-100 request-box-2 auth__btn request-js-box-2 enabled border-0 outline-0 ticket__message-box"
                                placeholder="Write your message to supporter"></textarea>
                            <div class="d-flex align-items-center justify-end mt-4">
                                <div class="photo__name"></div>

                                <label for="upload-btn" class="ticket__btns d-flex">
                                    <div class="ticket-upload__btn d-flex align-items-center">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24"
                                                 viewBox="0 0 26.992 30.849">
                                                <path id="Icon_metro-attachment" data-name="Icon metro-attachment"
                                                      d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                                      transform="translate(-3.535 -1.928)"/>
                                            </svg>
                                        </div>

                                        <input name="file" type="file" class="ticket-upload__input d-none"
                                               id="upload-btn"
                                               accept="image/*">

                                        <span>
                                  File
                              </span>
                                    </div>

                                    <button type="button" class="create-new-bank withdraw-btn ajaxStore">
                                        Send
                                    </button>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/ticket-2.js')}}"></script>
    <script src="{{asset('theme/user/scripts/ticket-3.js')}}"></script>
@endsection
