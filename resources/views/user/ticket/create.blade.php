@extends('templates.user.master_page')
@section('title_browser')
    New Ticket-Rootix Exchange
@endsection
@section('content_class','bank-content')

@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">
                    New Ticket
                </h1>
            </div>
        </div>
    </section>

    <form id="ticket-form" action="{{route('tickets.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2
                          enabled d-flex justify-between items-center">
                                <span class="request-box__title">Title</span>
                                <input type="text" dir="ltr" class="currency-value-input ml-4"
                                       id="currency-value-input-1" name="title"
                                       placeholder="Write the title"/>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-4">
                            <div dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2
                          enabled d-flex justify-between items-center">
                                <span class="request-box__title">Department</span>
                                <select class="border-0" name="role_id">
                                    <option value="1">Support</option>
                                    <option value="2">Technical</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div dir="ltr" class="request-box request-box-2 auth__btn request-js-box-2 enabled
                          d-flex justify-between items-center">
                                <span class="request-box__title">Urgency</span>
                                <select class="border-0" name="priority">
                                    <option value="1">Low</option>
                                    <option value="2"> Medium</option>
                                    <option value="3"> High</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div>
                        <textarea name="description"
                                  class="w-100 request-box request-box-2 auth__btn request-js-box-2 enabled
                            border-0 outline-0 ticket__message-box"
                                  placeholder="Write your message"
                        ></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex align-items-center justify-center mt-4">
                        <div class="photo__name"></div>

                        <label for="upload-btn" class="ticket__btns d-flex">
                            <div class="ticket-upload__btn d-flex align-items-center">
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24"
                                         viewBox="0 0 26.992 30.849">
                                        <path id="Icon_metro-attachment" data-name="Icon metro-attachment"
                                              d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                              transform="translate(-3.535 -1.928)"/>
                                    </svg>
                                </div>

                                <input name="file" type="file" class="ticket-upload__input d-none" id="upload-btn"
                                       accept="image/*">

                                <span>
                                File
                            </span>
                            </div>
                            <button id="" type="button" class="create-new-bank withdraw-btn ajaxStore">
                                Submit
                            </button>
                        </label>
                    </div>
                </div>
            </div>
        </section>
    </form>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/ticket-2.js')}}"></script>
@endsection
