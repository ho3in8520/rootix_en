@extends('templates.user.master_page')
@section('title_browser')
    Tickets-Rootix Exchange
@endsection
@section('content_class','bank-content')

@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">Tickets</h1>
                @can('ticket_create')
                    <a href="{{route('tickets.create')}}" class="create-new-bank">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18.111"
                            height="18.111"
                            viewBox="0 0 18.111 18.111">
                            <g
                                id="Icon_feather-plus"
                                data-name="Icon feather-plus"
                                transform="translate(-6.5 -6.5)">
                                <path
                                    id="Path_8591"
                                    data-name="Path 8591"
                                    d="M18,7.5V23.611"
                                    transform="translate(-2.444)"
                                    fill="none"
                                    stroke="#fff"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"/>
                                <path
                                    id="Path_8592"
                                    data-name="Path 8592"
                                    d="M7.5,18H23.611"
                                    transform="translate(0 -2.444)"
                                    fill="none"
                                    stroke="#fff"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"/>
                            </g>
                        </svg>
                        New ticket
                    </a>
                @endcan
            </div>

            <div>
                <div class="dashboard-table">
                    <div class="row">
                        <div class="col-12">
                            <div class="overflow-auto">
                                <div class="wallet-table-container">
                                    <table class="wallet-table bank-table ticket__table">
                                        <thead>
                                        <tr>
                                            <th>
                                                <div>
                                                    ID
                                                </div>
                                            </th>
                                            <th>
                                                <div>
                                                    Title
                                                </div>
                                            </th>
                                            <th>
                                                <div>
                                                    Department
                                                </div>
                                            </th>
                                            <th>
                                                <div>
                                                    Status
                                                </div>
                                            </th>

                                            <th>
                                                <div>
                                                    Date
                                                </div>
                                            </th>

                                            <th>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if(count($tickets)>0)
                                            @foreach($tickets as $ticket)
                                                @php
                                                    switch ($ticket->status){
                                                        case 1 :
                                                        case 3:
                                                            $status=['status'=>'Pending','class'=>'minus'];
                                                            break;
                                                        case 2:
                                                            $status=['status'=>'Answered','class'=>'plus'];
                                                            break;
                                                        case 4:
                                                            $status=['status'=>'Closed','class'=>'plus'];
                                                            break;
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="text-left">
                                                            {{$ticket->ticket_code}}
                                                        </div>
                                                    </td>

                                                    <td>{!! \Illuminate\Support\Str::limit($ticket->title,20) !!}</td>

                                                    <td>{{$ticket->role_id==1?'Support':'Technical'}}</td>

                                                    <td>

                                    <span class="{{$status['class']}}">
                                        {{$status['status']}}
                                    </span>
                                                    </td>

                                                    <td>{{date_format($ticket->created_at,'d F Y')}}</td>

                                                    <td dir="ltr">
                                                        @can('ticket_show')
                                                            <a href="{{route('tickets.show',$ticket->id)}}"
                                                               class="create-new-bank withdraw-btn">
                                                                View
                                                            </a>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">There is no ticket to
                                                    show
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    @if(count($tickets)>0)
                                        <div class="pagination__container">
                                            {!! $tickets->links() !!}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

@endsection
