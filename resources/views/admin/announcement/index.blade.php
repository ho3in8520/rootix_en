@extends('templates.admin.master_page')
@section('title_browser')
    لیست اطلاعیه ها-پنل مدیریت روتیکس
@endsection


@section('content')
    <section class="withdraw start-message pt-2">
        <div class="dashboard-cart requests-withdrawal">
            <div class="container">
                <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                    لیست پیغام ها
                </h1>

                <div>
                    <div class="dashboard-table transaction-table-container">

                        <div class="filters-container">
                            <button class="col-12 filter-btn" data-toggle="modal"
                                    data-target="#toggle-filters-inputs">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23.814" height="20.15"
                                     viewBox="0 0 23.814 20.15">
                                    <g transform="translate(-3.375 -5.625)">
                                        <path
                                            d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                            transform="translate(0 -3.554)"/>
                                        <path
                                            d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                            transform="translate(0 -1.777)"/>
                                        <path
                                            d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"/>
                                    </g>
                                </svg>
                                <span class="pr-1">فیلترها</span>
                            </button>
                            <div id="toggle-filters-inputs" class="up-animation modal">
                                <div class="modal-content pt-0">

                                    <div class="modal-header">
                                        <h6 class="modal-title">فیلترهای جدول</h6>
                                        <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">
                                            &times;
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <div class="form-controlers pb-2">
                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block fs-16">عنوان</label>
                                                    <input type="text" name="title">
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block LblVariz fs-16">گروه
                                                        ارسالی</label>
                                                    <select class="custom-selected" name="group">
                                                        <option value="all">همه</option>
                                                        <option value="roles">نقش ها</option>
                                                        <option value="users">کاربران</option>
                                                    </select>
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">تاریخ
                                                        از</label>
                                                    <input type="text" name="started_at" class="datePicker">
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">تاریخ
                                                        تا</label>
                                                    <input type="text" name="end_at" class="last-input datePicker">
                                                </div>

                                                <div class="w-100 mt-3">
                                                    <button type="button" class="btn fixed-bottom transaction__btn2"
                                                            data-dismiss="modal">جستجو
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="withdraw-rls">
                                            <thead>
                                            <tr>
                                                <th class="text-right LblNamefamilyall text-nowarp">
                                                    <div class="title-header">
                                                        ردیف
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        عنوان
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header" style="white-space: nowrap !important;">
                                                        گروه ارسالی
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        تاریخ شروع
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        تاریخ پایان
                                                    </div>
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if ($announcement->count() > 0)
                                                @foreach($announcement as $key=>$item)
                                                    <tr>
                                                        <td>
                                                            <div class="withdraw-rls">
                                                                <span>{{ $key+1 }}#</span>
                                                            </div>
                                                        </td>

                                                        <td class="description header">{{ $item->title }}</td>
                                                        <td class="more header pt-4">
                                                            <svg data-toggle="modal" data-target="#exampleModal"
                                                                 xmlns="http://www.w3.org/2000/svg" width="27"
                                                                 height="5.344"
                                                                 viewBox="0 0 27 5.344">
                                                                <g transform="translate(-4.5 -15.328)">
                                                                    <path
                                                                        d="M17.986,15.328A2.672,2.672,0,1,0,20.658,18a2.671,2.671,0,0,0-2.672-2.672Z"/>
                                                                    <path
                                                                        d="M7.172,15.328A2.672,2.672,0,1,0,9.844,18a2.671,2.671,0,0,0-2.672-2.672Z"/>
                                                                    <path
                                                                        d="M28.828,15.328A2.672,2.672,0,1,0,31.5,18a2.671,2.671,0,0,0-2.672-2.672Z"/>
                                                                </g>
                                                            </svg>
                                                            <div class="modal fade" id="exampleModal" tabindex="-1"
                                                                 role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title"
                                                                                id="exampleModalLabel">
                                                                                عنوان</h5>
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">{{ $item->title }}</div>
                                                                        <div class="modal-footer">
                                                                            <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-dismiss="modal">بستن
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><span class="withdraw-rls">{{ $item->group_text }}</span></td>
                                                        <td>
                                                            <span class="withdraw-rls d-block text-center">{{ jdate_from_gregorian($item->started_at,'Y.m.d') }}</span>
                                                            <span class="withdraw-rls">{{ jdate_from_gregorian($item->started_at,'H:i') }}</span>
                                                        </td>
                                                        <td>
                                                            <span class="withdraw-rls d-block text-center">{{ jdate_from_gregorian($item->end_at,'Y.m.d') }}</span>
                                                            <span class="withdraw-rls">{{ jdate_from_gregorian($item->end_at,'H:i') }}</span>
                                                        </td>

                                                        <td class="withdraw-rls-btns-td">
                                                            <div class="wallet-table-btns d-flex flex-column">
                                                                <div class="d-flex justify-content-between ">
                                                                    <a href="{{ route('admin.ann.show',$item) }}"
                                                                       class="btn withdraw-rls-btn harvest-btn2 mr-0 p-0">
                                                                        <svg class="view-edit" id="no_signal_no_wifi"
                                                                             data-name="no signal no wifi"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             width="26"
                                                                             height="26" viewBox="0 0 26 26">
                                                                            <defs>
                                                                                <style>
                                                                                    .cls-1 {
                                                                                        opacity: 0;
                                                                                    }
                                                                                </style>
                                                                            </defs>
                                                                            <rect id="Rectangle_1550"
                                                                                  data-name="Rectangle 1550"
                                                                                  class="cls-1"
                                                                                  width="26" height="26"/>
                                                                            <path id="Path_8685" data-name="Path 8685"
                                                                                  d="M23.264,14.383a13.479,13.479,0,0,0-19.029,0,.816.816,0,1,0,1.154,1.154,11.83,11.83,0,0,1,16.721,0,.816.816,0,1,0,1.154-1.154ZM20.249,19.16a11.911,11.911,0,0,1-8.881,1.706.829.829,0,1,0-.325,1.625,13.52,13.52,0,0,0,10.1-1.95.823.823,0,1,0-.894-1.381ZM8.6,19.932a13.219,13.219,0,0,1-1.349-.772.813.813,0,0,0-.894,1.349,12.74,12.74,0,0,0,1.544.886.764.764,0,0,0,.349.081A.813.813,0,0,0,8.6,19.932Z"
                                                                                  transform="translate(-0.749 -1.96)"/>
                                                                            <path id="Path_8686" data-name="Path 8686"
                                                                                  d="M14.49,18.438a3.25,3.25,0,1,0-3.25-3.25A3.25,3.25,0,0,0,14.49,18.438Zm0-4.875a1.625,1.625,0,1,1-1.625,1.625A1.625,1.625,0,0,1,14.49,13.563Zm0-6.5A.813.813,0,0,0,15.3,6.25V3.813a.813.813,0,0,0-1.625,0V6.25A.813.813,0,0,0,14.49,7.063Zm4.063.813a.812.812,0,0,0,.674-.366l1.625-2.437A.813.813,0,1,0,19.5,4.178L17.879,6.616a.813.813,0,0,0,.228,1.121A.764.764,0,0,0,18.553,7.875Zm-8.8-.366a.812.812,0,0,0,.674.366.764.764,0,0,0,.447-.138A.813.813,0,0,0,11.1,6.616L9.477,4.178a.813.813,0,1,0-1.349.894Z"
                                                                                  transform="translate(-1.49 -0.563)"/>
                                                                        </svg>
                                                                        مشاهده
                                                                    </a>
                                                                    <a href="{{ route('admin.ann.edit',$item) }}"
                                                                       class="btn withdraw-rls-btn harvest-btn2 mr-0 p-0">
                                                                        <svg class="view-edit" id="Group_1818"
                                                                             data-name="Group 1818"
                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                             width="20.047" height="20.047"
                                                                             viewBox="0 0 20.047 20.047">
                                                                            <path id="Path_8861" data-name="Path 8861"
                                                                                  d="M15.2,20.377H3.174A3.173,3.173,0,0,1,0,17.2V5.174A3.173,3.173,0,0,1,3.174,2H9.188a.789.789,0,0,1,.835.835.789.789,0,0,1-.835.835H3.174a1.5,1.5,0,0,0-1.5,1.5V17.119a1.5,1.5,0,0,0,1.5,1.5H15.119a1.5,1.5,0,0,0,1.5-1.5V11.188a.835.835,0,1,1,1.671,0V17.2A3.052,3.052,0,0,1,15.2,20.377Z"
                                                                                  transform="translate(0 -0.329)"/>
                                                                            <path id="Path_8862" data-name="Path 8862"
                                                                                  d="M5.835,15.871a.758.758,0,0,1-.585-.251A1.166,1.166,0,0,1,5,14.868l.835-4.177a.459.459,0,0,1,.251-.418L16.11.251a.807.807,0,0,1,1.169,0L20.62,3.592a.807.807,0,0,1,0,1.169L10.6,14.785a1.826,1.826,0,0,1-.418.251L6,15.871Zm1.587-4.594-.5,2.673,2.673-.5,9.272-9.272L16.694,2Z"
                                                                                  transform="translate(-0.823)"/>
                                                                        </svg>
                                                                        ویرایش
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="20" class="text-center">مورد برای نمایش وجود
                                                        ندارد
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $announcement->links() !!}

                                    <div class="pdf-exel mr-3 mt-5 mb-5">
                                        <a class="btn-pdf ml-3"
                                           href="{{ request()->fullUrlWithQuery(['export'=>'pdf']) }}">خروجی PDF
                                            <img class="pdf-image"
                                                 src="{{ asset('theme/user/images/Icon metro-file-pdf.svg') }}" alt="">
                                        </a>
                                        <a class="btn-exel"
                                           href="{{ request()->fullUrlWithQuery(['export'=>'excel']) }}">خروجی Excel
                                            <img class="excel-image"
                                                 src="{{ asset('theme/user/images/Icon simple-microsoftexcel.svg') }}"
                                                 alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

