@extends('templates.admin.master_page')
@section('title_browser')لیست برداشت ریالی@endsection

@section('content')
    <section id="basic-form-layouts">
        <!-- Reject Modal -->
        <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="post" action="">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="rejectModalLabel">رد کردن درخواست برداشت کاربر</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>دلیل رد درخواست</label>
                                <textarea class="form-control mt-1" rows="3" name="admin_des"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                            <button type="button" class="btn btn-info confirm-withdraw ajaxStore">تایید و رد کردن
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!--## Reject Modal -->

        <!-- Confirm Modal -->
        <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="post" action="">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="confirmModalLabel">تایید و واریز کردن درخواست برداشت کاربر</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>کد پیگیری</label>
                                <input type="text" class="form-control" name="tracking_code">
                            </div>
                            <div class="form-group">
                                <label>روش واریز</label>
                                <select class="form-control" name="deposit_type">
                                    <option value="0">شتاب</option>
                                    <option value="1">پایا</option>
                                    <option value="2">سانتا</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>توضیحات مدیر</label>
                                <textarea class="form-control" rows="3" name="admin_des"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                            <button type="button" class="btn btn-info confirm-withdraw ajaxStore">تایید و واریز</button>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!--## Confirm Modal -->

        <!-- More Modal -->
        <div class="modal fade" id="moreModal" tabindex="-1" role="dialog" aria-labelledby="moreModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                            <tr class="amount">
                                <th>مبلغ</th>
                                <td></td>
                            </tr>
                            <tr class="fee">
                                <th>کامزد</th>
                                <td></td>
                            </tr>
                            <tr class="total_amount">
                                <th>مبلغ قابل پرداخت</th>
                                <td></td>
                            </tr>
                            <tr class="status">
                                <th>وضعیت</th>
                                <td><span></span></td>
                            </tr>
                            <tr class="user_des">
                                <th>توضیحات کاربر</th>
                                <td></td>
                            </tr>
                            <tr class="admin_des">
                                <th>توضیحات ادمین</th>
                                <td></td>
                            </tr>
                            <tr class="card_number">
                                <th>شماره کارت</th>
                                <td></td>
                            </tr>
                            <tr class="sheba_number">
                                <th>شماره شبا</th>
                                <td></td>
                            </tr>
                            <tr class="account_number">
                                <th>شماره حساب</th>
                                <td></td>
                            </tr>
                            <tr class="tracking_code">
                                <th>کد پیگیری</th>
                                <td></td>
                            </tr>
                            <tr class="deposit_type">
                                <th>نوع واریز</th>
                                <td></td>
                            </tr>
                            <tr class="deposit_date">
                                <th>تاریخ واریز</th>
                                <td></td>
                            </tr>
                            <tr class="created_at">
                                <th>تاریخ ایجاد</th>
                                <td></td>
                            </tr>
                            <tr class="updated_at">
                                <th>تاریخ بروزرسانی</th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div> <!--## More Modal -->


        <div class="row">
            <div class="col-12 card">
                <div class="card-body">
                    <form method="get" action="" class="row">
                        <div class="form-group col-md-4 col-sm-6">
                            <label>کد کاربر</label>
                            <input type="text" name="user_code" class="form-control" value="{{ request()->user_code }}">
                        </div>
                        <div class="form-group col-md-4 col-sm-6">
                            <label>مبلغ</label>
                            <input type="text" name="amount" class="form-control" value="{{ request()->amount }}">
                        </div>
                        <div class="form-group col-md-4 col-sm-6">
                            <label>وضعیت</label>
                            <select class="form-control" name="status">
                                <option value="">همه</option>
                                <option value="0" {{ request()->status==='0'?'selected':'' }}>جدید</option>
                                <option value="1" {{ request()->status==='1'?'selected':'' }}>واریز شده</option>
                                <option value="2" {{ request()->status==='2'?'selected':'' }}>رد شده</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-6">
                            <label>کد پیگیری</label>
                            <input type="text" class="form-control" name="tracking_code"
                                   value="{{ request()->tracking_code }}">
                        </div>
                        <div class="form-group col-md-4 col-sm-6">
                            <label>نوع واریز</label>
                            <select class="form-control" name="deposit_type">
                                <option value="">همه</option>
                                <option value="0" {{ request()->deposit_type=='0'?'selected':'' }}>شتاب</option>
                                <option value="1" {{ request()->deposit_type==='1'?'selected':'' }}>پایا</option>
                                <option value="2" {{ request()->deposit_type==='2'?'selected':'' }}>سانتا</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-6">
                            <label>از تاریخ (واریز)</label>
                            <input type="text" class="form-control date-picker" name="start_deposit_date"
                                   value="{{ request()->start_deposit_date }}">
                        </div>
                        <div class="form-group col-md-4 col-sm-6">
                            <label>تا تاریخ (واریز)</label>
                            <input type="text" class="form-control date-picker" name="end_deposit_date"
                                   value="{{ request()->end_deposit_date }}">
                        </div>
                        <div class="form-group col-md-12">
                            <button type="button" class="btn btn-success float-right search-ajax">فیلتر کردن</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-12 card col-12 mt-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>کد کاربر</th>
                                <th>مبلغ</th>
                                <th>کارمزد</th>
                                <th>مبلغ قابل پرداخت</th>
                                <th>وضعیت</th>
                                <th>کد پیگیری</th>
                                <th>بیشتر</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($withdraw_rial->count() >0)
                                @php($i=1)
                                @foreach($withdraw_rial as $item)
                                    <tr data-more="{{ json_encode($item) }}" data-bank="{{ json_encode($item->bank) }}"
                                        data-status="{{ json_encode($item->status_info) }}">
                                        <td>{{ $i }}</td>
                                        <td>{{ $item->user->code }}</td>
                                        <td>{{ number_format($item->amount) }} ریال</td>
                                        <td>{{ number_format($item->fee) }} ریال</td>
                                        <td>{{ number_format($item->amount-$item->fee) }} ریال</td>
                                        <td><span
                                                class="badge badge-{{ $item->status_info['color'] }}">{{ $item->status_info['text'] }}</span>
                                        </td>
                                        <td>{{ $item->tracking_code?$item->tracking_code:'-' }}</td>
{{--                                        <td>{{ $item->deposit_type?$item->deposit_type:'-' }}</td>--}}
                                        <td><a href="" class="btn btn-info more-btn" data-toggle="modal"
                                               data-target="#moreModal">اطلاعات بیشتر</a></td>
                                        <td>
                                            @if($item->status==0)
                                                <button type="button" class="btn btn-danger mb-1 reject"
                                                        data-toggle="modal"
                                                        data-target="#rejectModal"
                                                        data-action="{{ route('admin.withdraw-rial.reject',$item->id) }}">
                                                    رد
                                                    کردن
                                                </button>
                                                <button type="button" class="btn btn-success mb-1 confirm"
                                                        data-toggle="modal" data-target="#confirmModal"
                                                        data-action="{{ route('admin.withdraw-rial.confirm',$item->id) }}">
                                                    تایید کردن
                                                </button>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="20" class="text-center">موردی جهت نمایش وجود ندارد</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $withdraw_rial->withQueryString()->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('button.reject,button.confirm').click(function () {
            let action = $(this).data('action');
            let target = $(this).data('target');
            $(`${target} form`).attr('action', action);
        });
        $('.more-btn').click(function () {
            let data = $(this).parents('tr').data('more');
            let bank = $(this).parents('tr').data('bank');
            let status = $(this).parents('tr').data('status');
            let table = $("#moreModal table tbody");
            table.find(".amount td:last").text(numberFormat(data.amount) + ' ریال ');
            table.find(".fee td:last").text(numberFormat(data.fee) + ' ریال ');
            table.find(".total_amount td:last").text(numberFormat(data.amount - data.fee) + ' ریال ');
            table.find(".status td:last span").text(status['text']);
            table.find(".status td:last span").addClass('badge badge-' + status['color']);
            table.find(".user_des td:last").text(data.user_des);
            table.find(".admin_des td:last").text(data.admin_des);
            table.find(".card_number td:last").text(bank.name + ' - ' + bank.card_number);
            table.find(".sheba_number td:last").text(bank.sheba_number);
            table.find(".account_number td:last").text(bank.account_number);
            table.find(".tracking_code td:last").text(data.tracking_code);
            table.find(".deposit_type td:last").text(data.deposit_type);
            table.find(".deposit_date td:last").text(new Date(data.deposit_date).toLocaleString('fa-IR'));
            table.find(".created_at td:last").text(new Date(data.created_at).toLocaleString('fa-IR'));
            table.find(".updated_at td:last").text(new Date(data.updated_at).toLocaleString('fa-IR'));
        })
    </script>
@endsection
