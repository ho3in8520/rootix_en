@extends('templates.admin.master_page')
@section('title_browser')
    داشبورد
@endsection
@php
    $array_steps=status_steps_auth_user($step_forms,$step_completed, 'all','array');
    $array_steps_notif=status_steps_auth_user($step_forms,$step_completed, 'all','notif');
@endphp
@section('content')
    <section class="dashboard-cart user-authentication" dir="rtl">
        <div class="container">
            <h1 class="dashboard-title desktop-title text-right">اطلاعات کاربران-احراز حویت</h1>
            <div class="dashboard-table transaction-table-container">
                <div class="row justify-center">
                    <div class="d-flex header-transaction mb-4">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله یک</span>
                                <span class="d-block transaction__filter-title">اطلاعات تماس</span>
                            </div>
                            <div>
                                <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
{{--                    <div class="col-sm-6 col-md-6 col-lg-4">--}}
{{--                        <div class="card mt-2">--}}
{{--                            <div class="cards-body">--}}
{{--                                <p class="card-text">شماره تماس--}}
{{--                                    <span class="text">{{$user->mobile?:''}}</span>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body text-right">
                                <p class="card-text">ایمیل
                                    <span class="text">{{$user->email?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-center">
                    <div class="d-flex header-transaction mb-4 mt-5">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله دو</span>
                                <span class="d-block transaction__filter-title">دریافت مدارک</span>
                            </div>
                            <div>
                                @if($array_steps['step.upload']['status']==3)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top" title="{{$array_steps['step.upload']['msg_reject']}}">
                                @elseif($array_steps['step.upload']['status']==2)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}"
                                         alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-center mb-4">
                    <div class="reject-confirm">
                        @if($array_steps['step.upload']['status']==1 ||$array_steps['step.upload']['status']==0)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.upload']['id'])}}">تایید
                                مرحله دو
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(3,{{ $array_steps['step.upload']['id'] }})">رد مرحله
                                دو
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.upload']['status']==3)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.upload']['id'])}}">تایید
                                مرحله دو
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(3,{{ $array_steps['step.upload']['id'] }})">رد مرحله
                                دو
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.upload']['status']==2)
                            <a class="reject" onclick="rejectUser(3,{{ $array_steps['step.upload']['id'] }})">رد مرحله
                                دو
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-4">
                    @if(count($images)>0)
                        @foreach($images as $key =>$item)
                            @php
                                switch ($item->type){
                                        case "passport":
                                            $title="تصویر پاسپورت";
                                            break;
                                        case "selfie_image":
                                            $title="تصویر سلفی";
                                            break;
                                        case "ID_Card_first":
                                             $title="تصویر آیدی کارت";
                                             break;
                                        case "ID_Card_second":
                                             $title="تصویر آیدی کارت";
                                             break;
                                        default:
                                             $title=null;
                                             break;
    }
                            @endphp
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="card-rectangle card mt-3">
                                    <div class="card-body">
                                        <p class="text">{{$title}}</p>
                                        <span
                                            class="card-text">{{jdate_from_gregorian($item->created_at,'%Y.%m.%d')}}</span>
                                        <div class="dotted mt-2">
                                            <img
                                                src="{{ getImage($item->path) }}"
                                                alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="row justify-center">
                    <div class="d-flex header-transaction mb-4 mt-5">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله سه</span>
                                <span class="d-block transaction__filter-title">اطلاعات کاربر</span>
                            </div>
                            <div>
                                @if($array_steps['step.information']['status']==3)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top"
                                         title="{{$array_steps['step.information']['msg_reject']}}">
                                @elseif($array_steps['step.information']['status']==2)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}"
                                         alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-center mb-4">
                    <div class="reject-confirm">
                        @if($array_steps['step.information']['status']==1 || $array_steps['step.information']['status']==0)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.information']['id'])}}">تایید
                                مرحله سه
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(3,{{ $array_steps['step.information']['id'] }})">رد
                                مرحله سه
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.information']['status']==3)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.information']['id'])}}">تایید
                                مرحله سه
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(3,{{ $array_steps['step.information']['id'] }})">رد
                                مرحله سه
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.information']['status']==2)
                            <a class="reject" onclick="rejectUser(3,{{ $array_steps['step.information']['id'] }})">رد
                                مرحله سه
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-4 text-right">
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">نام و نام خانوادگی
                                    <span class="text">{{$user->first_name?:''}} {{$user->last_name?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">شماره پاسپورت
                                    <span class="text">{{$user->id_card_number?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">تاریخ تولد
                                    <span class="text">{{$user->birth_day?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-sm-6 col-md-6 col-lg-4">--}}
{{--                        <div class="card mt-2">--}}
{{--                            <div class="cards-body">--}}
{{--                                <p class="card-text">تلفن ثابت (با کد شهر)--}}
{{--                                    <span class="text">{{$user->phone?:''}}</span>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-6 col-md-6 col-lg-4">--}}
{{--                        <div class="card mt-2">--}}
{{--                            <div class="cards-body">--}}
{{--                                <p class="card-text">کدپستی--}}
{{--                                    <span class="text">{{$user->postal_code?:''}}</span>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">ملیت
                                    <span
                                        class="text">{{!empty($user->nationality) ? $user->country->nicename : ' '}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-sm-12 col-md-6 col-lg-6">--}}
{{--                        <div class="card mt-2">--}}
{{--                            <div class="cards-body">--}}
{{--                                <p class="card-text">آدرس--}}
{{--                                    <span class="text">{{$user->address?:''}}</span>--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">جنسیت</p>
                                <div style="float: left">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input d-none" type="radio" name="inlineRadioOptions"
                                               id="inlineRadio1" value="option1" style="margin-left: 8px;"
                                            {{$user->gender_id==1?'checked':''}}>
                                        <label class="form-check-label" for="inlineRadio1">آقا</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input disabled class="form-check-input" type="radio"
                                               name="inlineRadioOptions"
                                               id="inlineRadio2" value="option2" style="margin-left: 8px;"
                                        {{$user->gender_id==2?'checked':''}}
                                        <label class="form-check-label" for="inlineRadio2">خانم</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input disabled class="form-check-input d-none" type="radio"
                                               name="inlineRadioOptions"
                                               id="inlineRadio3" value="option2" style="margin-left: 8px;"
                                            {{$user->gender_id==3?'checked':''}}>
                                        <label class="form-check-label" for="inlineRadio3">هیچکدام</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        function rejectUser(status, form_id) {

            swal("دلیل رد صلاحیت خود را بنویسید:", {
                content: "input",
            })
                .then((value) => {
                    if (value !== null) {
                        $.ajax({
                            url: "{{url('admin/user/disableStatus')}}",
                            type: 'get',
                            data: {
                                status: status,
                                id: "{{$user->id}}",
                                value: value,
                                form_id: form_id,
                                "_token": "{{csrf_token()}}"
                            },
                            headers:
                                {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                },
                            success: function (data) {
                                swal('موفق', data['msg'], data['type']);
                                setTimeout(function () {
                                    location.reload();
                                }, 2000)
                            }
                        });
                    }
                });
        }
    </script>
@endsection
