@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت کارمزد و برداشت ارزها
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت رفرال</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('referral-update')}}" method="post">
                                @csrf
                                <div class="record">
                                    <div class="row row-btt">
                                        <div class="col-md-3">
                                            <div class="form-group ">
                                                <div class="input-group ">
                                                    <label class="col-12">وضعیت رفرال</label>
                                                    <select class="form-control"
                                                            name="referral_status" autocomplete="off">
                                                        <option value="0"
                                                                @if($referral_pct->status == '0') selected @endif>غیر
                                                            فعال
                                                        </option>
                                                        <option value="1"
                                                                @if($referral_pct->status == '1') selected @endif>فعال
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="input-group ">
                                                    <label class="col-12">درصد رفرال</label>

                                                    <input name="referral_pct" type="text" class="form-control"
                                                           style="width: 200px"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{ $referral_pct->extra_field1 }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container fluid -->
    </div>
@endsection
<!-- Button trigger modal -->
@section('script')

    <script>

    </script>
@endsection
