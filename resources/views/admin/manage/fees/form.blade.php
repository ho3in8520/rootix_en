@if(count($result) > 0)
    @foreach($result as $key=>$item)
        @if($key != 0)
            <div class="dash3"></div>
        @endif
        <div class="row pl-5 risp-row2 row-btt">
            <div class="col-md-4  pr-2 pb-3 min-baze ">
                <div class="karmozd-traid2 d-flex ">
                    <p class="p-4">حداقل برداشت</p>
                    <input type="text" id="min-koinex1"
                           onkeyup="($(this).val(numberFormat($(this).val())))"
                           value="{{ number_format(floor($item->extra_field1 *100)/100, 2) }}"
                           name="from[]"
                           class="font-weight-bold inputs pl-3 loan_max_amount">
                </div>
            </div>
            <div class="col-md-4  pb-3  pr-2 ">
                <div class="min-koinex d-flex">
                    <p class="mr-3"> حداکثر برداشت</p>
                    <input type="text" id="min-koinex2"
                           onkeyup="($(this).val(numberFormat($(this).val())))"
                           value="{{ number_format(floor($item->extra_field2 *100)/100, 2)}}"
                           name="to[]"
                           class="font-weight-bold inputs pl-3">
                </div>
            </div>
            <div class="col-md-4 pr-2 karmozdcol">
                <div class="min-roitex d-flex  ">
                    <p class="mr-3 "> مقدار کارمزد</p>
                    <input type="text" id="min-rotix1"
                           onkeyup="($(this).val(numberFormat($(this).val())))"
                           value="{{ is_numeric($item->extra_field3)?$item->extra_field3:'' }}"
                           name="percent[]"
                           class="font-weight-bold inputs pl-3 inputic loan_max_amount">
                    <input type="hidden" name="id[]" value="{{ $item->id }}">
                    <div class="foricon trash">
                        <p class="foricomtex">حذف بازه</p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" class="svgtrash"
                             height="22"
                             viewBox="0 0 35 28">
                            <path
                                d="M25.75,27h7v3.5h-7Zm0-14H38v3.5H25.75Zm0,7h10.5v3.5H25.75Zm-21,10.5A3.51,3.51,0,0,0,8.25,34h10.5a3.51,3.51,0,0,0,3.5-3.5V13H4.75ZM24,7.75H18.75L17,6H10L8.25,7.75H3v3.5H24Z"
                                transform="translate(-3 -6)" fill="#e62b2b"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="row pl-5 risp-row2 row-btt">
        <div class="col-md-4  pr-2 pb-3 min-baze ">
            <div class="karmozd-traid2 d-flex ">
                <p class="p-4">حداقل برداشت</p>
                <input type="text" id="min-koinex1"
                       onkeyup="($(this).val(numberFormat($(this).val())))"
                       name="from[]"
                       class="font-weight-bold inputs pl-3 loan_max_amount">
            </div>
        </div>
        <div class="col-md-4  pb-3  pr-2 ">
            <div class="min-koinex d-flex">
                <p class="mr-3"> حداکثر برداشت</p>
                <input type="text" id="min-koinex2"
                       onkeyup="($(this).val(numberFormat($(this).val())))"
                       name="to[]"
                       class="font-weight-bold inputs pl-3">
            </div>
        </div>
        <div class="col-md-4 pr-2 karmozdcol">
            <div class="min-roitex d-flex  ">
                <p class="mr-3 "> مقدار کارمزد</p>
                <input type="text" id="min-rotix1"
                       onkeyup="($(this).val(numberFormat($(this).val())))"
                       name="percent[]"
                       class="font-weight-bold inputs pl-3 inputic loan_max_amount">
                <input type="hidden" name="id[]">
                <div class="foricon trash">
                    <p class="foricomtex">حذف بازه</p>
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" class="svgtrash"
                         height="22"
                         viewBox="0 0 35 28">
                        <path
                            d="M25.75,27h7v3.5h-7Zm0-14H38v3.5H25.75Zm0,7h10.5v3.5H25.75Zm-21,10.5A3.51,3.51,0,0,0,8.25,34h10.5a3.51,3.51,0,0,0,3.5-3.5V13H4.75ZM24,7.75H18.75L17,6H10L8.25,7.75H3v3.5H24Z"
                            transform="translate(-3 -6)" fill="#e62b2b"/>
                    </svg>
                </div>
            </div>
        </div>
    </div>
@endif
