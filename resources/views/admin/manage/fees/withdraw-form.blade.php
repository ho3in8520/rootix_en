<!-- مربوط به سکشن `مدیریت کارمزد و حداقل برداشت ارزها` -->
<div class="col-md-4 mb-4">
    <div class="min-koinex d-flex">
        <p class="mr-3">حداقل برداشت </p>
        <input type="text" id="min-koinex5"
               onkeyup="($(this).val(numberFormat($(this).val())))"
               value="{{ $min_withdraw }}"
               name="min_withdraw"
               class="font-weight-bold inputs pl-3">
    </div>
</div>
<div class="col-md-4 mb-4">
    <div class="min-roitex d-flex ">
        <p class="p-4"> مقدار کارمزد</p>
        <input type="text" id="min-rotix4"
               onkeyup="($(this).val(numberFormat($(this).val())))"
               value="{{ $fee_withdraw }}"
               name="fee_withdraw"
               class="font-weight-bold inputs pl-3">

    </div>
</div>
<!--<div class="col-md-4 mb-4">
    <div class="min-roitex d-flex ">
        <p class="p-4"> کارمزد برداشت کوینکس </p>
        <input type="text" id="min-rotix5" value="20" class="font-weight-bold inputs pl-3">
    </div>
</div>-->


<!--<div class="form-group col-md-6">
    <div class="input-group ">
        <label class="col-12">حداقل برداشت</label>
        <input name="min_withdraw" type="text" class="form-control" style="width: 200px" onkeyup="($(this).val(numberFormat($(this).val())))" value="{{ $min_withdraw }}">
    </div>
</div>
<div class="form-group col-md-6">
    <div class="input-group ">
        <label class="col-12">کارمزد برداشت</label>
        <input name="fee_withdraw" type="text" class="form-control" style="width: 200px" onkeyup="($(this).val(numberFormat($(this).val())))" value="{{ $fee_withdraw }}">
    </div>
</div>-->
