@extends('templates.admin.master_page')
@section('title_browser')
    لیست دسته بندی ها
@endsection
@section('after-title')
    <div class="text-zero top-right-button-container">
        <a href="{{route('categories.create')}}" class="btn btn-primary btn-lg top-right-button mr-1">افزودن جدید</a>
    </div>
@stop
@section('content')
    <section id="basic-form-layouts">
        <form method="post" action="{{route('categories.delete')}}">
        @csrf
        <!-- مدال مربوط به حذف دسته بندی -->
            <div id="delete_category_modal" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="change_pass_modalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0" id="BlockModalLabel">حذف دسته بندی</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <input id="category" type="hidden" value="" name="category">
                        <h5 class="text-center">این دسته حذف شود؟</h5>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">کنسل</button>
                            <button type="submit" class="btn btn-info waves-effect waves-light ajaxStore">تایید</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.مدال مربوط به حذف دسته بندی -->
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="get" action="">
                            @csrf
                            <div class="row mt-4">
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="name">نام</label>
                                    <input name='name' class="form-select form-control"
                                           value="{{ request()->name?request()->name:'' }}">
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>وضعیت</label>
                                    <select name="status" class="form-control">
                                        <option value="">همه</option>
                                        <option value="1" {{request()->status=="1"?'selected':''}}>فعال</option>
                                        <option value="0" {{request()->status=="0"?'selected':''}}>غیرفعال</option>
                                    </select>

                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="parent_id">زیرمجموعه</label>
                                    <select name="parent_id" class="form-control select2-single">
                                        <option value="">همه</option>
                                        @foreach($cats as $category)
                                            <option
                                                value="{{$category->id}}" {{ request()->category_id==$category->id?'selected':'' }}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 mt-4 text-left">
                                    <button type="button" class="btn btn-info search-ajax">جست وجو</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive text-center">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <td>نام</td>
                                    <td>توضیحات</td>
                                    <td>وضعیت</td>
                                    <td>زیرمجموعه</td>
                                    <td>عملیات</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($categories) && count($categories)>0)
                                    @foreach($categories as $item)
                                        <tr>
                                            <td>{{$item->name}}</td>
                                            <td>@if($item->description == null)
                                                    - @else{!! \Illuminate\Support\Str::limit($item->description,20) !!} @endif</td>
                                            <td>@if($item->status == 0)غیرفعال @else فعال@endif</td>
                                            <td>@if($item->parent_id == null) ندارد@else
                                                    @php
                                                        $parent_name=getCategory($item->parent_id);
                                                    @endphp
                                                    {{$parent_name}}
                                                @endif</td>
                                            <td>
                                                <a href="{{route('categories.edit',$item->id)}}"
                                                   class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                                <button data-category-id="{{$item->id}}"
                                                        class="btn btn-sm btn-danger delete"
                                                        data-target="#delete_category_modal"
                                                        data-toggle="modal" title="حذف">
                                                    <i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center">لیست دسته بندی ها خالی می باشد.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $categories->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $('.delete').on('click', function () {
            $('#category').val($(this).data('category-id'));
        })
    </script>
@endsection
