@extends('templates.admin.master_page')
@section('title_browser')
    ویرایش دسته بندی
@endsection
@section('style')
    <style>
        #ckeditor {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('categories.update',$category->id)}}" method="post">
                        @csrf
                        @method('put')
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="name" class="">عنوان</label>
                                <input type="text" name="name" class="form-control" value="{{$category->name}}">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="description" class="">توضیحات</label>
                                <textarea id="ckeditor" name="description" rows="10" cols="125">
                                    {{$category->description}}
            </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="parent_id">زیرمجموعه</label>
                                <select name="parent_id" class="form-control select2-single">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}" @if($category->parent_id ==$item->id ) selected @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="status">وضعیت</label>
                                <select name="status" class="form-control">
                                    <option value="">انتخاب کنید</option>
                                    <option @if($category->status ==1) selected @endif value="1">فعال</option>
                                    <option @if($category->status ==0 ) selected @endif value="0">غیرفعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-info btn-lg ajaxStore">ویرایش</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('ckeditor',
            {
                customConfig: 'config.js',
                toolbar: 'simple'
            });

    </script>
@endsection
