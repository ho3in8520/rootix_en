@extends('templates.landing_page.auth.desktop.master_page')
@section('style')

@endsection
@section('content')
    <livewire:landing.auth.login-register :countries="$countries" :viewMode="$type"/>
@endsection
