@mobile
@include('landing.auth.mobile.register')
@endmobile

@tablet
@include('landing.auth.mobile.register')
@endtablet

@desktop
@include('landing.auth.desktop.register')
@enddesktop
