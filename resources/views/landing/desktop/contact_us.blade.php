@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    Contact With Rootix
@endsection
@section('header')
    <header class="blog-page-header">
        <div class="container">
            <nav class="nav blog-nav">
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>
        </div>
    </header>
@endsection
@section('content')
    <!-- Start Section One -->
    <section>
        <div id="mapid"></div>

        <div class="container contact-container contact-container-2">
            <div class="row">
                <div class="col-12">
                    <div class="contacts__contact">
                        <div class="contact">
                            <div class="contact__sidebar">
                                <h2 class="contact__sidebar-title">Get in touch with us</h2>

                                <div class="contact__sidebar-body">
                                    <p>Email : info@rootix.io</p>
{{--                                    <p>شماره تماس : ۱۲۳۴۵۶۷۸۹-۰۳۱</p>--}}
{{--                                    <p class="address">--}}
{{--                                        آدرس : اصفهان ، نجف آباد خیابان شریعتی--}}
{{--                                    </p>--}}
                                </div>

                                <div class="contact__sidebar-footer">
                                    <a
                                        href="https://www.instagram.com/rootix.io/"
                                        target="_blank"
                                        class="contact__sidebar-item instagram"
                                    >
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    <a
                                        href="https://twitter.com/rootixexchange"
                                        target="_blank"
                                        class="contact__sidebar-item twitter"
                                    >
                                        <i class="fa fa-twitter"></i>
                                    </a>
{{--                                    <a--}}
{{--                                        href="#"--}}
{{--                                        target="_blank"--}}
{{--                                        class="contact__sidebar-item face-book"--}}
{{--                                    >--}}
{{--                                        <i class="fa fa-facebook"></i>--}}
{{--                                    </a>--}}
                                    <a href="https://t.me/rootix" class="social-network telegram">
                                        <img src="{{asset('theme/landing/images/tel2
.png')}}" width="39px"  height="39px" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="contact__body">
                                <h1 class="contact__body-title">Send us a message</h1>

                                <form action="" class="contact__form">
                                    <div class="contact__inputs">
                                        <div class="contact__input-data">
                                            <input
                                                type="text"
                                                class="contact__input"
                                                id="name"
                                                required
                                                autocomplete="off"
                                            />
                                            <label class="contact__label" for="name">Full name</label>
                                        </div>
                                        <div class="contact__input-data">
                                            <input
                                                type="text"
                                                class="contact__input"
                                                id="email"
                                                required
                                                autocomplete="off"
                                            />
                                            <label class="contact__label" for="email">Email</label>
                                        </div>
                                    </div>

                                    <div class="contact__inputs">
                                        <div class="contact__input-data">
                                            <input
                                                type="text"
                                                class="contact__input"
                                                id="company"
                                                required
                                                autocomplete="off"
                                            />
                                            <label class="contact__label" for="company">Factory</label>
                                        </div>
                                        <div class="contact__input-data">
                                            <input
                                                type="text"
                                                class="contact__input"
                                                id="number"
                                                required
                                                autocomplete="off"
                                            />
                                            <label class="contact__label" for="number">Phone number</label>
                                        </div>
                                    </div>
                                    <div class="contact__input-data contact__textarea-data">
                        <textarea
                            type="text"
                            class="contact__input contact__textarea"
                            id="message"
                            required
                            autocomplete="off"
                        ></textarea>
                                        <label class="contact__label" for="message">Message</label>
                                    </div>

                                    <div class="d-flex justify-end">
                                        <button type="submit" class="contact__btn" disabled="disabled">
                                            Send
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="newsletter__section">
        <div class="container contact-container">
            <div class="newsletter">
                <div class="newsletter__body">
                    <h2 class="newsletter__title">Subscribe to Newsletter</h2>
                    <p class="newsletter__subtitle">
                        Enter your email to receive company newsletters
                    </p>
                </div>

                <div class="newsletter__footer">
                    <form action="" class="newsletter__form">
                        <button disabled type="submit" class="newsletter__btn">Confirm</button>
                        <input
                            type="text"
                            class="newsletter__input"
                            placeholder="Enter Your Email Address...."
                        />
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')
    <script
        src="{{asset('theme/landing/scripts/map.js')}}"
        crossorigin=""
    ></script>
    <script src="{{asset('theme/landing/scripts/contact-map.js')}}"></script>
@endsection
