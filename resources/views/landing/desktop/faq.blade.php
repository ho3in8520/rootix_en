@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
     FAQ-Rootix
@endsection
@section('header')
    <header class="header faqs-header">
        <div class="container">
            <nav>
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>

        </div>
        <div class="faqs-search-container">
            <div class="faqs-search">
                <h1 class="faqs-search__title">Do you have any question? Ask us.</h1>
                <form action="#" class="faqs-search__search-box">
                    <input type="search" placeholder="What question do you have?" />
                </form>
            </div>
        </div>
    </header>
@endsection
@section('content')
    <section>
        <div class="container faqs-container">
            <div class="row">
                <div class="col-12">
                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What is Rootix?</h3>
                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            Rootix is a cryptocurrency Exchange. You can buy cryptocurrencies (like Bitcoin) What is Rootix? through its secure portal.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What is cryptocurrency?</h3>
                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            In the 21st century, cryptocurrencies are being replaced by common
                            currencies such as the rial, the dollar, the euro, and even gold. From now
                            on, all financial transactions worldwide and even daily purchases will be
                            made in cryptocurrency. Therefore, the use of cryptocurrencies will be
                            inevitable in the near future.                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How to buy cryptocurrency?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            By registering on the Rootix cryptocurrency Exchange site, you can easily
                            make Rial deposits through your bank portal and easily convert it into your
                            favorite cryptocurrency through the site.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How to sell cryptocurrency?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            At Rootix Exchange, use simple, powerful and secure facilities and convert
                            all types of digital currencies into Rials or any other type of cryptocurrency.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How much is the cryptocurrency trading fee in Rootix?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            Fees depend on the country's banking system and
                            cryptocurrency network fees and are different. In
                            Rootix Exchange, you will experience the lowest
                            amount of fees among all Exchanges.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How to start?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            with <a href="{{route('register.form')}}" style="color: deepskyblue">registering</a> on the site, you can easily and only with your email address and
                            mobile number, enter your user panel and buy cryptocurrency from now on.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What is authentication?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            You need authentication to use Rootix Exchange services, which has
                            different levels. In the first level, you can use the Exchange facilities only
                            with your mobile phone number and email.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What information do we need for authentication?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            The first level requires a mobile number and
                            email. At higher levels, in order to use Exchange
                            services, you need to authenticate in the form of
                            bank account information and national code.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">Why should I give my identity information to Rootix?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            Rootix needs authentication to secure your Rial
                            and digital capital. Authentication prevents
                            anyone other than you from withdrawing or
                            transferring your capital.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Section Eight -->
    <section>
        <div class="forms">
            <div class="form">
                <div>
                    <h2>Did not find the answer to the question?</h2>
                </div>

                <form action="#">
                    <div class="inputs">
                        <div class="input">
                            <label for="f-name">Name</label>
                            <input
                                type="text"
                                id="f-name"
                                placeholder="Enter your name"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name">Last Name</label>
                            <input
                                type="text"
                                id="l-name"
                                placeholder="Enter your last name"
                            />
                        </div>
                    </div>

                    <div class="inputs">
                        <div class="input">
                            <label>Email</label>
                            <input
                                type="text"
                                placeholder="Enter your Email"
                            />
                        </div>

                        <div class="input">
                            <label>Site address</label>
                            <input
                                type="text"
                                placeholder="Enter your site address"
                            />
                        </div>
                    </div>

                    <div class="inputs last-input">
                        <div class="input">
                            <label>Message</label>
                            <input
                                type="text"
                                placeholder="Enter your Message"
                            />
                        </div>
                    </div>

                    <button type="submit" class="btn light-blue-btn send-btn" disabled>
                        Send
                    </button>
                </form>
            </div>

            <div class="form-img">
                <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="form" />
            </div>
        </div>
    </section>
    <!-- End Section Eight -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
@endsection
