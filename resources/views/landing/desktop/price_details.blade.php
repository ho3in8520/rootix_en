@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    Rootix- {{$currency_info[$currency]['name']}} Price Details
@endsection
@section('header')
    <header class="blog-page-header">
        <div class="container">
            <nav class="nav blog-nav">
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>
        </div>
    </header>
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="page-address">
                <a href="{{route('landing.prices')}}"> Prices </a>
                >
                  {{$currency_info[$currency]['name']}} Details
            </div>

            <div class="d-flex currency-details">
                <div class="d-flex currency-details__info-container">
                    <div class="currency-details__img">
                        @php
                           $img= $currency_info[$currency]['logo_url']?'':$currency_info[$currency]['currency'];
                        @endphp
                        <img src="{{$currency_info[$currency]['logo_url']?str_replace('64x64','128x128',$currency_info[$currency]['logo_url']):asset("theme/landing/images/currency/$img.png")}}" alt="{{$currency_info[$currency]['currency']}}"/>
                    </div>
                </div>

                <div class="d-flex justify-between w-100 container-details-info">
                    <div class="currency-details__info">
                        <h1>{{$currency_info[$currency]['currency']}}</h1>
                        <span>{{$currency_info[$currency]['name']}}</span>
                    </div>

                    <div class="currency-details__prices-info">
                        <h3 class="currency-details__prices-title">
                             {{$currency_info[$currency]['name']}} Price:
                        </h3>
                        <div class="d-flex items-center currency-details__prices-container">
                            <h2 class="currency-details__prices">
                                @if($currency_info[$currency]['price'] >=1)
                                    {{number_format($currency_info[$currency]['price'],3)}}<span>USDT</span>
                                @else {{number_format($currency_info[$currency]['price'],7)}}<span>USDT</span>
                                @endif

                            </h2>

                            <div class="d-flex">
                                <div class="currency-details__prices-change-{{$currency_info[$currency]['1d']>0?'green':'red'}}">
                                                      <span>
                                                          <svg
                                                              xmlns="http://www.w3.org/2000/svg"
                                                              width="14"
                                                              height="11"
                                                              viewBox="0 0 17.704 11.652"
                                                          >
                                                          <path
                                                              id="Icon_feather-chevron-up"
                                                              data-name="Icon feather-chevron-up"
                                                              d="M14.892,9.246,7.446,0,0,9.246"
                                                              transform="translate(16.298 10.652) rotate(180)"
                                                              fill="none"
                                                              stroke="#fff"
                                                              stroke-linecap="round"
                                                              stroke-linejoin="round"
                                                              stroke-width="2"
                                                          />
                                                        </svg>
                                                       {{number_format($currency_info[$currency]['1d'],2)}}
                                                      </span>
                                </div>

                                <div class="currency-details__prices-selected">
                                    <div class="title-selected" dir="ltr">
                                        <div>
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="13"
                                                height="10"
                                                viewBox="0 0 18 11.115"
                                            >
                                                <path
                                                    id="Icon_material-expand-more"
                                                    data-name="Icon material-expand-more"
                                                    d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                    transform="translate(-9 -12.885)"
                                                    fill="#1b274c"
                                                />
                                            </svg>
                                        </div>
                                        <h5>USDT</h5>
                                    </div>

                                    <ul class="h-100 selected-currency">
                                        <li value="BTC">BTC</li>
                                        <li value="TRX">USDT</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="buying-btn">
                        <a href="{{route('exchange.index',['des_unit'=>strtolower($currency_info[$currency]['currency']),'source_unit'=>'usdt'])}}"> Buy </a>
                        <a href="{{route('exchange.index',['des_unit'=>strtolower($currency_info[$currency]['currency']),'source_unit'=>'usdt'])}}"> Sell </a>
                    </div>
                </div>
            </div>
        </div>

        <hr class="details-currency-line"/>
    </section>

    <section class="details-chart">
        <div class="container">
            <div class="details-chart-container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-9">
                        <div class="chart-info">
                            <h2>Chart</h2>
                            <div class="chart-image">
                                <div class="tradingview-widget-container" style="pointer-events: none">
                                    <div id="tradingview_81a31"></div>
                                    <div style="pointer-events: none">
                                        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                        <script type="text/javascript">
                                            new TradingView.MediumWidget(
                                                {
                                                    "symbols": [
                                                        [
                                                            "{{$currency_info[$currency]['currency']}}USDT",
                                                            "{{$currency_info[$currency]['currency']}}USDT"
                                                        ],
                                                    ],
                                                    "chartOnly": false,
                                                    "width": "100%",
                                                    "height": 400,
                                                    "locale": "en",
                                                    "colorTheme": "light",
                                                    "gridLineColor": "rgba(240, 243, 250, 0)",
                                                    "trendLineColor": "#2962FF",
                                                    "fontColor": "#787B86",
                                                    "underLineColor": "rgba(41, 98, 255, 0.3)",
                                                    "underLineBottomColor": "rgba(41, 98, 255, 0)",
                                                    "isTransparent": false,
                                                    "autosize": true,
                                                    "showFloatingTooltip": false,
                                                    "scalePosition": "right",
                                                    "scaleMode": "Normal",
                                                    "container_id": "tradingview_81a31"
                                                }
                                            );
                                        </script>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="h-100">
                            <h2>Changes Table</h2>

                            <div class="table-change">
                                <div class="table-change-item">
                                    <p>1 Hour</p>
                                    <span dir="ltr" class="{{$currency_info[$currency]['1h']>=0?'plus':'minus'}}-details">
                                        {{$currency_info[$currency]['1h']>=0?'+'.number_format($currency_info[$currency]['1h'],2):number_format($currency_info[$currency]['1h'],2)}}
                                    </span>
                                </div>
                                <div class="table-change-item">
                                    <p>24 Hour</p>
                                    <span dir="ltr" class="{{$currency_info[$currency]['1d']>=0?'plus':'minus'}}-details">
                                        {{$currency_info[$currency]['1d']>=0?'+'.number_format($currency_info[$currency]['1d'],2):number_format($currency_info[$currency]['1d'],2)}}
                                    </span>
                                </div>
                                <div class="table-change-item">
                                    <p>7 Days</p>
                                    <span dir="ltr" class="{{$currency_info[$currency]['7d']>=0?'plus':'minus'}}-details">
                                        {{$currency_info[$currency]['7d']>=0?'+'.number_format($currency_info[$currency]['7d'],2):number_format($currency_info[$currency]['7d'],2)}}
                                    </span>
                                </div>
                                <div class="table-change-item">
                                    <p>30 Days</p>
                                    <span dir="ltr" class="{{$currency_info[$currency]['30d']>=0?'plus':'minus'}}-details">
                                        {{$currency_info[$currency]['30d']>=0?'+'.number_format($currency_info[$currency]['30d'],2):number_format($currency_info[$currency]['30d'],2)}}
                                    </span>
                                </div>
                                <div class="table-change-item">
                                    <p>60 Days</p>
                                    <span dir="ltr" class="{{$currency_info[$currency]['60d']>=0?'plus':'minus'}}-details">
                                        {{$currency_info[$currency]['60d']>=0?'+'.number_format($currency_info[$currency]['60d'],2):number_format($currency_info[$currency]['60d'],2)}}
                                    </span>
                                </div>
                                <div class="table-change-item">
                                    <p>90 Days</p>
                                    <span dir="ltr" class="{{$currency_info[$currency]['90d']>=0?'plus':'minus'}}-details">
                                        {{$currency_info[$currency]['90d']>=0?'+'.number_format($currency_info[$currency]['90d'],2):number_format($currency_info[$currency]['90d'],2)}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="details-table-container details-chart-container">
                <span>Market</span>

                <hr class="details-currency-line details-currency-line-3"/>

                <table class="details-table">
                    <tr>
                        <td>Name</td>
                        <td>Price</td>
                        <td>% 24h</td>
                        <td>Most 24h</td>
                        <td>Lowest 24h</td>
                        <td>Volume 24h</td>
                        <td>Value 24h</td>
                    </tr>
                    <tr>
                        <td>
                            <small> {{$currency_info[$currency]['currency']}}/USDT </small>
                        </td>
                        <td>
                            <small>
                                @if($currency_info[$currency]['price']>=1)
                                    {{number_format($currency_info[$currency]['price'])}}
                                @else
                                    {{number_format($currency_info[$currency]['price'],3)}}
                                @endif
                            </small>
                        </td>
                        <td dir="ltr">
                            <small class="change-percent-price change-percent-price-{{$currency_info[$currency]['1d']>=0?'plus':'minus'}}">
                               % {{$currency_info[$currency]['1d']>=0?'+'.number_format($currency_info[$currency]['1d'],2):number_format($currency_info[$currency]['1d'],2)}}
                            </small>
                        </td>
                        <td>
                            <small>-</small>
                        </td>
                        <td>
                            <small>- </small>
                        </td>
                        <td>
                            @if(isset($currency_info[$currency]['volume']))
                            <small>{{number_format($currency_info[$currency]['volume']/100000000,2)}}B</small>
                            @else
                            -
                            @endif
                        </td>
                        <td>
                            <small> -</small>
                        </td>
                        <td>
                            <a href="{{route('exchange.index',['des_unit'=>strtolower($currency_info[$currency]['currency']),'source_unit'=>'usdt'])}}"> Trade </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>

    <section class="about-currency-details-container">
        <div class="container">

            <div class="prices-details-container">
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/selected-currency.js')}}"></script>
@endsection
