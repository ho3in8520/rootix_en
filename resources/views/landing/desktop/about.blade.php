@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    About Rootix
@endsection
@section('header')
    <header class="about-header">
        <div class="container">
            <nav>
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>

        </div>
        <div class="d-flex items-center header-row">

            <div class="about-header-image">
                <img src="{{asset('theme/landing/images/about-us/about-header-image.png')}}" alt="About Rootix">
            </div>

            <div class="about-header-info">
                <h5>About Rootix</h5>
                <h1>Creative and practical company</h1>
            </div>
        </div>

        <div class="go-down-container">
            <a href="#currency-table" class="go-down about-go-down-container">
                <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="Scroll down" />
            </a>
        </div>
    </header>
@endsection
@section('content')
    <!-- Start Section One -->
    <section class="history">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="company-history-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-1.png')}}" alt="history of the company">
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="history-descriptions">
                        <h3>history of the company</h3>
                        <p>
                            This exchange has originated from the efforts and zeal of the youth of this region with
                            the aim of facilitating digital currency transactions for our dear compatriots. Our goal
                            is to provide a safe, fast and convenient platform for buying and selling top digital
                            currencies such as Bitcoin, Tetra, Tron, BitTorrent, etc. in the shortest time and high
                            security between buyer and seller. Given the many problems for Iranians, including
                            difficult access to foreign markets, the company aims to use the experience and upto-date knowledge of its forces in the field of digital currency and as a mediator
                            between buyers and sellers, to ensure the security of transactions. People can now
                            view the price of bitcoins online from the trading platform in the professional
                            platform provided by Rootix, and buy or sell bitcoins in the shortest possible time
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="about-currency">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-5">
                    <div class="about-currency-descriptions">
                        <h5>About cryptocurrency</h5>
                        <h2>
                            Cryptocurrency
                            <br>
                            is always and everywhere economical
                        </h2>
                        <h3>How does cryptocurrency help your business?</h3>
                        <p>
                            Cryptocurrencies are tools for personal exchanges between individuals and are
                            not managed by any government. Therefore, inflation can be controlled. One of
                            the good things about cryptocurrencies is that they hide their identities. That is
                            why this financial market is not attributed to a specific person or persons. Other
                            features include its ubiquity, which has no limits and can be used by anyone
                            anywhere in the world. Elimination of intermediaries is also a positive point that
                            any person can do his transaction without the need for an intermediary or a third
                            party. All transactions that are recorded are added to the blockchain in the form
                            of chains in the form of blocks, and also all transactions must be approved by the
                            miners that exist in this network. Also keep in mind that no transaction can be
                            deleted or changed. To save and keep this cryptocurrency, you need wallets,
                            which have different types, and we will discuss them in the articles section.
                        </p>
                        <a href="{{route('login.form')}}" class="about-btn about-currency-btn">
                            Start trading
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-7">
                    <div class="about-currency-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-2.png')}}" alt="history of the company">
                    </div>
                </div>

            </div>
        </div>

        <img src="{{asset('theme/landing/images/about-us/left-background-sec-3.png')}}" alt="right background" class="back-sec-3 left-back">
        <img src="{{asset('theme/landing/images/about-us/right-background-sec-3.png')}}" alt="left background" class="back-sec-3 right-back">
    </section>
    <!-- End Section Two -->

    <!-- Start Section Three -->
    <section class="rootix-team">
        <div class="container-secondary">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="about-currency-img">
                        <img src="{{asset('theme/landing/images/team.png')}}" alt="team-work">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="team-info">
                        <h2>Rootix team members</h2>
                        <p>
                            The Rootix team consists of 10 top experts with experience in
                            web and software development and 5 experts and activists in
                            the field of crypto, who work in the field of development and
                            programming and educational articles, analysis and top news.
                            To date, this team has tried to raise the level of awareness of
                            users in this field and continues to do so.
                        </p>
                        <a href="#" class="about-btn">
                            Join us
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Three -->


    <!-- Start Section Four -->
    <section class="rootix-company counter-row-2">
        <div class="container-secondary">
            <div class="company-title">
                <h2>Strong company in the field of cryptocurrency</h2>
                <p>
                    Rootix is a strong digital currency company with the goal of creating a
                    secure platform for a secure future to trade with complete security in a
                    decentralized system, a few of which are: 1. Buy and sell cryptocurrencies
                    directly without Intermediaries on the blockchain platform 2. Creating a
                    platform for financial exchanges for reputable traders to import and export
                    in the most convenient and safe way in different countries of the world.
                </p>
           <p>Future plans: The possibility of buying and selling on store sites with cryptocurrencies in a decentralized manner.</p>
            </div>
            <div class="row row-sec-5-four">
                <div class="col-12">
                    <div>
                        <button class="about-video">
                            <img src="{{asset('theme/landing/images/about-us/sec-four.png')}}" alt="Film">
                            <video src="" controls class="self-video"></video>
                        </button>
                        <hr class="rootix-company-line">
                    </div>
                </div>
            </div>
            <div class="row counter-row-1">
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>Panel clients</h3>
                        <h5 id="about-customers">256,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>Daily transactions</h3>
                        <h5 id="daily-transaction">256,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>Monthly transactions</h3>
                        <h5 id="monthly-transaction">256,000</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="team-contact">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="team-contact__form-container">
                        <div>
                            <h2 class="team-contact__title">Contact support</h2>
                            <p class="team-contact__desc">Our team aims to provide the best services for Rootix site users. We are happy to share your criticisms and comments with us.</p>
                        </div>

                        <form action="#" class="team-contact__form">
                            <div class="team-contact__inputs">
                                <input type="email" placeholder="Nme ..." class="team-contact__input ">
                                <input type="text" placeholder="Email ..." class="team-contact__input ">
                            </div>

                            <textarea placeholder="Message ..." class="team-contact__textarea"></textarea>

                            <button type="submit" class="team-contact__btn about-btn " disabled>Send</button>
                        </form>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="about-form__image">
                        <img src="{{asset('theme/landing/images/about-us/Ai.png')}}" class="form-image" alt="Contact us">
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('theme/landing/images/about-us/back-sec-5.png')}}" alt="last background" class="back-sec-five">
    </section>
    <!-- End Section Five -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/about-counter.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/about-video-playr.js')}}"></script>
@endsection
