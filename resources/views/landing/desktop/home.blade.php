@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
   Rootix-online crypto exchange
@endsection
@section('style')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js" type="text/javascript"></script>
    <style>
        .currency-chart{
            width: 95px;
            height: 57px;
            margin: 0 auto;
        }
        .customer{
            height: 250px; !important;
        }
    </style>
@endsection
@section('header')
    <!-- Start Header -->
    <header class="header">
        <div class="container">
            <nav class="nav">
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>
        </div>

        <div class="container">
            <div class="row header-row">
                <div class="col-12 col-md-6">
                    <div class="header-info">
                        <div>
                            <h1>
                                An easy and safe way
                                <h2>to earn
                                    bitcoins</h2>
                            </h1>
                        </div>
                        <ul>
                            <li>Experience super security with us.</li>
                            <li>Have your own wallet</li>
                            <li>Trade with the lowest fee.</li>
                            <li>Variety in high speed trades only in Rootix.</li>
                        </ul>

                        <div class="header-btns">
                            <a href="{{route('register.form')}}" class="btn light-blue-btn animation-btn">
                                Register
                            </a>
                            <a href="#" class="btn main-btn">
                                How Rootix Works
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="header-currency-image">
                        <img src="{{asset('theme/landing/images/header-img.png')}}" alt="crypto-currency"
                             class="header-currency">
                    </div>
                </div>
            </div>
        </div>
        </div>

        <div class="header-currency-header-info">
            <div class="container">
                <h2>Rootix 24
                    -hour trading volume : 250.000.000</h2>
            </div>

            <div class="header-currency-informations-container">
                <div class="container">

                    <div class="currencys">

{{--                        <div class="header-currency-informations">--}}
{{--                            @php--}}
{{--                                $currencies1=get_specific_currencies('btc,eth,ltc,usdt,xrp');--}}
{{--                            @endphp--}}
{{--                            @foreach($currencies1 as $key=>$item)--}}
{{--                                @php--}}
{{--                                $sign=$item['1d']<0?'-':'+';--}}
{{--                                $value=str_replace('-','',$item['1d']);--}}
{{--                                @endphp--}}
{{--                                <div class="header-currency-information">--}}
{{--                                    <div class="header-currency-desc">--}}
{{--                                        <h4 class="header-currency-name">{{$key}}/USDT</h4>--}}

{{--                                        <h4 class="{{$item['1d']<0?'down':'up'}}-currency">--}}
{{--                                            {{$sign}}  {{number_format((float)$value,2)}}  {{'%'}}--}}
{{--                                        </h4>--}}
{{--                                    </div>--}}
{{--                                    <div class="header-currency-price-container">--}}
{{--                                        <h3 class="header-currency-price">--}}
{{--                                            {{number_format($item['price'],2)}}--}}
{{--                                        </h3>--}}
{{--                                        <h4>$0.0256</h4>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}

{{--                        <div class="header-currency-informations">--}}
{{--                            @php--}}
{{--                            $currencies2=get_specific_currencies('bch,bnb,eos,jst,win');--}}
{{--                            @endphp--}}
{{--                            @foreach($currencies2 as $key=>$item)--}}
{{--                                @php--}}
{{--                                    $sign=$item['1d']<0?'-':'+';--}}
{{--                                    $value=str_replace('-','',$item['1d']);--}}
{{--                                @endphp--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">{{$key}}/USDT</h4>--}}

{{--                                    <h4 class="{{$item['1d']<0?'down':'up'}}-currency">--}}
{{--                                        {{$sign}} {{number_format((float)$value,2)}} {{"%"}}--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">--}}
{{--                                        {{number_format($item['price'],2)}}--}}
{{--                                    </h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}

{{--                        <div class="header-currency-informations">--}}
{{--                            <div class="header-currency-information header-currency-information-1">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">DOGE/BUSD</h4>--}}

{{--                                    <h4 class="down-currency">--}}
{{--                                        -12.5%--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">0.0002569</h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">DOGE/BUSD</h4>--}}

{{--                                    <h4 class="up-currency">--}}
{{--                                        -12.5%--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">0.0002569</h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">DOGE/BUSD</h4>--}}

{{--                                    <h4 class="down-currency">--}}
{{--                                        -12.5%--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">0.0002569</h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">DOGE/BUSD</h4>--}}

{{--                                    <h4 class="up-currency">--}}
{{--                                        -12.5%--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">0.0002569</h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">DOGE/BUSD</h4>--}}

{{--                                    <h4 class="up-currency">--}}
{{--                                        -12.5%--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">0.0002569</h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                    </div>
                </div>
            </div>
        </div>
        <div class="go-down-container">
            <a href="#currency-table" class="go-down">
                <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="go-down"/>
            </a>
        </div>
    </header>
    <!-- End Header -->
@endsection

@section('content')
    <!-- Start Section One -->
    <section class="p-t table-currency">
        <div class="container">
            <a id="currency-table"></a>

            <div class="row">
                <div class="col-12">
                    <table class="currency-table usdt-table active">
                        <tr>
                            <td>Name</td>
                            <td>Last Price</td>
                            <td>24H change</td>
                            <td>Most Price</td>
                            <td>Weekly Changes Charts</td>
                            <td></td>
                        </tr>
{{--                        @foreach($currency as $key=>$item)--}}
{{--                            @php--}}

{{--                                $sing=$item['1d']<0?'-':'+';--}}
{{--                                $value=str_replace('-','',$item['1d']);--}}

{{--                            @endphp--}}
{{--                            <tr>--}}
{{--                                <td class="currency-title">--}}
{{--                                    <div class="currency-right-title">--}}
{{--                                        <img src="{{$item['logo_url']}}"--}}
{{--                                             alt="{{ $item['name_fa']}}"/>--}}
{{--                                        <h5>{{$item['currency']}}</h5>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td class="currency-last-price">--}}
{{--                                    @if($item['price']>=1)--}}
{{--                                        {{number_format($item['price'],2)}}--}}
{{--                                    @else--}}
{{--                                        {{number_format($item['price'],5)}}--}}
{{--                                    @endif--}}
{{--                                </td>--}}
{{--                                <td class="currency-present-change">--}}
{{--                                    @if(isset($item['1d']))--}}
{{--                                        <span--}}
{{--                                            class="currency-present-change {{(float)$item['1d']<0?'red':'green'}}-currency">--}}
{{--                           {{number_format((float)$value,2)}} {{$sing}}--}}
{{--                          </span>--}}
{{--                                    @endif--}}

{{--                                </td>--}}
{{--                                <td class="currency-most-price">--}}
{{--                                    @if(isset($item['day_high']))--}}
{{--                                        {{$item['day_high']}}--}}
{{--                                    @else--}}
{{--                                        ---}}
{{--                                    @endif--}}
{{--                                </td>--}}
{{--                                <td class="currency-chart">--}}
{{--                                    <canvas id="{{$item['name']}}_chart" width="160" height="40"></canvas>--}}
{{--                                    <script type="text/javascript" >--}}
{{--                                        setTimeout(function(){--}}
{{--                                            chart("{{$item['name']}}_chart", "{{$item['color']}}");--}}
{{--                                        },1000);--}}
{{--                                    </script>--}}
{{--                                </td>--}}
{{--                                <td class="currency-buying-btn">--}}
{{--                                    <a href="{{route('exchange.index',['des_unit'=>strtolower($key),'source_unit'=>'usdt'])}}"--}}
{{--                                       class="btn light-blue-btn">Trade</a>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
                    </table>
                </div>
            </div>
        </div>


        <div class="all-currency-btn">
            <a href="{{route('landing.prices')}}" class="btn all-crypto-currency">
                More
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25.242" viewBox="0 0 24 25.242"><g transform="translate(30 30.621) rotate(180)"><path d="M28.5,18H7.5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/><path d="M18,28.5,7.5,18,18,7.5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/></g></svg>
            </a>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="section section-two">
        <div class="container">
            <h2 class="app-title">
                Working with Rootix is
                <br/>
                <span class="gold-title"> really easy</span>
            </h2>

            <div class="auth">
                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-1.png')}}" alt="Register"/>
                    <span>Register on the exchange site </span>
                    <p>
                        Enter your identity information
                        in the first step
                    </p>
                </div>
                <svg class="auth-arrow" xmlns="http://www.w3.org/2000/svg" width="66.65" height="44.452" viewBox="0 0 66.65 44.452"><path d="M32.028,12.1a3.025,3.025,0,0,1,.023,4.26L18,30.461H71.546a3.01,3.01,0,0,1,0,6.019H18l14.075,14.1a3.047,3.047,0,0,1-.023,4.26,3,3,0,0,1-4.237-.023L8.739,35.6h0a3.38,3.38,0,0,1-.625-.949,2.872,2.872,0,0,1-.232-1.158,3.017,3.017,0,0,1,.857-2.107L27.815,12.172A2.949,2.949,0,0,1,32.028,12.1Z" transform="translate(74.532 55.704) rotate(180)" fill="#6bb9f0"/></svg>
                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-2.png')}}" alt="Authentication"/>
                    <span> Authentication </span>
                    <p>
                        Verify the entered information
                        and confirm the contact
                        number and email

                    </p>
                </div>
                <svg class="auth-arrow" xmlns="http://www.w3.org/2000/svg" width="66.65" height="44.452" viewBox="0 0 66.65 44.452"><path d="M32.028,12.1a3.025,3.025,0,0,1,.023,4.26L18,30.461H71.546a3.01,3.01,0,0,1,0,6.019H18l14.075,14.1a3.047,3.047,0,0,1-.023,4.26,3,3,0,0,1-4.237-.023L8.739,35.6h0a3.38,3.38,0,0,1-.625-.949,2.872,2.872,0,0,1-.232-1.158,3.017,3.017,0,0,1,.857-2.107L27.815,12.172A2.949,2.949,0,0,1,32.028,12.1Z" transform="translate(74.532 55.704) rotate(180)" fill="#6bb9f0"/></svg>
                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-3.png')}}" alt="Trade"/>
                    <span>Start trading</span>
                    <p>
                        Receive confirmation email and
                        enter bank account information.
                        Wait for confirmation in the next
                        section
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->

{{--    @if(count($new_post) > 5)--}}
        <!-- Start Section Three -->
{{--        <section>--}}
{{--            <div class="container">--}}
{{--                <div class="blog-title text-center">--}}
{{--                    <h2>Rootix Blog</h2>--}}
{{--                </div>--}}
{{--                <div class="row row-padding">--}}
{{--                    <div class="col-12 col-md-6 p-left p-right" dir="rtl">--}}
{{--                        <div class="blog-container" dir="rtl">--}}
{{--                            <div class="right-blog top-blog blog-info">--}}
{{--                                <a href="#">--}}
{{--                                    <img src="{{asset('theme/landing/images/et.png')}}" alt="Currency Title" />--}}
{{--                                    <div class="currency-blog">--}}
{{--                                        <p>--}}
{{--                                            How To Survive In the Declining Digital Currency Market?                      </p>--}}
{{--                                        <div class="calender">--}}
{{--                                            <p>‏20 Feb 2021</p>--}}

{{--                                            <div>--}}
{{--                                                19--}}
{{--                                                <i class="far fa-comment"></i>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}

{{--                            <div class="right-blog down-blog">--}}
{{--                                <div>--}}
{{--                                    <div class="blog-info blog-desc">--}}
{{--                                        <a href="#" class="link-currency">--}}
{{--                                            <img src="{{asset('theme/landing/images/ethereum@2x.png')}}" alt="Currency Title" />--}}
{{--                                            <div class="currency-blog">--}}
{{--                                                <p>--}}
{{--                                                    How To Survive In the Declining Digital Currency Market?--}}
{{--                                                </p>--}}
{{--                                                <div class="calender">--}}
{{--                                                    <p>20 Feb 2021</p>--}}

{{--                                                    <div>--}}
{{--                                                        19--}}
{{--                                                        <i class="far fa-comment"></i>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div dir="ltr">--}}
{{--                                    <div class="blog-info blog-desc" dir="rtl">--}}
{{--                                        <a href="#" class="link-currency">--}}
{{--                                            <img src="{{asset('theme/landing/images/ethereum@2x.png')}}" alt="Currency Title" />--}}
{{--                                            <div class="currency-blog">--}}
{{--                                                <p>--}}
{{--                                                    How To Survive In the Declining Digital Currency Market?--}}
{{--                                                </p>--}}
{{--                                                <div class="calender">--}}
{{--                                                    <p>20 Feb 2021</p>--}}

{{--                                                    <div>--}}
{{--                                                        19--}}
{{--                                                        <i class="far fa-comment"></i>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-12 col-md-6 p-left p-right">--}}
{{--                        <div class="left-blog blog-info">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('theme/landing/images/ethereum.png')}}" alt="Currency Title" />--}}
{{--                                <div class="currency-blog">--}}
{{--                                    <p>--}}
{{--                                        How To Survive In the Declining Digital Currency Market?--}}
{{--                                    </p>--}}
{{--                                    <div class="calender">--}}
{{--                                        <p>‏20 Feb 2021</p>--}}

{{--                                        <div>--}}
{{--                                            19--}}
{{--                                            <i class="far fa-comment"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
        <!-- End Section Three -->
{{--    @endif--}}

    <!-- Start Section Four -->
    <section class="what-currency section-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="what-currency-info">
                        <h5>What is cryptocurrency?</h5>
                        <div class="what-crypto-title">
                            <h2>Let's talk a little bit about</h2>
                            <span>cryptocurrency</span>
                        </div>
                        <p class="what-crypto-desc">
                            Cryptocurrency is a digital
                            currency. Currency generation
                            and authentication of money
                            transactions is controlled using
                            encryption algorithms, and
                            usually works decentrally
                            (without dependence on a
                            central bank).
                        </p>

                        <a href="#" class="what-currency-more-btn light-blue-btn btn">
                            Read more
                        </a>

                        <div class="about-us">
                            <span>A little about us: </span>
                            <a href="#"> Click </a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="crypto-carts">
                        <div class="crypto-carts-2 crypto-carts-3">
                            <div class="crypto-cart">
                                <img src="{{asset('theme/landing/images/bitcoin.png')}}" alt="Digital money"/>
                                <div class="crypto-cart-desc">
                                    <h3>Digital money</h3>
                                    <p>
                                        There are currencies that are stored and transferred electronically
                                    </p>
                                </div>
                            </div>

                            <div class="crypto-cart">
                                <img src="{{asset('theme/landing/images/Icon.png')}}" alt="Always available"/>
                                <div class="crypto-cart-desc">
                                    <h3>Always available</h3>
                                    <p>
                                        Strong 24-hour Rootix
                                        support for a better trading
                                        experience
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="crypto-carts-2">
                            <div class="crypto-cart">
                                <img src="{{asset('theme/landing/images/credit-card.png')}}" alt="Transaction security"/>
                                <div class="crypto-cart-desc">
                                    <h3>Transaction security</h3>
                                    <p>
                                        Protecting user
                                        information and
                                        transaction information
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="feature">
        <div class="container-fluid">
            <h2 class="feature-title">
                Rootix
                <span class="gold-title gold-feature"> features </span>
            </h2>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-1.jpg')}}" alt="ثبت نام سریع و راحت"/>
                    </div>
                </div>

                <div class="col-12 col-md-6" dir="ltr">
                    <div class="feature-info" dir="rtl">
                        <div class="feature-info-title">
                            <h2>Convenient and fast in registration </h2>
                            <span>andauthentication</span>
                        </div>

                        <div>
                            <p>
                                Register easily and quickly in one of
                                the best exchanges, Rootix, by
                                filling out two authentication forms
                                and entering your information
                            </p>

                            <a href="#" class="feature-btn">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25"><image width="25" height="25" transform="translate(25 25) rotate(180)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAB4UlEQVR4nO3cv0tWURzH8bc/UomIRKKWcAkaG1ukMRLEIPoTGuwfcMrZIWhoaXNpEYeWIGgIoiFoiCAIQuR5EAyCaAmCkPBxuEnpeRwa4vvF7/sFZ7vDBz5czr3nnnNBkiRJkiRJkiRJkiRJkiRJ0km3CHwBvgF3grOUtwjsAoPf42tsnNpuAD/5U8aA7k5RgOvADw6X8Qu4HRmqqmvAdw6XsQfcjQxV1VW6yftoGfciQ1V1hW6OGBwZy5GhqroMfKYt435kqKouAX3aMh5GhqrqAvCJtoxHkaGqOg98pC1jDRgJzFXSOeAdbRkbwFhgrpLOAm9py3gKjAfmKuk08Iq2jBfAZFysmiaA57RlvASmAnOVdAp4RlvGG+BMYK6SxoB12jLeA9OBuUoaBZ7QlvEBmAnMVdII8Ji2jE3gYmCush7QlrENzEaGqmqVtgzH/x09YH5YGSsJwlUdOwcljA5rRnH+Xn96TfeiNxeUpao+sARsHXeBk3oyPvYm5IthQi6dJOTiYkIuvyfkB6qE/ISbkJscEnIbUEJulEvIraQJudk6IY8jJOSBnYQ80pbQcYc+b0WGqm7YseheaCI1Pw7ox8YRwALdndEDbgZnkSRJkiRJkiRJkiRJkiRJkqR/sg+eUJJlBD5ToQAAAABJRU5ErkJggg=="/></svg>
                                Read more
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-sec-five">
                <div class="col-12 col-md-6">
                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>Security and
                                protection of
                                user information</h2>
                        </div>
                        <p>
                            Routix considers it its duty
                            to ensure the security of its
                            users' information so that
                            users can trade with ease.
                        </p>

                        <a href="#" class="feature-btn">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="25"
                                height="25"
                                viewBox="0 0 25 25"
                            >
                                <image
                                    id="icons8-arrows_long_left_copy"
                                    data-name="icons8-arrows_long_left copy"
                                    width="25"
                                    height="25"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAB4UlEQVR4nO3cv0tWURzH8bc/UomIRKKWcAkaG1ukMRLEIPoTGuwfcMrZIWhoaXNpEYeWIGgIoiFoiCAIQuR5EAyCaAmCkPBxuEnpeRwa4vvF7/sFZ7vDBz5czr3nnnNBkiRJkiRJkiRJkiRJkiRJ0km3CHwBvgF3grOUtwjsAoPf42tsnNpuAD/5U8aA7k5RgOvADw6X8Qu4HRmqqmvAdw6XsQfcjQxV1VW6yftoGfciQ1V1hW6OGBwZy5GhqroMfKYt435kqKouAX3aMh5GhqrqAvCJtoxHkaGqOg98pC1jDRgJzFXSOeAdbRkbwFhgrpLOAm9py3gKjAfmKuk08Iq2jBfAZFysmiaA57RlvASmAnOVdAp4RlvGG+BMYK6SxoB12jLeA9OBuUoaBZ7QlvEBmAnMVdII8Ji2jE3gYmCush7QlrENzEaGqmqVtgzH/x09YH5YGSsJwlUdOwcljA5rRnH+Xn96TfeiNxeUpao+sARsHXeBk3oyPvYm5IthQi6dJOTiYkIuvyfkB6qE/ISbkJscEnIbUEJulEvIraQJudk6IY8jJOSBnYQ80pbQcYc+b0WGqm7YseheaCI1Pw7ox8YRwALdndEDbgZnkSRJkiRJkiRJkiRJkiRJkqR/sg+eUJJlBD5ToQAAAABJRU5ErkJggg=="
                                />
                            </svg>
                            Read more
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-6" dir="ltr">
                    <div class="feature-img-2">
                        <img src="{{asset('theme/landing/images/sec-five-2.jpg')}}" alt="safe"/>
                    </div>
                </div>
            </div>

            <div class="row row-sec-five">
                <div class="col-12 col-md-6">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-3.jpg')}}" alt="Easy trading"/>
                    </div>
                </div>

                <div class="col-12 col-md-6" dir="ltr">
                    <div class="feature-info" dir="rtl">
                        <div class="feature-info-title">
                            <h2>Easy trading</h2>
                        </div>

                        <div>
                            <p>
                                Rootix with easy and
                                fast user interface
                            </p>

                            <a href="#" class="feature-btn">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25"><image width="25" height="25" transform="translate(25 25) rotate(180)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAB4UlEQVR4nO3cv0tWURzH8bc/UomIRKKWcAkaG1ukMRLEIPoTGuwfcMrZIWhoaXNpEYeWIGgIoiFoiCAIQuR5EAyCaAmCkPBxuEnpeRwa4vvF7/sFZ7vDBz5czr3nnnNBkiRJkiRJkiRJkiRJkiRJ0km3CHwBvgF3grOUtwjsAoPf42tsnNpuAD/5U8aA7k5RgOvADw6X8Qu4HRmqqmvAdw6XsQfcjQxV1VW6yftoGfciQ1V1hW6OGBwZy5GhqroMfKYt435kqKouAX3aMh5GhqrqAvCJtoxHkaGqOg98pC1jDRgJzFXSOeAdbRkbwFhgrpLOAm9py3gKjAfmKuk08Iq2jBfAZFysmiaA57RlvASmAnOVdAp4RlvGG+BMYK6SxoB12jLeA9OBuUoaBZ7QlvEBmAnMVdII8Ji2jE3gYmCush7QlrENzEaGqmqVtgzH/x09YH5YGSsJwlUdOwcljA5rRnH+Xn96TfeiNxeUpao+sARsHXeBk3oyPvYm5IthQi6dJOTiYkIuvyfkB6qE/ISbkJscEnIbUEJulEvIraQJudk6IY8jJOSBnYQ80pbQcYc+b0WGqm7YseheaCI1Pw7ox8YRwALdndEDbgZnkSRJkiRJkiRJkiRJkiRJkqR/sg+eUJJlBD5ToQAAAABJRU5ErkJggg=="/></svg>
                                Read more
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Five -->

    <!-- Start Section Six -->
    <section class="mobile-app section-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="what-currency-info">
                        <h5>Do not forget the mobile
                            application</h5>
                        <div class="what-crypto-title">
                            <h2>Check the market always
                                and everywhere</h2>
                            <span> with the
Rootix mobile application</span>
                        </div>
                        <p class="what-crypto-desc">
                            Rootix panel can also be used on
                            smartphones. Easily accessible and with
                            its advanced and easy user interface,
                            provide the experience of working with
                            an exchange at any time and any place
                            using a smartphone, for its users.
                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="mobile-images">
                        <img src="{{asset('theme/landing/images/mobile-1.png')}}" alt="mobile app "/>
                        <img src="{{asset('theme/landing/images/mobile-2.png')}}" alt=" mobile app"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Six -->

    <!-- Start Section Eight -->
    <section id="contact-us">
        <div class="forms">
            <div class="form">
                <div>
                    <h2>Contact Us</h2>
                    <p style="margin-bottom: 13px;">
                        Join us in the development of Rootix. Share your
                        comments and criticisms with us about your
                        experience working with Rootix. Our team is eager
                        for your feedback to make Rootix better.
                    </p>
                    <div style="font-size: 14px;">
{{--                        <i class="fa fa-phone"></i>--}}
{{--                        <span>Contact number</span>--}}
{{--                        <span></span>--}}
                    </div>
                    {{--<div style="font-size: 14px;margin-top: 10px;">
                        <i class="fa fa-map"></i>
                        <span>address:</span>
                        <span></span>
                    </div>--}}
                </div>

                <form action="#" style="margin-top: 25px;">
                    <div class="inputs">
                        <div class="input">
                            <label for="f-name1">Name</label>
                            <input
                                type="text"
                                id="f-name1"
                                placeholder="Enter name"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name1">Lastname</label>
                            <input
                                type="text"
                                id="l-name1"
                                placeholder="Enter lastname"
                            />
                        </div>
                    </div>

                    <div class="inputs">
                        <div class="input">
                            <label for="f-name2">Email</label>
                            <input
                                type="text"
                                id="f-name2"
                                placeholder="Enter email"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name2">Message</label>
                            <input
                                type="text"
                                id="l-name2"
                                placeholder="Enter message"
                            />
                        </div>
                    </div>

                    <button type="submit" class="btn light-blue-btn send-btn">
                       Send
                    </button>
                </form>
            </div>

            <div class="form-img">
                <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="form"/>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('#usdt').on('click', function () {
            $('.rial-table').removeClass('active');
            $('.usdt-table').addClass('active');
        });

        $('#rial').on('click', function () {
            $('.rial-table').addClass('active');
            $('.usdt-table').removeClass('active');
        })
    </script>
    <script src="{{asset('theme/user/scripts/currency_chart.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/header-slider.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/index-notification.js')}}"></script>
@endsection
