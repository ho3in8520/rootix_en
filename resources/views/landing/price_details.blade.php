@mobile
@include('landing.mobile.price_details')
@endmobile

@tablet
@include('landing.mobile.price_details')
@endtablet

@desktop
@include('landing.desktop.price_details')
@enddesktop
