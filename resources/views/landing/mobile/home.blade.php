{{--@extends('templates.landing_page.layout.mobile.master_page')--}}
@section('title_browser')
    Rootix-online crypto exchange
@endsection
@section('detail')
    <div class="res-menu__bottom">
        <div class="res-menu__search">
            <button>
                <svg xmlns="http://www.w3.org/2000/svg" width="17.302" height="17.302" viewBox="0 0 17.302 17.302">
                    <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(-4 -4)">
                        <path id="Path_8028" data-name="Path 8028"
                              d="M18.807,11.653A7.153,7.153,0,1,1,11.653,4.5,7.153,7.153,0,0,1,18.807,11.653Z"
                              fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                              stroke-width="1"/>
                        <path id="Path_8029" data-name="Path 8029" d="M28.865,28.865l-3.89-3.89"
                              transform="translate(-8.27 -8.27)" fill="none" stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="1"/>
                    </g>
                </svg>

            </button>

            <input type="text" placeholder="Search ...">
        </div>
        <a href="{{route('login.form')}}" class="res-menu__login">
            Login
            <svg xmlns="http://www.w3.org/2000/svg" width="20.243" height="13.501" viewBox="0 0 20.243 13.501">
                <path id="Icon_ionic-ios-arrow-round-forward" data-name="Icon ionic-ios-arrow-round-forward" d="M20.784,11.51a.919.919,0,0,0-.007,1.294l4.275,4.282H8.782a.914.914,0,0,0,0,1.828H25.045L20.77,23.2a.925.925,0,0,0,.007,1.294.91.91,0,0,0,1.287-.007l5.794-5.836h0a1.026,1.026,0,0,0,.19-.288.872.872,0,0,0,.07-.352.916.916,0,0,0-.26-.64l-5.794-5.836A.9.9,0,0,0,20.784,11.51Z" transform="translate(-7.875 -11.252)" fill="#fff"/>
            </svg>
        </a>
    </div>
@endsection
@section('header')
    <div class="row header-row">
        <div class="col-12">
            <div>
                <div class="header-info">
                    <div class="header-info__desc">
                        <h1>
                            Rootix
                            <h2>Best Experience In</h2>
                            <h2>crypto exchanges</h2>
                        </h1>
                    </div>
                    <p>
                    </p>


                    <div class="header-currency-image">
                        <img src="{{asset('theme/landing/images/header-img.png')}}" alt="crypto-currency"
                             class="header-currency">
                    </div>
                </div>
                <div class="header-btns">
                    <a href="{{route('register.form')}}" class="btn light-blue-btn animation-btn">
                       Register
                    </a>
                    <a href="#" class="btn main-btn">
                        <i class="fa fa-play"></i>
                        How Rootix Works
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="header-currency-header-info">
        <div class="container">
            <h2>Rootix 24
                -hour trading volume : 250.000.000</h2>
        </div>

        <div class="header-currency-informations-container">
            <div class="container">

                <div class="currencys">

{{--                    <div class="header-currency-informations">--}}
{{--                        @php--}}
{{--                            $currencies1=get_specific_currencies('btc,eth,ltc,usdt,xrp');--}}
{{--                        @endphp--}}
{{--                        @foreach($currencies1 as $key=>$item)--}}
{{--                            @php--}}
{{--                                $sign=$item['1d']<0?'-':'+';--}}
{{--                                $value=str_replace('-','',$item['1d']);--}}
{{--                            @endphp--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">{{$key}}/USDT</h4>--}}

{{--                                    <h4 class="{{$item['1d']<0?'down':'up'}}-currency">--}}
{{--                                        {{$sign}}  {{number_format((float)$value,2)}}  {{'%'}}--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">--}}
{{--                                        {{number_format($item['price'],2)}}--}}
{{--                                    </h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}

{{--                    <div class="header-currency-informations">--}}
{{--                        @php--}}
{{--                            $currencies2=get_specific_currencies('bch,bnb,eos,jst,win');--}}
{{--                        @endphp--}}
{{--                        @foreach($currencies2 as $key=>$item)--}}
{{--                            @php--}}
{{--                                $sign=$item['1d']<0?'-':'+';--}}
{{--                                $value=str_replace('-','',$item['1d']);--}}
{{--                            @endphp--}}
{{--                            <div class="header-currency-information">--}}
{{--                                <div class="header-currency-desc">--}}
{{--                                    <h4 class="header-currency-name">{{$key}}/USDT</h4>--}}

{{--                                    <h4 class="{{$item['1d']<0?'down':'up'}}-currency">--}}
{{--                                        {{$sign}} {{number_format((float)$value,2)}} {{"%"}}--}}
{{--                                    </h4>--}}
{{--                                </div>--}}
{{--                                <div class="header-currency-price-container">--}}
{{--                                    <h3 class="header-currency-price">--}}
{{--                                        {{number_format($item['price'],2)}}--}}
{{--                                    </h3>--}}
{{--                                    <h4>$0.0256</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}

{{--                    <div class="header-currency-informations">--}}
{{--                        <div class="header-currency-information header-currency-information-1">--}}
{{--                            <div class="header-currency-desc">--}}
{{--                                <h4 class="header-currency-name">BNB/USDT</h4>--}}

{{--                                <h4 class="down-currency">--}}
{{--                                    -12.5%--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-price-container">--}}
{{--                                <h3 class="header-currency-price">--}}
{{--                                    {{$currency->currency_to_usdt('bnb')}}--}}
{{--                                </h3>--}}
{{--                                <h4>$0.0256</h4>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="header-currency-information">--}}
{{--                            <div class="header-currency-desc">--}}
{{--                                <h4 class="header-currency-name">EOS/USDT</h4>--}}

{{--                                <h4 class="up-currency">--}}
{{--                                    -12.5%--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-price-container">--}}
{{--                                <h3 class="header-currency-price">--}}
{{--                                    {{$currency->currency_to_usdt('eos')}}--}}
{{--                                </h3>--}}
{{--                                <h4>$0.0256</h4>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="header-currency-information">--}}
{{--                            <div class="header-currency-desc">--}}
{{--                                <h4 class="header-currency-name">XLM/USDT</h4>--}}

{{--                                <h4 class="down-currency">--}}
{{--                                    -12.5%--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-price-container">--}}
{{--                                <h3 class="header-currency-price">--}}
{{--                                    {{$currency->currency_to_usdt('xlm')}}--}}
{{--                                </h3>--}}
{{--                                <h4>$0.0256</h4>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="header-currency-information">--}}
{{--                            <div class="header-currency-desc">--}}
{{--                                <h4 class="header-currency-name">ETC/USDT</h4>--}}

{{--                                <h4 class="up-currency">--}}
{{--                                    -12.5%--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-price-container">--}}
{{--                                <h3 class="header-currency-price">--}}
{{--                                    {{$currency->currency_to_usdt('etc')}}--}}
{{--                                </h3>--}}
{{--                                <h4>$0.0256</h4>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="header-currency-information">--}}
{{--                            <div class="header-currency-desc">--}}
{{--                                <h4 class="header-currency-name">DOGE/BUSD</h4>--}}

{{--                                <h4 class="up-currency">--}}
{{--                                    -12.5%--}}
{{--                                </h4>--}}
{{--                            </div>--}}
{{--                            <div class="header-currency-price-container">--}}
{{--                                <h3 class="header-currency-price">0.0002569</h3>--}}
{{--                                <h4>$0.0256</h4>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </div>
            </div>
        </div>
    </div>
    <div class="go-down-container">
        <a href="#currency-table" class="go-down">
            <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="gotodown"/>
        </a>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section class="p-t table-currency">
        <div class="container">
            <a id="currency-table"></a>

            <h2 class="text-center" style="color: #1d1f31;">market</h2>
            <div class="row">
                <div class="col-12">
                    <table class="currency-table currency-table-3">
                        <tr>
                            <td class="name-currency-title">Name</td>
                            <td class="last-price-title">Last Price</td>
                            <td class="change-price-title">Changes</td>
                            <td></td>
                        </tr>

                        {{--                        @foreach($currency->currency_to_rial(['btc','eth','usdt','trx','doge'],['latest','dayHigh','dayChange']) as $key=>$item)--}}
{{--                        @foreach($currency as $key=>$item)--}}
{{--                            @php--}}
{{--                                $sing=$item['1d']<0?'-':'+';--}}
{{--                                $value=str_replace('-','',$item['1d']);--}}
{{--                            @endphp--}}
{{--                            <tr>--}}
{{--                                <td class="currency-title">--}}
{{--                                    <div class="currency-right-title currency-right-title-2">--}}
{{--                                        <img src="{{asset("theme/landing/images/currency/$key.png" )}}"--}}
{{--                                             alt="{{ $item['name_fa']}}"/>--}}
{{--                                        <h5>{{$item['name']}}</h5>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                                <td class="currency-last-price">--}}
{{--                                    {{number_format($item['price'],2)}}--}}
{{--                                </td>--}}
{{--                                <td class="currency-present-change">--}}
{{--                                    @if(isset($item['1d']))--}}
{{--                                        <span--}}
{{--                                            class="currency-present-change {{(float)$item['1d']<0?'red':'green'}}-currency">--}}
{{--                           {{number_format((float)$value,2)}} {{$sing}}--}}
{{--                          </span>--}}
{{--                                    @else--}}
{{--                                        ---}}
{{--                                    @endif--}}

{{--                                </td>--}}
{{--                                <td class="currency-most-price">--}}
{{--                                    @if(isset($item['day_high']))--}}
{{--                                        {{number_format($item['dayHigh']/10)}}--}}
{{--                                    @else--}}
{{--                                        ---}}
{{--                                    @endif--}}
{{--                                </td>--}}
{{--                            </tr>--}}

{{--                        @endforeach--}}
                    </table>
                </div>
            </div>
        </div>


        <div class="all-currency-btn">
            <a href="{{route('landing.prices')}}" class="btn all-crypto-currency">
                More
                <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261" viewBox="0 0 6.336 10.261">
                    <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                          d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                          transform="translate(19.221 -9) rotate(90)"/>
                </svg>
            </a>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="section section-two">
        <div class="container">
            <h2 class="app-title">
                Working with Rootix is
                <span class="gold-title">  really easy </span>
            </h2>

            <div class="d-flex auth-container">

                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-1.png')}}" alt="register"/>
                    <span> Register on Rootix</span>
                    <p>
                        Enter your identity information
                        in the first step
                    </p>
                </div>

                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-2.png')}}" alt="register"/>
                    <span>Authentication</span>
                    <p>Verify the entered information
                        and confirm the contact
                        number and email
                    </p>
                </div>

            </div>
        </div>
    </section>
    <!-- End Section Two -->

    <!-- Start Section Three -->
{{--    <section>--}}
{{--        <div class="container">--}}
{{--            <h2 class="app-title app-title-2">--}}
{{--                Blog--}}
{{--                <span class="gold-title"> Rootix </span>--}}
{{--            </h2>--}}
{{--            <div class="row row-padding">--}}
{{--                <div class="col-12">--}}
{{--                    <div class="blog-container">--}}
{{--                        <div class="right-blog top-blog blog-info">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('theme/landing/images/et.png')}}" alt="Ethereum" />--}}
{{--                                <div class="currency-blog">--}}
{{--                                    <p>--}}
{{--                                        How to survive the declining digital currency market?--}}
{{--                                    </p>--}}
{{--                                    <div class="calender">--}}
{{--                                        <p>20 Feb 2021</p>--}}

{{--                                        <div>--}}
{{--                                            19--}}
{{--                                            <i class="far fa-comment"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="blog-container">--}}
{{--                        <div class="right-blog top-blog blog-info">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('theme/landing/images/et.png')}}" alt="اتریوم" />--}}
{{--                                <div class="currency-blog">--}}
{{--                                    <p>--}}
{{--                                        How to survive the declining digital currency market?--}}
{{--                                    </p>--}}
{{--                                    <div class="calender">--}}
{{--                                        <p>‏20 Feb 2021</p>--}}

{{--                                        <div>--}}
{{--                                            19--}}
{{--                                            <i class="far fa-comment"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="blog-container">--}}
{{--                        <div class="right-blog top-blog blog-info">--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{asset('theme/landing/images/et.png')}}" alt="اتریوم" />--}}
{{--                                <div class="currency-blog">--}}
{{--                                    <p>--}}
{{--                                        How to survive the declining digital currency market?--}}
{{--                                    </p>--}}
{{--                                    <div class="calender">--}}
{{--                                        <p>20 Feb 2021</p>--}}

{{--                                        <div>--}}
{{--                                            19--}}
{{--                                            <i class="far fa-comment"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End Section Three -->

    <!-- Start Section Four -->
    <section class="what-currency section-background">
        <div>
            <div class="currency-talk">
                <div class="information-ramz">
                    <div class="what-currency-info">
                        <h5>What is cryptocurrency?</h5>
                        <div class="what-crypto-title">
                            <h2>Let's talk a little bit </h2>
                            <span>about cryptocurrency</span>
                        </div>
                        <p class="what-crypto-desc">
                            Cryptocurrency is a digital
                            currency. Currency generation
                            and authentication of money
                            transactions is controlled using
                            encryption algorithms, and
                            usually works decentrally
                            (without dependence on a
                            central bank).
                        </p>

                        <a href="#" class="what-currency-more-btn light-blue-btn btn">
                            Read more
                        </a>
                    </div>
                </div>

                <div class="owl-carts-container">
                    <div class="owl-carousel owl-theme owl-crypto-carts">
                        <div class="crypto-cart">
                            <img src="{{asset('theme/landing/images/bitcoin.png')}}" alt="crypto"/>
                            <div class="crypto-cart-desc">
                                <h3>Digital money</h3>
                                <p>
                                    There are currencies that are stored and
                                    transferred electronically
                                </p>
                            </div>
                        </div>

                        <div class="crypto-cart">
                            <img src="{{asset('theme/landing/images/Icon.png')}}" alt="Always available"/>
                            <div class="crypto-cart-desc">
                                <h3>Always available</h3>
                                <p>
                                    Strong 24-hour Rootix
                                    support for a better trading
                                    experience

                                </p>
                            </div>
                        </div>

                        <div class="crypto-cart">
                            <img src="{{asset('theme/landing/images/credit-card.png')}}" alt="Transaction security"/>
                            <div class="crypto-cart-desc">
                                <h3>Transaction security</h3>
                                <p>
                                    Protecting user
                                    information and
                                    transaction information
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="feature">
        <div>
            <h2 class="feature-title">
                Rootix
                <span class="gold-title gold-feature"> features </span>
            </h2>

            <div class="owl-carousel owl-theme owl-feature">
                <div class="main-feature">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-1.jpg')}}" alt="easy"/>
                    </div>

                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>Convenient and fast in
                                registration and
                                </h2>
                            <span>authentication</span>
                        </div>

                        <div>
                            <p>
                                Register easily and quickly in one of
                                the best exchanges, Rootix, by
                                filling out two authentication forms
                                and entering your information.
                            </p>

                            <div class="all-currency-btn">
                                <a href="#" class="btn all-crypto-currency">
                                    Read more
                                    <svg xmlns="http://www.w3.org/2000/svg" width="8.334" height="13.496" viewBox="0 0 8.334 13.496"><path d="M11.91,0,6.748,5.151,1.586,0,0,1.586,6.748,8.334,13.5,1.586Z" transform="translate(0 13.496) rotate(-90)"/></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main-feature">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-2.png')}}" alt="safe"/>
                    </div>

                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>Security and
                                protection of
                                </h2>
                            <span>user information </span>
                        </div>

                        <div>
                            <p>
                                Routix considers it its duty
                                to ensure the security of its
                                users' information so that
                                users can trade with ease
                            </p>

                            <div class="all-currency-btn">
                                <a href="#" class="btn all-crypto-currency">
                                    Read more
                                    <svg xmlns="http://www.w3.org/2000/svg" width="8.334" height="13.496" viewBox="0 0 8.334 13.496"><path d="M11.91,0,6.748,5.151,1.586,0,0,1.586,6.748,8.334,13.5,1.586Z" transform="translate(0 13.496) rotate(-90)"/></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main-feature">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-3.png')}}" alt="trading"/>
                    </div>

                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>Easy  </h2>
                            <span>trading</span>
                        </div>

                        <div>
                            <p>
                                Rootix with easy and
                                fast user interface
                            </p>

                            <div class="all-currency-btn">
                                <a href="#" class="btn all-crypto-currency">
                                    Read more
                                    <svg xmlns="http://www.w3.org/2000/svg" width="8.334" height="13.496" viewBox="0 0 8.334 13.496"><path d="M11.91,0,6.748,5.151,1.586,0,0,1.586,6.748,8.334,13.5,1.586Z" transform="translate(0 13.496) rotate(-90)"/></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Section Five -->

    <!-- Start Section Six -->
    <section class="mobile-app section-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6" style="order: 1;">
                    <div class="what-currency-info">
                        <h5>Do not forget the mobile
                            application</h5>
                        <div class="what-crypto-title">
                            <h2>Check the market always
                                and everywhere </h2>
                            <span>with the
                                Rootix mobile application</span>
                        </div>
                        <p class="what-crypto-desc">
                            Rootix panel can also be used on
                            smartphones. Easily accessible and with
                            its advanced and easy user interface,
                            provide the experience of working with
                            an exchange at any time and any place
                            using a smartphone, for its users.

                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="mobile-images">
                        <img src="{{asset('theme/landing/images/mobile-1.png')}}" alt="mobile app"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Six -->

    <!-- Start Section Eight -->
    <section>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2>Contact Us</h2>
                            <p class="top-form-desc">
                                Join us in the development of Rootix. Share your
                                comments and criticisms with us about your
                                experience working with Rootix. Our team is eager
                                for your feedback to make Rootix better.

                            </p>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="form"/>
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Eamil">
                            <input type="text" placeholder="Address">
                            <textarea placeholder="Message"></textarea>
                            <button type="submit" class="res-form__btn btn light-blue-btn">
                                Send
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Eight -->
@endsection

