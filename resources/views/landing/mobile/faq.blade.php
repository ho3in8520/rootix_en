@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    FAQ-Rootix
@endsection
@section('header')
    <div>

        <div class="about-header-info-res">
            <h5>Do you have any question?</h5>
            <h1>Ask us.</h1>
        </div>

        <form action="#" class="faqs-search__search-box">
            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 17.302 17.302">
                <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(-4 -4)">
                    <path id="Path_8028" data-name="Path 8028" d="M18.807,11.653A7.153,7.153,0,1,1,11.653,4.5,7.153,7.153,0,0,1,18.807,11.653Z" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                    <path id="Path_8029" data-name="Path 8029" d="M28.865,28.865l-3.89-3.89" transform="translate(-8.27 -8.27)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                </g>
            </svg>

            <input type="search" placeholder="What question do you have?" />
        </form>
    </div>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container faqs-container">
            <div class="row">
                <div class="col-12">
                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What is Rootix?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            Rootix is a cryptocurrency Exchange. You can buy cryptocurrencies (like Bitcoin) What is Rootix? through its secure portal.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What is cryptocurrency?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            In the 21st century, cryptocurrencies are being replaced by common currencies such as the rial, the dollar, the euro, and even gold. From now on, all financial transactions worldwide and even daily purchases will be made in cryptocurrency. Therefore, the use of cryptocurrencies will be inevitable in the near future.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How to buy cryptocurrency?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            By registering on the Rootix cryptocurrency Exchange site, you can easily make Rial deposits through your bank portal and easily convert it into your favorite cryptocurrency through the site.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How to sell cryptocurrency</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            At Rootix Exchange, use simple, powerful and secure facilities and convert all types of digital currencies into Rials or any other type of cryptocurrency
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How much is the cryptocurrency trading fee in Rootix?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            Fees depend on the country's banking system and cryptocurrency network fees and are different. In Rootix Exchange, you will experience the lowest amount of fees among all Exchanges
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">How to start?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            By <a href="{{route('register.form')}}" style="color: deepskyblue">registering</a> on the site, you can easily and only with your email address and
                            mobile number, enter your user panel and buy cryptocurrency from now on.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What is authentication?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            You need authentication to use Rootix Exchange services, which has different levels. In the first level, you can use the Exchange facilities only with your mobile phone number and email
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">What information do we need for authentication?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            The first level requires a mobile number and email. At higher levels, in order to use Exchange services, you need to authenticate in the form of bank account information and national code.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">Why should I give my identity information to Rootix?</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            Rootix needs authentication to secure your Rial and digital capital. Authentication prevents anyone other than you from withdrawing or transferring your capital.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2>Did not find the answer to the question?</h2>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="form" />
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="Name and LastName ...">
                            <input type="email" placeholder="Email ...">
                            <input type="text" placeholder="Site Address ...">
                            <textarea placeholder="Message"></textarea>
                            <button type="submit" class="res-form__btn btn light-blue-btn" disabled>
                                Send
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
@endsection
