@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    Contact With Rootix
@endsection

@section('content')
    <section class="map">
        <div id="mapid"></div>
    </section>
    <!-- Start Section Five -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2 class="about-title-res">Get in touch with us</h2>
                            <p class="top-form-desc top-form-desc-2 about-top-desc">
                               ....................................................
                            </p>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/about-us/Ai.png')}}"/>
                        </div>

                        <div class="contact-info-responsive">
                            <h2 class="contact-title">Contacts</h2>
                            <p>Email : info@rootix.io</p>
{{--                            <p>شماره تماس : ۰۳۱-۱۲۳۴۵۶۷۸۹</p>--}}
{{--                            <p>آدرس : اصفهان ، نجف آباد خیابان شریعتی</p>--}}

                            <div class="contact__sidebar-footer justify-center contact__sidebar-footer-responsive">
                                <a
                                    href="https://www.instagram.com/rootix.io/"
                                    target="_blank"
                                    class="contact__sidebar-item instagram"
                                >
                                    <i class="fa fa-instagram"></i>
                                </a>
                                <a
                                    href="https://twitter.com/rootixexchange"
                                    target="_blank"
                                    class="contact__sidebar-item twitter"
                                >
                                    <i class="fa fa-twitter"></i>
                                </a>
{{--                                <a--}}
{{--                                    href="#"--}}
{{--                                    target="_blank"--}}
{{--                                    class="contact__sidebar-item face-book"--}}
{{--                                >--}}
{{--                                    <i class="fa fa-facebook"></i>--}}
{{--                                </a>--}}
                                <a href="https://t.me/rootix" class="social-network telegram">
                                    <img src="{{asset('theme/landing/images/telegram_mobile
.png')}}" width="55px" height="55px" alt="">
                                </a>
                            </div>
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="Full name"/>
                            <input type="email" placeholder="Email"/>
                            <input type="text" placeholder="Site Address"/>
                            <textarea placeholder="Message"></textarea>
                            <button disabled="disabled"
                                    type="submit"
                                    class="res-form__btn btn light-blue-btn"
                            >
                                Send
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Five -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/map.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/contact-map.js')}}"></script>
@endsection
