<?php

return [
    'title-profile'=>'Profile',
    'title-mobile' =>'Phone Validation',
    'title-minePage' =>'Dashboard',
    'title-exchange' =>'Exchange',
    'title-Payment' =>'Payment status',
    'title-Authentication' =>'Authentication',
    'title-buySell' =>'Deposit / Withdrawal page',
    'title-withdraw' =>'Withdraw',
    'title-ticket' =>'Ticket',
    'title-bankNew' =>'Add a new bank account',
    'title-bankEdit' =>'Edit bank account',
    'title-accountBank' =>'Bank Accounts',
    'title-charge' =>'Deposit page',
    'title-sellCurrency' =>'Sell Currency',
    'title-MyWallet' =>'My Wallet',
    'title-ticket' =>'Ticket',
    'title-withdraw' =>'Withdraw Currency Page',



];

